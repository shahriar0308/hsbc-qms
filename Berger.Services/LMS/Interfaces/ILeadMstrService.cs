﻿using Berger.Entities;
using Berger.Entities.LMS;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Berger.Services.LMS.Interfaces
{
    public interface ILeadMstrService : IDisposable
    {
        Task<(decimal TotalProductCost, decimal TotalPainterCost, decimal TotalSupervisionCharge)> GetLeadAllConfirmedCostByLdLeadGuidAsync(string LdLeadGuid);
        Task<LeadMstrHdr> GetLeadMstrHdrByIdAsync(string LdLeadGuid);
        Task<LeadMstrHdr> GetLeadMstrHdrFirstOrDefaultAsync(Expression<Func<LeadMstrHdr, bool>> predicate = null);
        Task<IList<LeadMstrHdr>> GetLeadMstrHdrAllAsync(Expression<Func<LeadMstrHdr, bool>> predicate = null);
        Task<LeadMstrDtl> GetLeadMstrDtlByIdAsync(string LdLeadDetailGuid);
        Task<LeadMstrDtl> GetLeadMstrDtlFirstOrDefaultAsync(Expression<Func<LeadMstrDtl, bool>> predicate = null);
        Task<IList<LeadMstrDtl>> GetLeadMstrDtlAllAsync(Expression<Func<LeadMstrDtl, bool>> predicate = null);
    }
}
