﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.LMS.Interfaces
{
    public interface ISyncService : IDisposable
    {
        Task<bool> AreaAsync();
    }
}
