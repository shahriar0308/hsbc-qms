﻿using Berger.Entities;
using Berger.Entities.LMS;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Berger.Services.LMS.Interfaces
{
    public interface ILeadQuotationService : IDisposable
    {
        Task<(string LdLeadGuid, decimal TotalProductCost, decimal TotalPainterCost, decimal TotalSupervisionCharge)> GetLeadQuotationAllConfirmedCostByLqhLeadDetailGuidAsync(string LqhLeadDetailGuid);
        Task<string> GetLdLeadGuidByLqhLeadDetailGuidAsync(string LqhLeadDetailGuid);
        Task<LeadQuotationHdr> GetLeadQuotationHdrByIdAsync(long LqhId);
        Task<LeadQuotationHdr> GetLeadQuotationHdrFirstOrDefaultAsync(Expression<Func<LeadQuotationHdr, bool>> predicate = null);
        Task<IList<LeadQuotationHdr>> GetLeadQuotationHdrAllAsync(Expression<Func<LeadQuotationHdr, bool>> predicate = null);
        Task<LeadQuotationDtl> GetLeadQuotationDtlByIdAsync(long LqdDetailsId);
        Task<LeadQuotationDtl> GetLeadQuotationDtlFirstOrDefaultAsync(Expression<Func<LeadQuotationDtl, bool>> predicate = null);
        Task<IList<LeadQuotationDtl>> GetLeadQuotationDtlAllAsync(Expression<Func<LeadQuotationDtl, bool>> predicate = null);
        Task<LeadQuotationSilverDtl> GetLeadQuotationSilverDtlByIdAsync(long LqsdDetailsId);
        Task<LeadQuotationSilverDtl> GetLeadQuotationSilverDtlFirstOrDefaultAsync(Expression<Func<LeadQuotationSilverDtl, bool>> predicate = null);
        Task<IList<LeadQuotationSilverDtl>> GetLeadQuotationSilverDtlAllAsync(Expression<Func<LeadQuotationSilverDtl, bool>> predicate = null);
    }
}
