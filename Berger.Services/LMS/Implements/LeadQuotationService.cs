﻿using Berger.Common.Extensions;
using Berger.Entities;
using Berger.Entities.LMS;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Repository.LMS.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using Berger.Services.LMS.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.LMS.Implements
{
    public class LeadQuotationService : ILeadQuotationService
    {        
        private readonly ILeadQuotationUnitOfWork _leadQuotationUnitOfWork;
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTime _dateTime;


        public LeadQuotationService(
            ILeadQuotationUnitOfWork leadQuotationUnitOfWork,
            ICurrentUserService currentUserService,
            IDateTime dateTime
            )
        {
            _leadQuotationUnitOfWork = leadQuotationUnitOfWork;
            _currentUserService = currentUserService;
            _dateTime = dateTime;
        }

        public async Task<(string LdLeadGuid, decimal TotalProductCost, decimal TotalPainterCost, decimal TotalSupervisionCharge)> GetLeadQuotationAllConfirmedCostByLqhLeadDetailGuidAsync(string LqhLeadDetailGuid)
        {
            var result = await _leadQuotationUnitOfWork.LeadQuotationHdrRepository.GetFirstOrDefaultAsync(x =>
                                        new
                                        {
                                            LdLeadGuid = x.LeadMstrDtl.LdLeadGuid,
                                            TotalProductCost = (decimal)(x.LeadQuotationDtls.Sum(y => ((y.LqdArea ?? 0) * (y.LqdPrice ?? 0)))),
                                            TotalPainterCost = (decimal)(x.LeadQuotationSilverDtls.Sum(y => ((y.LqsdArea ?? 0) * (y.LqsdPainterCost ?? 0)))),
                                            TotalSupervisionCharge = (decimal)(x.LeadQuotationSilverDtls.Sum(y => ((y.LqsdArea ?? 0) * (y.LqsdSupervisionCost ?? 0))))
                                        },
                                        x => x.LqhLeadDetailGuid == LqhLeadDetailGuid && x.LeadMstrDtl.LeadMstrHdr.Active == "Y" && x.LeadMstrDtl.Active == "Y" && 
                                                x.Active == "Y" && x.LqhStatus == "LES_04",
                                        x => x.Include(i => i.LeadMstrDtl).ThenInclude(i => i.LeadMstrHdr)
                                                .Include(i => i.LeadQuotationDtls).Include(i => i.LeadQuotationSilverDtls),
                                        true);

            return (result.LdLeadGuid, result.TotalProductCost, result.TotalPainterCost, result.TotalPainterCost);
        }
        
        public async Task<string> GetLdLeadGuidByLqhLeadDetailGuidAsync(string LqhLeadDetailGuid)
        {
            var result = await _leadQuotationUnitOfWork.LeadQuotationHdrRepository.GetFirstOrDefaultAsync(x =>
                                        x.LeadMstrDtl.LdLeadGuid,
                                        x => x.LqhLeadDetailGuid == LqhLeadDetailGuid && x.LeadMstrDtl.Active == "Y" && x.Active == "Y",
                                        x => x.Include(i => i.LeadMstrDtl),
                                        true);

            return result;
        }

        public async Task<LeadQuotationHdr> GetLeadQuotationHdrByIdAsync(long LqhId)
        {
            var leadQuotationHdr = await _leadQuotationUnitOfWork.LeadQuotationHdrRepository.GetByIdAsync(LqhId);

            if (leadQuotationHdr == null)
            {
                throw new NotFoundException(nameof(LeadQuotationHdr), LqhId);
            }

            return leadQuotationHdr;
        }

        public async Task<LeadQuotationHdr> GetLeadQuotationHdrFirstOrDefaultAsync(Expression<Func<LeadQuotationHdr, bool>> predicate = null)
        {
            var leadQuotationHdr = await _leadQuotationUnitOfWork.LeadQuotationHdrRepository.GetFirstOrDefaultAsync(x => x,
                                        predicate, null, true);

            if (leadQuotationHdr == null)
            {
                throw new NotFoundException(nameof(LeadQuotationHdr), string.Empty);
            }

            return leadQuotationHdr;
        }

        public async Task<IList<LeadQuotationHdr>> GetLeadQuotationHdrAllAsync(Expression<Func<LeadQuotationHdr, bool>> predicate = null)
        {
            return await _leadQuotationUnitOfWork.LeadQuotationHdrRepository.GetAsync(x => x,
                            predicate, null, null, true);
        }

        public async Task<LeadQuotationDtl> GetLeadQuotationDtlByIdAsync(long LqdDetailsId)
        {
            var leadQuotationDtl = await _leadQuotationUnitOfWork.LeadQuotationDtlRepository.GetByIdAsync(LqdDetailsId);

            if (leadQuotationDtl == null)
            {
                throw new NotFoundException(nameof(LeadQuotationDtl), LqdDetailsId);
            }

            return leadQuotationDtl;
        }

        public async Task<LeadQuotationDtl> GetLeadQuotationDtlFirstOrDefaultAsync(Expression<Func<LeadQuotationDtl, bool>> predicate = null)
        {
            var leadQuotationDtl = await _leadQuotationUnitOfWork.LeadQuotationDtlRepository.GetFirstOrDefaultAsync(x => x,
                                        predicate, null, true);

            if (leadQuotationDtl == null)
            {
                throw new NotFoundException(nameof(LeadQuotationDtl), string.Empty);
            }

            return leadQuotationDtl;
        }

        public async Task<IList<LeadQuotationDtl>> GetLeadQuotationDtlAllAsync(Expression<Func<LeadQuotationDtl, bool>> predicate = null)
        {
            return await _leadQuotationUnitOfWork.LeadQuotationDtlRepository.GetAsync(x => x,
                            predicate, null, null, true);
        }

        public async Task<LeadQuotationSilverDtl> GetLeadQuotationSilverDtlByIdAsync(long LqsdDetailsId)
        {
            var leadQuotationSilverDtl = await _leadQuotationUnitOfWork.LeadQuotationSilverDtlRepository.GetByIdAsync(LqsdDetailsId);

            if (leadQuotationSilverDtl == null)
            {
                throw new NotFoundException(nameof(LeadQuotationSilverDtl), LqsdDetailsId);
            }

            return leadQuotationSilverDtl;
        }

        public async Task<LeadQuotationSilverDtl> GetLeadQuotationSilverDtlFirstOrDefaultAsync(Expression<Func<LeadQuotationSilverDtl, bool>> predicate = null)
        {
            var leadQuotationSilverDtl = await _leadQuotationUnitOfWork.LeadQuotationSilverDtlRepository.GetFirstOrDefaultAsync(x => x,
                                        predicate, null, true);

            if (leadQuotationSilverDtl == null)
            {
                throw new NotFoundException(nameof(LeadQuotationSilverDtl), string.Empty);
            }

            return leadQuotationSilverDtl;
        }

        public async Task<IList<LeadQuotationSilverDtl>> GetLeadQuotationSilverDtlAllAsync(Expression<Func<LeadQuotationSilverDtl, bool>> predicate = null)
        {
            return await _leadQuotationUnitOfWork.LeadQuotationSilverDtlRepository.GetAsync(x => x,
                            predicate, null, null, true);
        }

        public void Dispose()
        {
            _leadQuotationUnitOfWork.Dispose();
        }
    }
}
