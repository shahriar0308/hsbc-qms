﻿using Berger.Common.Extensions;
using Berger.Entities;
using Berger.Entities.LMS;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Repository.LMS.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using Berger.Services.LMS.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.LMS.Implements
{
    public class LeadMstrService : ILeadMstrService
    {        
        private readonly ILeadMstrUnitOfWork _leadMstrUnitOfWork;
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTime _dateTime;


        public LeadMstrService(
            ILeadMstrUnitOfWork leadMstrUnitOfWork,
            ICurrentUserService currentUserService,
            IDateTime dateTime
            )
        {
            _leadMstrUnitOfWork = leadMstrUnitOfWork;
            _currentUserService = currentUserService;
            _dateTime = dateTime;
        }

        public async Task<(decimal TotalProductCost, decimal TotalPainterCost, decimal TotalSupervisionCharge)> GetLeadAllConfirmedCostByLdLeadGuidAsync(string LdLeadGuid)
        {
            var result = await _leadMstrUnitOfWork.LeadMstrHdrRepository.GetFirstOrDefaultAsync(x =>
                                        new
                                        {
                                            TotalProductCost = (decimal)(x.LeadMstrDtls.Where(y => y.Active == "Y").SelectMany(y => y.LeadQuotationHdrs).Where(y => y.LqhStatus == "LES_04" && y.Active == "Y").
                                                                    SelectMany(y => y.LeadQuotationDtls).Sum(y => ((y.LqdArea ?? 0) * (y.LqdPrice ?? 0)))),
                                            TotalPainterCost = (decimal)(x.LeadMstrDtls.Where(y => y.Active == "Y").SelectMany(y => y.LeadQuotationHdrs).Where(y => y.LqhStatus == "LES_04" && y.Active == "Y").
                                                                    SelectMany(y => y.LeadQuotationSilverDtls).Sum(y => ((y.LqsdArea ?? 0) * (y.LqsdPainterCost ?? 0)))),
                                            TotalSupervisionCharge = (decimal)(x.LeadMstrDtls.Where(y => y.Active == "Y").SelectMany(y => y.LeadQuotationHdrs).Where(y => y.LqhStatus == "LES_04" && y.Active == "Y").
                                                                        SelectMany(y => y.LeadQuotationSilverDtls).Sum(y => ((y.LqsdArea ?? 0) * (y.LqsdSupervisionCost ?? 0))))
                                        },
                                        x => x.LdLeadGuid == LdLeadGuid && x.Active == "Y" && x.LdStage == "LS_05",
                                        x => x.Include(i => i.LeadMstrDtls).ThenInclude(i => i.LeadQuotationHdrs).ThenInclude(i => i.LeadQuotationDtls)
                                                .Include(i => i.LeadMstrDtls).ThenInclude(i => i.LeadQuotationHdrs).ThenInclude(i => i.LeadQuotationSilverDtls),
                                        true);

            return (result.TotalProductCost, result.TotalPainterCost, result.TotalPainterCost);
        }

        public async Task<LeadMstrHdr> GetLeadMstrHdrByIdAsync(string LdLeadGuid)
        {
            var leadMstrHdr = await _leadMstrUnitOfWork.LeadMstrHdrRepository.GetByIdAsync(LdLeadGuid);

            if (leadMstrHdr == null)
            {
                throw new NotFoundException(nameof(LeadMstrHdr), LdLeadGuid);
            }

            return leadMstrHdr;
        }

        public async Task<LeadMstrHdr> GetLeadMstrHdrFirstOrDefaultAsync(Expression<Func<LeadMstrHdr, bool>> predicate = null)
        {
            var leadMstrHdr = await _leadMstrUnitOfWork.LeadMstrHdrRepository.GetFirstOrDefaultAsync(x => x,
                                        predicate, null, true);

            if (leadMstrHdr == null)
            {
                throw new NotFoundException(nameof(LeadMstrHdr), string.Empty);
            }

            return leadMstrHdr;
        }

        public async Task<IList<LeadMstrHdr>> GetLeadMstrHdrAllAsync(Expression<Func<LeadMstrHdr, bool>> predicate = null)
        {
            return await _leadMstrUnitOfWork.LeadMstrHdrRepository.GetAsync(x => x,
                            predicate, null, null, true);
        }

        public async Task<LeadMstrDtl> GetLeadMstrDtlByIdAsync(string LdLeadDetailGuid)
        {
            var leadMstrDtl = await _leadMstrUnitOfWork.LeadMstrDtlRepository.GetByIdAsync(LdLeadDetailGuid);

            if (leadMstrDtl == null)
            {
                throw new NotFoundException(nameof(LeadMstrDtl), LdLeadDetailGuid);
            }

            return leadMstrDtl;
        }

        public async Task<LeadMstrDtl> GetLeadMstrDtlFirstOrDefaultAsync(Expression<Func<LeadMstrDtl, bool>> predicate = null)
        {
            var leadMstrDtl = await _leadMstrUnitOfWork.LeadMstrDtlRepository.GetFirstOrDefaultAsync(x => x,
                                        predicate, null, true);

            if (leadMstrDtl == null)
            {
                throw new NotFoundException(nameof(LeadMstrDtl), string.Empty);
            }

            return leadMstrDtl;
        }

        public async Task<IList<LeadMstrDtl>> GetLeadMstrDtlAllAsync(Expression<Func<LeadMstrDtl, bool>> predicate = null)
        {
            return await _leadMstrUnitOfWork.LeadMstrDtlRepository.GetAsync(x => x,
                            predicate, null, null, true);
        }

        public void Dispose()
        {
            _leadMstrUnitOfWork.Dispose();
        }
    }
}
