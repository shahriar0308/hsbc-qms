﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Repository.LMS.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using Berger.Services.LMS.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.LMS.Implements
{
    public class SyncService : ISyncService
    {
        private readonly IBibhagUnitOfWork bibhagUnitOfWork;
        private readonly ISyncUnitOfWork syncUnitOfWork;
        private readonly IDistrictMstrUnitOfWork districtMstrUnitOfWork;
        private readonly IDateTime dateTimeService;

        public SyncService(IBibhagUnitOfWork bibhagUnitOfWork, ISyncUnitOfWork syncUnitOfWork
            , IDistrictMstrUnitOfWork districtMstrUnitOfWork, IDateTime dateTimeService)
        {
            this.bibhagUnitOfWork = bibhagUnitOfWork;
            this.syncUnitOfWork = syncUnitOfWork;
            this.districtMstrUnitOfWork = districtMstrUnitOfWork;
            this.dateTimeService = dateTimeService;
        }

        public async Task<bool> AreaAsync()
        {
            try
            {
                #region Sync Division
                var bibhags = await bibhagUnitOfWork.BibhagRepository.GetAsync(x => x, null, null, null, true);
                var divisions = await syncUnitOfWork.DivisionRepository.GetAsync(x => x
                , x => bibhags.Select(n => n.BbgName).Contains(x.Name), null, null, true);

                foreach (var division in divisions)
                {
                    division.LmsDivisionId = bibhags.FirstOrDefault(m => m.BbgName.ToLower() == division.Name.ToLower()).BbgId;
                }
                await syncUnitOfWork.DivisionRepository.UpdateRangeAsync(divisions);
                await syncUnitOfWork.SaveChangesAsync();
                #endregion

                #region Sync District
                var districtMstrs = (districtMstrUnitOfWork.DistrictMstrRepository.Get(x => x, null, null, null, true)).AsEnumerable();
                var districts = await syncUnitOfWork.DistrictRepository.GetAsync(x => x
                , x => districtMstrs
                .Select(n => n.DstName).Contains(x.Name), null, m => m.Include(a => a.Division), false);
                

                foreach (var district in districts)
                {
                    district.LmsDivisionId = district.Division.LmsDivisionId;
                    district.LmsDistrictId = districtMstrs.FirstOrDefault(m => m.DstName.ToLower() == district.Name.ToLower()
                        && m.DstBibhagId == district.Division.LmsDivisionId
                    ).DstId;
                }
                await syncUnitOfWork.DistrictRepository.UpdateRangeAsync(districts);

                return await syncUnitOfWork.SaveChangesAsync();
                #endregion
            }
            catch (Exception ex)
            {
                syncUnitOfWork.Rollback();
                throw ex;
            }

        }
        public void Dispose()
        {

            bibhagUnitOfWork.Dispose();
        }
    }
}
