﻿

using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface IThanaService : IDisposable
    {
        Task<int> AddAsync(Thana entity, int districtId);
        Task<int> UpdateAsync(Thana entity, int districtId);
        Task<bool> DeleteAsync(int id);
        Task<Thana> GetByIdAsync(int id);
        Task<IList<Thana>> GetAllAsync();
        Task<QueryResult<Thana>> GetAllAsync(ThanaQuery queryObj);
        Task<IList<KeyValuePairObject>> GetByDistrictSelectAsync(int districtId);
        Task<IList<KeyValuePairObject>> GetAllForSelectAsync();
    }
}
