﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface ILoyaltyProgramPointService : IDisposable
    {
        Task<LoyaltyProgramPoint> GetFirstOrDefaultAsync();
        Task<int> UpdateAsync(LoyaltyProgramPoint entity);
    }
}
