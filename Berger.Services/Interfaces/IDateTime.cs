﻿using System;

namespace Berger.Services.Interfaces
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
