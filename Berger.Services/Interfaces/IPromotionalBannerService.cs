﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface IPromotionalBannerService : IDisposable
    {
        Task<QueryResult<PromotionalBanner>> GetAllAsync(PromotionalBannerQuery queryObj);
        Task<IList<PromotionalBanner>> GetAllActiveAsync();
        Task<PromotionalBanner> GetByIdAsync(int id);
        Task<int> AddAsync(PromotionalBanner entity);
        Task<int> UpdateAsync(PromotionalBanner entity);
        Task<bool> ActiveInactiveAsync(int id);
        Task<bool> DeleteAsync(int id);
        Task<bool> IsExistsNameAsync(string name, int id);
    }
}
