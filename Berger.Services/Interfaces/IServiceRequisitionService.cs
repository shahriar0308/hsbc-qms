﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface IServiceRequisitionService : IDisposable
    {
        Task<IList<ServiceRequisition>> GetAllAsync(bool workIdProvided);
        Task<IList<ServiceRequisition>> GetByUserAsync(Guid userId);
        Task<ServiceRequisition> GetById(Guid id);
        Task<Guid> AddAsync(ServiceRequisition entity);
        bool UpdateColorConsultant(string lacUserId, string leadDetailId);
        Task<ServiceRequisition> GetDetailAsync(Guid id);
    }
}
