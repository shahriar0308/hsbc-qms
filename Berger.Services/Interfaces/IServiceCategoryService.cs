﻿using QMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Services.Interfaces
{
    public interface IServiceCategoryService :IDisposable
    {
        Task<IList<ServiceCategory>> GetAllAsync();
        Task<ServiceCategory> GetByIdAsync(int id);
        Task<ServiceCategory> CreateAsync(ServiceCategory entity);
        Task<int> UpdateAsync(ServiceCategory entity);
        Task<bool> DeleteAsync(int id);
    }
}
