﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface ISettingService : IDisposable
    {
        Task<QueryResult<Setting>> GetAllAsync(SettingQuery queryObj);
        Task<IList<Setting>> GetAllAsync();
        Task<Setting> GetByIdAsync(int id);
        Task<Setting> AddAsync(Setting entity);
        Task<int> UpdateAsync(Setting entity);
        Task<bool> DeleteAsync(int id);
        Task<Setting> GetByPoints(long points);
        Task<IList<KeyValuePairObject>> GetAllSelectAsync();
    }
}
