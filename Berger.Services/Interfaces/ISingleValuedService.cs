﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface ISingleValuedService : IDisposable
    {
        Task<SingleValuedEntity> GetByIdAsync(int id);
        Task<QueryResult<SingleValuedEntity>> GetByNameAsync(string name);
        Task<int> UpdateAsync(SingleValuedEntity entity);
    }
}
