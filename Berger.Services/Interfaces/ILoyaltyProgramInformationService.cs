﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface ILoyaltyProgramInformationService : IDisposable
    {
        //Task<LoyaltyProgramInformation> GetByUserId(string userId);
        //Task<LoyaltyProgramInformation> GetByUsername(string userName);
        Task<LoyaltyProgramInformation> Get();
        Task<LoyaltyProgramInformation> Get(string phoneNo);
    }
}
