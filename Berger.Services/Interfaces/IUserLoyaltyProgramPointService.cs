﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface IUserLoyaltyProgramPointService : IDisposable
    {
        Task<QueryResult<UserLoyaltyProgramPoint>> GetAllAsync(UserLoyaltyProgramPointQuery queryObj);
        Task<IList<UserLoyaltyProgramPoint>> GetAllActiveAsync();
        Task<UserLoyaltyProgramPoint> GetByIdAsync(int id);
        Task<int> AddAsync(UserLoyaltyProgramPoint entity);
        Task<int> UpdateUserLoyaltyPoint(string LqhLeadDetailGuid);
        Task<bool> ActiveInactiveAsync(int id);
        Task<bool> DeleteAsync(int id);
        Task<bool> IsExistsUserLeadAsync(Guid userId, string ldLeadGuid, int id);
    }
}
