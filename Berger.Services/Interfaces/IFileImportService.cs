﻿using Berger.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface IFileImportService
    {
        Task<(IList<ColorCode> Data, string Message)> ExcelImportColorCodeAsync(object fileObj);
    }
}
