﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface IProductService : IDisposable
    {
        Task<QueryResult<Product>> GetAllAsync(ProductQuery queryObj);
        Task<IList<Product>> GetAllAsync();
        Task<Product> GetByIdAsync(int id);
        Task<int> AddAsync(Product entity);
        Task<int> UpdateAsync(Product entity);
        Task<bool> DeleteAsync(int id);
        IList<KeyValuePairObject> GetSurfaceTypeSelectAsync();
        IList<KeyValuePairObject> GetSurfaceConditionSelectAsync();
        Task<IList<Product>> GetBySurfaceAsync(int surfaceType);
        Task<IList<KeyValuePairObject>> GetBySurfaceSelectAsync(int surfaceType);
    }
}
