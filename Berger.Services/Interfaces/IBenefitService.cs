﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface IBenefitService : IDisposable
    {
        Task<IList<KeyValuePairObject>> GetAllForSelectAsync();
        Task<QueryResult<Benefit>> GetAllAsync(BenefitQuery query);
        Task<IList<Benefit>> GetAllAsync();
        Task<Benefit> GetByIdAsync(int id);
        Task<IList<Benefit>> GetByCategoryAsync(int settingId);
        Task<int> AddAsync(Benefit entity, int settingId);
        Task<int> UpdateAsync(Benefit entity, int settingId);
        Task<bool> ActiveInactiveAsync(int id);
        Task<bool> DeleteAsync(int id);
    }
}
