﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface ITypeOfRequirementService : IDisposable
    {
        Task<IList<KeyValuePairObject>> GetAllForSelectAsync();
        Task<QueryResult<TypeOfRequirement>> GetAllAsync(TypeOfRequirementQuery query);
        Task<IList<TypeOfRequirement>> GetAllAsync();
        Task<TypeOfRequirement> GetByIdAsync(int id);
        Task<int> AddAsync(TypeOfRequirement entity);
        Task<int> UpdateAsync(TypeOfRequirement entity);
        Task<bool> ActiveInactiveAsync(int id);
        Task<bool> DeleteAsync(int id);
    }
}
