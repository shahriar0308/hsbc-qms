
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Berger.Entities;
using Berger.Entities.NotMapped;

namespace Berger.Services.Interfaces
{
    public interface IComplainService : IDisposable
    {
        Task<QueryResult<Complain>> GetAllAsync(ComplainQuery queryObj);
        Task<Complain> GetByIdAsync(int id);
        Task<int> AddAsync(Complain entity);
        Task<int> UpdateAsync(Complain entity);
        Task<bool> ActiveInactiveAsync(int id);
        Task<bool> DeleteAsync(int id);
        Task<IList<KeyValuePairObject>> GetComplainTypeSelectAsync();
    }
}
