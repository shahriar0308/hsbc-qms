﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface IUserColorCodeService : IDisposable
    {
        Task<QueryResult<UserColorCode>> GetAllAsync(UserColorCodeQuery queryObj);
        Task<UserColorCode> GetByIdAsync(int id);
        Task<UserColorCode> AddAsync(UserColorCode entity);
        Task<UserColorCode> UpdateAsync(UserColorCode entity);
        Task<bool> ActiveInactiveAsync(int id);
        Task<bool> DeleteAsync(int id);
        Task<IList<UserColorCode>> GetAllActiveAsync();
        Task<bool> IsExistsUserColorCodeAsync(Guid userId, int colorCodeId, int id);
        Task<IList<UserColorCode>> GetAllActiveByUserIdAsync(Guid userId);
    }
}
