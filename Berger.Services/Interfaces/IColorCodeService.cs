﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface IColorCodeService : IDisposable
    {
        Task<QueryResult<ColorCode>> GetAllAsync(ColorCodeQuery queryObj);
        Task<ColorCode> GetByIdAsync(int id);
        Task<ColorCode> AddAsync(ColorCode entity);
        Task<ColorCode> UpdateAsync(ColorCode entity);
        Task<bool> ActiveInactiveAsync(int id);
        Task<bool> DeleteAsync(int id);
        Task<IList<ColorCode>> GetAllActiveAsync();
        Task<(IList<ColorCode> Data, string Message)> DataTableSaveToDatabaseAsync(DataTable datatable);
        Task<bool> IsExistsShadeCodeOrSequenceAsync(string code, string sequence, int id);
    }
}
