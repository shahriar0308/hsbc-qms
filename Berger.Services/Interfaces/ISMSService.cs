﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface ISMSService
    {
        Task<bool> SendOTPSMSAsync(string phoneNumber, int otp);
    }
}
