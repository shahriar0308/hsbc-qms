﻿using QMS.Entities;
using QMS.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Services.Interfaces
{
    public interface ICustomerTypeService :IDisposable
    {
        Task<IList<CustomerType>> GetAllAsync();
        Task<CustomerType> GetByIdAsync(int id);
        Task<CustomerType> CreateAsync(CustomerType entity);
        Task<int> UpdateAsync(CustomerType entity);
        Task<bool> DeleteAsync(int id);
        
    }
}
