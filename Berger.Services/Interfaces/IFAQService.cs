﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface IFAQService : IDisposable
    {
        Task<QueryResult<FAQ>> GetAllAsync(FAQQuery queryObj);
        Task<IList<FAQ>> GetAllLoyaltyProgramAsync();
        Task<IList<FAQ>> GetAllServiceAsync();
        Task<FAQ> GetByIdAsync(int id);
        Task<int> AddAsync(FAQ entity);
        Task<int> UpdateAsync(FAQ entity);
        Task<bool> ActiveInactiveAsync(int id);
        Task<bool> DeleteAsync(int id);
        IList<KeyValuePairObject> GetFAQTypeSelectAsync();
    }
}
