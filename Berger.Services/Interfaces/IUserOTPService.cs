﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface IUserOTPService : IDisposable
    {
        Task<int> GenerateOTPAsync(Guid userId);
        Task<int> GenerateOTPByPhoneNumberAsync(string phoneNumber);
        Task<bool> IsValidOTPAsync(Guid userId, int otp, bool isOTPReset = false, bool isOTPExpires = false, int OTPExpiresMinutes = 0);
        Task<bool> IsValidOTPByPhoneNumberAsync(string phoneNumber, int otp, bool isOTPReset = false, bool isOTPExpires = false, int OTPExpiresMinutes = 0);
        Task<bool> ResetOTPAsync(Guid userId);
        Task<bool> ResetOTPByPhoneNumberAsync(string phoneNumber);
    }
}
