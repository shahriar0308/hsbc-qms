﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface IDealerService : IDisposable
    {
        Task<QueryResult<Dealer>> GetAllAsync(DealerQuery queryObj);
        Task<IList<Dealer>> GetAllAsync();
        Task<Dealer> GetByIdAsync(int id);
        Task<IList<Dealer>> GetByDivisionIdAsync(int divisionId);
        Task<IList<Dealer>> GetByDistrictIdAsync(int districtId);
        Task<Dealer> AddAsync(Dealer entity, int districtId, int divisionId);
        Task<int> UpdateAsync(Dealer entity, int districtId, int divisionId);
        Task<bool> DeleteAsync(int id);
        Task<IList<Dealer>> GetByRangeAsync(decimal minLat, decimal maxLat, decimal minLong, decimal maxLong);
    }
}
