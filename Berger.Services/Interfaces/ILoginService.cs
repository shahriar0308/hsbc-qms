﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface ILoginService : IDisposable
    {
        Task<int> UserLoggedInLogEntryAsync(Guid userId, string fcmToken);
    }
}
