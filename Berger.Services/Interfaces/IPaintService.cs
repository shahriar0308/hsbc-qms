﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface IPaintService : IDisposable
    {
        Task<QueryResult<PaintingServiceCost>> GetAllAsync();
        Task<PaintingServiceCost> Get(int surfaceType, int surfaceCondition, int productId);
        Task<IList<PaintingServiceCost>> GetForGrid(int surfaceType, int surfaceCondition);
        Task<PaintingServiceCost> GetByIdAsync(int id);
        Task<int> AddAsync(PaintingServiceCost entity, int productId);
        Task<int> UpdateAsync(PaintingServiceCost entity, int productId);
        Task<bool> AddOrUpdateAsync(IList<PaintingServiceCost> entity);
        Task<bool> DeleteAsync(int id);
    }
}
