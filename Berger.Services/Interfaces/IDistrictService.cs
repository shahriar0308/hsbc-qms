﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface IDistrictService : IDisposable
    {
        Task<IList<KeyValuePairObject>> GetAllForSelectAsync();
        Task<QueryResult<District>> GetAllAsync(DistrictQuery queryObj);
        Task<IList<District>> GetAllAsync();
        Task<District> GetByIdAsync(int id);
        Task<int> AddAsync(District entity, int divisionId);
        Task<int> UpdateAsync(District entity, int divisionId);
        Task<bool> ActiveInactiveAsync(int id);
        Task<bool> DeleteAsync(int id);
        Task<IList<KeyValuePairObject>> GetByDivisionSelectAsync(int divisionId);
    }
}
