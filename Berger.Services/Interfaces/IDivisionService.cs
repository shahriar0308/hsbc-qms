﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Interfaces
{
    public interface IDivisionService : IDisposable
    {
        Task<IList<KeyValuePairObject>> GetAllForSelectAsync();
        Task<QueryResult<Division>> GetAllAsync(DivisionQuery queryObj);
        Task<Division> GetByIdAsync(int id);
        Task<int> AddAsync(Division entity);
        Task<int> UpdateAsync(Division entity);
        Task<bool> ActiveInactiveAsync(int id);
        Task<bool> DeleteAsync(int id);
        Task<IList<Division>> GetAllWithDistrictAsync();
    }
}
