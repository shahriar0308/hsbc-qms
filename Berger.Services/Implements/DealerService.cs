﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Interfaces;
using Berger.Services.Interfaces;
using Berger.Repository.Extensions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Berger.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Berger.Services.Implements
{
    public class DealerService : IDealerService
    {
        private readonly IDealerUnitOfWork dealerUnitOfWork;
        private readonly IDateTime dateTime;
        private readonly ICurrentUserService currentUserService;
        private readonly IDivisionUnitOfWork divisionUnitOfWork;
        private readonly IDistrictUnitOfWork districtUnitOfWork;

        public DealerService(IDealerUnitOfWork dealerUnitOfWork, IDateTime dateTime, ICurrentUserService currentUserService
            , IDistrictUnitOfWork districtUnitOfWork, IDivisionUnitOfWork divisionUnitOfWork)
        {
            this.dealerUnitOfWork = dealerUnitOfWork;
            this.currentUserService = currentUserService;
            this.dateTime = dateTime;
            this.divisionUnitOfWork = divisionUnitOfWork;
            this.districtUnitOfWork = districtUnitOfWork;
        }

        public async Task<QueryResult<Dealer>> GetAllAsync(DealerQuery queryObj)
        {
            var queryResult = new QueryResult<Dealer>();

            var columnsMap = new Dictionary<string, Expression<Func<Dealer, object>>>()
            {
                ["name"] = v => v.Name,
                ["address"] = v => v.Address
            };

            var result = await dealerUnitOfWork.DealerRepository.GetAsync(x => x,
                            x =>
                            (string.IsNullOrEmpty(queryObj.Name) || x.Name.Contains(queryObj.Name)) &&
                            (string.IsNullOrEmpty(queryObj.Address) || x.Address.Contains(queryObj.Name)),
                            x => x.ApplyOrdering(columnsMap, queryObj.SortBy, queryObj.IsSortAscending),
                            x => x.Include(o=> o.Division).Include(o=>o.District), queryObj.Page, 5000);

            queryResult.Items = result.Items;
            queryResult.Total = result.Total;
            queryResult.TotalFilter = result.TotalFilter;

            return queryResult;
        }

        public async Task<IList<Dealer>> GetAllAsync()
        {
            return await dealerUnitOfWork.DealerRepository
                .GetAsync<Dealer>(x => x,
                w => w.IsActive && w.Division.IsActive && w.District.IsActive,
                x => x.OrderBy(m => m.Name),
                x=> x.Include(m=>m.District).Include(m=>m.Division), true);
        }

        public async Task<IList<Dealer>> GetByRangeAsync(decimal minLat, decimal maxLat, decimal minLong, decimal maxLong)
        {
            return await dealerUnitOfWork.DealerRepository
                .GetAsync(x => x,
                w => w.IsActive && w.Division.IsActive && w.District.IsActive &&
                w.Latitude >= minLat && w.Latitude <= maxLat &&
                w.Longitude >= minLong && w.Longitude <= maxLong,
                x => x.OrderBy(m => m.Name),
                x => x.Include(m => m.District).Include(m => m.Division), true);
        }

        public async Task<Dealer> GetByIdAsync(int id)
        {
            var dealer = await dealerUnitOfWork.DealerRepository.GetByIdAsync(id);
            dealer.Division = await divisionUnitOfWork.DivisionRepository.GetByIdAsync(dealer.DivisionId);
            dealer.District = await districtUnitOfWork.DistrictRepository.GetByIdAsync(dealer.DistrictId);
            if (dealer == null)
            {
                throw new NotFoundException(nameof(Dealer), id);
            }

            return dealer;
        }

        public async Task<IList<Dealer>> GetByDivisionIdAsync(int divisionId)
        {
            var dealers = await dealerUnitOfWork.DealerRepository.GetAsync(
                x=>x, 
                m=>m.DivisionId == divisionId && m.Division.IsActive && m.District.IsActive,
                x => x.OrderBy(m=>m.Name), m=>m.Include(x=>x.Division).Include(x=>x.District), true);

            if (dealers == null || dealers.Count() == 0)
            {
                throw new NotFoundException(nameof(Dealer), divisionId);
            }

            return dealers;
        }

        public async Task<IList<Dealer>> GetByDistrictIdAsync(int districtId)
        {
            var dealers = await dealerUnitOfWork.DealerRepository.GetAsync(x => x, 
                m => m.DistrictId == districtId && m.Division.IsActive && m.District.IsActive,
                x => x.OrderBy(m => m.Name), 
                m => m.Include(x => x.District).Include(x => x.Division), true);

            if (dealers == null || dealers.Count() == 0)
            {
                throw new NotFoundException(nameof(Dealer), districtId);
            }

            return dealers;
        }

        public async Task<Dealer> AddAsync(Dealer entity, int districtId, int divisionId)
        {
            entity.Division = divisionUnitOfWork.DivisionRepository.GetById(divisionId);
            entity.District = districtUnitOfWork.DistrictRepository.GetById(districtId);
            entity.Created = dateTime.Now;
            entity.CreatedBy = currentUserService.UserId;

            await dealerUnitOfWork.DealerRepository.AddAsync(entity);
            
            if(await dealerUnitOfWork.SaveChangesAsync() == true)
            {
                return entity;
            }
            else
            {
                throw new Exception("Failed to add Dealer");
            }

            //throw new NotImplementedException();
        }

        public async Task<int> UpdateAsync(Dealer entity, int districtId, int divisionId)
        {
            entity.Division = divisionUnitOfWork.DivisionRepository.GetById(divisionId);
            entity.District = districtUnitOfWork.DistrictRepository.GetById(districtId);
            entity.LastModified = dateTime.Now;
            entity.LastModifiedBy = currentUserService.UserId;

            await dealerUnitOfWork.DealerRepository.UpdateAsync(entity);

            if (await dealerUnitOfWork.SaveChangesAsync() == true)
            {
                return 1;
            }
            else
            {
                throw new Exception("Failed to update Dealer");
            }
            //throw new NotImplementedException();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            await dealerUnitOfWork.DealerRepository.DeleteAsync(id);
            if (await dealerUnitOfWork.SaveChangesAsync() == true)
            {
                return true;
            }
            else
            {
                throw new Exception("Failed to delete Dealer" );
            }
        }

        public void Dispose()
        {
            this.divisionUnitOfWork.Dispose();
            this.districtUnitOfWork.Dispose();
            this.dealerUnitOfWork.Dispose();
        }
    }
}
