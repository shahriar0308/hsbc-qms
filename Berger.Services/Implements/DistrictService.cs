﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Implements
{
    public class DistrictService : IDistrictService
    {
        private readonly IDistrictUnitOfWork _districtUnitOfWork;
        private readonly IDivisionUnitOfWork _divisionUnitOfWork;
        private readonly IDateTime _dateTimeService;

        public DistrictService(IDistrictUnitOfWork districtUnitOfWork, IDateTime dateTimeService, IDivisionUnitOfWork divisionUnitOfWork)
        {
            _districtUnitOfWork = districtUnitOfWork;
            _divisionUnitOfWork = divisionUnitOfWork;
            _dateTimeService = dateTimeService;
        }

        public async Task<IList<KeyValuePairObject>> GetAllForSelectAsync()
        {
            return await this._districtUnitOfWork.DistrictRepository.GetAsync<KeyValuePairObject>(x =>
                new KeyValuePairObject { Value = x.Id.ToString(), Text = x.Name },
                x => x.IsActive && !x.IsDeleted, x => x.OrderBy(o => o.Name), null, true);
        }

        public async Task<IList<KeyValuePairObject>> GetByDivisionSelectAsync(int divisionId)
        {
            return await this._districtUnitOfWork.DistrictRepository.GetAsync<KeyValuePairObject>(x =>
                new KeyValuePairObject { Value = x.Id.ToString(), Text = x.Name },
                x => x.IsActive && !x.IsDeleted & x.DivisionId == divisionId && x.Division.IsActive, x => x.OrderBy(o => o.Name), null, true);
        }


        public async Task<QueryResult<District>> GetAllAsync(DistrictQuery queryObj)
        {
            var queryResult = new QueryResult<District>();

            var columnsMap = new Dictionary<string, Expression<Func<District, object>>>()
            {
                ["Name"] = v => v.Name,
            };

            var result = await _districtUnitOfWork.DistrictRepository.GetAsync(x => x,
                            x => (string.IsNullOrEmpty(queryObj.Name) || x.Name.Contains(queryObj.Name)),
                            x => x.ApplyOrdering(columnsMap, queryObj.SortBy, queryObj.IsSortAscending),
                            x => x.Include(o=>o.Division), queryObj.Page, 5000);

            queryResult.Items = result.Items;
            queryResult.Total = result.Total;
            queryResult.TotalFilter = result.TotalFilter;

            return queryResult;
        }

        public async Task<IList<District>> GetAllAsync()
        {
            return await _districtUnitOfWork.DistrictRepository
                .GetAsync(x => x,
                w => w.IsActive && w.Division.IsActive ,
                x => x.OrderBy(m => m.Name),
                x => x.Include(m => m.Division), true);
        }

        public async Task<District> GetByIdAsync(int id)
        {
            var district = await _districtUnitOfWork.DistrictRepository.GetByIdAsync(id);
            district.Division = await _divisionUnitOfWork.DivisionRepository.GetByIdAsync(district.DivisionId);
            return district;
        }

        public async Task<int> AddAsync(District entity, int divisionId)
        {
            var isExists = _districtUnitOfWork.DistrictRepository.IsExists(m => m.Name == entity.Name);
            if (isExists)
            {
                throw new DuplicationException(nameof(entity.Name));
            }
            // entity.CreatedBy = Guid.Parse("EC3D80B8-A13C-4696-B40D-97DA6F5148A0");
            entity.Division = _divisionUnitOfWork.DivisionRepository.GetById(divisionId);
            entity.Created = _dateTimeService.Now;
            await _districtUnitOfWork.DistrictRepository.AddAsync(entity);
            await _districtUnitOfWork.SaveChangesAsync();
            return entity.Id;

        }

        public async Task<int> UpdateAsync(District entity, int divisionId)
        {
            var isExists = _districtUnitOfWork.DistrictRepository.IsExists(m => m.Name == entity.Name && m.Id != entity.Id);
            if (isExists)
            {
                throw new DuplicationException(nameof(entity.Name));
            }
            entity.Division = _divisionUnitOfWork.DivisionRepository.GetById(divisionId);
            entity.LastModified = _dateTimeService.Now;
            await _districtUnitOfWork.DistrictRepository.UpdateAsync(entity);
            await _districtUnitOfWork.SaveChangesAsync();
            return entity.Id;
        }

        public Task<bool> ActiveInactiveAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var district = await GetByIdAsync(id);
            if (district == null)
            {
                throw new NotFoundException(nameof(district), id);

            }
            await _districtUnitOfWork.DistrictRepository.DeleteAsync(district.Id);
            await _districtUnitOfWork.SaveChangesAsync();
            return true;
        }

        public void Dispose()
        {
            this._districtUnitOfWork.Dispose();
        }
    }
}
