﻿using Berger.Common.Enums;
using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Implements
{
    public class LoyaltyProgramInformationService : ILoyaltyProgramInformationService
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly ISettingUnitOfWork settingUnitOfWork;
        private readonly IPromotionalBannerUnitOfWork promotionalBannerUnitOfWork;
        private readonly IBenefitUnitOfWork benefitUnitOfWork;
        private readonly IFAQUnitOfWork fAQUnitOfWork;
        private readonly ISingleValuedUnitOfWork singleValuedUnitOfWork;

        public LoyaltyProgramInformationService(UserManager<ApplicationUser> userManager,
            ISettingUnitOfWork settingUnitOfWork,
            IPromotionalBannerUnitOfWork promotionalBannerUnitOfWork,
            IBenefitUnitOfWork benefitUnitOfWork,
            IFAQUnitOfWork fAQUnitOfWork,
            ISingleValuedUnitOfWork singleValuedUnitOfWork)
        {
            this.userManager = userManager;
            this.promotionalBannerUnitOfWork = promotionalBannerUnitOfWork;
            this.settingUnitOfWork = settingUnitOfWork;
            this.benefitUnitOfWork = benefitUnitOfWork;
            this.fAQUnitOfWork = fAQUnitOfWork;
            this.singleValuedUnitOfWork = singleValuedUnitOfWork;
        }

        public async Task<IList<Benefit>> GetAllAsync()
        {
            return await benefitUnitOfWork.BenefitRepository.GetAsync(x => x,
                            x => x.IsActive,
                            x => x.OrderBy(m => m.Name),
                            null, true);
        }

        public async Task<Benefit> GetByIdAsync(int id)
        {
            var benefit = await benefitUnitOfWork.BenefitRepository.GetByIdAsync(id);

            return benefit;
        }

        public void Dispose()
        {
            userManager.Dispose();
            promotionalBannerUnitOfWork.Dispose();
            benefitUnitOfWork.Dispose();
            fAQUnitOfWork.Dispose();
        }
        
        public async Task<LoyaltyProgramInformation> Get(string phoneNo)
        {
            LoyaltyProgramInformation loyalty = new LoyaltyProgramInformation();
            loyalty.User = userManager.Users.FirstOrDefault(m=>m.PhoneNumber == phoneNo);
            if(loyalty.User == null)
            {
                throw new NotFoundException(nameof(phoneNo), phoneNo);
            }
            var settings = await settingUnitOfWork.SettingRepository.GetAsync(
                    x => x, w => w.Points <= loyalty.User.Points && w.IsActive,
                    x => x.OrderByDescending(m => m.Points), null, true);
            if (settings != null)
                loyalty.Setting = settings.FirstOrDefault();
            loyalty.Benefits = await benefitUnitOfWork.BenefitRepository.GetAsync(x => x,
                            x => x.IsActive,
                            x => x.OrderBy(m => m.Name),
                            m => m.Include(a => a.Setting), true);
            loyalty.PromotionalBanners = await promotionalBannerUnitOfWork.PromotionalBannerRepository
                            .GetAsync(x => x,
                            x => x.IsActive,
                            x => x.OrderBy(m => m.Sequence),
                            null, true);

            loyalty.LoyaltyProgramFAQs = await fAQUnitOfWork.FAQRepository
                            .GetAsync(x => x,
                            w => w.FAQType == EnumFAQType.Loyalty_Program_FAQ,
                            x => x.OrderBy(m => m.Sequence),
                            null, true);
            loyalty.TermsConditions = await singleValuedUnitOfWork.SingleValuedRepository
                            .GetFirstOrDefaultAsync(x => x, x => x.Name.Equals("TNC"), null);
            return loyalty;
        }

        public Task<LoyaltyProgramInformation> Get()
        {
            throw new NotImplementedException();
        }

        //public async Task<LoyaltyProgramInformation> GetByUserId(string userId)
        //{
        //    LoyaltyProgramInformation loyalty = new LoyaltyProgramInformation();
        //    loyalty.User = await userManager.FindByIdAsync(userId);

        //    var settings = await settingUnitOfWork.SettingRepository.GetAsync(
        //            x => x, w => w.Points <= loyalty.User.Points && w.IsActive,
        //            x => x.OrderByDescending(m => m.Points), null, true);
        //    if (settings != null)
        //        loyalty.Setting = settings.FirstOrDefault();
        //    loyalty.Benefits = await benefitUnitOfWork.BenefitRepository.GetAsync(x => x,
        //                    x => x.IsActive,
        //                    x => x.OrderBy(m => m.Name),
        //                    m => m.Include(a => a.Setting), true);
        //    loyalty.PromotionalBanners = await promotionalBannerUnitOfWork.PromotionalBannerRepository
        //                    .GetAsync(x => x,
        //                    x => x.IsActive,
        //                    x => x.OrderBy(m => m.Sequence),
        //                    null, true);

        //    loyalty.LoyaltyProgramFAQs = await fAQUnitOfWork.FAQRepository
        //                    .GetAsync(x => x,
        //                    w => w.FAQType == EnumFAQType.Loyalty_Program_FAQ,
        //                    x => x.OrderBy(m => m.Sequence),
        //                    null, true);
        //    loyalty.TermsConditions = await singleValuedUnitOfWork.SingleValuedRepository
        //                    .GetFirstOrDefaultAsync(x => x, x => x.Name.Equals("TNC"), null);

        //    return loyalty;
        //}

        //public async Task<LoyaltyProgramInformation> GetByUsername(string userName)
        //{
        //    LoyaltyProgramInformation loyalty = new LoyaltyProgramInformation();
        //    loyalty.User = await userManager.FindByNameAsync(userName);

        //    var settings = await settingUnitOfWork.SettingRepository.GetAsync(
        //            x => x, w => w.Points <= loyalty.User.Points && w.IsActive,
        //            x => x.OrderByDescending(m => m.Points), null, true);
        //    if (settings != null)
        //        loyalty.Setting = settings.FirstOrDefault();
        //    loyalty.Benefits = await benefitUnitOfWork.BenefitRepository.GetAsync(x => x,
        //                    x => x.IsActive,
        //                    x => x.OrderBy(m => m.Name),
        //                    m=>m.Include(a=>a.Setting), true);
        //    loyalty.PromotionalBanners = await promotionalBannerUnitOfWork.PromotionalBannerRepository
        //                    .GetAsync(x => x,
        //                    x => x.IsActive,
        //                    x => x.OrderBy(m => m.Sequence),
        //                    null, true);

        //    loyalty.LoyaltyProgramFAQs = await fAQUnitOfWork.FAQRepository
        //                    .GetAsync(x => x,
        //                    w => w.FAQType == EnumFAQType.Loyalty_Program_FAQ,
        //                    x => x.OrderBy(m => m.Sequence),
        //                    null, true);
        //    loyalty.TermsConditions = await singleValuedUnitOfWork.SingleValuedRepository
        //                    .GetFirstOrDefaultAsync(x => x, x => x.Name.Equals("TNC"), null);
        //    return loyalty;
        //}
    }
}
