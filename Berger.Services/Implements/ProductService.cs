﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Berger.Common.Enums;
using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;

namespace Berger.Services.Implements
{
    public class ProductService : IProductService
    {
        private readonly IProductUnitOfWork productUnitOfWork;
        private readonly IDateTime _dateTimeService;

        public ProductService(IProductUnitOfWork productUnitOfWork, IDateTime dateTimeService)
        {
            this.productUnitOfWork = productUnitOfWork;
            this._dateTimeService = dateTimeService;
        }

        public async Task<QueryResult<Product>> GetAllAsync(ProductQuery queryObj)
        {
            var queryResult = new QueryResult<Product>();

            var columnsMap = new Dictionary<string, Expression<Func<Product, object>>>()
            {
                ["name"] = v => v.Name,
            };

            var result = await productUnitOfWork.ProductRepository.GetAsync(x => x,
                            x => (string.IsNullOrEmpty(queryObj.Name) || x.Name.Contains(queryObj.Name)),
                            x => x.ApplyOrdering(columnsMap, queryObj.SortBy, queryObj.IsSortAscending),
                            null, queryObj.Page, 5000);

            queryResult.Items = result.Items;
            queryResult.Total = result.Total;
            queryResult.TotalFilter = result.TotalFilter;

            return queryResult;
        }
        public IList<KeyValuePairObject> GetSurfaceTypeSelectAsync()
        {
            return Enum.GetValues(typeof(EnumSurfaceType))
                .Cast<EnumSurfaceType>()
                .Select(p => new KeyValuePairObject { Value = ((int)p), Text = p.ToString().Replace('_','/')})
                .ToList();
        }
        public IList<KeyValuePairObject> GetSurfaceConditionSelectAsync()
        {
            return Enum.GetValues(typeof(EnumSurfaceCondition))
                .Cast<EnumSurfaceCondition>()
                .Select(p => new KeyValuePairObject { Value = ((int)p), Text = p.ToString() })
                .ToList();
        }

        public async Task<IList<Product>> GetAllAsync()
        {
            return await productUnitOfWork.ProductRepository.GetAsync(x => x,
                            x => x.IsActive,
                            x => x.OrderBy(m=>m.Name),
                            null, true);
        }
        public async Task<IList<KeyValuePairObject>> GetBySurfaceSelectAsync(int surfaceType)
        {
            return await productUnitOfWork.ProductRepository.GetAsync(x =>
                new KeyValuePairObject { Value = x.Id, Text = x.Name.Replace('_','/') },
                x => x.IsActive & x.SurfaceType == (EnumSurfaceType)surfaceType, x => x.OrderBy(o => o.Name), null, true);
        }
        public async Task<IList<Product>> GetBySurfaceAsync(int surfaceType)
        {
            return await productUnitOfWork.ProductRepository.GetAsync(x => x,
                            x => x.IsActive & x.SurfaceType == (EnumSurfaceType) surfaceType,
                            x => x.OrderBy(m => m.Name),
                            null, true);
        }

        public async Task<Product> GetByIdAsync(int id)
        {
            var product= await productUnitOfWork.ProductRepository.GetByIdAsync(id);

            return product;
        }

        public async Task<int> AddAsync(Product entity)
        {
            var isExists = productUnitOfWork.ProductRepository.IsExists(m => m.Name == entity.Name);
            if (isExists)
            {
                throw new DuplicationException(nameof(entity.Name));
            }
            // entity.CreatedBy = Guid.Parse("EC3D80B8-A13C-4696-B40D-97DA6F5148A0");
            entity.Created = _dateTimeService.Now;
           await productUnitOfWork.ProductRepository.AddAsync(entity);
           await productUnitOfWork.SaveChangesAsync();
           return entity.Id;

        }

        public async Task<int> UpdateAsync(Product product)
        {
            var isExists = productUnitOfWork.ProductRepository.IsExists(m => m.Name == product.Name && m.Id != product.Id);
            if (isExists)
            {
                throw new DuplicationException(nameof(product.Name));
            }
            var entity = await productUnitOfWork.ProductRepository.GetByIdAsync(product.Id);
            entity.Name = product.Name;
            entity.Description = product.Description;
            entity.LastModified = _dateTimeService.Now;
            entity.ImageUrl = string.IsNullOrWhiteSpace(product.ImageUrl) ? entity.ImageUrl : product.ImageUrl;
            await productUnitOfWork.ProductRepository.UpdateAsync(entity);
           await productUnitOfWork.SaveChangesAsync();
           return entity.Id;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var product = await GetByIdAsync(id);
            if(product == null)
            {
                throw new NotFoundException(nameof(product),id); 

            }
            await productUnitOfWork.ProductRepository.DeleteAsync(product.Id);
            await productUnitOfWork.SaveChangesAsync();
            return true;
    }

        public void Dispose()
        {
            this.productUnitOfWork.Dispose();
        }
    }
}
