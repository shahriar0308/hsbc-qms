﻿using Berger.Common.Constants;
using Berger.Common.Enums;
using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Repository.LMS.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Implements
{
    public class ServiceRequisitionService : IServiceRequisitionService
    {
        private readonly IServiceRequisitionUnitOfWork serviceRequisitionUnitOfWork;
        private readonly ILeadMstrDtlUnitOfWork leadMstrDtlUnitOfWork;
        private readonly IDateTime dateTimeService;
        private readonly ICurrentUserService currentUserService;
        private readonly IUserProfileUnitOfWork userProfileUnitOfWork;

        public ServiceRequisitionService(IServiceRequisitionUnitOfWork serviceRequisitionUnitOfWork,
        IDateTime dateTimeService, ICurrentUserService currentUserService, IUserProfileUnitOfWork userProfileUnitOfWork,
        ILeadMstrDtlUnitOfWork leadMstrDtlUnitOfWork)
        {
            this.serviceRequisitionUnitOfWork = serviceRequisitionUnitOfWork;
            this.dateTimeService = dateTimeService;
            this.currentUserService = currentUserService;
            this.userProfileUnitOfWork = userProfileUnitOfWork;
            this.leadMstrDtlUnitOfWork = leadMstrDtlUnitOfWork;
        }
        public async Task<IList<ServiceRequisition>> GetAllAsync(bool workIdProvided)
        {
            Expression<Func<ServiceRequisition, bool>> expression = m => !workIdProvided && string.IsNullOrEmpty(m.WorkId);
            if (workIdProvided) expression = null;
            return await serviceRequisitionUnitOfWork.ServiceRequisitionRepository.GetAsync(x => x,
                            expression,
                            x =>  x.OrderBy(m => m.WorkId).ThenByDescending(m=>m.Created),
                            m=>m.Include(a=>a.Thana).Include(a=>a.District).Include(a=>a.TypeOfRequirement), true);
        }
        public async Task<ServiceRequisition> GetDetailAsync(Guid id)
        {
            return await serviceRequisitionUnitOfWork.ServiceRequisitionRepository.GetFirstOrDefaultAsync(x => x,
                            m=>m.Id == id,
                            m => m.Include(a => a.Thana).Include(a => a.District).Include(a => a.TypeOfRequirement).Include(m=>m.ServiceRequisitionDetails), true);
        }

        public async Task<IList<ServiceRequisition>> GetByUserAsync(Guid userId)
        {
            return await serviceRequisitionUnitOfWork.ServiceRequisitionRepository.GetAsync(x => x,
                            a=>a.CreatedBy == userId,
                            x => x.OrderBy(m => m.WorkId).ThenByDescending(m => m.Created),
                            m => m.Include(a => a.Thana).Include(a => a.District).Include(a => a.TypeOfRequirement), true);
        }
        public async Task<ServiceRequisition> GetById(Guid id)
        {
            return await serviceRequisitionUnitOfWork.ServiceRequisitionRepository.GetFirstOrDefaultAsync(x => x,
                            a => a.Id == id,
                            m => m.Include(a => a.Thana).Include(a => a.District).Include(a => a.TypeOfRequirement), true);
        }
        public async Task<Guid> AddAsync(ServiceRequisition entity)
        {
            #region Entity Defaults
            entity.Created = dateTimeService.Now;
            entity.CreatedBy = currentUserService.UserId;
            entity.Stage = LeadStages.NEW;
            entity.IsActive = true;
            #endregion

            await serviceRequisitionUnitOfWork.ServiceRequisitionRepository.AddAsync(entity);
            await serviceRequisitionUnitOfWork.SaveChangesAsync();

            return entity.Id;
        }

        public bool UpdateColorConsultant(string lacUserId, string leadDetailId)
        {
            try
            {
                var userProfile = userProfileUnitOfWork.UserProfileRepository.GetFirstOrDefault(x => x,
                    m => m.UspUserId == lacUserId, null, true);
                var requisitionDetails = serviceRequisitionUnitOfWork.ServiceRequisitionDetailRepository.GetFirstOrDefault(x => x,
                    x => x.WorkDetailsId == leadDetailId);
                if (requisitionDetails != null)
                {

                    requisitionDetails.ColorConsultantName = userProfile.UspFirstName;
                    requisitionDetails.ColorConsultantPhoneNo = userProfile.UspMobile;

                    var leadNotification = GenerateColorConsultantAssignmentNotification(userProfile.UspFirstName, leadDetailId);
                    serviceRequisitionUnitOfWork.LeadNotificationRepository.Add(leadNotification);
                    serviceRequisitionUnitOfWork.ServiceRequisitionDetailRepository.Update(requisitionDetails);
                    serviceRequisitionUnitOfWork.SaveChanges();
                    return true;
                }
                return false;
            }
            catch(Exception ex)
            {
                serviceRequisitionUnitOfWork.Rollback();
                return false;
            }
        }

        private LeadNotification GenerateColorConsultantAssignmentNotification(string firstName, string leadDetailId)
        {
            return new LeadNotification
            {
                Title = "Color Consultant Assigned",
                Body = string.Format(NotificationTemplates.ColorConsultantAssigned, firstName, leadDetailId),
                Created = dateTimeService.Now,
                EnumNotificationType = EnumNotificationType.ColorConsultantAssignment,
                LeadDetailId = leadDetailId,
                IsSent = false
            };
        }

        public void Dispose()
        {
            serviceRequisitionUnitOfWork.Dispose();
        }
    }
}
