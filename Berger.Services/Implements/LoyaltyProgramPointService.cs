﻿using Berger.Common.Extensions;
using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Implements
{
    public class LoyaltyProgramPointService : ILoyaltyProgramPointService
    {        
        private readonly ILoyaltyProgramPointUnitOfWork _loyaltyProgramPointUnitOfWork;
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTime _dateTime;


        public LoyaltyProgramPointService(
            ILoyaltyProgramPointUnitOfWork loyaltyProgramPointUnitOfWork,
            ICurrentUserService currentUserService,
            IDateTime dateTime
            )
        {
            _loyaltyProgramPointUnitOfWork = loyaltyProgramPointUnitOfWork;
            _currentUserService = currentUserService;
            _dateTime = dateTime;
        }

        public async Task<LoyaltyProgramPoint> GetFirstOrDefaultAsync()
        {
            var loyaltyProgramPoint = await _loyaltyProgramPointUnitOfWork.LoyaltyProgramPointRepository.GetFirstOrDefaultAsync(x => x);

            if (loyaltyProgramPoint == null)
            {
                throw new NotFoundException(nameof(LoyaltyProgramPoint), string.Empty);
            }

            return loyaltyProgramPoint;
        }

        public async Task<int> UpdateAsync(LoyaltyProgramPoint entity)
        {
            var existingEntity = await _loyaltyProgramPointUnitOfWork.LoyaltyProgramPointRepository.GetByIdAsync(entity.Id);
            if (existingEntity == null) throw new NotFoundException(nameof(LoyaltyProgramPoint), entity.Id);

            existingEntity.ProductCost = entity.ProductCost;
            existingEntity.PainterCost = entity.PainterCost;
            existingEntity.SupervisionCharge = entity.SupervisionCharge;

            existingEntity.LastModified = _dateTime.Now;
            existingEntity.LastModifiedBy = _currentUserService.UserId;

            await _loyaltyProgramPointUnitOfWork.LoyaltyProgramPointRepository.UpdateAsync(existingEntity);
            var result = await _loyaltyProgramPointUnitOfWork.SaveChangesAsync();

            if (result == true)
            {
                return entity.Id;
            }
            else
            {
                throw new Exception("Failed to update Loyalty Point");
            }
        }

        public void Dispose()
        {
            _loyaltyProgramPointUnitOfWork.Dispose();
        }
    }
}
