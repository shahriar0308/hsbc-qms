﻿using QMS.Entities;
using QMS.Repository.Interfaces;
using QMS.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Services.Implements
{
    public class CustomerTypeService : ICustomerTypeService
    {
        private readonly ICustomerTypeUnitOfWork _customerTypeUnitOfWork;

        public CustomerTypeService(ICustomerTypeUnitOfWork customerTypeUnitOfWork)
        {
            _customerTypeUnitOfWork = customerTypeUnitOfWork;
        }
        public async Task<CustomerType> CreateAsync(CustomerType entity)
        {
            entity.Created = DateTime.UtcNow;
            //entity.Created
            await _customerTypeUnitOfWork.CustomerTypeRepository.AddAsync(entity);
            await _customerTypeUnitOfWork.SaveChangesAsync();
            return entity;
        }

        public Task<bool> DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task<IList<CustomerType>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public Task<CustomerType> GetByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<int> UpdateAsync(CustomerType entity)
        {
            throw new NotImplementedException();
        }
    }
}
