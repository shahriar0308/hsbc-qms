﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Implements
{
    public class UserOTPService : IUserOTPService
    {
        private readonly IUserOTPUnitOfWork _userOTPUnitOfWork;
        private readonly IDateTime _dateTimeService;

        public UserOTPService(IUserOTPUnitOfWork userOTPUnitOfWork,
            IDateTime dateTimeService)
        {
            this._userOTPUnitOfWork = userOTPUnitOfWork;
            this._dateTimeService = dateTimeService;
        }

        public async Task<int> GenerateOTPAsync(Guid userId)
        {
            Random random = new Random();
            int OTP = random.Next(1000, 9999);
            var entity = await _userOTPUnitOfWork.UserOTPRepository.GetFirstOrDefaultAsync(x => x, 
                            x => x.UserId == userId, null, true);

            if(entity == null)
            {
                entity = new UserOTP()
                {
                    UserId = userId,
                    OTP = OTP,
                    OTPAttemptCount = 0,
                    OTPSendCount = 1,
                    OTPSendDate = _dateTimeService.Now
                };

                await _userOTPUnitOfWork.UserOTPRepository.AddAsync(entity);
            }
            else 
            {
                entity.OTP = OTP;
                entity.OTPSendCount += 1;
                entity.OTPSendDate = _dateTimeService.Now;

                await _userOTPUnitOfWork.UserOTPRepository.UpdateAsync(entity);
            }

            await _userOTPUnitOfWork.SaveChangesAsync();

           return OTP;
        }
        
        public async Task<int> GenerateOTPByPhoneNumberAsync(string phoneNumber)
        {
            Random random = new Random();
            int OTP = random.Next(1000, 9999);
            var entity = await _userOTPUnitOfWork.UserOTPRepository.GetFirstOrDefaultAsync(x => x, 
                            x => x.PhoneNumber == phoneNumber, null, true);

            if(entity == null)
            {
                entity = new UserOTP()
                {
                    PhoneNumber = phoneNumber,
                    OTP = OTP,
                    OTPAttemptCount = 0,
                    OTPSendCount = 1,
                    OTPSendDate = _dateTimeService.Now
                };

                await _userOTPUnitOfWork.UserOTPRepository.AddAsync(entity);
            }
            else 
            {
                entity.OTP = OTP;
                entity.OTPSendCount += 1;
                entity.OTPSendDate = _dateTimeService.Now;

                await _userOTPUnitOfWork.UserOTPRepository.UpdateAsync(entity);
            }

            await _userOTPUnitOfWork.SaveChangesAsync();

           return OTP;
        }

        public async Task<bool> IsValidOTPAsync(Guid userId, int otp, bool isOTPReset = false, bool isOTPExpires = false, int OTPExpiresMinutes = 0)
        {
            var isValid = false;
            var entity = await _userOTPUnitOfWork.UserOTPRepository.GetFirstOrDefaultAsync(x => x,
                            x => x.UserId == userId, null, true);

            if (entity != null)
            {
                var eOSDate = entity.OTPSendDate.AddMinutes(OTPExpiresMinutes);
                if (entity.OTP != 0 && entity.OTP == otp && (!isOTPExpires || eOSDate >= _dateTimeService.Now))
                {
                    if (isOTPReset) 
                        entity.OTP = 0;
                    isValid = true;
                }
                entity.OTPAttemptCount += 1;

                await _userOTPUnitOfWork.UserOTPRepository.UpdateAsync(entity);
                await _userOTPUnitOfWork.SaveChangesAsync();
            }

            return isValid;
        }

        public async Task<bool> IsValidOTPByPhoneNumberAsync(string phoneNumber, int otp, bool isOTPReset = false, bool isOTPExpires = false, int OTPExpiresMinutes = 0)
        {
            var isValid = false;
            var entity = await _userOTPUnitOfWork.UserOTPRepository.GetFirstOrDefaultAsync(x => x,
                            x => x.PhoneNumber == phoneNumber, null, true);

            if (entity != null)
            {
                var eOSDate = entity.OTPSendDate.AddMinutes(OTPExpiresMinutes);
                if (entity.OTP != 0 && entity.OTP == otp && (!isOTPExpires || eOSDate >= _dateTimeService.Now))
                {
                    if (isOTPReset) 
                        entity.OTP = 0;
                    isValid = true;
                }
                entity.OTPAttemptCount += 1;

                await _userOTPUnitOfWork.UserOTPRepository.UpdateAsync(entity);
                await _userOTPUnitOfWork.SaveChangesAsync();
            }

            return isValid;
        }

        public async Task<bool> ResetOTPAsync(Guid userId)
        {
            var isReset = false;
            var entity = await _userOTPUnitOfWork.UserOTPRepository.GetFirstOrDefaultAsync(x => x,
                            x => x.UserId == userId, null, true);

            if (entity != null)
            {
                entity.OTP = 0;

                await _userOTPUnitOfWork.UserOTPRepository.UpdateAsync(entity);
                await _userOTPUnitOfWork.SaveChangesAsync();

                isReset = true;
            }

            return isReset;
        }
        
        public async Task<bool> ResetOTPByPhoneNumberAsync(string phoneNumber)
        {
            var isReset = false;
            var entity = await _userOTPUnitOfWork.UserOTPRepository.GetFirstOrDefaultAsync(x => x,
                            x => x.PhoneNumber == phoneNumber, null, true);

            if (entity != null)
            {
                entity.OTP = 0;

                await _userOTPUnitOfWork.UserOTPRepository.UpdateAsync(entity);
                await _userOTPUnitOfWork.SaveChangesAsync();

                isReset = true;
            }

            return isReset;
        }

        public void Dispose()
        {
            this._userOTPUnitOfWork.Dispose();
        }
    }
}
