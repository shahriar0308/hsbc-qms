﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Interfaces;
using Berger.Services.Interfaces;
using Berger.Repository.Extensions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Berger.Services.Exceptions;
using System.Linq;

namespace Berger.Services.Implements
{
    public class SettingService : ISettingService
    {
        private readonly ISettingUnitOfWork settingUnitOfWork;
        private readonly IDateTime dateTime;
        private readonly ICurrentUserService currentUserService;

        public SettingService(ISettingUnitOfWork settingUnitOfWork, IDateTime dateTime, ICurrentUserService currentUserService)
        {
            this.settingUnitOfWork = settingUnitOfWork;
            this.currentUserService = currentUserService;
            this.dateTime = dateTime;
        }
        public async Task<IList<KeyValuePairObject>> GetAllSelectAsync()
        {
            return await this.settingUnitOfWork.SettingRepository.GetAsync<KeyValuePairObject>(x =>
                new KeyValuePairObject { Value = x.Id.ToString(), Text = x.Category },
                x => x.IsActive, x => x.OrderBy(o => o.Points), null, true);
        }

        public async Task<QueryResult<Setting>> GetAllAsync(SettingQuery queryObj)
        {
            var queryResult = new QueryResult<Setting>();

            var columnsMap = new Dictionary<string, Expression<Func<Setting, object>>>()
            {
                ["category"] = v => v.Category
            };

            var result = await settingUnitOfWork.SettingRepository.GetAsync(x => x,
                            x =>
                            (string.IsNullOrEmpty(queryObj.Category) || x.Category.Contains(queryObj.Category)),
                            x => x.ApplyOrdering(columnsMap, queryObj.SortBy, queryObj.IsSortAscending),
                            null, queryObj.Page, 5000);

            queryResult.Items = result.Items;
            queryResult.Total = result.Total;
            queryResult.TotalFilter = result.TotalFilter;

            return queryResult;
        }

        public async Task<IList<Setting>> GetAllAsync()
        {
            
            return await settingUnitOfWork.SettingRepository.GetAsync(x => x,
                m=> m.IsActive,
                m=>m.OrderBy(a=>a.Points), null, true);
        }

        public async Task<Setting> GetByIdAsync(int id)
        {
            var setting = await settingUnitOfWork.SettingRepository.GetByIdAsync(id);

            if (setting == null)
            {
                throw new NotFoundException(nameof(Setting), id);
            }

            return setting;
        }

        public async Task<Setting> GetByPoints(long points)
        {
            var setting = await settingUnitOfWork.SettingRepository.GetAsync(
                    x=>x, w=> w.Points <= points && w.IsActive,
                    x=>x.OrderByDescending(m=>m.Points), null, true);

            if (setting == null)
            {
                throw new NotFoundException(nameof(Setting), points);
            }

            return setting.FirstOrDefault();
        }

        public async Task<Setting> AddAsync(Setting entity)
        {
            var isExists = settingUnitOfWork.SettingRepository.IsExists(m => m.Category == entity.Category);
            if (isExists)
            {
                throw new DuplicationException(nameof(entity.Category));
            }
            isExists = settingUnitOfWork.SettingRepository.IsExists(m => m.Points == entity.Points);
            if (isExists)
            {
                throw new DuplicationException(nameof(entity.Points));
            }
            entity.Created = dateTime.Now;
            entity.CreatedBy = currentUserService.UserId;

            await settingUnitOfWork.SettingRepository.AddAsync(entity);
            
            if(await settingUnitOfWork.SaveChangesAsync() == true)
            {
                return entity;
            }
            else
            {
                throw new Exception("Failed to add Setting");
            }

            //throw new NotImplementedException();
        }

        public async Task<int> UpdateAsync(Setting entity)
        {
            var isExists = settingUnitOfWork.SettingRepository.IsExists(m => m.Category == entity.Category && m.Id != entity.Id);
            if (isExists)
            {
                throw new DuplicationException(nameof(entity.Category));
            }
            isExists = settingUnitOfWork.SettingRepository.IsExists(m => m.Points == entity.Points);
            if (isExists)
            {
                throw new DuplicationException(nameof(entity.Points));
            }
            entity.LastModified = dateTime.Now;
            entity.LastModifiedBy = currentUserService.UserId;

            await settingUnitOfWork.SettingRepository.UpdateAsync(entity);

            if (await settingUnitOfWork.SaveChangesAsync() == true)
            {
                return 1;
            }
            else
            {
                throw new Exception("Failed to update Setting");
            }
            //throw new NotImplementedException();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            await settingUnitOfWork.SettingRepository.DeleteAsync(id);
            if (await settingUnitOfWork.SaveChangesAsync() == true)
            {
                return true;
            }
            else
            {
                throw new Exception("Failed to delete Setting" );
            }
        }

        public void Dispose()
        {
            this.settingUnitOfWork.Dispose();
        }
    }
}
