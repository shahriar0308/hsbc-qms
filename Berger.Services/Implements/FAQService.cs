﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Berger.Common.Enums;
using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;

namespace Berger.Services.Implements
{
    public class FAQService : IFAQService
    {
        private readonly IFAQUnitOfWork _FAQUnitOfWork;
        private readonly IDateTime _dateTimeService;

        public FAQService(IFAQUnitOfWork FAQUnitOfWork, IDateTime dateTimeService)
        {
            this._FAQUnitOfWork = FAQUnitOfWork;
            this._dateTimeService = dateTimeService;
        }

        public async Task<QueryResult<FAQ>> GetAllAsync(FAQQuery queryObj)
        {
            var queryResult = new QueryResult<FAQ>();

            var columnsMap = new Dictionary<string, Expression<Func<FAQ, object>>>()
            {
                ["questionTitle"] = v => v.QuestionTitle,
                ["sequence"] = v => v.Sequence,
            };

            var result = await _FAQUnitOfWork.FAQRepository.GetAsync(x => x,
                            x => (string.IsNullOrEmpty(queryObj.QuestionTitle) || x.QuestionTitle.Contains(queryObj.QuestionTitle)),
                            x => x.ApplyOrdering(columnsMap, queryObj.SortBy, queryObj.IsSortAscending),
                            null, queryObj.Page, 5000);

            queryResult.Items = result.Items;
            queryResult.Total = result.Total;
            queryResult.TotalFilter = result.TotalFilter;

            return queryResult;
        }

        public async Task<IList<FAQ>> GetAllLoyaltyProgramAsync()
        {
            return await _FAQUnitOfWork.FAQRepository
                .GetAsync<FAQ>(x=> x,
                w=>w.FAQType == EnumFAQType.Loyalty_Program_FAQ, x => x.OrderBy(m => m.Sequence), null, true);

        }

        public async Task<IList<FAQ>> GetAllServiceAsync()
        {
            return await _FAQUnitOfWork.FAQRepository
                .GetAsync<FAQ>(x => x,
                w => w.FAQType == EnumFAQType.Service_FAQ, x=>x.OrderBy(m=>m.Sequence),  null, true);
        }

        public IList<KeyValuePairObject> GetFAQTypeSelectAsync()
        {
            IList<KeyValuePairObject> FAQTypes =
            Enum.GetValues(typeof(EnumFAQType))
                .Cast<EnumFAQType>()
                .Select(p => new KeyValuePairObject { Value = ((int)p).ToString(), Text = p.ToString().Replace('_',' ') })
                .ToList();


            return FAQTypes;
        }

        public async Task<FAQ> GetByIdAsync(int id)
        {
            var faq = await _FAQUnitOfWork.FAQRepository.GetByIdAsync(id);

            return faq;
        }

        public async Task<int> AddAsync(FAQ entity)
        {
           // entity.CreatedBy = Guid.Parse("EC3D80B8-A13C-4696-B40D-97DA6F5148A0");
           entity.Created = _dateTimeService.Now;
           await _FAQUnitOfWork.FAQRepository.AddAsync(entity);
           await _FAQUnitOfWork.SaveChangesAsync();
           return entity.Id;

        }

        public async Task<int> UpdateAsync(FAQ entity)
        {
           entity.LastModified = _dateTimeService.Now;
           await _FAQUnitOfWork.FAQRepository.UpdateAsync(entity);
           await _FAQUnitOfWork.SaveChangesAsync();
           return entity.Id;
        }

        public Task<bool> ActiveInactiveAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var faq = await GetByIdAsync(id);
            if(faq == null)
            {
                throw new NotFoundException(nameof(faq),id); 

            }
            await _FAQUnitOfWork.FAQRepository.DeleteAsync(faq.Id);
            await _FAQUnitOfWork.SaveChangesAsync();
            return true;
    }

        public void Dispose()
        {
            this._FAQUnitOfWork.Dispose();
        }
    }
}
