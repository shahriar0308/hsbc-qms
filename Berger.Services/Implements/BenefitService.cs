﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Implements
{
    public class BenefitService : IBenefitService
    {
        private readonly IBenefitUnitOfWork benefitUnitOfWork;
        private readonly ISettingUnitOfWork settingUnitOfWork;
        private readonly IDateTime _dateTimeService;

        public BenefitService(IBenefitUnitOfWork benefitUnitOfWork, ISettingUnitOfWork settingUnitOfWork,
        IDateTime dateTimeService)
        {
            this.benefitUnitOfWork = benefitUnitOfWork;
            this.settingUnitOfWork = settingUnitOfWork;
            this._dateTimeService = dateTimeService;
        }

        public async Task<IList<KeyValuePairObject>> GetAllForSelectAsync()
        {
            return await benefitUnitOfWork.BenefitRepository.GetAsync(x =>
                new KeyValuePairObject { Value = x.Id.ToString(), Text = x.Name },
                x => x.IsActive && !x.IsDeleted, x => x.OrderBy(o => o.Name), null, true);
        }

        public async Task<QueryResult<Benefit>> GetAllAsync(BenefitQuery queryObj)
        {
            var queryResult = new QueryResult<Benefit>();

            var columnsMap = new Dictionary<string, Expression<Func<Benefit, object>>>()
            {
                ["name"] = v => v.Name,
                ["sequence"] = v => v.Sequence
            };

            var result = await benefitUnitOfWork.BenefitRepository.GetAsync(x => x,
                            x =>
                            (string.IsNullOrEmpty(queryObj.Name) || x.Name.Contains(queryObj.Name)),
                            x => x.ApplyOrdering(columnsMap, queryObj.SortBy, queryObj.IsSortAscending),
                            x=>x.Include(m=>m.Setting), queryObj.Page, 5000);

            queryResult.Items = result.Items;
            queryResult.Total = result.Total;
            queryResult.TotalFilter = result.TotalFilter;

            return queryResult;
        }

        public async Task<IList<Benefit>> GetAllAsync()
        {
            return await benefitUnitOfWork.BenefitRepository.GetAsync(x => x,
                            x => x.IsActive && x.Setting.IsActive,
                            x => x.OrderBy(m=>m.Sequence).ThenBy( m=> m.Name),
                            x=>x.Include(m=>m.Setting), true);
        }

        public async Task<Benefit> GetByIdAsync(int id)
        {
            var benefit = await benefitUnitOfWork.BenefitRepository.GetByIdAsync(id);
            benefit.Setting = await settingUnitOfWork.SettingRepository.GetByIdAsync(benefit.SettingId);

            return benefit;
        }

        public async Task<IList<Benefit>> GetByCategoryAsync(int settingId)
        {
            return await benefitUnitOfWork.BenefitRepository.GetAsync(x => x,
                            x => x.IsActive && x.Setting.IsActive && x.SettingId == settingId,
                            x => x.OrderBy(m => m.Name),
                            x => x.Include(m => m.Setting), true);
        }

        public async Task<int> AddAsync(Benefit entity, int settingId)
        {
            var isExists = benefitUnitOfWork.BenefitRepository.IsExists(m => m.Name == entity.Name);
            if (isExists)
            {
                throw new DuplicationException(nameof(entity.Name));
            }
            entity.Created = _dateTimeService.Now;
            entity.Setting = settingUnitOfWork.SettingRepository.GetById(settingId);
            await benefitUnitOfWork.BenefitRepository.AddAsync(entity);
            await benefitUnitOfWork.SaveChangesAsync();
            return entity.Id;

        }

        public async Task<int> UpdateAsync(Benefit entity, int settingId)
        {
            var isExists = benefitUnitOfWork.BenefitRepository.IsExists(m => m.Name == entity.Name && m.Id != entity.Id);
            if (isExists)
            {
                throw new DuplicationException(nameof(entity.Name));
            }
            entity.LastModified = _dateTimeService.Now;
            entity.Setting = settingUnitOfWork.SettingRepository.GetById(settingId);
            await benefitUnitOfWork.BenefitRepository.UpdateAsync(entity);
            await benefitUnitOfWork.SaveChangesAsync();
            return entity.Id;
        }

        public Task<bool> ActiveInactiveAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var benefit = await GetByIdAsync(id);
            if (benefit == null)
            {
                throw new NotFoundException(nameof(benefit), id);

            }
            await benefitUnitOfWork.BenefitRepository.DeleteAsync(benefit.Id);
            await benefitUnitOfWork.SaveChangesAsync();
            return true;
        }

        public void Dispose()
        {
            settingUnitOfWork.Dispose();
            benefitUnitOfWork.Dispose();
        }
    }
}
