﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Implements
{
    public class LoginService : ILoginService
    {
        private readonly ILoginUnitOfWork _loginUnitOfWork;
        private readonly IDateTime _dateTimeService;

        public LoginService(ILoginUnitOfWork loginUnitOfWork,
            IDateTime dateTimeService)
        {
            this._loginUnitOfWork = loginUnitOfWork;
            this._dateTimeService = dateTimeService;
        }

        public async Task<int> UserLoggedInLogEntryAsync(Guid userId, string fcmToken)
        {
            var entity = new LoginHistory()
            {
                UserId = userId,
                FCMToken = fcmToken,
                IsLoggedIn = true,
                LoggedInDate = _dateTimeService.Now
            };

            await _loginUnitOfWork.LoginHistoryRepository.AddAsync(entity);
            await _loginUnitOfWork.SaveChangesAsync();

            return entity.Id; ;
        }

        public void Dispose()
        {
            this._loginUnitOfWork.Dispose();
        }
    }
}
