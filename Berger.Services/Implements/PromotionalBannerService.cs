﻿using Berger.Common.Extensions;
using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Implements
{
    public class PromotionalBannerService : IPromotionalBannerService
    {        
        private readonly IPromotionalBannerUnitOfWork _promotionalBannerUnitOfWork;
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTime _dateTime;


        public PromotionalBannerService(
            IPromotionalBannerUnitOfWork promotionalBannerUnitOfWork,
            ICurrentUserService currentUserService,
            IDateTime dateTime
            )
        {
            _promotionalBannerUnitOfWork = promotionalBannerUnitOfWork;
            _currentUserService = currentUserService;
            _dateTime = dateTime;
        }

        public async Task<QueryResult<PromotionalBanner>> GetAllAsync(PromotionalBannerQuery queryObj)
        {
            var queryResult = new QueryResult<PromotionalBanner>();

            var columnsMap = new Dictionary<string, Expression<Func<PromotionalBanner, object>>>()
            {
                ["name"] = v => v.Name,
            };

            var result = await _promotionalBannerUnitOfWork.PromotionalBannerRepository.GetAsync(x => x,
                            x => (string.IsNullOrEmpty(queryObj.Name) || x.Name.Contains(queryObj.Name)),
                            x => x.ApplyOrdering(columnsMap, queryObj.SortBy, queryObj.IsSortAscending),
                            null, queryObj.Page, queryObj.PageSize);           

            queryResult.Items = result.Items;
            queryResult.Total = result.Total;
            queryResult.TotalFilter = result.TotalFilter;

            return queryResult;
        }

        public async Task<IList<PromotionalBanner>> GetAllActiveAsync()
        {
            return (await _promotionalBannerUnitOfWork.PromotionalBannerRepository.GetAsync(x => x,
                x => x.IsActive && !x.IsDeleted, x => x.OrderBy(o => o.Name), null, true));
        }

        public async Task<PromotionalBanner> GetByIdAsync(int id)
        {
            var colorCode = await _promotionalBannerUnitOfWork.PromotionalBannerRepository.GetByIdAsync(id);

            if (colorCode == null)
            {
                throw new NotFoundException(nameof(PromotionalBanner), id);
            }

            return colorCode;
        }

        public async Task<int> AddAsync(PromotionalBanner entity)
        {
            //var isExist = await this.IsExistsNameAsync(entity.Name, entity.Id);
            //if (isExist)
            //    throw new DuplicationException(nameof(entity.Name));

            entity.Created = _dateTime.Now;
            entity.CreatedBy = _currentUserService.UserId;

            await _promotionalBannerUnitOfWork.PromotionalBannerRepository.AddAsync(entity);
            var result = await _promotionalBannerUnitOfWork.SaveChangesAsync();

            if (result == true)
            {
                return entity.Id;
            }
            else
            {
                throw new Exception("Failed to add Promotional Banner");
            }
        }

        public async Task<int> UpdateAsync(PromotionalBanner entity)
        {
            //var isExist = await this.IsExistsNameAsync(entity.Name, entity.Id);
            //if (isExist)
            //    throw new DuplicationException(nameof(entity.Name));

            var existingEntity = await _promotionalBannerUnitOfWork.PromotionalBannerRepository.GetByIdAsync(entity.Id);
            if (existingEntity == null) throw new NotFoundException(nameof(PromotionalBanner), entity.Id);

            existingEntity.Name = entity.Name;
            existingEntity.Description = entity.Description;
            existingEntity.Sequence = entity.Sequence;
            existingEntity.IsActive = entity.IsActive;
            existingEntity.ImageUrl = string.IsNullOrWhiteSpace(entity.ImageUrl) ? existingEntity.ImageUrl : entity.ImageUrl;

            existingEntity.LastModified = _dateTime.Now;
            existingEntity.LastModifiedBy = _currentUserService.UserId;

            await _promotionalBannerUnitOfWork.PromotionalBannerRepository.UpdateAsync(existingEntity);
            var result = await _promotionalBannerUnitOfWork.SaveChangesAsync();

            if (result == true)
            {
                return entity.Id;
            }
            else
            {
                throw new Exception("Failed to update Promotional Banner");
            }
        }

        public Task<bool> ActiveInactiveAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            await _promotionalBannerUnitOfWork.PromotionalBannerRepository.DeleteAsync(id);
            var result = await _promotionalBannerUnitOfWork.SaveChangesAsync();

            if (result == true)
            {
                return true;
            }
            else
            {
                throw new Exception("Failed to delete Promotional Banner");
            }
        }

        public async Task<bool> IsExistsNameAsync(string name, int id)
        {
            var result = await _promotionalBannerUnitOfWork.PromotionalBannerRepository.IsExistsAsync(x =>
                            (x.Name.ToLower() == name.ToLower()) && x.Id != id && !x.IsDeleted);
            return result;
        }

        public void Dispose()
        {
            _promotionalBannerUnitOfWork.Dispose();
        }
    }
}
