﻿using Berger.Common.Enums;
using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Implements
{
    public class PaintService : IPaintService
    {
        private readonly IPaintUnitOfWork paintUnitOfWork;
        private readonly IProductUnitOfWork productUnitOfWork;
        private readonly IDateTime _dateTimeService;

        public PaintService(IPaintUnitOfWork paintUnitOfWork, IProductUnitOfWork productUnitOfWork,
        IDateTime dateTimeService)
        {
            this.productUnitOfWork = productUnitOfWork;
            this.paintUnitOfWork = paintUnitOfWork;
            this._dateTimeService = dateTimeService;
        }

        public async Task<QueryResult<PaintingServiceCost>> GetAllAsync()
        {
            var queryResult = new QueryResult<PaintingServiceCost>();

            var result = await paintUnitOfWork.PaintRepository.GetAsync(x => x, null,null,
                            x=>x.Include(m=>m.Product), 1, 5000);

            queryResult.Items = result.Items;
            queryResult.Total = result.Total;
            queryResult.TotalFilter = result.TotalFilter;

            return queryResult;
        }

        public async Task<PaintingServiceCost> Get(int surfaceType, int surfaceCondition, int productId)
        {
            return await paintUnitOfWork.PaintRepository.GetFirstOrDefaultAsync(
                x=>x, x=>x.SurfaceType == (EnumSurfaceType)surfaceType && x.SurfaceCondition == (EnumSurfaceCondition)surfaceCondition
                && x.ProductId == productId, x=>x.Include(m=>m.Product), true);
        }

        public async Task<IList<PaintingServiceCost>> GetForGrid(int surfaceType, int surfaceCondition)
        {
            var products = await productUnitOfWork.ProductRepository.GetAsync(x => x,
                x => x.SurfaceType == (EnumSurfaceType)surfaceType, null, null, true);
            IList<PaintingServiceCost> paintingServices = new List<PaintingServiceCost>();
            foreach(var product in products)
            {
                var paintingService = await paintUnitOfWork.PaintRepository.GetFirstOrDefaultAsync(
                x => x, x => x.SurfaceType == (EnumSurfaceType)surfaceType && x.SurfaceCondition == (EnumSurfaceCondition)surfaceCondition
                  && x.ProductId == product.Id, x => x.Include(m => m.Product), true);
                if(paintingService == null)
                {
                    paintingService = new PaintingServiceCost
                    {
                        GoldRatePerSft = 0.00M,
                        SurfaceType = (EnumSurfaceType)surfaceType,
                        SurfaceCondition = (EnumSurfaceCondition)surfaceCondition,
                        PlatinumRatePerSft = 0.00M,
                        Product = product,
                        ProductId = product.Id,
                        SilverRatePerSft = 0.00M,
                        Vat = 0.00M
                    };
                    paintingService.ProductId = product.Id;
                    paintingService.Product = product;
                    paintingService.SurfaceType = (EnumSurfaceType)surfaceType;
                    paintingService.SurfaceCondition = (EnumSurfaceCondition)surfaceCondition;
                }
                paintingServices.Add(paintingService);
            }
            return paintingServices;

            //var list = await paintUnitOfWork.PaintRepository.GetFirstOrDefaultAsync(
            //    x => x, x => x.SurfaceType == (EnumSurfaceType)surfaceType && x.SurfaceCondition == (EnumSurfaceCondition)surfaceCondition
            //      && x.ProductId == productId, x => x.Include(m => m.Product), true);
        }

        public async Task<PaintingServiceCost> GetByIdAsync(int id)
        {
            var painting = await paintUnitOfWork.PaintRepository.GetByIdAsync(id);
            painting.Product = await productUnitOfWork.ProductRepository.GetByIdAsync(painting.ProductId);

            return painting;
        }

        public async Task<int> AddAsync(PaintingServiceCost entity, int productId)
        {
            var isExists = paintUnitOfWork.PaintRepository.IsExists(m =>
                m.SurfaceType == entity.SurfaceType && m.SurfaceCondition == entity.SurfaceCondition
                && m.ProductId == entity.ProductId);
            if (isExists)
            {
                throw new DuplicationException(
                    nameof(entity.SurfaceType)+", "+nameof(entity.SurfaceCondition) + ", " +
                    nameof(entity.Product));
            }
            entity.Created = _dateTimeService.Now;
            entity.Product = productUnitOfWork.ProductRepository.GetById(productId);
            await paintUnitOfWork.PaintRepository.AddAsync(entity);
            await paintUnitOfWork.SaveChangesAsync();
            return entity.Id;
        }

        public async Task<int> UpdateAsync(PaintingServiceCost entity, int productId)
        {
            var isExists = paintUnitOfWork.PaintRepository.IsExists(m =>
                m.SurfaceType == entity.SurfaceType && m.SurfaceCondition == entity.SurfaceCondition
                && m.ProductId == entity.ProductId && m.Id != entity.Id);
            if (isExists)
            {
                throw new DuplicationException(
                    nameof(entity.SurfaceType) + nameof(entity.SurfaceCondition) +
                    nameof(entity.Product));
            }
            entity.LastModified = _dateTimeService.Now;
            entity.Product = productUnitOfWork.ProductRepository.GetById(productId);
            await paintUnitOfWork.PaintRepository.UpdateAsync(entity);
            await paintUnitOfWork.SaveChangesAsync();
            return entity.Id;
        }

        public async Task<bool> AddOrUpdateAsync(IList<PaintingServiceCost> entities)
        {
            try
            {
                foreach (var entity in entities)
                {
                    var update = paintUnitOfWork.PaintRepository.GetFirstOrDefault(x => x, x=> x.ProductId == entity.ProductId
                        && x.SurfaceType == entity.SurfaceType && x.SurfaceCondition == entity.SurfaceCondition);
                    if (update == null)
                    {
                        entity.Created = _dateTimeService.Now;
                        entity.Product = productUnitOfWork.ProductRepository.GetById(entity.ProductId);
                        await paintUnitOfWork.PaintRepository.AddAsync(entity);
                    }
                    else
                    {
                        entity.LastModified = _dateTimeService.Now;
                        await paintUnitOfWork.PaintRepository.UpdateAsync(entity);
                    }
                }
                return await paintUnitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                paintUnitOfWork.Rollback();
                throw ex;
            }
        }

        

        public async Task<bool> DeleteAsync(int id)
        {
            var paintingServiceCost = await GetByIdAsync(id);
            if (paintingServiceCost == null)
            {
                throw new NotFoundException(nameof(paintingServiceCost), id);

            }
            await paintUnitOfWork.PaintRepository.DeleteAsync(paintingServiceCost.Id);
            await paintUnitOfWork.SaveChangesAsync();
            return true;
        }

        public void Dispose()
        {
            paintUnitOfWork.Dispose();
            productUnitOfWork.Dispose();
        }
    }
}
