﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Berger.Services.Implements
{
    public class ThanaService : IThanaService
    {
        private readonly IThanaUnitOfWork _thanaUnitOfWork;
        private readonly IDistrictUnitOfWork _districtUnitOfWork;
        private readonly IDateTime _dateTimeService;

        public ThanaService(IThanaUnitOfWork thanaUnitOfWork, IDateTime dateTimeService, IDistrictUnitOfWork districtUnitOfWork)
        {
            _thanaUnitOfWork = thanaUnitOfWork;
            _districtUnitOfWork = districtUnitOfWork;
            _dateTimeService = dateTimeService;
        }

        public async Task<Thana> GetByIdAsync(int id)
        {
            var thana = await _thanaUnitOfWork.ThanaRepository.GetByIdAsync(id);
            thana.District = await _districtUnitOfWork.DistrictRepository.GetByIdAsync(thana.DistrictId);
            return thana;
        }

        public async Task<IList<Thana>> GetAllAsync()
        {
            return await _thanaUnitOfWork.ThanaRepository
                .GetAsync(x => x,
                w => w.IsActive && w.District.IsActive,
                x => x.OrderBy(m => m.Name),
                x => x.Include(m => m.District), true);
        }

        public async Task<QueryResult<Thana>> GetAllAsync(ThanaQuery queryObj)
        {
            var queryResult = new QueryResult<Thana>();

            var columnsMap = new Dictionary<string, Expression<Func<Thana, object>>>()
            {
                ["Name"] = v => v.Name,
            };

            var result = await _thanaUnitOfWork.ThanaRepository.GetAsync(x => x,
                            x => (string.IsNullOrEmpty(queryObj.Name) || x.Name.Contains(queryObj.Name)),
                            x => x.ApplyOrdering(columnsMap, queryObj.SortBy, queryObj.IsSortAscending),
                            x => x.Include(o => o.District), queryObj.Page, 5000);

            queryResult.Items = result.Items;
            queryResult.Total = result.Total;
            queryResult.TotalFilter = result.TotalFilter;

            return queryResult;
        }

        public async Task<IList<KeyValuePairObject>> GetAllForSelectAsync()
        {
            return await this._thanaUnitOfWork.ThanaRepository.GetAsync<KeyValuePairObject>(x =>
                new KeyValuePairObject { Value = x.Id.ToString(), Text = x.Name },
                x => x.IsActive && !x.IsDeleted, x => x.OrderBy(o => o.Name), null, true);
        }

        public async Task<IList<KeyValuePairObject>> GetByDistrictSelectAsync(int districtId)
        {
            return await this._thanaUnitOfWork.ThanaRepository.GetAsync<KeyValuePairObject>(x =>
                new KeyValuePairObject { Value = x.Id.ToString(), Text = x.Name },
                x => x.IsActive && !x.IsDeleted & x.DistrictId == districtId && x.District.IsActive, x => x.OrderBy(o => o.Name), null, true);
        }

        public async Task<int> AddAsync(Thana entity, int districtId)
        {
            var isExists = _thanaUnitOfWork.ThanaRepository.IsExists(m => m.Name == entity.Name);
            if (isExists)
            {
                throw new DuplicationException(nameof(entity.Name));
            }
            entity.District = _districtUnitOfWork.DistrictRepository.GetById(districtId);
            entity.Created = _dateTimeService.Now;
            await _thanaUnitOfWork.ThanaRepository.AddAsync(entity);
            await _thanaUnitOfWork.SaveChangesAsync();
            return entity.Id;
        }

        public async Task<int> UpdateAsync(Thana entity, int districtId)
        {
            var isExists = _thanaUnitOfWork.ThanaRepository.IsExists(m => m.Name == entity.Name && m.Id != entity.Id);
            if (isExists)
            {
                throw new DuplicationException(nameof(entity.Name));
            }
            entity.District = _districtUnitOfWork.DistrictRepository.GetById(districtId);
            entity.LastModified = _dateTimeService.Now;
            await _thanaUnitOfWork.ThanaRepository.UpdateAsync(entity);
            await _thanaUnitOfWork.SaveChangesAsync();
            return entity.Id;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var thana = await GetByIdAsync(id);
            if (thana == null)
            {
                throw new NotFoundException(nameof(thana), id);

            }
            await _thanaUnitOfWork.ThanaRepository.DeleteAsync(thana.Id);
            await _thanaUnitOfWork.SaveChangesAsync();
            return true;
        }

        public void Dispose()
        {
            this._thanaUnitOfWork.Dispose();
        }
    }
}
