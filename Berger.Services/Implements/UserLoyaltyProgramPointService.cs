﻿using Berger.Common.Extensions;
using Berger.Entities;
using Berger.Entities.LMS;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using Berger.Services.LMS.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Implements
{
    public class UserLoyaltyProgramPointService : IUserLoyaltyProgramPointService
    {        
        private readonly IUserLoyaltyProgramPointUnitOfWork _userLoyaltyProgramPointUnitOfWork;
        private readonly IApplicationUserService _applicationUserService;
        private readonly ILeadMstrService _leadMstrService;
        private readonly ILoyaltyProgramPointService _loyaltyPointService;
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTime _dateTime;


        public UserLoyaltyProgramPointService(
            IUserLoyaltyProgramPointUnitOfWork loyaltyProgramPointUnitOfWork,
            ILeadMstrService leadMstrService,
            ILoyaltyProgramPointService loyaltyPointService,
            IApplicationUserService applicationUserService,
            ICurrentUserService currentUserService,
            IDateTime dateTime
            )
        {
            _userLoyaltyProgramPointUnitOfWork = loyaltyProgramPointUnitOfWork;
            _applicationUserService = applicationUserService;
            _leadMstrService = leadMstrService;
            _loyaltyPointService = loyaltyPointService;
            _currentUserService = currentUserService;
            _dateTime = dateTime;
        }

        public async Task<QueryResult<UserLoyaltyProgramPoint>> GetAllAsync(UserLoyaltyProgramPointQuery queryObj)
        {
            var queryResult = new QueryResult<UserLoyaltyProgramPoint>();

            var columnsMap = new Dictionary<string, Expression<Func<UserLoyaltyProgramPoint, object>>>()
            {
                ["fullName"] = v => v.User.FullName,
            };

            var result = await _userLoyaltyProgramPointUnitOfWork.UserLoyaltyProgramPointRepository.GetAsync(x => x,
                            x => (!queryObj.UserId.HasValue || x.UserId.Equals(queryObj.UserId)),
                            x => x.ApplyOrdering(columnsMap, queryObj.SortBy, queryObj.IsSortAscending),
                            null, queryObj.Page, queryObj.PageSize);

            queryResult.Items = result.Items;
            queryResult.Total = result.Total;
            queryResult.TotalFilter = result.TotalFilter;

            return queryResult;
        }

        public async Task<IList<UserLoyaltyProgramPoint>> GetAllActiveAsync()
        {
            return (await _userLoyaltyProgramPointUnitOfWork.UserLoyaltyProgramPointRepository.GetAsync(x => x,
                x => x.IsActive && !x.IsDeleted, x => x.OrderBy(o => o.User.FullName), null, true));
        }

        public async Task<UserLoyaltyProgramPoint> GetByIdAsync(int id)
        {
            var colorCode = await _userLoyaltyProgramPointUnitOfWork.UserLoyaltyProgramPointRepository.GetByIdAsync(id);

            if (colorCode == null)
            {
                throw new NotFoundException(nameof(UserLoyaltyProgramPoint), id);
            }

            return colorCode;
        }

        public async Task<int> AddAsync(UserLoyaltyProgramPoint entity)
        {
            var isExist = await this.IsExistsUserLeadAsync(entity.UserId, entity.LdLeadGuid, entity.Id);
            if (isExist)
                throw new DuplicationException(nameof(entity.UserId));

            entity.Created = _dateTime.Now;
            entity.CreatedBy = _currentUserService.UserId;

            await _userLoyaltyProgramPointUnitOfWork.UserLoyaltyProgramPointRepository.AddAsync(entity);
            var result = await _userLoyaltyProgramPointUnitOfWork.SaveChangesAsync();

            if (result == true)
            {
                return entity.Id;
            }
            else
            {
                throw new Exception("Failed to add User Loyalty Program Point");
            }
        }

        public async Task<int> UpdateUserLoyaltyPoint(string LdLeadGuid)
        {
            using (var transaction = await _userLoyaltyProgramPointUnitOfWork.BeginTransaction())
            {
                try
                {
                    var userLoyalPoint = await _userLoyaltyProgramPointUnitOfWork.UserLoyaltyProgramPointRepository.GetFirstOrDefaultAsync(x => x,
                                                    x => x.LdLeadGuid == LdLeadGuid, null, true);
                    if (userLoyalPoint == null) throw new NotFoundException(nameof(UserLoyaltyProgramPoint), LdLeadGuid);

                    var loyalPointSettings = await _loyaltyPointService.GetFirstOrDefaultAsync();
                    if (loyalPointSettings == null) throw new NotFoundException(nameof(LoyaltyProgramPoint), string.Empty);

                    var leadMstrHdr = await _leadMstrService.GetLeadAllConfirmedCostByLdLeadGuidAsync(LdLeadGuid);

                    #region User Loyalty Point Update
                    long points = (long)((leadMstrHdr.TotalProductCost / loyalPointSettings.ProductCost) +
                                        (leadMstrHdr.TotalPainterCost / loyalPointSettings.PainterCost) +
                                        (leadMstrHdr.TotalSupervisionCharge / loyalPointSettings.SupervisionCharge));

                    userLoyalPoint.TotalPainterCost = leadMstrHdr.TotalPainterCost;
                    userLoyalPoint.TotalProductCost = leadMstrHdr.TotalProductCost;
                    userLoyalPoint.TotalSupervisionCharge = leadMstrHdr.TotalSupervisionCharge;
                    userLoyalPoint.Points = points;
                    userLoyalPoint.IsCompleted = true;

                    userLoyalPoint.LastModified = _dateTime.Now;
                    userLoyalPoint.LastModifiedBy = _currentUserService.UserId;

                    await _userLoyaltyProgramPointUnitOfWork.UserLoyaltyProgramPointRepository.UpdateAsync(userLoyalPoint);
                    var result = await _userLoyaltyProgramPointUnitOfWork.SaveChangesAsync();
                    #endregion

                    #region User Points Update
                    await this._applicationUserService.UpdateUserPoints(userLoyalPoint.UserId, points);
                    #endregion

                    if (result == true)
                    {
                        await transaction.CommitAsync();
                        return userLoyalPoint.Id;
                    }
                    else
                    {
                        throw new Exception("Failed to update User Loyalty Program Point");
                    }
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
        }

        public Task<bool> ActiveInactiveAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            await _userLoyaltyProgramPointUnitOfWork.UserLoyaltyProgramPointRepository.DeleteAsync(id);
            var result = await _userLoyaltyProgramPointUnitOfWork.SaveChangesAsync();

            if (result == true)
            {
                return true;
            }
            else
            {
                throw new Exception("Failed to delete User Loyalty Program Point");
            }
        }

        public async Task<bool> IsExistsUserLeadAsync(Guid userId, string ldLeadGuid, int id) 
        {
            var result = await _userLoyaltyProgramPointUnitOfWork.UserLoyaltyProgramPointRepository.IsExistsAsync(x =>
                            x.UserId.Equals(userId) && x.LdLeadGuid.Equals(ldLeadGuid) && x.Id != id && !x.IsDeleted);
            return result;
        }

        public void Dispose()
        {
            _userLoyaltyProgramPointUnitOfWork.Dispose();
        }
    }
}
