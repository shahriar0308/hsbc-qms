﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Implements
{
    public class DivisionService : IDivisionService
    {
        private readonly IDivisionUnitOfWork _divisionUnitOfWork;
        private readonly IDateTime _dateTimeService;

        public DivisionService(IDivisionUnitOfWork divisionUnitOfWork,
        IDateTime dateTimeService)
        {
            this._divisionUnitOfWork = divisionUnitOfWork;
            this._dateTimeService = dateTimeService;
        }

        public async Task<IList<KeyValuePairObject>> GetAllForSelectAsync()
        {
            return await this._divisionUnitOfWork.DivisionRepository.GetAsync<KeyValuePairObject>(x =>
                new KeyValuePairObject { Value = x.Id.ToString(), Text = x.Name },
                x => x.IsActive && !x.IsDeleted, x => x.OrderBy(o => o.Name), null, true);
        }


        public async Task<QueryResult<Division>> GetAllAsync(DivisionQuery queryObj)
        {
            var queryResult = new QueryResult<Division>();

            var columnsMap = new Dictionary<string, Expression<Func<Division, object>>>()
            {
                ["Name"] = v => v.Name,
            };

            var result = await _divisionUnitOfWork.DivisionRepository.GetAsync(x => x,
                            x => (string.IsNullOrEmpty(queryObj.Name) || x.Name.Contains(queryObj.Name)),
                            x => x.ApplyOrdering(columnsMap, queryObj.SortBy, queryObj.IsSortAscending),
                            null, queryObj.Page, 5000);

            queryResult.Items = result.Items;
            queryResult.Total = result.Total;
            queryResult.TotalFilter = result.TotalFilter;

            return queryResult;
        }

        public async Task<IList<Division>> GetAllWithDistrictAsync()
        {
            var result = await _divisionUnitOfWork.DivisionRepository.GetAsync(x => x, null,
                               x => x.OrderBy(o => o.Name),
                               x => x.Include(i => i.Districts), true);

            result.ToList().ForEach(x => x.Districts = x.Districts.OrderBy(o => o.Name).ToList());

            return result;
        }

        public async Task<Division> GetByIdAsync(int id)
        {
            var division = await _divisionUnitOfWork.DivisionRepository.GetByIdAsync(id);

            return division;
        }

        public async Task<int> AddAsync(Division entity)
        {
            var isExists = _divisionUnitOfWork.DivisionRepository.IsExists(m => m.Name == entity.Name);
            if (isExists)
            {
                throw new DuplicationException(nameof(entity.Name));
            }
            entity.Created = _dateTimeService.Now;
            await _divisionUnitOfWork.DivisionRepository.AddAsync(entity);
            await _divisionUnitOfWork.SaveChangesAsync();
            return entity.Id;

        }

        public async Task<int> UpdateAsync(Division entity)
        {
            var isExists = _divisionUnitOfWork.DivisionRepository.IsExists(m => m.Name == entity.Name && m.Id != entity.Id);
            if (isExists)
            {
                throw new DuplicationException(nameof(entity.Name));
            }
            entity.LastModified = _dateTimeService.Now;
            await _divisionUnitOfWork.DivisionRepository.UpdateAsync(entity);
            await _divisionUnitOfWork.SaveChangesAsync();
            return entity.Id;
        }

        public Task<bool> ActiveInactiveAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var division = await GetByIdAsync(id);
            if (division == null)
            {
                throw new NotFoundException(nameof(division), id);

            }
            await _divisionUnitOfWork.DivisionRepository.DeleteAsync(division.Id);
            await _divisionUnitOfWork.SaveChangesAsync();
            return true;
        }

        public void Dispose()
        {
            this._divisionUnitOfWork.Dispose();
        }
    }
}
