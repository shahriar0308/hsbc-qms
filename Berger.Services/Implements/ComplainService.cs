
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Berger.Common.Enums;
using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;

namespace Berger.Services.Implements
{
    public class ComplainService : IComplainService
    {
        private readonly IComplainUnitOfWork _complainUnitOfWork;
        private readonly IDateTime _dateTimeService;

        public ComplainService(IComplainUnitOfWork complainUnitOfWork, IDateTime dateTimeService)
        {
            this._complainUnitOfWork = complainUnitOfWork;
            this._dateTimeService = dateTimeService;
        }

        public async Task<QueryResult<Complain>> GetAllAsync(ComplainQuery queryObj)
        {
            var queryResult = new QueryResult<Complain>();

            var columnsMap = new Dictionary<string, Expression<Func<Complain, object>>>()
            {
                ["complainDetails"] = v => v.ComplainDetails,
                ["Created"] = v => v.Created
            };

            var result = await _complainUnitOfWork.ComplainRepository.GetAsync(x => x,
                            x => (string.IsNullOrEmpty(queryObj.ComplainDetails) || x.ComplainDetails.Contains(queryObj.ComplainDetails)),
                            x => x.ApplyOrdering(columnsMap, queryObj.SortBy, queryObj.IsSortAscending),
                            null, queryObj.Page, 5000);

            queryResult.Items = result.Items;
            queryResult.Total = result.Total;
            queryResult.TotalFilter = result.TotalFilter;

            return queryResult;
        }

        public async Task<Complain> GetByIdAsync(int id)
        {
            var complain = await _complainUnitOfWork.ComplainRepository.GetByIdAsync(id);

            return complain;
        }

        public async Task<int> AddAsync(Complain entity)
        {
           // entity.CreatedBy = Guid.Parse("EC3D80B8-A13C-4696-B40D-97DA6F5148A0");
           entity.Created = _dateTimeService.Now;
           await _complainUnitOfWork.ComplainRepository.AddAsync(entity);
           await _complainUnitOfWork.SaveChangesAsync();
           return entity.Id;

        }

        public async Task<int> UpdateAsync(Complain entity)
        {
           entity.LastModified = _dateTimeService.Now;
           await _complainUnitOfWork.ComplainRepository.UpdateAsync(entity);
           await _complainUnitOfWork.SaveChangesAsync();
           return entity.Id;
        }

        public Task<bool> ActiveInactiveAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var complain = await GetByIdAsync(id);
            if(complain == null)
            {
                throw new NotFoundException(nameof(complain),id); 

            }
            await _complainUnitOfWork.ComplainRepository.DeleteAsync(complain.Id);
            await _complainUnitOfWork.SaveChangesAsync();
            return true;
        }

        public async Task<IList<KeyValuePairObject>> GetComplainTypeSelectAsync()
        {
            IList<KeyValuePairObject> ComplainTypes =
            Enum.GetValues(typeof(EnumComplainType))
                .Cast<EnumComplainType>()
                .Select(p => new KeyValuePairObject { Value = ((int)p).ToString(), Text = p.ToString() })
                .ToList();


            return ComplainTypes;
        }

        public void Dispose()
        {
            this._complainUnitOfWork.Dispose();
        }
    }
}
