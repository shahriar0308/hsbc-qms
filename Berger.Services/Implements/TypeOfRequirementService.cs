﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Implements
{
    public class TypeOfRequirementService : ITypeOfRequirementService
    {
        private readonly ITypeOfRequirementUnitOfWork typeOfRequirementUnitOfWork;
        private readonly IDateTime _dateTimeService;

        public TypeOfRequirementService(ITypeOfRequirementUnitOfWork typeOfRequirementUnitOfWork,
        IDateTime dateTimeService)
        {
            this.typeOfRequirementUnitOfWork = typeOfRequirementUnitOfWork;
            this._dateTimeService = dateTimeService;
        }

        public async Task<IList<KeyValuePairObject>> GetAllForSelectAsync()
        {
            return await typeOfRequirementUnitOfWork.TypeOfRequirementRepository.GetAsync(x =>
                new KeyValuePairObject { Value = x.Id.ToString(), Text = x.Name },
                x => x.IsActive && !x.IsDeleted, x => x.OrderBy(o => o.Name), null, true);
        }

        public async Task<QueryResult<TypeOfRequirement>> GetAllAsync(TypeOfRequirementQuery queryObj)
        {
            var queryResult = new QueryResult<TypeOfRequirement>();

            var columnsMap = new Dictionary<string, Expression<Func<TypeOfRequirement, object>>>()
            {
                ["name"] = v => v.Name,
            };

            var result = await typeOfRequirementUnitOfWork.TypeOfRequirementRepository.GetAsync(x => x,
                            x =>
                            (string.IsNullOrEmpty(queryObj.Name) || x.Name.Contains(queryObj.Name)),
                            x => x.ApplyOrdering(columnsMap, queryObj.SortBy, queryObj.IsSortAscending),
                            null, queryObj.Page, 5000);

            queryResult.Items = result.Items;
            queryResult.Total = result.Total;
            queryResult.TotalFilter = result.TotalFilter;

            return queryResult;
        }

        public async Task<IList<TypeOfRequirement>> GetAllAsync()
        {
            return await typeOfRequirementUnitOfWork.TypeOfRequirementRepository.GetAsync(x => x,
                            x => x.IsActive,
                            x => x.OrderBy(m=>m.Name),
                            null, true);
        }

        public async Task<TypeOfRequirement> GetByIdAsync(int id)
        {
            var ServiceCategory = await typeOfRequirementUnitOfWork.TypeOfRequirementRepository.GetByIdAsync(id);

            return ServiceCategory;
        }

        public async Task<int> AddAsync(TypeOfRequirement entity)
        {
            var isExists = typeOfRequirementUnitOfWork.TypeOfRequirementRepository.IsExists(m => m.Name == entity.Name);
            if (isExists)
            {
                throw new DuplicationException(nameof(entity.Name));
            }
            entity.Created = _dateTimeService.Now;
            await typeOfRequirementUnitOfWork.TypeOfRequirementRepository.AddAsync(entity);
            await typeOfRequirementUnitOfWork.SaveChangesAsync();
            return entity.Id;

        }

        public async Task<int> UpdateAsync(TypeOfRequirement entity)
        {
            var isExists = typeOfRequirementUnitOfWork.TypeOfRequirementRepository.IsExists(m => m.Name == entity.Name && m.Id != entity.Id);
            if (isExists)
            {
                throw new DuplicationException(nameof(entity.Name));
            }
            entity.LastModified = _dateTimeService.Now;
            await typeOfRequirementUnitOfWork.TypeOfRequirementRepository.UpdateAsync(entity);
            await typeOfRequirementUnitOfWork.SaveChangesAsync();
            return entity.Id;
        }

        public Task<bool> ActiveInactiveAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var ServiceCategory = await GetByIdAsync(id);
            if (ServiceCategory == null)
            {
                throw new NotFoundException(nameof(ServiceCategory), id);

            }
            await typeOfRequirementUnitOfWork.TypeOfRequirementRepository.DeleteAsync(ServiceCategory.Id);
            await typeOfRequirementUnitOfWork.SaveChangesAsync();
            return true;
        }

        public void Dispose()
        {
            typeOfRequirementUnitOfWork.Dispose();
        }
    }
}
