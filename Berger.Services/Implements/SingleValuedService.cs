﻿using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Implements
{
    public class SingleValuedService : ISingleValuedService
    {
        private readonly ISingleValuedUnitOfWork singleValuedUnitOfWork;
        private readonly IDateTime _dateTimeService;

        public SingleValuedService(ISingleValuedUnitOfWork singleValuedUnitOfWork,
        IDateTime dateTimeService)
        {
            this.singleValuedUnitOfWork = singleValuedUnitOfWork;
            this._dateTimeService = dateTimeService;
        }

        public async Task<QueryResult<SingleValuedEntity>> GetByNameAsync(string name)
        {
            var queryResult = new QueryResult<SingleValuedEntity>();

            var result = await singleValuedUnitOfWork.SingleValuedRepository.GetAsync(x => x,
                            x => x.Name.Equals(name),
                            null, null, 1, 1);

            queryResult.Items = result.Items;
            queryResult.Total = result.Total;
            queryResult.TotalFilter = result.TotalFilter;
            return queryResult;
        }

        public async Task<SingleValuedEntity> GetByIdAsync(int id)
        {
            var benefit = await singleValuedUnitOfWork.SingleValuedRepository.GetByIdAsync(id);

            return benefit;
        }

        public async Task<int> UpdateAsync(SingleValuedEntity entity)
        {
            entity.LastModified = _dateTimeService.Now;
            await singleValuedUnitOfWork.SingleValuedRepository.UpdateAsync(entity);
            await singleValuedUnitOfWork.SaveChangesAsync();
            return entity.Id;
        }


        public void Dispose()
        {
            singleValuedUnitOfWork.Dispose();
        }
    }
}
