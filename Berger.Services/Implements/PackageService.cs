﻿using Berger.Common.Enums;
using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Implements
{
    public class PackageService : IPackageService
    {

        public PackageService() { }

        public IList<KeyValuePairObject> GetAllForSelect()
        {
            return Enum.GetValues(typeof(EnumPackage))
                .Cast<EnumPackage>()
                .Select(p => new KeyValuePairObject { Value = p, Text = p.ToString() })
                .ToList();
        }
    }
}
