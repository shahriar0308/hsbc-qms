﻿using Berger.Common.Extensions;
using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Implements
{
    public class ColorCodeService : IColorCodeService
    {        
        private readonly IColorCodeUnitOfWork _colorCodeUnitOfWork;
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTime _dateTime;


        public ColorCodeService(
            IColorCodeUnitOfWork colorCodeUnitOfWork,
            ICurrentUserService currentUserService,
            IDateTime dateTime
            )
        {
            _colorCodeUnitOfWork = colorCodeUnitOfWork;
            _currentUserService = currentUserService;
            _dateTime = dateTime;
        }

        public async Task<QueryResult<ColorCode>> GetAllAsync(ColorCodeQuery queryObj)
        {
            var queryResult = new QueryResult<ColorCode>();

            var columnsMap = new Dictionary<string, Expression<Func<ColorCode, object>>>()
            {
                ["shadeSequence"] = v => v.ShadeSequence,
            };

            var result = await _colorCodeUnitOfWork.ColorCodeRepository.GetAsync(x => x,
                            x => (string.IsNullOrEmpty(queryObj.ShadeSequence) || x.ShadeSequence.Contains(queryObj.ShadeSequence)),
                            x => x.ApplyOrdering(columnsMap, queryObj.SortBy, queryObj.IsSortAscending),
                            null, queryObj.Page, queryObj.PageSize);           

            queryResult.Items = result.Items;
            queryResult.Total = result.Total;
            queryResult.TotalFilter = result.TotalFilter;

            return queryResult;
        }

        public async Task<ColorCode> GetByIdAsync(int id)
        {
            var colorCode = await _colorCodeUnitOfWork.ColorCodeRepository.GetByIdAsync(id);

            if (colorCode == null)
            {
                throw new NotFoundException(nameof(ColorCode), id);
            }

            return colorCode;
        }

        public async Task<ColorCode> AddAsync(ColorCode entity)
        {
            var isExistShadeCode = await this.IsExistsShadeCodeAsync(entity.ShadeCode, entity.Id);
            if (isExistShadeCode)
                throw new DuplicationException(nameof(entity.ShadeCode));

            var isExistShadeSequence = await this.IsExistsShadeSequenceAsync(entity.ShadeSequence, entity.Id);
            if (isExistShadeSequence)
                throw new DuplicationException(nameof(entity.ShadeSequence));

            entity.Created = _dateTime.Now;
            entity.CreatedBy = _currentUserService.UserId;

            await _colorCodeUnitOfWork.ColorCodeRepository.AddAsync(entity);                      

            if (await _colorCodeUnitOfWork.SaveChangesAsync() == true)
            {
                return entity;
            }
            else
            {
                throw new Exception("Failed to add Color Codes");
            }
        }

        public async Task<ColorCode> UpdateAsync(ColorCode entity)
        {
            var isExistShadeCode = await this.IsExistsShadeCodeAsync(entity.ShadeCode, entity.Id);
            if (isExistShadeCode)
                throw new DuplicationException(nameof(entity.ShadeCode));

            var isExistShadeSequence = await this.IsExistsShadeSequenceAsync(entity.ShadeSequence, entity.Id);
            if (isExistShadeSequence)
                throw new DuplicationException(nameof(entity.ShadeSequence));

            var existingEntity = await _colorCodeUnitOfWork.ColorCodeRepository.GetByIdAsync(entity.Id);
            if (existingEntity == null) throw new NotFoundException(nameof(ColorCode), entity.Id);

            existingEntity.ShadeSequence = entity.ShadeSequence;
            existingEntity.ShadeCode = entity.ShadeCode;
            existingEntity.R = entity.R;
            existingEntity.G = entity.G;
            existingEntity.B = entity.B;

            existingEntity.LastModified = _dateTime.Now;
            existingEntity.LastModifiedBy = _currentUserService.UserId;

            await _colorCodeUnitOfWork.ColorCodeRepository.UpdateAsync(existingEntity);

            if (await _colorCodeUnitOfWork.SaveChangesAsync() == true)
            {
                return entity;
            }
            else
            {
                throw new Exception("Failed to update Color Codes");
            }
        }

        public Task<bool> ActiveInactiveAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            await _colorCodeUnitOfWork.ColorCodeRepository.DeleteAsync(id);
            if (await _colorCodeUnitOfWork.SaveChangesAsync() == true)
            {
                return true;
            }
            else
            {
                throw new Exception("Failed to delete Color Codes");
            }
        }

        public async Task<IList<ColorCode>> GetAllActiveAsync()
        {
            return (await _colorCodeUnitOfWork.ColorCodeRepository.GetAsync(x => x,
                x => x.IsActive && !x.IsDeleted, x => x.OrderBy(o => o.ShadeSequence), null, true))
                .OrderBy(o => Convert.ToInt32(o.ShadeSequence.Substring(1, o.ShadeSequence.IndexOf('-') - 1)))
                .ThenBy(o => Convert.ToInt32(o.ShadeSequence.Substring(o.ShadeSequence.IndexOf('-') + 1))).ToList();
        }

        public async Task<(IList<ColorCode> Data, string Message)> DataTableSaveToDatabaseAsync(DataTable datatable)
        {
            var items = new List<ColorCode>();
            var existCount = 0;
            var ignoreCount = 0;

            foreach (DataRow row in datatable.Rows)
            {
                var item = new ColorCode();
                item.ShadeSequence = row["SHADESEQUENCE"].ObjectToString();
                item.ShadeCode = row["SHADECODE"].ObjectToString();
                item.R = row["R"].ObjectToString();
                item.G = row["G"].ObjectToString();
                item.B = row["B"].ObjectToString();

                if (string.IsNullOrEmpty(item.ShadeCode) || string.IsNullOrEmpty(item.ShadeCode) ||
                    string.IsNullOrEmpty(item.R) || string.IsNullOrEmpty(item.G) || string.IsNullOrEmpty(item.B))
                {
                    ignoreCount++;
                    continue;
                }

                var isExist = await this.IsExistsShadeCodeOrSequenceAsync(item.ShadeCode, item.ShadeSequence, item.Id);
                if (isExist)
                {
                    existCount++;
                    continue;
                }

                items.Add(item);
            }

            await _colorCodeUnitOfWork.ColorCodeRepository.AddRangeAsync(items);
            await _colorCodeUnitOfWork.SaveChangesAsync();

            var message = items.Count + " new data saved" +
                (existCount > 0 ? " and " + existCount + " data already exists" : "") +
                (ignoreCount > 0 ? " and " + ignoreCount + " data could not be saved" : "") + ".";

            return (items, message);
        }

        public async Task<bool> IsExistsShadeCodeOrSequenceAsync(string code, string sequence, int id)
        {
            var result = await _colorCodeUnitOfWork.ColorCodeRepository.IsExistsAsync(x =>
                            (x.ShadeCode.ToLower() == code.ToLower() || x.ShadeSequence.ToLower() == sequence.ToLower()) && x.Id != id && !x.IsDeleted);
            return result;
        }

        public async Task<bool> IsExistsShadeCodeAsync(string code, int id)
        {
            var result = await _colorCodeUnitOfWork.ColorCodeRepository.IsExistsAsync(x =>
                            (x.ShadeCode.ToLower() == code.ToLower()) && x.Id != id && !x.IsDeleted);
            return result;
        }

        public async Task<bool> IsExistsShadeSequenceAsync(string sequence, int id)
        {
            var result = await _colorCodeUnitOfWork.ColorCodeRepository.IsExistsAsync(x =>
                            (x.ShadeSequence.ToLower() == sequence.ToLower()) && x.Id != id && !x.IsDeleted);
            return result;
        }

        public void Dispose()
        {
            _colorCodeUnitOfWork.Dispose();
        }
    }
}
