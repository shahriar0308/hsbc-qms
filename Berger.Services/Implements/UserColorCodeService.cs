﻿using Berger.Common.Extensions;
using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Repository.Extensions;
using Berger.Repository.Interfaces;
using Berger.Services.Exceptions;
using Berger.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Implements
{
    public class UserColorCodeService : IUserColorCodeService
    {        
        private readonly IUserColorCodeUnitOfWork _userColorCodeUnitOfWork;
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTime _dateTime;


        public UserColorCodeService(
            IUserColorCodeUnitOfWork userColorCodeUnitOfWork,
            ICurrentUserService currentUserService,
            IDateTime dateTime
            )
        {
            _userColorCodeUnitOfWork = userColorCodeUnitOfWork;
            _currentUserService = currentUserService;
            _dateTime = dateTime;
        }

        public async Task<QueryResult<UserColorCode>> GetAllAsync(UserColorCodeQuery queryObj)
        {
            var queryResult = new QueryResult<UserColorCode>();

            var columnsMap = new Dictionary<string, Expression<Func<UserColorCode, object>>>()
            {
                ["fullName"] = v => v.User.FullName,
                ["shadeSequence"] = v => v.ColorCode.ShadeSequence,
            };

            var result = await _userColorCodeUnitOfWork.UserColorCodeRepository.GetAsync(x => x,
                            x => (string.IsNullOrEmpty(queryObj.ShadeSequence) || x.ColorCode.ShadeSequence.Contains(queryObj.ShadeSequence)) &&
                                (!queryObj.UserId.HasValue || x.UserId.Equals(queryObj.UserId.Value)),
                            x => x.ApplyOrdering(columnsMap, queryObj.SortBy, queryObj.IsSortAscending),
                            null, queryObj.Page, queryObj.PageSize);           

            queryResult.Items = result.Items;
            queryResult.Total = result.Total;
            queryResult.TotalFilter = result.TotalFilter;

            return queryResult;
        }

        public async Task<UserColorCode> GetByIdAsync(int id)
        {
            var userColorCode = await _userColorCodeUnitOfWork.UserColorCodeRepository.GetByIdAsync(id);

            if (userColorCode == null)
            {
                throw new NotFoundException(nameof(UserColorCode), id);
            }

            return userColorCode;
        }

        public async Task<UserColorCode> AddAsync(UserColorCode entity)
        {
            var isExistUserShadeCode = await this.IsExistsUserColorCodeAsync(entity.UserId, entity.ColorCodeId, entity.Id);
            if (isExistUserShadeCode)
                throw new DuplicationException("User Color Code");

            await _userColorCodeUnitOfWork.UserColorCodeRepository.AddAsync(entity);                      

            if (await _userColorCodeUnitOfWork.SaveChangesAsync() == true)
            {
                return entity;
            }
            else
            {
                throw new Exception("Failed to add User Color Codes");
            }
        }

        public async Task<UserColorCode> UpdateAsync(UserColorCode entity)
        {
            var isExistUserShadeCode = await this.IsExistsUserColorCodeAsync(entity.UserId, entity.ColorCodeId, entity.Id);
            if (isExistUserShadeCode)
                throw new DuplicationException("User Color Code");

            var existingEntity = await _userColorCodeUnitOfWork.UserColorCodeRepository.GetByIdAsync(entity.Id);
            if (existingEntity == null) throw new NotFoundException(nameof(UserColorCode), entity.Id);

            existingEntity.Name = entity.Name;
            existingEntity.ColorCodeId = entity.ColorCodeId;

            await _userColorCodeUnitOfWork.UserColorCodeRepository.UpdateAsync(existingEntity);

            if (await _userColorCodeUnitOfWork.SaveChangesAsync() == true)
            {
                return entity;
            }
            else
            {
                throw new Exception("Failed to update User Color Codes");
            }
        }

        public Task<bool> ActiveInactiveAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            await _userColorCodeUnitOfWork.UserColorCodeRepository.DeleteAsync(id);
            if (await _userColorCodeUnitOfWork.SaveChangesAsync() == true)
            {
                return true;
            }
            else
            {
                throw new Exception("Failed to delete User Color Codes");
            }
        }

        public async Task<IList<UserColorCode>> GetAllActiveAsync()
        {
            return (await _userColorCodeUnitOfWork.UserColorCodeRepository.GetAsync(x => x,
                x => x.IsActive && !x.IsDeleted, x => x.OrderBy(o => o.ColorCode.ShadeSequence), null, true))
                .OrderBy(o => Convert.ToInt32(o.ColorCode.ShadeSequence.Substring(1, o.ColorCode.ShadeSequence.IndexOf('-') - 1)))
                .ThenBy(o => Convert.ToInt32(o.ColorCode.ShadeSequence.Substring(o.ColorCode.ShadeSequence.IndexOf('-') + 1))).ToList();
        }

        public async Task<IList<UserColorCode>> GetAllActiveByUserIdAsync(Guid userId)
        {
            return (await _userColorCodeUnitOfWork.UserColorCodeRepository.GetAsync(x => x,
                x => x.IsActive && !x.IsDeleted && x.UserId == userId, x => x.OrderBy(o => o.ColorCode.ShadeSequence), x => x.Include(i => i.ColorCode), true))
                .OrderBy(o => Convert.ToInt32(o.ColorCode.ShadeSequence.Substring(1, o.ColorCode.ShadeSequence.IndexOf('-') - 1)))
                .ThenBy(o => Convert.ToInt32(o.ColorCode.ShadeSequence.Substring(o.ColorCode.ShadeSequence.IndexOf('-') + 1))).ToList();
        }

        public async Task<bool> IsExistsUserColorCodeAsync(Guid userId, int colorCodeId, int id)
        {
            var result = await _userColorCodeUnitOfWork.UserColorCodeRepository.IsExistsAsync(x =>
                            (x.UserId == userId && x.ColorCodeId == colorCodeId) && x.Id != id && !x.IsDeleted);
            return result;
        }

        public void Dispose()
        {
            _userColorCodeUnitOfWork.Dispose();
        }
    }
}
