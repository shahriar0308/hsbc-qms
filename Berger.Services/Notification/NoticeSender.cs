﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Services.Notification
{
    public class NotificationSender
    {
        public string ApiKey { get; set; }
        public string FcmUrl { get; set; }

        public NotificationSender(string apiKey, string fcmUrl)
        {
            ApiKey = apiKey;
            FcmUrl = fcmUrl;
        }

        public void Send_to_Android(JObject message, JArray devices)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.TryAddWithoutValidation(
                    "Authorization", "key=" + ApiKey);

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "relativeAddress");


                var fcmData = new JObject
                {
                    { "registration_ids", devices },
                    { "data", message }
                };

                //var se = new JavaScriptSerializer();
                //var jsonData = se.Serialize(fcmData);

                var jsonData = JsonConvert.SerializeObject(fcmData, Formatting.Indented);

                client.Timeout = TimeSpan.FromHours(1);
                var response = client.PostAsync(new Uri(FcmUrl), new StringContent(fcmData.ToString(), Encoding.Default, "application/json"));

                var x = response.Result.Content.ReadAsStringAsync();

                if (response.IsCanceled)
                {
                    throw new Exception("Cancelled");
                }

                if (response.IsFaulted)
                {
                    throw new Exception("Faulted");
                }

                if (response.IsCompleted)
                {

                }
            }
        }



        public async Task Send_to_Ios(JObject notification, JObject message, JArray devices)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.TryAddWithoutValidation(
                    "Authorization", "key=" + ApiKey);
                var fcmData = new JObject
                {
                    { "registration_ids", devices },
                    { "notification", notification },
                    { "data", message }
                };
                var response = await client.PostAsync(new Uri(FcmUrl), new StringContent(fcmData.ToString(), Encoding.Default, "application/json")).ConfigureAwait(false);

                if (!response.IsSuccessStatusCode)
                {
                    throw new NotSupportedException();
                }
            }
        }
    }
}
