﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Entities
{
    public class LoyaltyProgramPoint : AuditableEntity<int>
    {
        public int ProductCost { get; set; }
        public int PainterCost { get; set; }
        public int SupervisionCharge { get; set; }
    }
}
