using Berger.Common.Enums;
using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Entities
{
    public class SingleValuedEntity : AuditableEntity<int>
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
