﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Berger.Entities
{
    public class District : AuditableEntity<int>
    {
        public string Name { get; set; }

        [ForeignKey("DivisionId")]
        public virtual Division Division { get; set; }
        public int DivisionId { get; set; }
        public int LmsDivisionId { get; set; }
        public int LmsDistrictId { get; set; }
    }
}
