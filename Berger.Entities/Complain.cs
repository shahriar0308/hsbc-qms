using Berger.Common.Enums;
using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Entities
{
    public class Complain : AuditableEntity<int>
    {
        public EnumComplainType ComplainType { get; set; }
        public string ComplainDetails { get; set; }
    }
}
