﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Entities
{
    public class Setting : AuditableEntity<int>
    {
        public string Category { get; set; }
        public long Points { get; set; }
    }
}
