﻿using Berger.Common.Enums;
using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Berger.Entities
{
    public class PaintingServiceCost : AuditableEntity<int>
    {
        public EnumSurfaceType SurfaceType { get; set; }
        public EnumSurfaceCondition SurfaceCondition { get; set; }
        [ForeignKey("ProductId")]
        public Product Product { get; set; }
        public int ProductId { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal PlatinumRatePerSft { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal SilverRatePerSft { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal GoldRatePerSft { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal SevenToNine { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal AboveNine { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Vat { get; set; }
    }
}
