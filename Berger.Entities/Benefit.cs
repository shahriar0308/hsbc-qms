﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Berger.Entities
{
    public class Benefit : AuditableEntity<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        [ForeignKey("SettingId")]
        public virtual Setting Setting  { get; set; }
        public int SettingId { get; set; }
        public int Sequence { get; set; }
    }
}
