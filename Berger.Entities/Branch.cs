﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Entities
{
    public class Branch :AuditableEntity<int>
    {
        public string BranchName { get; set; }

        public string BranchCode { get; set; }
    }
}
