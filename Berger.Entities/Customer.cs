﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Entities
{
    public class Customer :Entity<int>
    {
        public string Token { get; set; }
        public string MobileNumber { get; set ; }
        public bool IsPriority { get; set; }
        public int BranchId { get; set; }
        public int CounterId { get; set; }
    }
}
