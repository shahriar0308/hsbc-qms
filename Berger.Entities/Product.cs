﻿using Berger.Common.Enums;
using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Berger.Entities
{
    public class Product : AuditableEntity<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public EnumSurfaceType SurfaceType { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal PaintCost { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal SealerCost { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal PuttyCost { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TopCoatCost { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal BaseCoatCost { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal PrimerCost { get; set; }
    }
}
