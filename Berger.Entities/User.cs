﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Entities
{
    public class User :Entity<int>
    {
        public string  UserName{ get; set; }

        public int BranchId{ get; set; }
    }
}
