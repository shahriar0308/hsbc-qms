﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Entities
{
    public class ServiceCategory :AuditableEntity<int>
    {
        public int ServiceName { get; set; }

        public string Code { get; set; }

    }
}
