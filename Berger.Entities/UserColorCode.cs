﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Entities
{
    public class UserColorCode : Entity<int>
    {
        public string Name { get; set; }
        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }
        public int ColorCodeId { get; set; }
        public ColorCode ColorCode { get; set; }
    }
}
