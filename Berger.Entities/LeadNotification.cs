﻿using Berger.Common.Enums;
using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Berger.Entities
{
    public class LeadNotification : AuditableEntity<int>
    {
        public string LeadId { get; set; }
        public string LeadDetailId { get; set; }
        public EnumNotificationType EnumNotificationType { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public bool IsSent { get; set; }
    }
}
