﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Berger.Entities
{
    public class ServiceRequisition : AuditableEntity<Guid>
    {
        public string WorkId { get; set; }
        [ForeignKey("TypeOfRequirementId")]
        public TypeOfRequirement TypeOfRequirement { get; set; }
        public int TypeOfRequirementId { get; set; }
        [ForeignKey("DistrictId")]
        public District District { get; set; }
        public int DistrictId { get; set; }
        [ForeignKey("ThanaId")]
        public Thana Thana { get; set; }
        public int ThanaId { get; set; }
        public string AddressLine1 { get; set; }
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string CustomerName { get; set; }
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string CustomerPhoneNo { get; set; }
        public string Stage { get; set; }
        public string RequirementDetail { get; set; }
        public virtual IList<ServiceRequisitionDetail> ServiceRequisitionDetails { get; set; }
    }
}
