using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Entities.NotMapped
{
    public class ComplainQuery : IQueryObject
    {
        public string ComplainDetails { get; set; }
        public DateTime Created { get; set; }
    }
}
