﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Entities.NotMapped
{
    public class UserLoyaltyProgramPointQuery : IQueryObject
    {
        public Guid? UserId { get; set; }
    }
}
