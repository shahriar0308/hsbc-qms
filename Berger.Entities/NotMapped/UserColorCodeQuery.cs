﻿
using System;

namespace Berger.Entities.NotMapped
{
    public class UserColorCodeQuery : IQueryObject
    {
        public Guid? UserId { get; set; }
        public string ShadeSequence { get; set; }
    }
}
