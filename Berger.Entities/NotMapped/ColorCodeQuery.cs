﻿
namespace Berger.Entities.NotMapped
{
    public class ColorCodeQuery : IQueryObject
    {
        public string ShadeSequence { get; set; }
    }
}
