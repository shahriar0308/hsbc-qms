﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Entities.NotMapped
{
    public class DealerQuery : IQueryObject
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
