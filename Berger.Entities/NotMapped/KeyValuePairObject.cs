using System;

namespace Berger.Entities
{
    public class KeyValuePairObject
    {
        public object Value { get; set; }
        public string Text { get; set; }
    }
}