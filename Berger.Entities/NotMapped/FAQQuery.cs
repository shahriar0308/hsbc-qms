﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Entities.NotMapped
{
    public class FAQQuery : IQueryObject
    {
        public string QuestionTitle { get; set; }
        public int Sequence { get; set; }
    }
}
