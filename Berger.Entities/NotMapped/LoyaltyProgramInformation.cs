﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Berger.Entities.NotMapped
{
    public class LoyaltyProgramInformation
    {
        public ApplicationUser User { get; set; }
        public Setting Setting { get; set; }
        public IList<PromotionalBanner> PromotionalBanners { get; set; }
        public IList<Benefit> Benefits { get; set; }
        public IList<FAQ> LoyaltyProgramFAQs { get; set; }
        public SingleValuedEntity TermsConditions { get; set; }
    }
}
