﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Entities
{
    public class UserOTP : Entity<int>
    {
        public Guid? UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string PhoneNumber { get; set; }
        public int OTP { get; set; }
        public int OTPAttemptCount { get; set; }
        public int OTPSendCount { get; set; }
        public DateTime OTPSendDate { get; set; }
    }
}
