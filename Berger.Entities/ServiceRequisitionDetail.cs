﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Berger.Entities
{
    public class ServiceRequisitionDetail : AuditableEntity<Guid>
    {

        [ForeignKey("ServiceRequisitionId")]
        public ServiceRequisition ServiceRequisition { get; set; }
        public Guid ServiceRequisitionId { get; set; }
        public string WorkDetailsId { get; set; }
        public string Stage { get; set; }
        public string FinancialEstimateUrl { get; set; }
        public string PaymentSlipUrl { get; set; }
        public string ColorConsultantName { get; set; }
        public string ColorConsultantPhoneNo { get; set; }
    }
}
