﻿using Berger.Entities.Core;

namespace Berger.Entities
{
    public class ColorCode : AuditableEntity<int>
    {
        public string ShadeSequence { get; set; }
        public string ShadeCode { get; set; }
        public string R { get; set; }
        public string G { get; set; }
        public string B { get; set; }
    }
}
