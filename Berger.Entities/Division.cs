﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Entities
{
    public class Division : AuditableEntity<int>
    {
        public string Name { get; set; }
        public int LmsDivisionId { get; set; }

        public IList<District> Districts { get; set; }

        public Division()
        {
            this.Districts = new List<District>();
        }
    }
}
