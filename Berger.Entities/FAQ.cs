﻿using Berger.Common.Enums;
using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Entities
{
    public class FAQ : AuditableEntity<int>
    {
        public string QuestionTitle { get; set; }
        public string QuestionAnswer { get; set; }
        public int Sequence { get; set; }
        public EnumFAQType FAQType { get; set; }
    }
}
