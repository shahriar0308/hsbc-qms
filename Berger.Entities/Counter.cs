﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Entities
{
    public class Counter: AuditableEntity<int>
    {
        public int BranchId{ get; set; }
        public int BranchUserId { get; set; }
        public bool IsPriority { get; set; }

    }
}
