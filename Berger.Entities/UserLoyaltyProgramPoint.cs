﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Berger.Entities
{
    public class UserLoyaltyProgramPoint : AuditableEntity<int>
    {
        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string LdLeadGuid { get; set; }
        public string LdLeadId { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalProductCost { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalPainterCost { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalSupervisionCharge { get; set; }
        public long Points { get; set; }
        public bool IsCompleted { get; set; }
    }
}
