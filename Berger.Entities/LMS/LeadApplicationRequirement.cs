﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_application_requirement")]
    public partial class LeadApplicationRequirement
    {
        [Key]
        [Column("lar_id")]
        public long LarId { get; set; }
        [Column("lar_quotation_hdr_id")]
        public long LarQuotationHdrId { get; set; }
        [Column("lar_quotation_dtl_id")]
        public long LarQuotationDtlId { get; set; }
        [Column("lar_quotation_item_id")]
        public long? LarQuotationItemId { get; set; }
        [Column("lar_item_application_code")]
        [StringLength(50)]
        public string LarItemApplicationCode { get; set; }
        [Column("lar_application_cost", TypeName = "decimal(18, 2)")]
        public decimal? LarApplicationCost { get; set; }
        [Column("lar_area", TypeName = "decimal(18, 3)")]
        public decimal? LarArea { get; set; }
        [Column("lar_total_cost", TypeName = "decimal(18, 2)")]
        public decimal? LarTotalCost { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
