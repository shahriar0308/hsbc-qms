﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    public partial class VwCompanyPartnerMstr
    {
        [Required]
        [Column("customer_type")]
        [StringLength(8)]
        public string CustomerType { get; set; }
        [Column("company_id")]
        public long CompanyId { get; set; }
        [Column("type")]
        [StringLength(500)]
        public string Type { get; set; }
        [Required]
        [Column("name")]
        [StringLength(103)]
        public string Name { get; set; }
        [Column("verical")]
        [StringLength(500)]
        public string Verical { get; set; }
        [Column("sub_verical")]
        [StringLength(500)]
        public string SubVerical { get; set; }
        [Column("desc")]
        public string Desc { get; set; }
        [Required]
        [Column("add1")]
        [StringLength(250)]
        public string Add1 { get; set; }
        [Column("add2")]
        [StringLength(250)]
        public string Add2 { get; set; }
        [Column("add3")]
        [StringLength(250)]
        public string Add3 { get; set; }
        [Column("city")]
        [StringLength(50)]
        public string City { get; set; }
        [Column("thana")]
        public int? Thana { get; set; }
        [Column("contact_no")]
        [StringLength(15)]
        public string ContactNo { get; set; }
        [Column("email")]
        [StringLength(30)]
        public string Email { get; set; }
        [Column("website")]
        [StringLength(30)]
        public string Website { get; set; }
        [Required]
        [Column("activity_code")]
        [StringLength(11)]
        public string ActivityCode { get; set; }
    }
}
