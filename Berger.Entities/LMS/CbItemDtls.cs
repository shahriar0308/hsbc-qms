﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("cb_item_dtls")]
    public partial class CbItemDtls
    {
        [Column("cb_details_id")]
        public long CbDetailsId { get; set; }
        [Column("cb_hdr_id")]
        public long? CbHdrId { get; set; }
        [Column("cb_item_code")]
        [StringLength(50)]
        public string CbItemCode { get; set; }
        [Column("cb_item_base")]
        [StringLength(50)]
        public string CbItemBase { get; set; }
        [Column("cb_item_price", TypeName = "decimal(18, 2)")]
        public decimal? CbItemPrice { get; set; }
        [Column("cb_fg_item_code")]
        [StringLength(50)]
        public string CbFgItemCode { get; set; }
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
