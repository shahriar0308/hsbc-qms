﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_sampling_dtl")]
    public partial class LeadSamplingDtl
    {
        [Key]
        [Column("lsd_lead_detail_guid")]
        [StringLength(50)]
        public string LsdLeadDetailGuid { get; set; }
        [Required]
        [Column("lsd_lead_detail_id")]
        [StringLength(50)]
        public string LsdLeadDetailId { get; set; }
        [Key]
        [Column("lsd_header_id")]
        public long LsdHeaderId { get; set; }
        [Key]
        [Column("lsd_detail_id")]
        public long LsdDetailId { get; set; }
        [Key]
        [Column("lsd_item_id")]
        [StringLength(50)]
        public string LsdItemId { get; set; }
        [Column("lsd_item_quantity")]
        public int LsdItemQuantity { get; set; }
        [Column("lsd_remarks")]
        [StringLength(100)]
        public string LsdRemarks { get; set; }
        [Column("lsd_result")]
        [StringLength(50)]
        public string LsdResult { get; set; }
        [Column("lsd_item_value", TypeName = "decimal(18, 2)")]
        public decimal? LsdItemValue { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("lsd_item_group_code")]
        [StringLength(50)]
        public string LsdItemGroupCode { get; set; }
        [Column("lsd_shade_type")]
        [StringLength(50)]
        public string LsdShadeType { get; set; }
        [Column("lsd_shade_code")]
        [StringLength(50)]
        public string LsdShadeCode { get; set; }
        [Column("lsd_shade_name")]
        [StringLength(100)]
        public string LsdShadeName { get; set; }
        [Column("lsd_item_name")]
        [StringLength(100)]
        public string LsdItemName { get; set; }
        [Column("lsd_pack_size", TypeName = "decimal(18, 2)")]
        public decimal? LsdPackSize { get; set; }
    }
}
