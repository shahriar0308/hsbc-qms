﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_quotation_dtl")]
    public partial class LeadQuotationDtl
    {
        [Key]
        [Column("lqd_details_id")]
        public long LqdDetailsId { get; set; }
        [Column("lqd_header_id")]
        public long? LqdHeaderId { get; set; }
        [Column("lqd_location_code")]
        [StringLength(50)]
        public string LqdLocationCode { get; set; }
        [Column("lqd_surface_code")]
        [StringLength(50)]
        public string LqdSurfaceCode { get; set; }
        [Column("lqd_brand")]
        [StringLength(50)]
        public string LqdBrand { get; set; }
        [Column("lqd_base_shade")]
        [StringLength(50)]
        public string LqdBaseShade { get; set; }
        [Column("lqd_shade_code")]
        [StringLength(50)]
        public string LqdShadeCode { get; set; }
        [Column("lqd_fg_shade_code")]
        [StringLength(50)]
        public string LqdFgShadeCode { get; set; }
        [Column("lqd_product_id")]
        [StringLength(50)]
        public string LqdProductId { get; set; }
        [Column("lqd_area", TypeName = "decimal(18, 3)")]
        public decimal? LqdArea { get; set; }
        [Column("lqd_price", TypeName = "decimal(18, 2)")]
        public decimal? LqdPrice { get; set; }
        [Column("lqd_total_cost", TypeName = "decimal(18, 2)")]
        public decimal? LqdTotalCost { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("lqd_item_id")]
        public string LqdItemId { get; set; }


        //------------------------------------------
        [ForeignKey("LqdHeaderId")]
        public LeadQuotationHdr LeadQuotationHdr { get; set; }
    }
}
