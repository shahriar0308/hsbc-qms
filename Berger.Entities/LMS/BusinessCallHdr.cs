﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("business_call_hdr")]
    public partial class BusinessCallHdr
    {
        [Key]
        [Column("bch_id")]
        public long BchId { get; set; }
        [Column("bch_type_id")]
        [StringLength(50)]
        public string BchTypeId { get; set; }
        [Column("bch_dealer_code")]
        [StringLength(50)]
        public string BchDealerCode { get; set; }
        [Column("bch_date", TypeName = "datetime")]
        public DateTime BchDate { get; set; }
        [Required]
        [Column("bch_user")]
        [StringLength(50)]
        public string BchUser { get; set; }
        [Column("bch_check_in_time", TypeName = "datetime")]
        public DateTime? BchCheckInTime { get; set; }
        [Column("bch_check_out_time", TypeName = "datetime")]
        public DateTime? BchCheckOutTime { get; set; }
        [Column("bch_spent_minutes")]
        public int? BchSpentMinutes { get; set; }
        [Column("bch_check_in_lat")]
        [StringLength(50)]
        public string BchCheckInLat { get; set; }
        [Column("bch_check_in_long")]
        [StringLength(50)]
        public string BchCheckInLong { get; set; }
        [Column("bch_check_in_accuracy")]
        [StringLength(50)]
        public string BchCheckInAccuracy { get; set; }
        [Column("bch_check_out_lat")]
        [StringLength(50)]
        public string BchCheckOutLat { get; set; }
        [Column("bch_check_out_long")]
        [StringLength(50)]
        public string BchCheckOutLong { get; set; }
        [Column("bch_check_out_accuracy")]
        [StringLength(50)]
        public string BchCheckOutAccuracy { get; set; }
        [Column("bch_mode")]
        [StringLength(50)]
        public string BchMode { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
