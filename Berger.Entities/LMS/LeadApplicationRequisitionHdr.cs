﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_application_requisition_hdr")]
    public partial class LeadApplicationRequisitionHdr
    {
        [Key]
        [Column("larh_id")]
        public long LarhId { get; set; }
        [Required]
        [Column("larh_lead_detail_guid")]
        [StringLength(50)]
        public string LarhLeadDetailGuid { get; set; }
        [Required]
        [Column("larh_lead_detail_id")]
        [StringLength(50)]
        public string LarhLeadDetailId { get; set; }
        [Required]
        [Column("larh_paintner_id")]
        [StringLength(50)]
        public string LarhPaintnerId { get; set; }
        [Column("larh_quotation_hdr_id")]
        public long? LarhQuotationHdrId { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
