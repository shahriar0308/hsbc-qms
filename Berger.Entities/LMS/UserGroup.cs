﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("user_group")]
    public partial class UserGroup
    {
        [Column("grp_user_legal_entity")]
        public int? GrpUserLegalEntity { get; set; }
        [Column("grp_user_division")]
        [StringLength(50)]
        public string GrpUserDivision { get; set; }
        [Key]
        [Column("grp_user_group_id")]
        public int GrpUserGroupId { get; set; }
        [Required]
        [Column("grp_user_group_code")]
        [StringLength(15)]
        public string GrpUserGroupCode { get; set; }
        [Required]
        [Column("grp_user_group_desc")]
        [StringLength(30)]
        public string GrpUserGroupDesc { get; set; }
        [Required]
        [Column("grp_user_group_type")]
        [StringLength(1)]
        public string GrpUserGroupType { get; set; }
        [Column("grp_approv_level")]
        public int? GrpApprovLevel { get; set; }
        [Column("grp_userid_flag")]
        [StringLength(1)]
        public string GrpUseridFlag { get; set; }
        [Column("grp_company_flag")]
        [StringLength(1)]
        public string GrpCompanyFlag { get; set; }
        [Column("grp_branch_flag")]
        [StringLength(1)]
        public string GrpBranchFlag { get; set; }
        [Column("grp_dept_flag")]
        [StringLength(1)]
        public string GrpDeptFlag { get; set; }
        [Column("grp_gps_tracking_yn")]
        [StringLength(1)]
        public string GrpGpsTrackingYn { get; set; }
        [Column("lead_assign_access")]
        [StringLength(1)]
        public string LeadAssignAccess { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
