﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("user_forms_access")]
    public partial class UserFormsAccess
    {
        [Key]
        [Column("user_group_code")]
        public int UserGroupCode { get; set; }
        [Key]
        [Column("user_id")]
        [StringLength(20)]
        public string UserId { get; set; }
        [Key]
        [Column("form_code")]
        [StringLength(15)]
        public string FormCode { get; set; }
        [Column("form_access_type")]
        [StringLength(50)]
        public string FormAccessType { get; set; }
        [Column("quick_link")]
        [StringLength(1)]
        public string QuickLink { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("user_division_code")]
        [StringLength(50)]
        public string UserDivisionCode { get; set; }
    }
}
