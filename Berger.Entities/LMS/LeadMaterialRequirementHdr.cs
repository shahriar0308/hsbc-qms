﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_material_requirement_hdr")]
    public partial class LeadMaterialRequirementHdr
    {
        [Key]
        [Column("lqid_details_line_id")]
        public long LqidDetailsLineId { get; set; }
        [Required]
        [Column("lqid_lead_detail_guid")]
        [StringLength(50)]
        public string LqidLeadDetailGuid { get; set; }
        [Required]
        [Column("lqid_lead_detail_id")]
        [StringLength(50)]
        public string LqidLeadDetailId { get; set; }
        [Column("lqid_quoutation_hdr_id")]
        public long? LqidQuoutationHdrId { get; set; }
        [Column("lqid_details_id")]
        public long? LqidDetailsId { get; set; }
        [Column("lqid_formula_id")]
        public long? LqidFormulaId { get; set; }
        [Column("lqid_recipe_id")]
        public long? LqidRecipeId { get; set; }
        [Column("lqid_item_id")]
        [StringLength(50)]
        public string LqidItemId { get; set; }
        [Column("lqid_item_type_code")]
        [StringLength(50)]
        public string LqidItemTypeCode { get; set; }
        [Column("lqid_item_group_code")]
        [StringLength(50)]
        public string LqidItemGroupCode { get; set; }
        [Column("lqid_fg_shade_code")]
        [StringLength(50)]
        public string LqidFgShadeCode { get; set; }
        [Column("lqid_item_sqft", TypeName = "decimal(18, 2)")]
        public decimal? LqidItemSqft { get; set; }
        [Column("lqid_item_qty", TypeName = "decimal(18, 2)")]
        public decimal? LqidItemQty { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("lqid_base")]
        [StringLength(50)]
        public string LqidBase { get; set; }
    }
}
