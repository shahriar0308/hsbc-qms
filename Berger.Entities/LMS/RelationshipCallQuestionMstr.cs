﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("relationship_call_question_mstr")]
    public partial class RelationshipCallQuestionMstr
    {
        [Key]
        [Column("rcqm_qid")]
        public long RcqmQid { get; set; }
        [Required]
        [Column("rcqm_description")]
        public string RcqmDescription { get; set; }
        [Required]
        [Column("rcqm_type")]
        [StringLength(100)]
        public string RcqmType { get; set; }
        [Column("rcqm_sequence")]
        public int RcqmSequence { get; set; }
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
