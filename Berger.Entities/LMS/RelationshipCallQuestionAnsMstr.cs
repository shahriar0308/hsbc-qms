﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("relationship_call_question_ans_mstr")]
    public partial class RelationshipCallQuestionAnsMstr
    {
        [Key]
        [Column("rcqam_id")]
        public long RcqamId { get; set; }
        [Column("rcqam_question_id")]
        public long RcqamQuestionId { get; set; }
        [Required]
        [Column("rcqam_ans_desc")]
        public string RcqamAnsDesc { get; set; }
        [Column("rcqam_ans_sec")]
        public int RcqamAnsSec { get; set; }
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
