﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("app_version")]
    public partial class AppVersion
    {
        [Key]
        [Column("vr_code")]
        public int VrCode { get; set; }
        [Required]
        [Column("vr_number")]
        [StringLength(10)]
        public string VrNumber { get; set; }
        [Column("vr_msg")]
        [StringLength(500)]
        public string VrMsg { get; set; }
        [Column("vr_obsolete_yn")]
        [StringLength(1)]
        public string VrObsoleteYn { get; set; }
        [Column("vr_bita_vr_yn")]
        [StringLength(1)]
        public string VrBitaVrYn { get; set; }
        [Column("vr_release_date", TypeName = "date")]
        public DateTime? VrReleaseDate { get; set; }
    }
}
