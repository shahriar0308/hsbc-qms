﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lov_details")]
    public partial class LovDetails
    {
        [Key]
        [Column("lov_type")]
        [StringLength(50)]
        public string LovType { get; set; }
        [Key]
        [Column("lov_code")]
        [StringLength(200)]
        public string LovCode { get; set; }
        [Required]
        [Column("lov_value")]
        [StringLength(500)]
        public string LovValue { get; set; }
        [Column("lov_shrt_desc")]
        [StringLength(500)]
        public string LovShrtDesc { get; set; }
        [Column("lov_value_seq", TypeName = "numeric(5, 0)")]
        public decimal? LovValueSeq { get; set; }
        [Column("lov_field1_value")]
        [StringLength(200)]
        public string LovField1Value { get; set; }
        [Column("lov_field2_value")]
        [StringLength(50)]
        public string LovField2Value { get; set; }
        [Column("lov_field3_value")]
        [StringLength(500)]
        public string LovField3Value { get; set; }
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
