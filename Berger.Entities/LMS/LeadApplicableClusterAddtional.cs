﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_applicable_cluster_addtional")]
    public partial class LeadApplicableClusterAddtional
    {
        [Column("laca_id")]
        public long LacaId { get; set; }
        [Key]
        [Column("laca_lead_detail_guid")]
        [StringLength(50)]
        public string LacaLeadDetailGuid { get; set; }
        [Key]
        [Column("laca_lead_detail_id")]
        [StringLength(50)]
        public string LacaLeadDetailId { get; set; }
        [Key]
        [Column("laca_division")]
        [StringLength(50)]
        public string LacaDivision { get; set; }
        [Key]
        [Column("laca_depot_code")]
        [StringLength(50)]
        public string LacaDepotCode { get; set; }
        [Key]
        [Column("laca_cl_code")]
        [StringLength(50)]
        public string LacaClCode { get; set; }
        [Key]
        [Column("laca_group_id")]
        public int LacaGroupId { get; set; }
        [Key]
        [Column("laca_user_id")]
        [StringLength(50)]
        public string LacaUserId { get; set; }
        [Column("laca_activated_date", TypeName = "datetime")]
        public DateTime? LacaActivatedDate { get; set; }
        [Column("laca_deactivated_date", TypeName = "datetime")]
        public DateTime? LacaDeactivatedDate { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
