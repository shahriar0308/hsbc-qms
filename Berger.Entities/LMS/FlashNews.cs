﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("flash_news")]
    public partial class FlashNews
    {
        [Key]
        [Column("flash_srlno")]
        public int FlashSrlno { get; set; }
        [Column("ld_division")]
        [StringLength(50)]
        public string LdDivision { get; set; }
        [Required]
        [Column("flash_userid")]
        [StringLength(20)]
        public string FlashUserid { get; set; }
        [Column("flash_msg")]
        [StringLength(200)]
        public string FlashMsg { get; set; }
        [Column("flash_to", TypeName = "datetime")]
        public DateTime FlashTo { get; set; }
        [Column("flash_retain_till", TypeName = "datetime")]
        public DateTime FlashRetainTill { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
