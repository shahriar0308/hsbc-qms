﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_estimate_dtl")]
    public partial class LeadEstimateDtl
    {
        [Key]
        [Column("led_details_id")]
        public long LedDetailsId { get; set; }
        [Column("led_header_id")]
        public long? LedHeaderId { get; set; }
        [Column("led_location_code")]
        [StringLength(50)]
        public string LedLocationCode { get; set; }
        [Column("led_surface_code")]
        [StringLength(50)]
        public string LedSurfaceCode { get; set; }
        [Column("led_product_id")]
        [StringLength(50)]
        public string LedProductId { get; set; }
        [Column("led_area", TypeName = "decimal(18, 3)")]
        public decimal? LedArea { get; set; }
        [Column("led_price", TypeName = "decimal(18, 2)")]
        public decimal? LedPrice { get; set; }
        [Column("led_total_cost", TypeName = "decimal(18, 2)")]
        public decimal? LedTotalCost { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
