﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("item_recipe_dtl")]
    public partial class ItemRecipeDtl
    {
        [Key]
        [Column("item_recipe_line_id")]
        public long ItemRecipeLineId { get; set; }
        [Column("item_recipe_id")]
        public long ItemRecipeId { get; set; }
        [Column("item_type_code")]
        [StringLength(50)]
        public string ItemTypeCode { get; set; }
        [Required]
        [Column("item_brand_code")]
        [StringLength(50)]
        public string ItemBrandCode { get; set; }
        [Required]
        [Column("item_group_code")]
        [StringLength(50)]
        public string ItemGroupCode { get; set; }
        [Required]
        [Column("item_id")]
        [StringLength(50)]
        public string ItemId { get; set; }
        [Column("item_pack_size", TypeName = "decimal(18, 2)")]
        public decimal ItemPackSize { get; set; }
        [Column("item_mrp", TypeName = "decimal(18, 2)")]
        public decimal ItemMrp { get; set; }
        [Column("item_discount_percentage", TypeName = "decimal(18, 2)")]
        public decimal ItemDiscountPercentage { get; set; }
        [Column("item_net_price", TypeName = "decimal(18, 2)")]
        public decimal ItemNetPrice { get; set; }
        [Column("item_coverage_per_uom", TypeName = "decimal(18, 2)")]
        public decimal ItemCoveragePerUom { get; set; }
        [Column("item_coverage_per_pack", TypeName = "decimal(18, 2)")]
        public decimal ItemCoveragePerPack { get; set; }
        [Column("item_cost_per_sqft", TypeName = "decimal(18, 2)")]
        public decimal ItemCostPerSqft { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
