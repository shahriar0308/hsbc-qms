﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_payment_receipt_history")]
    public partial class LeadPaymentReceiptHistory
    {
        [Key]
        [Column("lprh_id")]
        public long LprhId { get; set; }
        [Column("lprh_lpr_id")]
        public long LprhLprId { get; set; }
        [Required]
        [Column("lprh_status")]
        [StringLength(50)]
        public string LprhStatus { get; set; }
        [Column("lprh_status_date", TypeName = "datetime")]
        public DateTime? LprhStatusDate { get; set; }
        [Column("lprh_remarks")]
        [StringLength(100)]
        public string LprhRemarks { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
