﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Entities.LMS
{
    public class lead_mstr_dtl1
    {
        public string ld_lead_guid { get; set; }

        public string ld_lead_id { get; set; }

        public string ld_lead_detail_guid { get; set; }

        public string ld_lead_detail_id { get; set; }

        public string ld_division { get; set; }

        public string ld_description { get; set; }

        public string ld_cl_code { get; set; }

        public string ld_job_type { get; set; }

        public DateTime? ld_date { get; set; }

        public string ld_priority { get; set; }

        public string ld_stage { get; set; }

        public decimal? ld_job_value { get; set; }

        public decimal? ld_material_value { get; set; }

        public decimal? ld_labour_value { get; set; }

        public decimal? ld_job_value_actual { get; set; }

        public decimal? ld_material_value_actual { get; set; }

        public decimal? ld_labour_value_actual { get; set; }

        public decimal? ld_remittance { get; set; }

        public DateTime? ld_deal_closure_date { get; set; }

        public DateTime? ld_job_start_date_planned { get; set; }

        public DateTime? ld_won_date { get; set; }

        public DateTime? ld_job_start_date { get; set; }

        public DateTime? ld_job_end_date { get; set; }

        public string ld_supply_mode { get; set; }

        public decimal? ld_discount_percentage { get; set; }

        public DateTime? ld_completed_date { get; set; }

        public DateTime? ld_repainting_date { get; set; }

        public DateTime? ld_closed_date { get; set; }

        public string ld_csat { get; set; }

        public DateTime? ld_job_lost_date { get; set; }

        public string ld_job_lost_reason { get; set; }

        public string ld_job_lost_competitor { get; set; }

        public string ld_job_lost_key_influencer { get; set; }

        public string ld_job_remarks { get; set; }

        public string ld_attribute1 { get; set; }

        public string ld_attribute2 { get; set; }

        public string ld_attribute3 { get; set; }

        public string ld_attribute4 { get; set; }

        public string ld_attribute5 { get; set; }

        public int? ld_attribute6 { get; set; }

        public int? ld_attribute7 { get; set; }

        public decimal? ld_attribute8 { get; set; }

        public decimal? ld_attribute9 { get; set; }

        public decimal? ld_attribute10 { get; set; }

        public DateTime? ld_attribute11 { get; set; }

        public DateTime? ld_attribute12 { get; set; }

        public DateTime? ld_attribute13 { get; set; }

        public DateTime? ld_attribute14 { get; set; }

        public DateTime? ld_attribute15 { get; set; }

        public string created_user { get; set; }

        public DateTime created_date { get; set; }

        public string modified_user { get; set; }

        public DateTime? modified_date { get; set; }

        public string deleted_user { get; set; }

        public DateTime? deleted_date { get; set; }

        public string active { get; set; }

    }
}
