﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_mstr_dtl")]
    public partial class LeadMstrDtl
    {
        [Required]
        [Column("ld_lead_guid")]
        [StringLength(50)]
        public string LdLeadGuid { get; set; }
        [Required]
        [Column("ld_lead_id")]
        [StringLength(50)]
        public string LdLeadId { get; set; }
        [Key]
        [Column("ld_lead_detail_guid")]
        [StringLength(50)]
        public string LdLeadDetailGuid { get; set; }
        [Required]
        [Column("ld_lead_detail_id")]
        [StringLength(50)]
        public string LdLeadDetailId { get; set; }
        [Required]
        [Column("ld_division")]
        [StringLength(50)]
        public string LdDivision { get; set; }
        [Required]
        [Column("ld_description")]
        public string LdDescription { get; set; }
        [Column("ld_cl_code")]
        [StringLength(50)]
        public string LdClCode { get; set; }
        [Column("ld_job_type")]
        [StringLength(100)]
        public string LdJobType { get; set; }
        [Column("ld_date", TypeName = "datetime")]
        public DateTime? LdDate { get; set; }
        [Column("ld_priority")]
        [StringLength(100)]
        public string LdPriority { get; set; }
        [Required]
        [Column("ld_stage")]
        [StringLength(100)]
        public string LdStage { get; set; }
        [Column("ld_job_value", TypeName = "decimal(18, 2)")]
        public decimal? LdJobValue { get; set; }
        [Column("ld_material_value", TypeName = "decimal(18, 2)")]
        public decimal? LdMaterialValue { get; set; }
        [Column("ld_labour_value", TypeName = "decimal(18, 2)")]
        public decimal? LdLabourValue { get; set; }
        [Column("ld_job_value_actual", TypeName = "decimal(18, 2)")]
        public decimal? LdJobValueActual { get; set; }
        [Column("ld_material_value_actual", TypeName = "decimal(18, 2)")]
        public decimal? LdMaterialValueActual { get; set; }
        [Column("ld_labour_value_actual", TypeName = "decimal(18, 2)")]
        public decimal? LdLabourValueActual { get; set; }
        [Column("ld_remittance", TypeName = "decimal(18, 2)")]
        public decimal? LdRemittance { get; set; }
        [Column("ld_deal_closure_date", TypeName = "datetime")]
        public DateTime? LdDealClosureDate { get; set; }
        [Column("ld_job_start_date_planned", TypeName = "datetime")]
        public DateTime? LdJobStartDatePlanned { get; set; }
        [Column("ld_won_date", TypeName = "datetime")]
        public DateTime? LdWonDate { get; set; }
        [Column("ld_job_start_date", TypeName = "datetime")]
        public DateTime? LdJobStartDate { get; set; }
        [Column("ld_job_end_date", TypeName = "datetime")]
        public DateTime? LdJobEndDate { get; set; }
        [Column("ld_supply_mode")]
        [StringLength(50)]
        public string LdSupplyMode { get; set; }
        [Column("ld_discount_percentage", TypeName = "decimal(18, 2)")]
        public decimal? LdDiscountPercentage { get; set; }
        [Column("ld_completed_date", TypeName = "datetime")]
        public DateTime? LdCompletedDate { get; set; }
        [Column("ld_repainting_date", TypeName = "datetime")]
        public DateTime? LdRepaintingDate { get; set; }
        [Column("ld_closed_date", TypeName = "datetime")]
        public DateTime? LdClosedDate { get; set; }
        [Column("ld_csat")]
        [StringLength(50)]
        public string LdCsat { get; set; }
        [Column("ld_job_lost_date", TypeName = "datetime")]
        public DateTime? LdJobLostDate { get; set; }
        [Column("ld_job_lost_reason")]
        [StringLength(50)]
        public string LdJobLostReason { get; set; }
        [Column("ld_job_lost_competitor")]
        [StringLength(50)]
        public string LdJobLostCompetitor { get; set; }
        [Column("ld_job_lost_key_influencer")]
        [StringLength(50)]
        public string LdJobLostKeyInfluencer { get; set; }
        [Column("ld_job_remarks")]
        public string LdJobRemarks { get; set; }
        [Column("ld_attribute1")]
        [StringLength(50)]
        public string LdAttribute1 { get; set; }
        [Column("ld_attribute2")]
        [StringLength(50)]
        public string LdAttribute2 { get; set; }
        [Column("ld_attribute3")]
        [StringLength(50)]
        public string LdAttribute3 { get; set; }
        [Column("ld_attribute4")]
        [StringLength(50)]
        public string LdAttribute4 { get; set; }
        [Column("ld_attribute5")]
        [StringLength(50)]
        public string LdAttribute5 { get; set; }
        [Column("ld_attribute6")]
        public int? LdAttribute6 { get; set; }
        [Column("ld_attribute7")]
        public int? LdAttribute7 { get; set; }
        [Column("ld_attribute8", TypeName = "decimal(18, 0)")]
        public decimal? LdAttribute8 { get; set; }
        [Column("ld_attribute9", TypeName = "decimal(18, 0)")]
        public decimal? LdAttribute9 { get; set; }
        [Column("ld_attribute10", TypeName = "decimal(18, 0)")]
        public decimal? LdAttribute10 { get; set; }
        [Column("ld_attribute11", TypeName = "datetime")]
        public DateTime? LdAttribute11 { get; set; }
        [Column("ld_attribute12", TypeName = "datetime")]
        public DateTime? LdAttribute12 { get; set; }
        [Column("ld_attribute13", TypeName = "datetime")]
        public DateTime? LdAttribute13 { get; set; }
        [Column("ld_attribute14", TypeName = "datetime")]
        public DateTime? LdAttribute14 { get; set; }
        [Column("ld_attribute15", TypeName = "datetime")]
        public DateTime? LdAttribute15 { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }


        //------------------------------------------
        [ForeignKey("LdLeadGuid")]
        public LeadMstrHdr LeadMstrHdr { get; set; }
        public IList<LeadQuotationHdr> LeadQuotationHdrs { get; set; }
        public LeadMstrDtl()
        {
            this.LeadQuotationHdrs = new List<LeadQuotationHdr>();
        }
    }
}
