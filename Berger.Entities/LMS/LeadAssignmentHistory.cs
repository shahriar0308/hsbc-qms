﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_assignment_history")]
    public partial class LeadAssignmentHistory
    {
        [Key]
        [Column("lah_lead_detail_guid")]
        [StringLength(50)]
        public string LahLeadDetailGuid { get; set; }
        [Required]
        [Column("lah_lead_detail_id")]
        [StringLength(50)]
        public string LahLeadDetailId { get; set; }
        [Key]
        [Column("lah_cl_code")]
        [StringLength(50)]
        public string LahClCode { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
