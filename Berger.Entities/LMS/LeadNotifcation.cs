﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_notifcation")]
    public partial class LeadNotifcation
    {
        [Key]
        [Column("ln_id")]
        public long LnId { get; set; }
        [Key]
        [Column("ln_lead_details_guid")]
        [StringLength(50)]
        public string LnLeadDetailsGuid { get; set; }
        [Key]
        [Column("ln_lead_details_id")]
        [StringLength(50)]
        public string LnLeadDetailsId { get; set; }
        [Column("ln_type")]
        [StringLength(50)]
        public string LnType { get; set; }
        [Column("ln_desc")]
        [StringLength(500)]
        public string LnDesc { get; set; }
        [Column("ln_pushed")]
        [StringLength(1)]
        public string LnPushed { get; set; }
        [Column("ln_valid_date_fr", TypeName = "datetime")]
        public DateTime? LnValidDateFr { get; set; }
        [Column("ln_valid_date_to", TypeName = "datetime")]
        public DateTime? LnValidDateTo { get; set; }
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
