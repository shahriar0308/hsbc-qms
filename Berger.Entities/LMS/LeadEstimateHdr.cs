﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_estimate_hdr")]
    public partial class LeadEstimateHdr
    {
        [Key]
        [Column("leh_id")]
        public long LehId { get; set; }
        [Required]
        [Column("leh_lead_detail_guid")]
        [StringLength(50)]
        public string LehLeadDetailGuid { get; set; }
        [Required]
        [Column("leh_lead_detail_id")]
        [StringLength(50)]
        public string LehLeadDetailId { get; set; }
        [Required]
        [Column("leh_package_id")]
        [StringLength(50)]
        public string LehPackageId { get; set; }
        [Column("leh_total_item_amount", TypeName = "decimal(18, 2)")]
        public decimal? LehTotalItemAmount { get; set; }
        [Column("leh_vat_per", TypeName = "decimal(18, 2)")]
        public decimal? LehVatPer { get; set; }
        [Column("leh_vat_amount", TypeName = "decimal(18, 2)")]
        public decimal? LehVatAmount { get; set; }
        [Column("leh_net_amount", TypeName = "decimal(18, 2)")]
        public decimal? LehNetAmount { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("leh_surface_id")]
        [StringLength(50)]
        public string LehSurfaceId { get; set; }
        [Column("leh_attribute_id")]
        [StringLength(50)]
        public string LehAttributeId { get; set; }
    }
}
