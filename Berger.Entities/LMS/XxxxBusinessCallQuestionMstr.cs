﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("XXXX-business_call_question_mstr")]
    public partial class XxxxBusinessCallQuestionMstr
    {
        [Key]
        [Column("bcqm_qid")]
        public long BcqmQid { get; set; }
        [Required]
        [Column("bcqm_description")]
        public string BcqmDescription { get; set; }
        [Required]
        [Column("bcqm_type")]
        [StringLength(100)]
        public string BcqmType { get; set; }
        [Column("bcqm_sequence")]
        public int BcqmSequence { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
