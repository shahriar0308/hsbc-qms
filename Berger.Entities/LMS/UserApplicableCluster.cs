﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("user_applicable_cluster")]
    public partial class UserApplicableCluster
    {
        [Required]
        [Column("uac_user_id")]
        [StringLength(20)]
        public string UacUserId { get; set; }
        [Key]
        [Column("uac_cl_code")]
        [StringLength(20)]
        public string UacClCode { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Key]
        [Column("uac_applicable_division_code")]
        [StringLength(50)]
        public string UacApplicableDivisionCode { get; set; }
        [Key]
        [Column("uac_applicable_depot_code")]
        [StringLength(50)]
        public string UacApplicableDepotCode { get; set; }
        [Key]
        [Column("uac_user_group_id")]
        public int UacUserGroupId { get; set; }
    }
}
