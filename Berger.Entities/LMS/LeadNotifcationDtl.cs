﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_notifcation_dtl")]
    public partial class LeadNotifcationDtl
    {
        [Key]
        [Column("lnd_id")]
        public long LndId { get; set; }
        [Column("lnh_id")]
        public long? LnhId { get; set; }
        [Key]
        [Column("lnd_lead_details_guid")]
        [StringLength(50)]
        public string LndLeadDetailsGuid { get; set; }
        [Key]
        [Column("lnd_lead_details_id")]
        [StringLength(50)]
        public string LndLeadDetailsId { get; set; }
        [Column("lnd_division")]
        [StringLength(50)]
        public string LndDivision { get; set; }
        [Column("lnd_cluster")]
        [StringLength(50)]
        public string LndCluster { get; set; }
        [Column("lnd_user_groupid")]
        public long? LndUserGroupid { get; set; }
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
