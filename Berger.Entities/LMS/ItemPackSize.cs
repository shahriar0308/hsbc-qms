﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("item_pack_size")]
    public partial class ItemPackSize
    {
        [Column("item_hdr_id")]
        public long ItemHdrId { get; set; }
        [Key]
        [Column("item_id")]
        [StringLength(50)]
        public string ItemId { get; set; }
        [Column("item_pack_size", TypeName = "decimal(18, 2)")]
        public decimal? ItemPackSize1 { get; set; }
        [Column("item_group_code")]
        [StringLength(50)]
        public string ItemGroupCode { get; set; }
        [Column("item_shade_code")]
        [StringLength(50)]
        public string ItemShadeCode { get; set; }
        [Column("item_shade_name")]
        [StringLength(50)]
        public string ItemShadeName { get; set; }
        [Column("item_coverage_per_uom", TypeName = "decimal(18, 2)")]
        public decimal? ItemCoveragePerUom { get; set; }
        [Column("item_discount_percentage", TypeName = "decimal(18, 2)")]
        public decimal? ItemDiscountPercentage { get; set; }
        [Column("item_hd_indicator")]
        [StringLength(50)]
        public string ItemHdIndicator { get; set; }
        [Column("item_category")]
        [StringLength(50)]
        public string ItemCategory { get; set; }
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
