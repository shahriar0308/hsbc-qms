﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_invoice_link")]
    public partial class LeadInvoiceLink
    {
        [Column("lil_id")]
        public long LilId { get; set; }
        [Required]
        [Column("lil_lead_detail_guid")]
        [StringLength(50)]
        public string LilLeadDetailGuid { get; set; }
        [Required]
        [Column("lil_lead_detail_id")]
        [StringLength(50)]
        public string LilLeadDetailId { get; set; }
        [Key]
        [Column("lil_trx_id")]
        [StringLength(50)]
        public string LilTrxId { get; set; }
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
