﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_material_supplier_bill_hdr")]
    public partial class LeadMaterialSupplierBillHdr
    {
        [Key]
        [Column("lmsbh_bill_id")]
        public long LmsbhBillId { get; set; }
        [Required]
        [Column("lmsbh_lead_detail_guid")]
        [StringLength(50)]
        public string LmsbhLeadDetailGuid { get; set; }
        [Required]
        [Column("lmsbh_lead_detail_id")]
        [StringLength(50)]
        public string LmsbhLeadDetailId { get; set; }
        [Required]
        [Column("lmsbh_dlr_id")]
        [StringLength(50)]
        public string LmsbhDlrId { get; set; }
        [Required]
        [Column("lmsbh_invoice_no")]
        [StringLength(50)]
        public string LmsbhInvoiceNo { get; set; }
        [Column("lmshb_invoice_date", TypeName = "datetime")]
        public DateTime? LmshbInvoiceDate { get; set; }
        [Column("imshb_invoice_amount", TypeName = "decimal(18, 2)")]
        public decimal? ImshbInvoiceAmount { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("lmsbh_requisition_id")]
        public long? LmsbhRequisitionId { get; set; }
        [Column("lmsbh_doc_path")]
        [StringLength(200)]
        public string LmsbhDocPath { get; set; }
        [Column("lmsbh_doc_extension")]
        [StringLength(50)]
        public string LmsbhDocExtension { get; set; }
        [Column("lmsbh_doc_guid")]
        [StringLength(200)]
        public string LmsbhDocGuid { get; set; }
    }
}
