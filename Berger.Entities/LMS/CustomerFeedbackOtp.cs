﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("customer_feedback_otp")]
    public partial class CustomerFeedbackOtp
    {
        [Key]
        [Column("cfo_id")]
        public long CfoId { get; set; }
        [Required]
        [Column("cfo_lead_guid")]
        [StringLength(50)]
        public string CfoLeadGuid { get; set; }
        [Column("cfo_contact_srl")]
        public int CfoContactSrl { get; set; }
        [Required]
        [Column("cfo_mobile_no")]
        [StringLength(10)]
        public string CfoMobileNo { get; set; }
        [Required]
        [Column("cfo_otp")]
        [StringLength(6)]
        public string CfoOtp { get; set; }
        [Column("cfo_consumed")]
        [StringLength(1)]
        public string CfoConsumed { get; set; }
        [Column("cfo_otp_valid_till", TypeName = "datetime")]
        public DateTime? CfoOtpValidTill { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("created_by")]
        [StringLength(20)]
        public string CreatedBy { get; set; }
    }
}
