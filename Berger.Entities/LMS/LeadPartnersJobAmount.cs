﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_partners_job_amount")]
    public partial class LeadPartnersJobAmount
    {
        [Key]
        [Column("lpja_id")]
        public long LpjaId { get; set; }
        [Required]
        [Column("lpja_lead_details_guid")]
        [StringLength(50)]
        public string LpjaLeadDetailsGuid { get; set; }
        [Required]
        [Column("lpja_lead_details_id")]
        [StringLength(50)]
        public string LpjaLeadDetailsId { get; set; }
        [Required]
        [Column("lpja_lead_partner_id")]
        [StringLength(50)]
        public string LpjaLeadPartnerId { get; set; }
        [Column("lpja_amount", TypeName = "decimal(18, 2)")]
        public decimal? LpjaAmount { get; set; }
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
