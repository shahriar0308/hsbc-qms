﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_docs")]
    public partial class LeadDocs
    {
        [Key]
        [Column("ld_lead_detail_guid")]
        [StringLength(50)]
        public string LdLeadDetailGuid { get; set; }
        [Required]
        [Column("ld_lead_detail_id")]
        [StringLength(50)]
        public string LdLeadDetailId { get; set; }
        [Key]
        [Column("ld_doc_srl_no")]
        public int LdDocSrlNo { get; set; }
        [Column("ld_doc_type")]
        [StringLength(50)]
        public string LdDocType { get; set; }
        [Column("ld_doc_description")]
        public string LdDocDescription { get; set; }
        [Column("ld_doc_path")]
        [StringLength(200)]
        public string LdDocPath { get; set; }
        [Column("ld_doc_extension")]
        [StringLength(50)]
        public string LdDocExtension { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
