﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("mobile_menu_mstr")]
    public partial class MobileMenuMstr
    {
        [Key]
        [Column("mm_menu_code")]
        public int MmMenuCode { get; set; }
        [Required]
        [Column("mm_menu_name")]
        [StringLength(50)]
        public string MmMenuName { get; set; }
        [Column("mm_menu_seq")]
        public int? MmMenuSeq { get; set; }
    }
}
