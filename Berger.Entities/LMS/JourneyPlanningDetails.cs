﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("journey_planning_details")]
    public partial class JourneyPlanningDetails
    {
        [Key]
        [Column("jpd_date", TypeName = "date")]
        public DateTime JpdDate { get; set; }
        [Key]
        [Column("jpd_user")]
        [StringLength(20)]
        public string JpdUser { get; set; }
        [Key]
        [Column("jpd_srl")]
        public int JpdSrl { get; set; }
        [Column("jpd_sap_div")]
        public int? JpdSapDiv { get; set; }
        [Required]
        [Column("jpd_act_id")]
        [StringLength(50)]
        public string JpdActId { get; set; }
        [Required]
        [Column("jpd_act_type")]
        [StringLength(30)]
        public string JpdActType { get; set; }
        [Column("jpd_act_time", TypeName = "datetime")]
        public DateTime? JpdActTime { get; set; }
        [Column("jpd_visited")]
        [StringLength(1)]
        public string JpdVisited { get; set; }
        [Column("jpd_visited_on", TypeName = "datetime")]
        public DateTime? JpdVisitedOn { get; set; }
        [Column("jpd_visit_id")]
        public long? JpdVisitId { get; set; }
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
