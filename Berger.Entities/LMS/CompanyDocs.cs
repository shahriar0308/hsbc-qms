﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("company_docs")]
    public partial class CompanyDocs
    {
        [Required]
        [Column("cd_cm_code")]
        [StringLength(10)]
        public string CdCmCode { get; set; }
        [Key]
        [Column("cd_doc_id")]
        public int CdDocId { get; set; }
        [Column("cd_doc_type")]
        [StringLength(50)]
        public string CdDocType { get; set; }
        [Column("cd_doc_description")]
        [StringLength(200)]
        public string CdDocDescription { get; set; }
        [Column("cd_doc_path")]
        [StringLength(200)]
        public string CdDocPath { get; set; }
        [Column("cd_doc_extension")]
        [StringLength(50)]
        public string CdDocExtension { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
