﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("app_user_log")]
    public partial class AppUserLog
    {
        [Key]
        [Column("log_id")]
        public long LogId { get; set; }
        [Required]
        [Column("log_user_id")]
        [StringLength(20)]
        public string LogUserId { get; set; }
        [Column("log_datetime", TypeName = "datetime")]
        public DateTime LogDatetime { get; set; }
        [Required]
        [Column("loc_action")]
        [StringLength(50)]
        public string LocAction { get; set; }
        [Required]
        [Column("log_msg")]
        public string LogMsg { get; set; }
        [Column("crreated_date", TypeName = "datetime")]
        public DateTime CrreatedDate { get; set; }
    }
}
