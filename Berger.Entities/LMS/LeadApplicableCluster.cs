﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_applicable_cluster")]
    public partial class LeadApplicableCluster
    {
        [Column("lac_id")]
        public long LacId { get; set; }
        [Key]
        [Column("lac_lead_detail_guid")]
        [StringLength(50)]
        public string LacLeadDetailGuid { get; set; }
        [Key]
        [Column("lac_lead_detail_id")]
        [StringLength(50)]
        public string LacLeadDetailId { get; set; }
        [Key]
        [Column("lac_cl_code")]
        [StringLength(50)]
        public string LacClCode { get; set; }
        [Key]
        [Column("lac_group_id")]
        [StringLength(50)]
        public string LacGroupId { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Key]
        [Column("lac_activated_date", TypeName = "datetime")]
        public DateTime LacActivatedDate { get; set; }
        [Column("lac_deactivated_date", TypeName = "datetime")]
        public DateTime? LacDeactivatedDate { get; set; }
        [Column("lac_user_id")]
        [StringLength(50)]
        public string LacUserId { get; set; }
    }
}
