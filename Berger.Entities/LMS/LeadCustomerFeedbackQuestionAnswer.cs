﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_customer_feedback_question_answer")]
    public partial class LeadCustomerFeedbackQuestionAnswer
    {
        [Key]
        [Column("ans_id")]
        public int AnsId { get; set; }
        [Column("ans_ques_id")]
        public int? AnsQuesId { get; set; }
        [Column("ans_desc")]
        public string AnsDesc { get; set; }
        [Column("ans_rating")]
        public int? AnsRating { get; set; }
        [Column("ans_seq")]
        public int? AnsSeq { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
