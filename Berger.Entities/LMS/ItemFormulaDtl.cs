﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("item_formula_dtl")]
    public partial class ItemFormulaDtl
    {
        [Key]
        [Column("item_formula_line_id")]
        public long ItemFormulaLineId { get; set; }
        [Column("item_formula_id")]
        public long ItemFormulaId { get; set; }
        [Column("item_recipe_id")]
        public long ItemRecipeId { get; set; }
        [Required]
        [Column("item_id")]
        [StringLength(50)]
        public string ItemId { get; set; }
        [Column("item_cost_per_sqft", TypeName = "decimal(18, 2)")]
        public decimal ItemCostPerSqft { get; set; }
        [Column("item_no_of_coat")]
        public int ItemNoOfCoat { get; set; }
        [Column("item_final_cost_per_sqft", TypeName = "decimal(18, 2)")]
        public decimal ItemFinalCostPerSqft { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
