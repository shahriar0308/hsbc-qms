﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("srl_control_mstr")]
    public partial class SrlControlMstr
    {
        [Key]
        [Column("srl_id")]
        public int SrlId { get; set; }
        [Key]
        [Column("srl_company")]
        [StringLength(50)]
        public string SrlCompany { get; set; }
        [Key]
        [Column("srl_fin_year")]
        [StringLength(4)]
        public string SrlFinYear { get; set; }
        [Key]
        [Column("srl_doc_type")]
        [StringLength(50)]
        public string SrlDocType { get; set; }
        [Column("srl_branch")]
        [StringLength(50)]
        public string SrlBranch { get; set; }
        [Column("srl_dept")]
        [StringLength(15)]
        public string SrlDept { get; set; }
        [Column("srl_loc")]
        [StringLength(15)]
        public string SrlLoc { get; set; }
        [Column("srl_prefix")]
        [StringLength(15)]
        public string SrlPrefix { get; set; }
        [Column("srl_no", TypeName = "numeric(18, 0)")]
        public decimal SrlNo { get; set; }
        [Column("srl_increment", TypeName = "numeric(18, 0)")]
        public decimal SrlIncrement { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
