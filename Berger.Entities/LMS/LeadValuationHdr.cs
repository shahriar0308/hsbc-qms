﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_valuation_hdr")]
    public partial class LeadValuationHdr
    {
        [Key]
        [Column("lvh_hdr_id")]
        public long LvhHdrId { get; set; }
        [Required]
        [Column("lvh_lead_detail_guid")]
        [StringLength(50)]
        public string LvhLeadDetailGuid { get; set; }
        [Required]
        [Column("lvh_lead_detail_id")]
        [StringLength(50)]
        public string LvhLeadDetailId { get; set; }
        [Column("lvh_date", TypeName = "date")]
        public DateTime LvhDate { get; set; }
        [Column("lvh_value", TypeName = "numeric(18, 2)")]
        public decimal LvhValue { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
