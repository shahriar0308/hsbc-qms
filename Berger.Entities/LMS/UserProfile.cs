﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("user_profile")]
    public partial class UserProfile
    {
        [Key]
        [Column("usp_user_id")]
        [StringLength(20)]
        public string UspUserId { get; set; }
        [Column("usp_pswd")]
        public string UspPswd { get; set; }
        [Required]
        [Column("usp_first_name")]
        [StringLength(50)]
        public string UspFirstName { get; set; }
        [Column("usp_last_name")]
        [StringLength(50)]
        public string UspLastName { get; set; }
        [Required]
        [Column("usp_group_code")]
        [StringLength(20)]
        public string UspGroupCode { get; set; }
        [Column("usp_reporting_group")]
        [StringLength(50)]
        public string UspReportingGroup { get; set; }
        [Column("usp_reporting_user")]
        [StringLength(50)]
        public string UspReportingUser { get; set; }
        [Column("usp_desig")]
        [StringLength(50)]
        public string UspDesig { get; set; }
        [Column("usp_mailid")]
        [StringLength(100)]
        public string UspMailid { get; set; }
        [Column("usp_mobile")]
        [StringLength(20)]
        public string UspMobile { get; set; }
        [Column("usp_old_pswd1")]
        public string UspOldPswd1 { get; set; }
        [Column("usp_old_pswd2")]
        public string UspOldPswd2 { get; set; }
        [Column("usp_old_pswd3")]
        public string UspOldPswd3 { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("usp_employee_id")]
        [StringLength(50)]
        public string UspEmployeeId { get; set; }
        [Column("usp_image_base64")]
        public string UspImageBase64 { get; set; }
        [Column("usp_legal_entityid")]
        [StringLength(20)]
        public string UspLegalEntityid { get; set; }
    }
}
