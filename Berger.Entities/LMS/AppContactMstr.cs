﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("app_contact_mstr")]
    public partial class AppContactMstr
    {
        [Key]
        [Column("contact_name")]
        [StringLength(50)]
        public string ContactName { get; set; }
        [Key]
        [Column("contact_number")]
        [StringLength(50)]
        public string ContactNumber { get; set; }
        [Column("contact_seq")]
        public int? ContactSeq { get; set; }
    }
}
