﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("user_session_current")]
    public partial class UserSessionCurrent
    {
        [Key]
        [Column("usc_unique_id")]
        public long UscUniqueId { get; set; }
        [Column("usc_user_legal_entity")]
        public int? UscUserLegalEntity { get; set; }
        [Required]
        [Column("usc_user_id")]
        [StringLength(20)]
        public string UscUserId { get; set; }
        [Required]
        [Column("usc_device_uuid")]
        [StringLength(200)]
        public string UscDeviceUuid { get; set; }
        [Required]
        [Column("usc_token")]
        public string UscToken { get; set; }
        [Required]
        [Column("usc_ip_address")]
        [StringLength(50)]
        public string UscIpAddress { get; set; }
        [Column("usc_start_time_stamp", TypeName = "datetime")]
        public DateTime UscStartTimeStamp { get; set; }
        [Column("usc_last_activity_time_stamp", TypeName = "datetime")]
        public DateTime UscLastActivityTimeStamp { get; set; }
        [Column("usc_expire_time", TypeName = "datetime")]
        public DateTime? UscExpireTime { get; set; }
        [Column("usc_expire_reason")]
        [StringLength(100)]
        public string UscExpireReason { get; set; }
    }
}
