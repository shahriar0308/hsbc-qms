﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_payment_application")]
    public partial class LeadPaymentApplication
    {
        [Key]
        [Column("lpa_id")]
        public long LpaId { get; set; }
        [Column("lpa_lpr_id")]
        public long LpaLprId { get; set; }
        [Required]
        [Column("lpa_lead_detail_guid")]
        [StringLength(50)]
        public string LpaLeadDetailGuid { get; set; }
        [Required]
        [Column("lpa_lead_detail_id")]
        [StringLength(50)]
        public string LpaLeadDetailId { get; set; }
        [Column("lpa_amount_applied", TypeName = "decimal(18, 2)")]
        public decimal? LpaAmountApplied { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
