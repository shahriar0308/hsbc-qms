﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_material_requision_dtl")]
    public partial class LeadMaterialRequisionDtl
    {
        [Key]
        [Column("lmrdt_details_id")]
        public long LmrdtDetailsId { get; set; }
        [Column("lmrdt_header_id")]
        public long LmrdtHeaderId { get; set; }
        [Column("lmrdt_quotation_hdr_id")]
        public long LmrdtQuotationHdrId { get; set; }
        [Column("lmrdt_quotation_shade_code")]
        [StringLength(50)]
        public string LmrdtQuotationShadeCode { get; set; }
        [Column("lmrdt_item_shade_code")]
        [StringLength(50)]
        public string LmrdtItemShadeCode { get; set; }
        [Column("lmrdt_item_group_code")]
        [StringLength(50)]
        public string LmrdtItemGroupCode { get; set; }
        [Column("lmrdt_item_pack_size")]
        [StringLength(50)]
        public string LmrdtItemPackSize { get; set; }
        [Column("lmrdt_item_id")]
        [StringLength(50)]
        public string LmrdtItemId { get; set; }
        [Column("lmrdt_qty")]
        public long? LmrdtQty { get; set; }
        [Column("lmrdt_volume", TypeName = "decimal(18, 2)")]
        public decimal? LmrdtVolume { get; set; }
        [Column("lmrdt_item_DSP", TypeName = "decimal(18, 2)")]
        public decimal? LmrdtItemDsp { get; set; }
        [Column("lmrdt_item_DISC", TypeName = "decimal(18, 2)")]
        public decimal? LmrdtItemDisc { get; set; }
        [Column("lmrdt_item_NSP", TypeName = "decimal(18, 2)")]
        public decimal? LmrdtItemNsp { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("lmrdt_base")]
        [StringLength(50)]
        public string LmrdtBase { get; set; }
    }
}
