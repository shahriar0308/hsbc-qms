﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("regular_shade")]
    public partial class RegularShade
    {
        [Key]
        [Column("rs_brand_code")]
        [StringLength(50)]
        public string RsBrandCode { get; set; }
        [Key]
        [Column("rs_shade_code")]
        [StringLength(50)]
        public string RsShadeCode { get; set; }
        [Column("rs_addtional_cost", TypeName = "decimal(8, 2)")]
        public decimal RsAddtionalCost { get; set; }
        [Column("rs_primary_ind")]
        [StringLength(1)]
        public string RsPrimaryInd { get; set; }
        [Column("rs_seq")]
        public int? RsSeq { get; set; }
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
