﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_application_painter_payment")]
    public partial class LeadApplicationPainterPayment
    {
        [Key]
        [Column("lapp_payment_id")]
        public long LappPaymentId { get; set; }
        [Required]
        [Column("lapp_lead_detail_guid")]
        [StringLength(50)]
        public string LappLeadDetailGuid { get; set; }
        [Required]
        [Column("lapp_lead_detail_id")]
        [StringLength(50)]
        public string LappLeadDetailId { get; set; }
        [Required]
        [Column("lapp_painter_id")]
        [StringLength(50)]
        public string LappPainterId { get; set; }
        [Column("lapp_requisition_id")]
        public long LappRequisitionId { get; set; }
        [Column("lapp_invoice_no")]
        [StringLength(50)]
        public string LappInvoiceNo { get; set; }
        [Column("lapp_payment_mode")]
        [StringLength(50)]
        public string LappPaymentMode { get; set; }
        [Column("lapp_instrument_no")]
        [StringLength(50)]
        public string LappInstrumentNo { get; set; }
        [Column("lapp_instrument_date", TypeName = "datetime")]
        public DateTime? LappInstrumentDate { get; set; }
        [Column("lapp_amount", TypeName = "decimal(18, 2)")]
        public decimal? LappAmount { get; set; }
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("lapp_uploaded_document")]
        public string LappUploadedDocument { get; set; }
        [Column("uploaded_document_path")]
        public string UploadedDocumentPath { get; set; }
    }
}
