﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_specification_hdr")]
    public partial class LeadSpecificationHdr
    {
        [Key]
        [Column("lsh_id")]
        public long LshId { get; set; }
        [Required]
        [Column("lsh_lead_detail_guid")]
        [StringLength(50)]
        public string LshLeadDetailGuid { get; set; }
        [Column("lsh_Specification")]
        public string LshSpecification { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
