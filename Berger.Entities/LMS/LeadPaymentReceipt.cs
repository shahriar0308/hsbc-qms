﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_payment_receipt")]
    public partial class LeadPaymentReceipt
    {
        [Key]
        [Column("lpr_id")]
        public long LprId { get; set; }
        [Required]
        [Column("lpr_ld_lead_guid")]
        [StringLength(50)]
        public string LprLdLeadGuid { get; set; }
        [Required]
        [Column("lpr_ld_lead_id")]
        [StringLength(50)]
        public string LprLdLeadId { get; set; }
        [Column("lpr_payment_mode")]
        [StringLength(20)]
        public string LprPaymentMode { get; set; }
        [Column("lpr_ifsc_code")]
        [StringLength(50)]
        public string LprIfscCode { get; set; }
        [Column("lpr_bank_name")]
        [StringLength(50)]
        public string LprBankName { get; set; }
        [Column("lpr_branch_name")]
        [StringLength(50)]
        public string LprBranchName { get; set; }
        [Column("lpr_account_no")]
        [StringLength(50)]
        public string LprAccountNo { get; set; }
        [Column("lpr_instrument_no")]
        [StringLength(50)]
        public string LprInstrumentNo { get; set; }
        [Column("lpr_instrument_date", TypeName = "datetime")]
        public DateTime? LprInstrumentDate { get; set; }
        [Column("lpr_amount", TypeName = "decimal(18, 2)")]
        public decimal? LprAmount { get; set; }
        [Column("lpr_status")]
        [StringLength(50)]
        public string LprStatus { get; set; }
        [Column("lpr_status_date", TypeName = "datetime")]
        public DateTime? LprStatusDate { get; set; }
        [Column("lpr_remarks")]
        [StringLength(100)]
        public string LprRemarks { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
