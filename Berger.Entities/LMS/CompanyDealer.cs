﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("company_dealer")]
    public partial class CompanyDealer
    {
        [Key]
        [Column("cdl_cm_code")]
        [StringLength(10)]
        public string CdlCmCode { get; set; }
        [Column("cdl_id")]
        public int CdlId { get; set; }
        [Key]
        [Column("cdl_dealer")]
        [StringLength(50)]
        public string CdlDealer { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
