﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("standard_parameter")]
    public partial class StandardParameter
    {
        [Key]
        [Column("parm_name")]
        [StringLength(20)]
        public string ParmName { get; set; }
        [Column("param_status")]
        [StringLength(50)]
        public string ParamStatus { get; set; }
        [Column("param_decimal_value", TypeName = "numeric(18, 3)")]
        public decimal? ParamDecimalValue { get; set; }
        [Column("param_char_value")]
        [StringLength(500)]
        public string ParamCharValue { get; set; }
        [Column("param_datetime_value", TypeName = "datetime")]
        public DateTime? ParamDatetimeValue { get; set; }
        [Column("param_time_value")]
        [StringLength(5)]
        public string ParamTimeValue { get; set; }
        [Column("param_auto_lead_me")]
        [StringLength(20)]
        public string ParamAutoLeadMe { get; set; }
        [Column("param_false_lead_me")]
        [StringLength(20)]
        public string ParamFalseLeadMe { get; set; }
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
