﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_supervision_qusetion_mstr")]
    public partial class LeadSupervisionQusetionMstr
    {
        [Column("lsq_legal_entity")]
        public int LsqLegalEntity { get; set; }
        [Key]
        [Column("lsq_qns_id")]
        public long LsqQnsId { get; set; }
        [Required]
        [Column("lsq_division")]
        [StringLength(50)]
        public string LsqDivision { get; set; }
        [Required]
        [Column("lsq_sub_division")]
        [StringLength(50)]
        public string LsqSubDivision { get; set; }
        [Column("lsq_supervision_type")]
        public int LsqSupervisionType { get; set; }
        [Column("isq_qns_desc")]
        public string IsqQnsDesc { get; set; }
        [Required]
        [Column("lsq_input_type")]
        [StringLength(20)]
        public string LsqInputType { get; set; }
        [Column("lsq_img_required_yn")]
        [StringLength(1)]
        public string LsqImgRequiredYn { get; set; }
        [Column("lsq_seq")]
        public int? LsqSeq { get; set; }
        [Column("lsq_dependent_on_qns_id")]
        public long? LsqDependentOnQnsId { get; set; }
        [Column("lsq_dependent_on_ans_id")]
        public long? LsqDependentOnAnsId { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
