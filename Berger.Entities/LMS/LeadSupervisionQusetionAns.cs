﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_supervision_qusetion_ans")]
    public partial class LeadSupervisionQusetionAns
    {
        [Key]
        [Column("lsa_ans_id")]
        public long LsaAnsId { get; set; }
        [Column("lsa_qns_id")]
        public long LsaQnsId { get; set; }
        [Required]
        [Column("lsa_ans_desc")]
        public string LsaAnsDesc { get; set; }
        [Column("lsa_seq")]
        public int? LsaSeq { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
