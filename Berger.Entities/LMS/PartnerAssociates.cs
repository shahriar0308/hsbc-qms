﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("partner_associates")]
    public partial class PartnerAssociates
    {
        [Key]
        [Column("pa_id")]
        public int PaId { get; set; }
        [Required]
        [Column("pa_pm_id")]
        [StringLength(10)]
        public string PaPmId { get; set; }
        [Required]
        [Column("pa_lvl_code")]
        [StringLength(10)]
        public string PaLvlCode { get; set; }
        [Column("pa_first_name")]
        [StringLength(100)]
        public string PaFirstName { get; set; }
        [Column("pa_middle_name")]
        [StringLength(100)]
        public string PaMiddleName { get; set; }
        [Column("pa_last_name")]
        [StringLength(100)]
        public string PaLastName { get; set; }
        [Column("pa_designation")]
        [StringLength(200)]
        public string PaDesignation { get; set; }
        [Column("pa_gender")]
        [StringLength(1)]
        public string PaGender { get; set; }
        [Column("pa_dob", TypeName = "datetime")]
        public DateTime? PaDob { get; set; }
        [Column("pa_date_joining", TypeName = "datetime")]
        public DateTime? PaDateJoining { get; set; }
        [Column("pa_primary_contact_no")]
        [StringLength(20)]
        public string PaPrimaryContactNo { get; set; }
        [Column("pa_secondary_contact_no")]
        [StringLength(20)]
        public string PaSecondaryContactNo { get; set; }
        [Column("pa_email")]
        [StringLength(200)]
        public string PaEmail { get; set; }
        [Column("pa_link_code")]
        [StringLength(50)]
        public string PaLinkCode { get; set; }
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
