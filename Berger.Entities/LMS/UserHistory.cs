﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("user_history")]
    public partial class UserHistory
    {
        [Key]
        [Column("uh_id")]
        public long UhId { get; set; }
        [Required]
        [Column("uh_userid")]
        [StringLength(20)]
        public string UhUserid { get; set; }
        [Required]
        [Column("uh_user_group")]
        [StringLength(15)]
        public string UhUserGroup { get; set; }
        [Column("uh_logged_date", TypeName = "datetime")]
        public DateTime UhLoggedDate { get; set; }
        [Column("uh_logged_ip")]
        [StringLength(50)]
        public string UhLoggedIp { get; set; }
    }
}
