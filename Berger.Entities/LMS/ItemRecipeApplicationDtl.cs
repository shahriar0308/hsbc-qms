﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("item_recipe_application_dtl")]
    public partial class ItemRecipeApplicationDtl
    {
        [Key]
        [Column("item_recipe_application_id")]
        public long ItemRecipeApplicationId { get; set; }
        [Column("item_recipe_id")]
        public long ItemRecipeId { get; set; }
        [Column("item_application_code")]
        [StringLength(50)]
        public string ItemApplicationCode { get; set; }
        [Column("item_application_cost", TypeName = "decimal(18, 2)")]
        public decimal? ItemApplicationCost { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
