﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_valuation_dtl")]
    public partial class LeadValuationDtl
    {
        [Key]
        [Column("lvd_hdr_id")]
        public long LvdHdrId { get; set; }
        [Key]
        [Column("lvd_item_id")]
        [StringLength(20)]
        public string LvdItemId { get; set; }
        [Column("lvd_item_mrp", TypeName = "decimal(18, 2)")]
        public decimal? LvdItemMrp { get; set; }
        [Column("lvd_coverage", TypeName = "decimal(18, 2)")]
        public decimal? LvdCoverage { get; set; }
        [Column("lvd_cost", TypeName = "decimal(18, 2)")]
        public decimal? LvdCost { get; set; }
        [Column("lvd_area_sqft", TypeName = "decimal(18, 2)")]
        public decimal? LvdAreaSqft { get; set; }
        [Column("lvd_value", TypeName = "decimal(18, 2)")]
        public decimal? LvdValue { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("lvd_coat")]
        public int? LvdCoat { get; set; }
        [Column("category_code")]
        [StringLength(100)]
        public string CategoryCode { get; set; }
        [Column("item_group_code")]
        [StringLength(100)]
        public string ItemGroupCode { get; set; }
        [Column("base_code")]
        [StringLength(100)]
        public string BaseCode { get; set; }
        [Column("pack_size", TypeName = "decimal(18, 2)")]
        public decimal? PackSize { get; set; }
        [Column("lvd_discount_per", TypeName = "decimal(18, 2)")]
        public decimal? LvdDiscountPer { get; set; }
    }
}
