﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_prospect_mstr")]
    public partial class LeadProspectMstr
    {
        [Column("ldp_legal_entity_id")]
        public int LdpLegalEntityId { get; set; }
        [Key]
        [Column("ldp_lead_guid")]
        [StringLength(50)]
        public string LdpLeadGuid { get; set; }
        [Required]
        [Column("ldp_lead_id")]
        [StringLength(50)]
        public string LdpLeadId { get; set; }
        [Column("ldp_division")]
        [StringLength(50)]
        public string LdpDivision { get; set; }
        [Required]
        [Column("ldp_name")]
        [StringLength(100)]
        public string LdpName { get; set; }
        [Column("ldp_organization")]
        [StringLength(100)]
        public string LdpOrganization { get; set; }
        [Column("ldp_type")]
        [StringLength(100)]
        public string LdpType { get; set; }
        [Column("ldp_lead_opportunity")]
        [StringLength(50)]
        public string LdpLeadOpportunity { get; set; }
        [Column("ldp_date", TypeName = "datetime")]
        public DateTime? LdpDate { get; set; }
        [Column("ldp_priority")]
        [StringLength(100)]
        public string LdpPriority { get; set; }
        [Column("ldp_condition")]
        [StringLength(50)]
        public string LdpCondition { get; set; }
        [Required]
        [Column("ldp_stage")]
        [StringLength(100)]
        public string LdpStage { get; set; }
        [Column("ldp_location")]
        [StringLength(250)]
        public string LdpLocation { get; set; }
        [Column("ldp_description")]
        public string LdpDescription { get; set; }
        [Column("ldp_address_line1")]
        [StringLength(250)]
        public string LdpAddressLine1 { get; set; }
        [Column("ldp_address_line2")]
        [StringLength(250)]
        public string LdpAddressLine2 { get; set; }
        [Column("ldp_address_line3")]
        [StringLength(250)]
        public string LdpAddressLine3 { get; set; }
        [Column("ldp_bibhag")]
        public int? LdpBibhag { get; set; }
        [Column("ldp_district")]
        public int? LdpDistrict { get; set; }
        [Column("ldp_police_station")]
        public int? LdpPoliceStation { get; set; }
        [Column("ldp_pin")]
        [StringLength(50)]
        public string LdpPin { get; set; }
        [Column("ldp_source")]
        [StringLength(100)]
        public string LdpSource { get; set; }
        [Column("ldp_primary_contact_name")]
        [StringLength(50)]
        public string LdpPrimaryContactName { get; set; }
        [Column("ldp_primary_contact_number")]
        [StringLength(50)]
        public string LdpPrimaryContactNumber { get; set; }
        [Column("ldp_primary_email")]
        [StringLength(50)]
        public string LdpPrimaryEmail { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("ldp_primary_contact_type")]
        [StringLength(50)]
        public string LdpPrimaryContactType { get; set; }
        [Column("ldp_primary_contact_designation")]
        [StringLength(50)]
        public string LdpPrimaryContactDesignation { get; set; }
        [Column("ldp_national_ac")]
        [StringLength(50)]
        public string LdpNationalAc { get; set; }
        [Column("ldp_regional_ac")]
        [StringLength(50)]
        public string LdpRegionalAc { get; set; }
    }
}
