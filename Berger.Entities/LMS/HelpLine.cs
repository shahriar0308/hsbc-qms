﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("help_line")]
    public partial class HelpLine
    {
        [Key]
        [Column("hlp_id")]
        public long HlpId { get; set; }
        [Column("hpl_name")]
        [StringLength(200)]
        public string HplName { get; set; }
        [Column("hlp_phone")]
        [StringLength(20)]
        public string HlpPhone { get; set; }
        [Column("hlp_email")]
        [StringLength(100)]
        public string HlpEmail { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("profile_image")]
        public string ProfileImage { get; set; }
    }
}
