﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_po_hdr")]
    public partial class LeadPoHdr
    {
        [Key]
        [Column("lpo_id")]
        public long LpoId { get; set; }
        [Column("lpo_specification_id")]
        public long? LpoSpecificationId { get; set; }
        [Column("lpo_lead_detail_guid")]
        [StringLength(50)]
        public string LpoLeadDetailGuid { get; set; }
        [Column("lpo_subject")]
        public string LpoSubject { get; set; }
        [Column("lpo_grand_total", TypeName = "decimal(18, 2)")]
        public decimal? LpoGrandTotal { get; set; }
        [Column("lpo_disc_per", TypeName = "decimal(18, 2)")]
        public decimal? LpoDiscPer { get; set; }
        [Column("lpo_disc_amount", TypeName = "decimal(18, 2)")]
        public decimal? LpoDiscAmount { get; set; }
        [Column("lpo_net_amount", TypeName = "decimal(18, 2)")]
        public decimal? LpoNetAmount { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
