﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("relationship_call_dtls")]
    public partial class RelationshipCallDtls
    {
        [Key]
        [Column("rcd_id")]
        public long RcdId { get; set; }
        [Key]
        [Column("rch_hdr_id")]
        public long RchHdrId { get; set; }
        [Column("rcd_qus_id")]
        public long RcdQusId { get; set; }
        [Column("rcd_ans_selected")]
        public string RcdAnsSelected { get; set; }
        [Column("rcd_ans_entered")]
        public string RcdAnsEntered { get; set; }
        [Column("rcd_remarks")]
        public string RcdRemarks { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
