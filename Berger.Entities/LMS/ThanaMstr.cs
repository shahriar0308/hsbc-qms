﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("thana_mstr")]
    public partial class ThanaMstr
    {
        [Column("tm_legal_entity_id")]
        public int? TmLegalEntityId { get; set; }
        [Column("tm_id")]
        public int TmId { get; set; }
        [Key]
        [Column("tm_name")]
        [StringLength(50)]
        public string TmName { get; set; }
        [Column("tm_dst_id")]
        public int? TmDstId { get; set; }
        [Column("tm_depot_code")]
        [StringLength(50)]
        public string TmDepotCode { get; set; }
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
