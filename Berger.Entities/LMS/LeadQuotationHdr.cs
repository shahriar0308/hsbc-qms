﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_quotation_hdr")]
    public partial class LeadQuotationHdr
    {
        [Key]
        [Column("lqh_id")]
        public long LqhId { get; set; }
        [Required]
        [Column("lqh_lead_detail_guid")]
        [StringLength(50)]
        public string LqhLeadDetailGuid { get; set; }
        [Required]
        [Column("lqh_lead_detail_id")]
        [StringLength(50)]
        public string LqhLeadDetailId { get; set; }
        [Required]
        [Column("lqh_package_id")]
        [StringLength(50)]
        public string LqhPackageId { get; set; }
        [Required]
        [Column("lqh_surface_id")]
        [StringLength(50)]
        public string LqhSurfaceId { get; set; }
        [Required]
        [Column("lqh_attribute_id")]
        [StringLength(50)]
        public string LqhAttributeId { get; set; }
        [Column("lqh_total_item_amount", TypeName = "decimal(18, 2)")]
        public decimal? LqhTotalItemAmount { get; set; }
        [Column("lqh_discount_per", TypeName = "decimal(18, 2)")]
        public decimal? LqhDiscountPer { get; set; }
        [Column("lqh_discount_amount", TypeName = "decimal(18, 2)")]
        public decimal? LqhDiscountAmount { get; set; }
        [Column("lqh_vat_per", TypeName = "decimal(18, 2)")]
        public decimal? LqhVatPer { get; set; }
        [Column("lqh_vat_amount", TypeName = "decimal(18, 2)")]
        public decimal? LqhVatAmount { get; set; }
        [Column("lqh_net_amount", TypeName = "decimal(18, 2)")]
        public decimal? LqhNetAmount { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("lqh_status")]
        [StringLength(50)]
        public string LqhStatus { get; set; }
        [Column("lqh_Remarks")]
        public string LqhRemarks { get; set; }
        [Column("lqh_dealer_allocation_type")]
        [StringLength(50)]
        public string LqhDealerAllocationType { get; set; }
        [Column("lqh_dealer")]
        [StringLength(50)]
        public string LqhDealer { get; set; }


        //------------------------------------------
        [ForeignKey("LqhLeadDetailGuid")]
        public LeadMstrDtl LeadMstrDtl { get; set; }
        public IList<LeadQuotationDtl> LeadQuotationDtls { get; set; }
        public IList<LeadQuotationSilverDtl> LeadQuotationSilverDtls { get; set; }
        public LeadQuotationHdr()
        {
            this.LeadQuotationDtls = new List<LeadQuotationDtl>();
            this.LeadQuotationSilverDtls = new List<LeadQuotationSilverDtl>();
        }
    }
}
