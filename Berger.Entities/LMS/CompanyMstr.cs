﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("company_mstr")]
    public partial class CompanyMstr
    {
        [Required]
        [Column("cm_legal_entiry")]
        [StringLength(50)]
        public string CmLegalEntiry { get; set; }
        [Key]
        [Column("cm_id")]
        public long CmId { get; set; }
        [Required]
        [Column("cm_type")]
        [StringLength(50)]
        public string CmType { get; set; }
        [Required]
        [Column("cm_name")]
        [StringLength(50)]
        public string CmName { get; set; }
        [Column("cm_business_verticals")]
        [StringLength(50)]
        public string CmBusinessVerticals { get; set; }
        [Column("cm_business_sub_verticals")]
        [StringLength(50)]
        public string CmBusinessSubVerticals { get; set; }
        [Column("cm_description")]
        public string CmDescription { get; set; }
        [Column("cm_business_status")]
        [StringLength(50)]
        public string CmBusinessStatus { get; set; }
        [Column("cm_sapid")]
        [StringLength(50)]
        public string CmSapid { get; set; }
        [Column("cm_trade_license")]
        [StringLength(50)]
        public string CmTradeLicense { get; set; }
        [Column("cm_vatno")]
        [StringLength(50)]
        public string CmVatno { get; set; }
        [Column("cm_category")]
        [StringLength(50)]
        public string CmCategory { get; set; }
        [Column("cm_mother_category")]
        [StringLength(50)]
        public string CmMotherCategory { get; set; }
        [Required]
        [Column("cm_address_line1")]
        [StringLength(250)]
        public string CmAddressLine1 { get; set; }
        [Column("cm_address_line2")]
        [StringLength(250)]
        public string CmAddressLine2 { get; set; }
        [Column("cm_address_line3")]
        [StringLength(250)]
        public string CmAddressLine3 { get; set; }
        [Column("cm_bibhag")]
        public int? CmBibhag { get; set; }
        [Column("cm_city")]
        [StringLength(50)]
        public string CmCity { get; set; }
        [Column("cm_state")]
        [StringLength(50)]
        public string CmState { get; set; }
        [Column("cm_district")]
        public int? CmDistrict { get; set; }
        [Column("cm_pin")]
        [StringLength(50)]
        public string CmPin { get; set; }
        [Column("cm_police_station")]
        public int? CmPoliceStation { get; set; }
        [Column("cm_primary_contact_number")]
        [StringLength(15)]
        public string CmPrimaryContactNumber { get; set; }
        [Column("cm_primary_email")]
        [StringLength(30)]
        public string CmPrimaryEmail { get; set; }
        [Column("cm_fax")]
        [StringLength(30)]
        public string CmFax { get; set; }
        [Column("cm_website")]
        [StringLength(30)]
        public string CmWebsite { get; set; }
        [Column("cm_account_title")]
        [StringLength(50)]
        public string CmAccountTitle { get; set; }
        [Column("cm_bank_name")]
        [StringLength(50)]
        public string CmBankName { get; set; }
        [Column("cm_bank_branch")]
        [StringLength(50)]
        public string CmBankBranch { get; set; }
        [Column("cm_account_type")]
        [StringLength(50)]
        public string CmAccountType { get; set; }
        [Column("cm_account_no")]
        [StringLength(50)]
        public string CmAccountNo { get; set; }
        [Column("cm_annual_turn_over", TypeName = "decimal(18, 2)")]
        public decimal? CmAnnualTurnOver { get; set; }
        [Column("cm_rating")]
        [StringLength(50)]
        public string CmRating { get; set; }
        [Column("cm_multi_location")]
        [StringLength(50)]
        public string CmMultiLocation { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
