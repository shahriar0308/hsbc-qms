﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("item_formula_hdr")]
    public partial class ItemFormulaHdr
    {
        [Column("item_formula_id")]
        public long ItemFormulaId { get; set; }
        [Column("item_recipe_id")]
        public long ItemRecipeId { get; set; }
        [Key]
        [Column("item_formula_code")]
        [StringLength(50)]
        public string ItemFormulaCode { get; set; }
        [Required]
        [Column("item_formula_name")]
        [StringLength(50)]
        public string ItemFormulaName { get; set; }
        [Column("item_formula_package")]
        [StringLength(50)]
        public string ItemFormulaPackage { get; set; }
        [Column("item_formula_surface")]
        [StringLength(50)]
        public string ItemFormulaSurface { get; set; }
        [Column("item_formula_attribute1")]
        [StringLength(50)]
        public string ItemFormulaAttribute1 { get; set; }
        [Column("item_formula_shade")]
        [StringLength(50)]
        public string ItemFormulaShade { get; set; }
        [Column("item_cost_per_sqft", TypeName = "decimal(18, 2)")]
        public decimal ItemCostPerSqft { get; set; }
        [Column("item_application_cost", TypeName = "decimal(18, 2)")]
        public decimal ItemApplicationCost { get; set; }
        [Column("item_supervision_cost", TypeName = "decimal(18, 2)")]
        public decimal? ItemSupervisionCost { get; set; }
        [Column("item_additional_cost", TypeName = "decimal(18, 2)")]
        public decimal? ItemAdditionalCost { get; set; }
        [Column("item_total_cost", TypeName = "decimal(18, 2)")]
        public decimal ItemTotalCost { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
