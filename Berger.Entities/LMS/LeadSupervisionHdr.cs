﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_supervision_hdr")]
    public partial class LeadSupervisionHdr
    {
        [Key]
        [Column("lsh_hdr_id")]
        public long LshHdrId { get; set; }
        [Required]
        [Column("lsh_lead_detail_guid")]
        [StringLength(50)]
        public string LshLeadDetailGuid { get; set; }
        [Required]
        [Column("lsh_lead_detail_id")]
        [StringLength(50)]
        public string LshLeadDetailId { get; set; }
        [Column("lsh_supervision_date", TypeName = "date")]
        public DateTime LshSupervisionDate { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
