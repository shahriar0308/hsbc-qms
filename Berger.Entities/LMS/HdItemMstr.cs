﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("hd_item_mstr")]
    public partial class HdItemMstr
    {
        [Column("item_package")]
        [StringLength(50)]
        public string ItemPackage { get; set; }
        [Column("item_surface")]
        [StringLength(50)]
        public string ItemSurface { get; set; }
        [Column("item_floor")]
        [StringLength(50)]
        public string ItemFloor { get; set; }
        [Column("item_brand")]
        [StringLength(50)]
        public string ItemBrand { get; set; }
        [Column("item_base")]
        [StringLength(50)]
        public string ItemBase { get; set; }
        [Column("item_id")]
        [StringLength(50)]
        public string ItemId { get; set; }
    }
}
