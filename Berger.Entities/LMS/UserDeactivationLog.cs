﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("user_deactivation_log")]
    public partial class UserDeactivationLog
    {
        [Key]
        [Column("u_id")]
        public long UId { get; set; }
        [Column("u_user_id")]
        [StringLength(20)]
        public string UUserId { get; set; }
        [Column("u_app_id")]
        public int? UAppId { get; set; }
        [Column("u_remarks")]
        [StringLength(50)]
        public string URemarks { get; set; }
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
    }
}
