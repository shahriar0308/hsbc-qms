﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("visit_company_dtl")]
    public partial class VisitCompanyDtl
    {
        [Key]
        [Column("vcd_id")]
        public long VcdId { get; set; }
        [Column("vcd_hdr_id")]
        public long VcdHdrId { get; set; }
        [Column("vcd_qus_id")]
        public long VcdQusId { get; set; }
        [Column("vcd_selected_yn")]
        [StringLength(1)]
        public string VcdSelectedYn { get; set; }
        [Column("vcd_attribute_date1", TypeName = "date")]
        public DateTime? VcdAttributeDate1 { get; set; }
        [Column("vcd_attribute_date2", TypeName = "date")]
        public DateTime? VcdAttributeDate2 { get; set; }
        [Column("vcd_attribute_int1")]
        public int? VcdAttributeInt1 { get; set; }
        [Column("vcd_attribute_int2")]
        public int? VcdAttributeInt2 { get; set; }
        [Column("vcd_atrribute_decimal1", TypeName = "decimal(18, 2)")]
        public decimal? VcdAtrributeDecimal1 { get; set; }
        [Column("vcd_atrribute_decimal2", TypeName = "decimal(18, 2)")]
        public decimal? VcdAtrributeDecimal2 { get; set; }
        [Column("vcd_attribute_char1")]
        public string VcdAttributeChar1 { get; set; }
        [Column("vcd_attribute_char2")]
        public string VcdAttributeChar2 { get; set; }
        [Column("vcd_img_path")]
        public string VcdImgPath { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
