﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_po_dtl")]
    public partial class LeadPoDtl
    {
        [Key]
        [Column("lpod_id")]
        public long LpodId { get; set; }
        [Column("lpod_hdr_id")]
        public long? LpodHdrId { get; set; }
        [Column("lpod_item_id")]
        [StringLength(50)]
        public string LpodItemId { get; set; }
        [Column("lpod_pack_size", TypeName = "decimal(18, 2)")]
        public decimal? LpodPackSize { get; set; }
        [Column("lpod_qty", TypeName = "decimal(18, 2)")]
        public decimal? LpodQty { get; set; }
        [Column("lpod_mrp", TypeName = "decimal(18, 2)")]
        public decimal? LpodMrp { get; set; }
        [Column("lpod_total", TypeName = "decimal(18, 2)")]
        public decimal? LpodTotal { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
