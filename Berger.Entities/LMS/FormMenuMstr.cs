﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("form_menu_mstr")]
    public partial class FormMenuMstr
    {
        [Key]
        [Column("fmm_id")]
        public int FmmId { get; set; }
        [Column("fmm_name")]
        [StringLength(100)]
        public string FmmName { get; set; }
        [Column("fmm_link")]
        [StringLength(100)]
        public string FmmLink { get; set; }
        [Column("fmm_parent_id")]
        public int? FmmParentId { get; set; }
        [Column("fmm_sequence")]
        public int? FmmSequence { get; set; }
        [Column("fafa_icon")]
        [StringLength(50)]
        public string FafaIcon { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
