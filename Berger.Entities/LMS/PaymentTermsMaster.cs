﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("payment_terms_master")]
    public partial class PaymentTermsMaster
    {
        [Key]
        [Column("ptm_id")]
        public int PtmId { get; set; }
        [Column("ptm_legal_entity")]
        public int PtmLegalEntity { get; set; }
        [Required]
        [Column("ptm_division")]
        [StringLength(50)]
        public string PtmDivision { get; set; }
        [Column("ptm_terms")]
        public string PtmTerms { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
