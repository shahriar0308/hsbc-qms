﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_geo_location_updation")]
    public partial class LeadGeoLocationUpdation
    {
        [Key]
        [Column("glu_id")]
        public long GluId { get; set; }
        [Required]
        [Column("glu_lead_dtl_guid")]
        [StringLength(50)]
        public string GluLeadDtlGuid { get; set; }
        [Column("glu_type_id")]
        public int GluTypeId { get; set; }
        [Column("glu_latitude")]
        [StringLength(50)]
        public string GluLatitude { get; set; }
        [Column("glu_longitude")]
        [StringLength(50)]
        public string GluLongitude { get; set; }
        [Column("glu_accuracy")]
        [StringLength(50)]
        public string GluAccuracy { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime UpdatedOn { get; set; }
    }
}
