﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_notifcation_hdr")]
    public partial class LeadNotifcationHdr
    {
        [Key]
        [Column("lnh_id")]
        public long LnhId { get; set; }
        [Key]
        [Column("lnh_lead_details_guid")]
        [StringLength(50)]
        public string LnhLeadDetailsGuid { get; set; }
        [Key]
        [Column("lnh_lead_details_id")]
        [StringLength(50)]
        public string LnhLeadDetailsId { get; set; }
        [Column("lnh_type")]
        [StringLength(50)]
        public string LnhType { get; set; }
        [Column("lnh_desc")]
        [StringLength(500)]
        public string LnhDesc { get; set; }
        [Column("lnh_pushed")]
        [StringLength(1)]
        public string LnhPushed { get; set; }
        [Column("lnh_valid_date_fr", TypeName = "datetime")]
        public DateTime? LnhValidDateFr { get; set; }
        [Column("lnh_valid_date_to", TypeName = "datetime")]
        public DateTime? LnhValidDateTo { get; set; }
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("lnh_details")]
        [StringLength(50)]
        public string LnhDetails { get; set; }
    }
}
