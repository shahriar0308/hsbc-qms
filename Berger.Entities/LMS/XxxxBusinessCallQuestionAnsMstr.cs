﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("XXXX-business_call_question_ans_mstr")]
    public partial class XxxxBusinessCallQuestionAnsMstr
    {
        [Key]
        [Column("bcqam_id")]
        public long BcqamId { get; set; }
        [Column("bcqam_question_id")]
        public long BcqamQuestionId { get; set; }
        [Required]
        [Column("bcqam_ans_desc")]
        public string BcqamAnsDesc { get; set; }
        [Column("bcqam_ans_sec")]
        public int BcqamAnsSec { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
