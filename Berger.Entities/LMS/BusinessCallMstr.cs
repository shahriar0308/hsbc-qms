﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("business_call_mstr")]
    public partial class BusinessCallMstr
    {
        [Key]
        [Column("bc_id")]
        public long BcId { get; set; }
        [Required]
        [Column("bc_type")]
        [StringLength(50)]
        public string BcType { get; set; }
        [Required]
        [Column("bc_catg")]
        [StringLength(50)]
        public string BcCatg { get; set; }
        [Required]
        [Column("bc_desc")]
        public string BcDesc { get; set; }
        [Column("bc_seq")]
        public int? BcSeq { get; set; }
        [Column("bc_attrtibute_date1_desc")]
        public string BcAttrtibuteDate1Desc { get; set; }
        [Column("bc_attrtibute_date2_desc")]
        public string BcAttrtibuteDate2Desc { get; set; }
        [Column("bc_attrtibute_int1_desc")]
        public string BcAttrtibuteInt1Desc { get; set; }
        [Column("bc_attrtibute_int2_desc")]
        public string BcAttrtibuteInt2Desc { get; set; }
        [Column("bc_attrtibute_decimal1_desc")]
        public string BcAttrtibuteDecimal1Desc { get; set; }
        [Column("bc_attrtibute_decimal2_desc")]
        public string BcAttrtibuteDecimal2Desc { get; set; }
        [Column("bc_attrtibute_char1_desc")]
        public string BcAttrtibuteChar1Desc { get; set; }
        [Column("bc_attrtibute_char2_desc")]
        public string BcAttrtibuteChar2Desc { get; set; }
        [Column("bc_img_reqired_yn")]
        [StringLength(1)]
        public string BcImgReqiredYn { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
