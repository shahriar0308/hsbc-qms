﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("non_decor_price_dtl")]
    public partial class NonDecorPriceDtl
    {
        [Key]
        [Column("ndpd_details_id")]
        public long NdpdDetailsId { get; set; }
        [Column("ndpd_recipe_id")]
        public long NdpdRecipeId { get; set; }
        [Column("ndpd_category")]
        [StringLength(50)]
        public string NdpdCategory { get; set; }
        [Column("ndpd_brand")]
        [StringLength(50)]
        public string NdpdBrand { get; set; }
        [Column("ndpd_item_id")]
        [StringLength(50)]
        public string NdpdItemId { get; set; }
        [Column("ndpd_pack_size", TypeName = "decimal(18, 2)")]
        public decimal? NdpdPackSize { get; set; }
        [Column("ndpd_mixin_ratio", TypeName = "decimal(18, 2)")]
        public decimal? NdpdMixinRatio { get; set; }
        [Column("ndpd_qty", TypeName = "decimal(18, 2)")]
        public decimal? NdpdQty { get; set; }
        [Column("ndpd_unit_price", TypeName = "decimal(18, 2)")]
        public decimal? NdpdUnitPrice { get; set; }
        [Column("ndpd_discount", TypeName = "decimal(18, 2)")]
        public decimal? NdpdDiscount { get; set; }
        [Column("ndpd_price", TypeName = "decimal(18, 2)")]
        public decimal? NdpdPrice { get; set; }
        [Column("ndpd_amount", TypeName = "decimal(18, 2)")]
        public decimal? NdpdAmount { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
