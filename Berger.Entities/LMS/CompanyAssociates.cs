﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("company_associates")]
    public partial class CompanyAssociates
    {
        [Key]
        [Column("ca_id")]
        [StringLength(10)]
        public string CaId { get; set; }
        [Key]
        [Column("ca_cm_id")]
        [StringLength(10)]
        public string CaCmId { get; set; }
        [Key]
        [Column("ca_lvl_code")]
        [StringLength(10)]
        public string CaLvlCode { get; set; }
        [Column("ca_first_name")]
        [StringLength(20)]
        public string CaFirstName { get; set; }
        [Column("ca_middle_name")]
        [StringLength(20)]
        public string CaMiddleName { get; set; }
        [Column("ca_last_name")]
        [StringLength(20)]
        public string CaLastName { get; set; }
        [Column("ca_designation")]
        [StringLength(200)]
        public string CaDesignation { get; set; }
        [Column("ca_gender")]
        [StringLength(1)]
        public string CaGender { get; set; }
        [Column("ca_dob", TypeName = "datetime")]
        public DateTime? CaDob { get; set; }
        [Column("ca_date_joining", TypeName = "datetime")]
        public DateTime? CaDateJoining { get; set; }
        [Column("ca_primary_contact_no")]
        [StringLength(10)]
        public string CaPrimaryContactNo { get; set; }
        [Column("ca_secondary_contact_no")]
        [StringLength(10)]
        public string CaSecondaryContactNo { get; set; }
        [Column("ca_email")]
        [StringLength(30)]
        public string CaEmail { get; set; }
        [Column("ca_link_code")]
        [StringLength(10)]
        public string CaLinkCode { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
