﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_customer_feedback_dtl")]
    public partial class LeadCustomerFeedbackDtl
    {
        [Key]
        [Column("lcfd_lcf_hdr_id")]
        public int LcfdLcfHdrId { get; set; }
        [Key]
        [Column("lcfd_ques_id")]
        public int LcfdQuesId { get; set; }
        [Column("lcfd_ans_id_cust")]
        public int LcfdAnsIdCust { get; set; }
        [Column("lcfd_ans_id_verified")]
        public int LcfdAnsIdVerified { get; set; }
        [Column("lcfd_ans_rating_cust")]
        public int LcfdAnsRatingCust { get; set; }
        [Column("lcfd_ans_rating_verified")]
        public int LcfdAnsRatingVerified { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("lcfd_remarks_cust")]
        public string LcfdRemarksCust { get; set; }
    }
}
