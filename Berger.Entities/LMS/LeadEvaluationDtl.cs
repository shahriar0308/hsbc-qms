﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_evaluation_dtl")]
    public partial class LeadEvaluationDtl
    {
        [Key]
        [Column("led_hdr_id")]
        public long LedHdrId { get; set; }
        [Key]
        [Column("led_activity_id")]
        public long LedActivityId { get; set; }
        [Column("led_standard_range")]
        public string LedStandardRange { get; set; }
        [Column("led_present_condition")]
        public string LedPresentCondition { get; set; }
        [Column("led_treatment_required")]
        public string LedTreatmentRequired { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
