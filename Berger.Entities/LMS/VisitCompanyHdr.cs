﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("visit_company_hdr")]
    public partial class VisitCompanyHdr
    {
        [Key]
        [Column("vch_id")]
        public long VchId { get; set; }
        [Required]
        [Column("vch_customer_type")]
        [StringLength(50)]
        public string VchCustomerType { get; set; }
        [Column("vch_company_id")]
        public long VchCompanyId { get; set; }
        [Column("vch_date", TypeName = "datetime")]
        public DateTime VchDate { get; set; }
        [Required]
        [Column("vch_user")]
        [StringLength(50)]
        public string VchUser { get; set; }
        [Column("vch_check_in_time", TypeName = "datetime")]
        public DateTime? VchCheckInTime { get; set; }
        [Column("vch_check_out_time", TypeName = "datetime")]
        public DateTime? VchCheckOutTime { get; set; }
        [Column("vch_spent_minutes")]
        public int? VchSpentMinutes { get; set; }
        [Column("vch_check_in_lat")]
        [StringLength(50)]
        public string VchCheckInLat { get; set; }
        [Column("vch_check_in_long")]
        [StringLength(50)]
        public string VchCheckInLong { get; set; }
        [Column("vch_check_in_accuracy")]
        [StringLength(50)]
        public string VchCheckInAccuracy { get; set; }
        [Column("vch_check_out_lat")]
        [StringLength(50)]
        public string VchCheckOutLat { get; set; }
        [Column("vch_check_out_long")]
        [StringLength(50)]
        public string VchCheckOutLong { get; set; }
        [Column("vch_check_out_accuracy")]
        [StringLength(50)]
        public string VchCheckOutAccuracy { get; set; }
        [Column("vch_mode")]
        [StringLength(50)]
        public string VchMode { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
