﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("dashboard_lead")]
    public partial class DashboardLead
    {
        [Key]
        [Column("dl_year")]
        [StringLength(4)]
        public string DlYear { get; set; }
        [Key]
        [Column("dl_month")]
        [StringLength(2)]
        public string DlMonth { get; set; }
        [Key]
        [Column("dl_user")]
        [StringLength(20)]
        public string DlUser { get; set; }
        [Key]
        [Column("dl_stage")]
        [StringLength(50)]
        public string DlStage { get; set; }
        [Key]
        [Column("dl_division")]
        [StringLength(50)]
        public string DlDivision { get; set; }
        [Column("dl_lead_counts")]
        public long DlLeadCounts { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
