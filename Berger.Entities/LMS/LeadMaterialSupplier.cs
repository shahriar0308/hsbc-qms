﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_material_supplier")]
    public partial class LeadMaterialSupplier
    {
        [Key]
        [Column("lms_lead_detail_guid")]
        [StringLength(50)]
        public string LmsLeadDetailGuid { get; set; }
        [Required]
        [Column("lms_lead_detail_id")]
        [StringLength(50)]
        public string LmsLeadDetailId { get; set; }
        [Key]
        [Column("lms_dlr_id")]
        [StringLength(50)]
        public string LmsDlrId { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("lms_cluster_id")]
        [StringLength(100)]
        public string LmsClusterId { get; set; }
    }
}
