﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_material_supplier_payment")]
    public partial class LeadMaterialSupplierPayment
    {
        [Key]
        [Column("lmsp_payment_id")]
        public long LmspPaymentId { get; set; }
        [Required]
        [Column("lmsp_lead_detail_guid")]
        [StringLength(50)]
        public string LmspLeadDetailGuid { get; set; }
        [Required]
        [Column("lmsp_lead_detail_id")]
        [StringLength(50)]
        public string LmspLeadDetailId { get; set; }
        [Required]
        [Column("lmsp_dlr_id")]
        [StringLength(50)]
        public string LmspDlrId { get; set; }
        [Column("Imsp_bill_id")]
        public long ImspBillId { get; set; }
        [Column("lmsp_payment_mode")]
        [StringLength(50)]
        public string LmspPaymentMode { get; set; }
        [Column("lmsp_ifsc_code")]
        [StringLength(50)]
        public string LmspIfscCode { get; set; }
        [Column("lmsp_bank_name")]
        [StringLength(50)]
        public string LmspBankName { get; set; }
        [Column("lmsp_branch_name")]
        [StringLength(50)]
        public string LmspBranchName { get; set; }
        [Column("lmsp_account_no")]
        [StringLength(50)]
        public string LmspAccountNo { get; set; }
        [Column("lmsp_instrument_no")]
        [StringLength(50)]
        public string LmspInstrumentNo { get; set; }
        [Column("lmsp_instrument_date", TypeName = "datetime")]
        public DateTime? LmspInstrumentDate { get; set; }
        [Column("lmsp_amount", TypeName = "decimal(18, 2)")]
        public decimal? LmspAmount { get; set; }
        [Column("lmsp_status")]
        [StringLength(50)]
        public string LmspStatus { get; set; }
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("lmsp_uploaded_document")]
        public string LmspUploadedDocument { get; set; }
        [Column("uploaded_document_path")]
        public string UploadedDocumentPath { get; set; }
    }
}
