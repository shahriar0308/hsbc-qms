﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_customer_feedback_hdr")]
    public partial class LeadCustomerFeedbackHdr
    {
        [Key]
        [Column("lcf_hdr_id")]
        public int LcfHdrId { get; set; }
        [Required]
        [Column("lcf_lead_detail_guid")]
        [StringLength(50)]
        public string LcfLeadDetailGuid { get; set; }
        [Required]
        [Column("lcf_lead_detail_id")]
        [StringLength(50)]
        public string LcfLeadDetailId { get; set; }
        [Required]
        [Column("lcf_mode")]
        [StringLength(50)]
        public string LcfMode { get; set; }
        [Column("lcf_otp")]
        [StringLength(50)]
        public string LcfOtp { get; set; }
        [Column("lcf_verified")]
        [StringLength(1)]
        public string LcfVerified { get; set; }
        [Column("lcf_verified_by")]
        [StringLength(50)]
        public string LcfVerifiedBy { get; set; }
        [Column("lcf_verified_date", TypeName = "datetime")]
        public DateTime? LcfVerifiedDate { get; set; }
        [Column("lcf_score")]
        public int? LcfScore { get; set; }
        [Column("lcf_total_no")]
        public int? LcfTotalNo { get; set; }
        [Column("lcf_remarks_cust")]
        public string LcfRemarksCust { get; set; }
        [Column("lcf_remarks_verified")]
        public string LcfRemarksVerified { get; set; }
        [Column("lcf_otp_contact_srl")]
        public int? LcfOtpContactSrl { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
