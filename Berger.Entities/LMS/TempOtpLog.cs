﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("temp_otp_log")]
    public partial class TempOtpLog
    {
        [Key]
        [Column("otp_log_id")]
        public int OtpLogId { get; set; }
        [Column("usp_user_id")]
        [StringLength(20)]
        public string UspUserId { get; set; }
        [Column("usp_email_id")]
        [StringLength(200)]
        public string UspEmailId { get; set; }
        [Column("otp_password")]
        [StringLength(10)]
        public string OtpPassword { get; set; }
        [Column("usp_contact_no")]
        [StringLength(20)]
        public string UspContactNo { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("expire_time", TypeName = "datetime")]
        public DateTime? ExpireTime { get; set; }
        [Column("otp_expiration_flag")]
        public int? OtpExpirationFlag { get; set; }
    }
}
