﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_material_requision_item_received_hdr")]
    public partial class LeadMaterialRequisionItemReceivedHdr
    {
        [Key]
        [Column("lmrirh_id")]
        public long LmrirhId { get; set; }
        [Required]
        [Column("lmrirh_lead_detail_guid")]
        [StringLength(50)]
        public string LmrirhLeadDetailGuid { get; set; }
        [Required]
        [Column("lmrirh_lead_detail_id")]
        [StringLength(50)]
        public string LmrirhLeadDetailId { get; set; }
        [Required]
        [Column("lmrirh_dlr_id")]
        [StringLength(50)]
        public string LmrirhDlrId { get; set; }
        [Required]
        [Column("lmrirh_allocation_id")]
        [StringLength(50)]
        public string LmrirhAllocationId { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
