﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("company_division")]
    public partial class CompanyDivision
    {
        [Required]
        [Column("cdv_cm_code")]
        [StringLength(10)]
        public string CdvCmCode { get; set; }
        [Key]
        [Column("cdv_id")]
        public int CdvId { get; set; }
        [Column("cdv_division")]
        [StringLength(50)]
        public string CdvDivision { get; set; }
        [Column("cdv_credit_limit_amount", TypeName = "decimal(18, 2)")]
        public decimal? CdvCreditLimitAmount { get; set; }
        [Column("cdv_credit_limit_days")]
        public int? CdvCreditLimitDays { get; set; }
        [Column("cdv_discount", TypeName = "decimal(18, 2)")]
        public decimal? CdvDiscount { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
