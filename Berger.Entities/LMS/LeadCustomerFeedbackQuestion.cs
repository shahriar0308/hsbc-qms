﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_customer_feedback_question")]
    public partial class LeadCustomerFeedbackQuestion
    {
        [Key]
        [Column("ques_id")]
        public int QuesId { get; set; }
        [Column("ques_legal_entity")]
        public int? QuesLegalEntity { get; set; }
        [Column("ques_division")]
        [StringLength(50)]
        public string QuesDivision { get; set; }
        [Column("ques_desc")]
        public string QuesDesc { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
