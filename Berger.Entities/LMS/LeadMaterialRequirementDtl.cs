﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_material_requirement_dtl")]
    public partial class LeadMaterialRequirementDtl
    {
        [Key]
        [Column("imrd_srl_id")]
        public long ImrdSrlId { get; set; }
        [Column("lmrd_quoutation_hdr_id")]
        public long LmrdQuoutationHdrId { get; set; }
        [Required]
        [Column("lmrd_item_id")]
        [StringLength(50)]
        public string LmrdItemId { get; set; }
        [Column("lmrd_base_shade")]
        [StringLength(50)]
        public string LmrdBaseShade { get; set; }
        [Column("lmrd_quotation_shade_code")]
        [StringLength(50)]
        public string LmrdQuotationShadeCode { get; set; }
        [Column("lmrd_item_shade_code")]
        [StringLength(50)]
        public string LmrdItemShadeCode { get; set; }
        [Column("lmrd_item_group_code")]
        [StringLength(50)]
        public string LmrdItemGroupCode { get; set; }
        [Column("lmrd_item_pack_size", TypeName = "decimal(18, 3)")]
        public decimal LmrdItemPackSize { get; set; }
        [Column("lmrd_item_qty_nop")]
        public int LmrdItemQtyNop { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
