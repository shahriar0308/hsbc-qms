﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lov_mstr")]
    public partial class LovMstr
    {
        [Key]
        [Column("lov_type")]
        [StringLength(50)]
        public string LovType { get; set; }
        [Column("lov_desc")]
        [StringLength(50)]
        public string LovDesc { get; set; }
        [Column("lov_value")]
        [StringLength(1)]
        public string LovValue { get; set; }
        [Column("lov_seq", TypeName = "numeric(8, 0)")]
        public decimal? LovSeq { get; set; }
        [Column("lov_field1_type")]
        [StringLength(1)]
        public string LovField1Type { get; set; }
        [Column("lov_field2_type")]
        [StringLength(1)]
        public string LovField2Type { get; set; }
        [Column("lov_field3_type")]
        [StringLength(1)]
        public string LovField3Type { get; set; }
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
