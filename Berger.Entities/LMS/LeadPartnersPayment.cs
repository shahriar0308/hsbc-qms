﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_partners_payment")]
    public partial class LeadPartnersPayment
    {
        [Key]
        [Column("lpp_id")]
        public long LppId { get; set; }
        [Required]
        [Column("lpp_lead_details_guid")]
        [StringLength(50)]
        public string LppLeadDetailsGuid { get; set; }
        [Required]
        [Column("lpp_lead_details_id")]
        [StringLength(50)]
        public string LppLeadDetailsId { get; set; }
        [Required]
        [Column("lpp_lead_partner_id")]
        [StringLength(50)]
        public string LppLeadPartnerId { get; set; }
        [Column("lpp_payment_mode")]
        [StringLength(50)]
        public string LppPaymentMode { get; set; }
        [Column("lpp_ifsc_code")]
        [StringLength(50)]
        public string LppIfscCode { get; set; }
        [Column("lpp_bank_name")]
        [StringLength(50)]
        public string LppBankName { get; set; }
        [Column("lpp_branch_name")]
        [StringLength(50)]
        public string LppBranchName { get; set; }
        [Column("lpp_account_no")]
        [StringLength(50)]
        public string LppAccountNo { get; set; }
        [Column("lpp_instrument_no")]
        [StringLength(50)]
        public string LppInstrumentNo { get; set; }
        [Column("lpp_instrument_date", TypeName = "datetime")]
        public DateTime? LppInstrumentDate { get; set; }
        [Column("lpp_amount", TypeName = "decimal(18, 2)")]
        public decimal? LppAmount { get; set; }
        [Column("lpp_status")]
        [StringLength(50)]
        public string LppStatus { get; set; }
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
