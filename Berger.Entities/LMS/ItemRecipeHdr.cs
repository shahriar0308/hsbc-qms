﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("item_recipe_hdr")]
    public partial class ItemRecipeHdr
    {
        [Key]
        [Column("item_recipe_id")]
        public long ItemRecipeId { get; set; }
        [Required]
        [Column("item_recipe_name")]
        public string ItemRecipeName { get; set; }
        [Required]
        [Column("item_recipe_code")]
        [StringLength(50)]
        public string ItemRecipeCode { get; set; }
        [Required]
        [Column("item_recipe_package")]
        [StringLength(50)]
        public string ItemRecipePackage { get; set; }
        [Required]
        [Column("item_recipe_surface")]
        [StringLength(50)]
        public string ItemRecipeSurface { get; set; }
        [Required]
        [Column("item_recipe_shade")]
        [StringLength(50)]
        public string ItemRecipeShade { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
