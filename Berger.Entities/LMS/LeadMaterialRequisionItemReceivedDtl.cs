﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_material_requision_item_received_dtl")]
    public partial class LeadMaterialRequisionItemReceivedDtl
    {
        [Key]
        [Column("lmrird_details_id")]
        public long LmrirdDetailsId { get; set; }
        [Column("lmrird_header_id")]
        public long LmrirdHeaderId { get; set; }
        [Column("lmrird_allocation_details_id")]
        public long? LmrirdAllocationDetailsId { get; set; }
        [Column("lmrird_product_id")]
        [StringLength(50)]
        public string LmrirdProductId { get; set; }
        [Column("lmrird_qty", TypeName = "decimal(18, 3)")]
        public decimal? LmrirdQty { get; set; }
        [Column("lmrird_bill_status")]
        [StringLength(50)]
        public string LmrirdBillStatus { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
