﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_mstr_dtl_surface")]
    public partial class LeadMstrDtlSurface
    {
        [Key]
        [Column("lds_srl_id")]
        public long LdsSrlId { get; set; }
        [Required]
        [Column("lds_lead_detail_guid")]
        [StringLength(50)]
        public string LdsLeadDetailGuid { get; set; }
        [Required]
        [Column("lds_lead_detail_id")]
        [StringLength(50)]
        public string LdsLeadDetailId { get; set; }
        [Required]
        [Column("lds_surface_type")]
        [StringLength(50)]
        public string LdsSurfaceType { get; set; }
        [Required]
        [Column("lds_surface_sqft")]
        [StringLength(50)]
        public string LdsSurfaceSqft { get; set; }
        [Column("lds_surface_value", TypeName = "decimal(18, 2)")]
        public decimal? LdsSurfaceValue { get; set; }
        [Column("lds_surface_volume", TypeName = "decimal(18, 2)")]
        public decimal? LdsSurfaceVolume { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
