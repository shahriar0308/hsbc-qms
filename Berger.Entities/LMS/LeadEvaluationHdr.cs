﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_evaluation_hdr")]
    public partial class LeadEvaluationHdr
    {
        [Key]
        [Column("leh_hdr_id")]
        public long LehHdrId { get; set; }
        [Column("leh_lead_detail_guid")]
        [StringLength(50)]
        public string LehLeadDetailGuid { get; set; }
        [Column("leh_lead_detail_id")]
        [StringLength(50)]
        public string LehLeadDetailId { get; set; }
        [Column("leh_evaluation_date", TypeName = "date")]
        public DateTime? LehEvaluationDate { get; set; }
        [Column("leh_painting_type")]
        [StringLength(50)]
        public string LehPaintingType { get; set; }
        [Column("leh_painting_surface")]
        [StringLength(50)]
        public string LehPaintingSurface { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
