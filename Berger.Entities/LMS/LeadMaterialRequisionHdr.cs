﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_material_requision_hdr")]
    public partial class LeadMaterialRequisionHdr
    {
        [Key]
        [Column("lmrh_id")]
        public long LmrhId { get; set; }
        [Required]
        [Column("lmrh_lead_detail_guid")]
        [StringLength(50)]
        public string LmrhLeadDetailGuid { get; set; }
        [Required]
        [Column("lmrh_lead_detail_id")]
        [StringLength(50)]
        public string LmrhLeadDetailId { get; set; }
        [Required]
        [Column("lmrh_dlr_id")]
        [StringLength(50)]
        public string LmrhDlrId { get; set; }
        [Column("lmrh_quotation_hdr_id")]
        public long? LmrhQuotationHdrId { get; set; }
        [Column("lmrh_received_status")]
        [StringLength(50)]
        public string LmrhReceivedStatus { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
