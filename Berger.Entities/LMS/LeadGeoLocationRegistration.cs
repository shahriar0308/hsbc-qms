﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_geo_location_registration")]
    public partial class LeadGeoLocationRegistration
    {
        [Key]
        [Column("glr_lead_dtl_guid")]
        [StringLength(50)]
        public string GlrLeadDtlGuid { get; set; }
        [Key]
        [Column("glr_type_id")]
        public int GlrTypeId { get; set; }
        [Column("glr_latitude")]
        [StringLength(50)]
        public string GlrLatitude { get; set; }
        [Column("glr_longitude")]
        [StringLength(50)]
        public string GlrLongitude { get; set; }
        [Column("glr_accuracy")]
        [StringLength(50)]
        public string GlrAccuracy { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
