﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("user_session_all")]
    public partial class UserSessionAll
    {
        [Key]
        [Column("us_unique_id")]
        public int UsUniqueId { get; set; }
        [Column("us_user_legal_entity")]
        public int? UsUserLegalEntity { get; set; }
        [Column("us_user_id")]
        [StringLength(20)]
        public string UsUserId { get; set; }
        [Column("us_device_uuid")]
        [StringLength(200)]
        public string UsDeviceUuid { get; set; }
        [Column("us_token")]
        public string UsToken { get; set; }
        [Column("us_ip_address")]
        [StringLength(50)]
        public string UsIpAddress { get; set; }
        [Column("us_start_time_stamp", TypeName = "datetime")]
        public DateTime? UsStartTimeStamp { get; set; }
        [Column("us_last_activity_time_stamp", TypeName = "datetime")]
        public DateTime? UsLastActivityTimeStamp { get; set; }
        [Column("us_expire_time", TypeName = "datetime")]
        public DateTime? UsExpireTime { get; set; }
        [Column("us_expire_reason")]
        [StringLength(100)]
        public string UsExpireReason { get; set; }
    }
}
