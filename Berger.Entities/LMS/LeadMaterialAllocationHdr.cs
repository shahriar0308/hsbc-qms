﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_material_allocation_hdr")]
    public partial class LeadMaterialAllocationHdr
    {
        [Key]
        [Column("lmah_id")]
        public long LmahId { get; set; }
        [Required]
        [Column("lmah_lead_detail_guid")]
        [StringLength(50)]
        public string LmahLeadDetailGuid { get; set; }
        [Column("lmah_requisition_id")]
        public long? LmahRequisitionId { get; set; }
        [Column("lmah_dlr_id")]
        [StringLength(50)]
        public string LmahDlrId { get; set; }
        [Column("lmah_received_status")]
        [StringLength(50)]
        public string LmahReceivedStatus { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
