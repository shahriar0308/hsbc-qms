﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("business_call_dtls")]
    public partial class BusinessCallDtls
    {
        [Key]
        [Column("bcd_id")]
        public long BcdId { get; set; }
        [Key]
        [Column("bcd_hdr_id")]
        public long BcdHdrId { get; set; }
        [Column("bcd_qus_id")]
        public long BcdQusId { get; set; }
        [Column("bcd_selected_yn")]
        [StringLength(1)]
        public string BcdSelectedYn { get; set; }
        [Column("bcd_attribute_date1", TypeName = "date")]
        public DateTime? BcdAttributeDate1 { get; set; }
        [Column("bcd_attribute_date2", TypeName = "date")]
        public DateTime? BcdAttributeDate2 { get; set; }
        [Column("bcd_attribute_int1")]
        public int? BcdAttributeInt1 { get; set; }
        [Column("bcd_attribute_int2")]
        public int? BcdAttributeInt2 { get; set; }
        [Column("bcd_atrribute_decimal1", TypeName = "decimal(18, 2)")]
        public decimal? BcdAtrributeDecimal1 { get; set; }
        [Column("bcd_atrribute_decimal2", TypeName = "decimal(18, 2)")]
        public decimal? BcdAtrributeDecimal2 { get; set; }
        [Column("bcd_attribute_char1")]
        public string BcdAttributeChar1 { get; set; }
        [Column("bcd_attribute_char2")]
        public string BcdAttributeChar2 { get; set; }
        [Column("bcd_img_path")]
        public string BcdImgPath { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
