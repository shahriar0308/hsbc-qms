﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("app_message_mstr")]
    public partial class AppMessageMstr
    {
        [Key]
        [Column("msg_id")]
        public long MsgId { get; set; }
        [Column("msg_subject")]
        [StringLength(100)]
        public string MsgSubject { get; set; }
        [Column("msg_content")]
        [StringLength(500)]
        public string MsgContent { get; set; }
        [Column("msg_valid_from", TypeName = "date")]
        public DateTime? MsgValidFrom { get; set; }
        [Column("msg_valid_to", TypeName = "date")]
        public DateTime? MsgValidTo { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
