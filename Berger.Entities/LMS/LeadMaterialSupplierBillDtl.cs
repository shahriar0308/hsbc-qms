﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_material_supplier_bill_dtl")]
    public partial class LeadMaterialSupplierBillDtl
    {
        [Key]
        [Column("lmsbd_details_id")]
        public long LmsbdDetailsId { get; set; }
        [Column("lmsbd_header_id")]
        public long LmsbdHeaderId { get; set; }
        [Column("lmsbd_receive_details_id")]
        public long LmsbdReceiveDetailsId { get; set; }
        [Column("lmsbd_product_id")]
        [StringLength(50)]
        public string LmsbdProductId { get; set; }
        [Column("lmsbd_qty", TypeName = "decimal(18, 3)")]
        public decimal? LmsbdQty { get; set; }
        [Column("lmsbd_price", TypeName = "decimal(18, 2)")]
        public decimal? LmsbdPrice { get; set; }
        [Column("lmsbd_amount", TypeName = "decimal(18, 2)")]
        public decimal? LmsbdAmount { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
