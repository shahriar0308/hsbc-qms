﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("app_version_update_log")]
    public partial class AppVersionUpdateLog
    {
        [Key]
        [Column("avl_id")]
        public long AvlId { get; set; }
        [Column("avl_user_id")]
        [StringLength(20)]
        public string AvlUserId { get; set; }
        [Column("avl_uuid")]
        [StringLength(200)]
        public string AvlUuid { get; set; }
        [Column("avl_version_code")]
        [StringLength(50)]
        public string AvlVersionCode { get; set; }
        [Column("avl_version")]
        [StringLength(50)]
        public string AvlVersion { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
    }
}
