﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("non_decor_price_hdr")]
    public partial class NonDecorPriceHdr
    {
        [Key]
        [Column("ndph_recipe_id")]
        public long NdphRecipeId { get; set; }
        [Column("ndph_lms_division")]
        [StringLength(50)]
        public string NdphLmsDivision { get; set; }
        [Column("ndph_sap_division")]
        [StringLength(50)]
        public string NdphSapDivision { get; set; }
        [Column("ndph_recipe_name")]
        [StringLength(50)]
        public string NdphRecipeName { get; set; }
        [Column("ndph_dft_mic_coat", TypeName = "decimal(18, 2)")]
        public decimal? NdphDftMicCoat { get; set; }
        [Column("ndph_vs", TypeName = "decimal(18, 2)")]
        public decimal? NdphVs { get; set; }
        [Column("ndph_tsr", TypeName = "decimal(18, 2)")]
        public decimal? NdphTsr { get; set; }
        [Column("ndph_lf", TypeName = "decimal(18, 2)")]
        public decimal? NdphLf { get; set; }
        [Column("ndph_practical_coverage", TypeName = "decimal(18, 2)")]
        public decimal? NdphPracticalCoverage { get; set; }
        [Column("ndph_set", TypeName = "decimal(18, 2)")]
        public decimal? NdphSet { get; set; }
        [Column("ndph_coverage_set", TypeName = "decimal(18, 2)")]
        public decimal? NdphCoverageSet { get; set; }
        [Column("ndph_total_area", TypeName = "decimal(18, 2)")]
        public decimal? NdphTotalArea { get; set; }
        [Column("ndph_total_set_req", TypeName = "decimal(18, 2)")]
        public decimal? NdphTotalSetReq { get; set; }
        [Column("ndph_per_sft_cost", TypeName = "decimal(18, 2)")]
        public decimal? NdphPerSftCost { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
