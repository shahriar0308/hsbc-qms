﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("position_registration_company")]
    public partial class PositionRegistrationCompany
    {
        [Key]
        [Column("prc_customer_type")]
        [StringLength(50)]
        public string PrcCustomerType { get; set; }
        [Key]
        [Column("prc_company_id")]
        public long PrcCompanyId { get; set; }
        [Required]
        [Column("prc_lat")]
        [StringLength(50)]
        public string PrcLat { get; set; }
        [Required]
        [Column("prc_long")]
        [StringLength(50)]
        public string PrcLong { get; set; }
        [Required]
        [Column("prc_accuracy")]
        [StringLength(50)]
        public string PrcAccuracy { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
