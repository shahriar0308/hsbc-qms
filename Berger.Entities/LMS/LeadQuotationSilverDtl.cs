﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_quotation_silver_dtl")]
    public partial class LeadQuotationSilverDtl
    {
        [Key]
        [Column("lqsd_details_id")]
        public long LqsdDetailsId { get; set; }
        [Column("lqsd_header_id")]
        public long? LqsdHeaderId { get; set; }
        [Column("lqsd_surface_code")]
        [StringLength(50)]
        public string LqsdSurfaceCode { get; set; }
        [Column("lqsd_location_code")]
        [StringLength(50)]
        public string LqsdLocationCode { get; set; }
        [Column("lqsd_category")]
        [StringLength(50)]
        public string LqsdCategory { get; set; }
        [Column("lqsd_brand")]
        [StringLength(50)]
        public string LqsdBrand { get; set; }
        [Column("lqsd_base")]
        [StringLength(50)]
        public string LqsdBase { get; set; }
        [Column("lqsd_shade_code")]
        [StringLength(50)]
        public string LqsdShadeCode { get; set; }
        [Column("lqsd_coat")]
        public int? LqsdCoat { get; set; }
        [Column("lqsd_area", TypeName = "decimal(18, 2)")]
        public decimal? LqsdArea { get; set; }
        [Column("lqsd_painter_cost", TypeName = "decimal(18, 2)")]
        public decimal? LqsdPainterCost { get; set; }
        [Column("lqsd_supervision_cost", TypeName = "decimal(18, 2)")]
        public decimal? LqsdSupervisionCost { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }


        //------------------------------------------
        [ForeignKey("LqsdHeaderId")]
        public LeadQuotationHdr LeadQuotationHdr { get; set; }
    }
}
