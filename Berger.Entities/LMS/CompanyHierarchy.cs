﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("company_hierarchy")]
    public partial class CompanyHierarchy
    {
        [Key]
        [Column("lvl_id")]
        public int LvlId { get; set; }
        [Required]
        [Column("lvl_code")]
        [StringLength(10)]
        public string LvlCode { get; set; }
        [Key]
        [Column("lvl_cm_id")]
        [StringLength(10)]
        public string LvlCmId { get; set; }
        [Column("lvl_name")]
        [StringLength(100)]
        public string LvlName { get; set; }
        [Column("lvl_sequence")]
        public int LvlSequence { get; set; }
        [Column("lvl_upper_code")]
        [StringLength(10)]
        public string LvlUpperCode { get; set; }
        [Column("lvl_lower_code")]
        [StringLength(10)]
        public string LvlLowerCode { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
