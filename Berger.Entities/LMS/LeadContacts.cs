﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_contacts")]
    public partial class LeadContacts
    {
        [Key]
        [Column("ldc_lead_guid")]
        [StringLength(50)]
        public string LdcLeadGuid { get; set; }
        [Required]
        [Column("ldc_lead_id")]
        [StringLength(50)]
        public string LdcLeadId { get; set; }
        [Required]
        [Column("ldc_contact_type")]
        [StringLength(50)]
        public string LdcContactType { get; set; }
        [Key]
        [Column("ldc_srl")]
        public int LdcSrl { get; set; }
        [Required]
        [Column("ldc_name")]
        [StringLength(50)]
        public string LdcName { get; set; }
        [Column("ldc_designation")]
        [StringLength(50)]
        public string LdcDesignation { get; set; }
        [Key]
        [Column("ldc_primary_no")]
        [StringLength(50)]
        public string LdcPrimaryNo { get; set; }
        [Column("ldc_email_id")]
        [StringLength(50)]
        public string LdcEmailId { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
