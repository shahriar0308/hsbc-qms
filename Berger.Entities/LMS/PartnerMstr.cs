﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("partner_mstr")]
    public partial class PartnerMstr
    {
        [Required]
        [Column("pm_legal_entiry")]
        [StringLength(50)]
        public string PmLegalEntiry { get; set; }
        [Key]
        [Column("pm_id")]
        public long PmId { get; set; }
        [Required]
        [Column("pm_type")]
        [StringLength(50)]
        public string PmType { get; set; }
        [Required]
        [Column("pm_firm_name")]
        [StringLength(50)]
        public string PmFirmName { get; set; }
        [Required]
        [Column("pm_name")]
        [StringLength(50)]
        public string PmName { get; set; }
        [Column("pm_business_verticals")]
        [StringLength(50)]
        public string PmBusinessVerticals { get; set; }
        [Column("pm_business_sub_verticals")]
        [StringLength(50)]
        public string PmBusinessSubVerticals { get; set; }
        [Column("pm_description")]
        public string PmDescription { get; set; }
        [Required]
        [Column("pm_address_line1")]
        [StringLength(250)]
        public string PmAddressLine1 { get; set; }
        [Column("pm_address_line2")]
        [StringLength(250)]
        public string PmAddressLine2 { get; set; }
        [Column("pm_address_line3")]
        [StringLength(250)]
        public string PmAddressLine3 { get; set; }
        [Column("pm_city")]
        [StringLength(50)]
        public string PmCity { get; set; }
        [Column("pm_state")]
        [StringLength(50)]
        public string PmState { get; set; }
        [Column("pm_bibhag")]
        public int? PmBibhag { get; set; }
        [Column("pm_district")]
        public int? PmDistrict { get; set; }
        [Column("pm_pin")]
        [StringLength(50)]
        public string PmPin { get; set; }
        [Column("pm_police_station")]
        public int? PmPoliceStation { get; set; }
        [Column("pm_primary_contact_number")]
        [StringLength(15)]
        public string PmPrimaryContactNumber { get; set; }
        [Column("pm_primary_email")]
        [StringLength(30)]
        public string PmPrimaryEmail { get; set; }
        [Column("pm_fax")]
        [StringLength(30)]
        public string PmFax { get; set; }
        [Column("pm_website")]
        [StringLength(30)]
        public string PmWebsite { get; set; }
        [Column("pm_annual_turn_over", TypeName = "decimal(18, 2)")]
        public decimal? PmAnnualTurnOver { get; set; }
        [Column("pm_rating")]
        [StringLength(50)]
        public string PmRating { get; set; }
        [Column("pm_multi_location")]
        [StringLength(50)]
        public string PmMultiLocation { get; set; }
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
