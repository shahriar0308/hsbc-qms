﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("dealer_position_registration_updated_deleted")]
    public partial class DealerPositionRegistrationUpdatedDeleted
    {
        [Column("dpr_id")]
        public long DprId { get; set; }
        [Required]
        [Column("dpr_dealer_id")]
        [StringLength(50)]
        public string DprDealerId { get; set; }
        [Column("dpr_lat")]
        [StringLength(50)]
        public string DprLat { get; set; }
        [Column("dpr_long")]
        [StringLength(50)]
        public string DprLong { get; set; }
        [Column("dpr_accuracy")]
        [StringLength(50)]
        public string DprAccuracy { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("deleted_on", TypeName = "datetime")]
        public DateTime? DeletedOn { get; set; }
    }
}
