﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_mstr_competitor")]
    public partial class LeadMstrCompetitor
    {
        [Key]
        [Column("lc_id")]
        public long LcId { get; set; }
        [Required]
        [Column("lc_lead_guid")]
        [StringLength(50)]
        public string LcLeadGuid { get; set; }
        [Required]
        [Column("lc_lead_id")]
        [StringLength(50)]
        public string LcLeadId { get; set; }
        [Column("lc_competitor_code")]
        [StringLength(50)]
        public string LcCompetitorCode { get; set; }
        [Column("lc_position_code")]
        [StringLength(50)]
        public string LcPositionCode { get; set; }
        [Column("lc_credit_limit_amount", TypeName = "decimal(18, 2)")]
        public decimal? LcCreditLimitAmount { get; set; }
        [Column("lc_credit_limit_days")]
        public int? LcCreditLimitDays { get; set; }
        [Column("lc_discount", TypeName = "decimal(18, 2)")]
        public decimal? LcDiscount { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
