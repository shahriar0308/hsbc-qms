﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_lost_value")]
    public partial class LeadLostValue
    {
        [Key]
        [Column("llv_id")]
        public long LlvId { get; set; }
        [Required]
        [Column("llv_lead_detail_guid")]
        [StringLength(50)]
        public string LlvLeadDetailGuid { get; set; }
        [Required]
        [Column("llv_item_group")]
        [StringLength(50)]
        public string LlvItemGroup { get; set; }
        [Column("llv_lost_volume", TypeName = "decimal(18, 2)")]
        public decimal LlvLostVolume { get; set; }
        [Column("llv_lost_value", TypeName = "decimal(18, 2)")]
        public decimal LlvLostValue { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
