﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("user_device_registration_delete")]
    public partial class UserDeviceRegistrationDelete
    {
        [Key]
        [Column("udr_id")]
        public long UdrId { get; set; }
        [Required]
        [Column("udr_user_id")]
        [StringLength(20)]
        public string UdrUserId { get; set; }
        [Required]
        [Column("udr_uuid")]
        [StringLength(200)]
        public string UdrUuid { get; set; }
        [Column("udr_model")]
        [StringLength(200)]
        public string UdrModel { get; set; }
        [Column("udr_cordova")]
        [StringLength(200)]
        public string UdrCordova { get; set; }
        [Column("udr_platform")]
        [StringLength(200)]
        public string UdrPlatform { get; set; }
        [Column("udr_version")]
        [StringLength(200)]
        public string UdrVersion { get; set; }
        [Column("udr_name")]
        [StringLength(200)]
        public string UdrName { get; set; }
        [Column("udr_registration_status")]
        [StringLength(200)]
        public string UdrRegistrationStatus { get; set; }
        [Column("udr_fcm_token")]
        public string UdrFcmToken { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("inserted_date", TypeName = "datetime")]
        public DateTime? InsertedDate { get; set; }
    }
}
