﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_complaint_mstr")]
    public partial class LeadComplaintMstr
    {
        [Key]
        [Column("lcm_sr_id")]
        public long LcmSrId { get; set; }
        [Required]
        [Column("lcm_lead_detail_guid")]
        [StringLength(50)]
        public string LcmLeadDetailGuid { get; set; }
        [Required]
        [Column("lcm_lead_detail_id")]
        [StringLength(50)]
        public string LcmLeadDetailId { get; set; }
        [Required]
        [Column("lcm_sr_code")]
        [StringLength(50)]
        public string LcmSrCode { get; set; }
        [Required]
        [Column("lcm_type")]
        [StringLength(50)]
        public string LcmType { get; set; }
        [Column("lcm_sub_type")]
        [StringLength(50)]
        public string LcmSubType { get; set; }
        [Column("lcm_description")]
        public string LcmDescription { get; set; }
        [Column("lcm_date", TypeName = "datetime")]
        public DateTime LcmDate { get; set; }
        [Column("lcm_last_action_taken")]
        public string LcmLastActionTaken { get; set; }
        [Column("lcm_last_action_taken_on", TypeName = "datetime")]
        public DateTime? LcmLastActionTakenOn { get; set; }
        [Column("lcm_last_action_teken_by")]
        [StringLength(20)]
        public string LcmLastActionTekenBy { get; set; }
        [Column("lcm_status")]
        [StringLength(50)]
        public string LcmStatus { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
