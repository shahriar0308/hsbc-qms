﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("relationship_call_hdr")]
    public partial class RelationshipCallHdr
    {
        [Key]
        [Column("rch_id")]
        public long RchId { get; set; }
        [Column("rch_category")]
        [StringLength(20)]
        public string RchCategory { get; set; }
        [Column("rch_type_id")]
        [StringLength(50)]
        public string RchTypeId { get; set; }
        [Column("rch_date", TypeName = "datetime")]
        public DateTime RchDate { get; set; }
        [Required]
        [Column("rch_user")]
        [StringLength(50)]
        public string RchUser { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
