﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("cluster_hdr")]
    public partial class ClusterHdr
    {
        [Required]
        [Column("cl_legal_entity")]
        [StringLength(50)]
        public string ClLegalEntity { get; set; }
        [Key]
        [Column("cl_code")]
        [StringLength(50)]
        public string ClCode { get; set; }
        [Required]
        [Column("cl_division")]
        [StringLength(50)]
        public string ClDivision { get; set; }
        [Required]
        [Column("cl_description")]
        public string ClDescription { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
