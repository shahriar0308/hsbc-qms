﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("cluster_dtls")]
    public partial class ClusterDtls
    {
        [Key]
        [Column("cld_code")]
        [StringLength(50)]
        public string CldCode { get; set; }
        [Key]
        [Column("cld_depot_code")]
        [StringLength(50)]
        public string CldDepotCode { get; set; }
        [Required]
        [Column("cld_territory_code")]
        [StringLength(50)]
        public string CldTerritoryCode { get; set; }
        [Required]
        [Column("cld_zone_code")]
        [StringLength(50)]
        public string CldZoneCode { get; set; }
        [Key]
        [Column("cld_dealer_code")]
        [StringLength(50)]
        public string CldDealerCode { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
