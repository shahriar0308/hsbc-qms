﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("journey_planning_header")]
    public partial class JourneyPlanningHeader
    {
        [Key]
        [Column("jph_date", TypeName = "date")]
        public DateTime JphDate { get; set; }
        [Key]
        [Column("jph_user")]
        [StringLength(20)]
        public string JphUser { get; set; }
        [Required]
        [Column("jph_approval_status")]
        [StringLength(10)]
        public string JphApprovalStatus { get; set; }
        [Column("jph_approved_by")]
        [StringLength(20)]
        public string JphApprovedBy { get; set; }
        [Column("jph_approved_on", TypeName = "datetime")]
        public DateTime? JphApprovedOn { get; set; }
        [Column("jph_deviation_occured")]
        [StringLength(1)]
        public string JphDeviationOccured { get; set; }
        [Column("jph_deviation_perc", TypeName = "decimal(10, 2)")]
        public decimal? JphDeviationPerc { get; set; }
        [Column("jph_remarks")]
        [StringLength(200)]
        public string JphRemarks { get; set; }
        [Column("jph_approval_remarks")]
        [StringLength(200)]
        public string JphApprovalRemarks { get; set; }
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
