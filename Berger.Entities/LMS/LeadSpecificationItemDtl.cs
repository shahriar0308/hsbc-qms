﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_specification_item_dtl")]
    public partial class LeadSpecificationItemDtl
    {
        [Key]
        [Column("lsid_id")]
        public long LsidId { get; set; }
        [Column("lsid_hdr_id")]
        public long? LsidHdrId { get; set; }
        [Column("lsid_dtl_id")]
        public long? LsidDtlId { get; set; }
        [Column("lsid_category")]
        [StringLength(50)]
        public string LsidCategory { get; set; }
        [Column("lsid_item_id")]
        [StringLength(50)]
        public string LsidItemId { get; set; }
        [Column("lsid_coat")]
        public long? LsidCoat { get; set; }
        [Column("lsid_area", TypeName = "decimal(18, 2)")]
        public decimal? LsidArea { get; set; }
        [Column("lsid_area_sft", TypeName = "decimal(18, 2)")]
        public decimal? LsidAreaSft { get; set; }
        [Column("lsid_dft", TypeName = "decimal(18, 2)")]
        public decimal? LsidDft { get; set; }
        [Column("lsid_tsr", TypeName = "decimal(18, 2)")]
        public decimal? LsidTsr { get; set; }
        [Column("lsid_vs", TypeName = "decimal(18, 2)")]
        public decimal? LsidVs { get; set; }
        [Column("lsid_lf", TypeName = "decimal(18, 2)")]
        public decimal? LsidLf { get; set; }
        [Column("lsid_theoriitical_con", TypeName = "decimal(18, 2)")]
        public decimal? LsidTheoriiticalCon { get; set; }
        [Column("lsid_practical_con", TypeName = "decimal(18, 2)")]
        public decimal? LsidPracticalCon { get; set; }
        [Column("lsid_practical_con_coat", TypeName = "decimal(18, 2)")]
        public decimal? LsidPracticalConCoat { get; set; }
        [Column("lsid_packsize", TypeName = "decimal(18, 2)")]
        public decimal? LsidPacksize { get; set; }
        [Column("lsid_req_packsize", TypeName = "decimal(18, 2)")]
        public decimal? LsidReqPacksize { get; set; }
        [Column("lsid_mrp", TypeName = "decimal(18, 2)")]
        public decimal? LsidMrp { get; set; }
        [Column("lsid_total_amount", TypeName = "decimal(18, 2)")]
        public decimal? LsidTotalAmount { get; set; }
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
