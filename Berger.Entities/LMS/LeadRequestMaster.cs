﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_request_master")]
    public partial class LeadRequestMaster
    {
        [Key]
        [Column("lrm_id")]
        public long LrmId { get; set; }
        [Key]
        [Column("lrm_lead_detail_guid")]
        [StringLength(50)]
        public string LrmLeadDetailGuid { get; set; }
        [Column("lrm_request_to")]
        [StringLength(50)]
        public string LrmRequestTo { get; set; }
        [Column("lrm_description")]
        [StringLength(200)]
        public string LrmDescription { get; set; }
        [Column("lrm_action")]
        [StringLength(50)]
        public string LrmAction { get; set; }
        [Column("lrm_remark")]
        [StringLength(200)]
        public string LrmRemark { get; set; }
        [Column("lrm_action_assigned_cluster")]
        [StringLength(50)]
        public string LrmActionAssignedCluster { get; set; }
        [Column("lrm_action_taken_by")]
        [StringLength(50)]
        public string LrmActionTakenBy { get; set; }
        [Column("lrm_action_on", TypeName = "datetime")]
        public DateTime? LrmActionOn { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
