﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_partners")]
    public partial class LeadPartners
    {
        [Key]
        [Column("lp_lead_detail_guid")]
        [StringLength(50)]
        public string LpLeadDetailGuid { get; set; }
        [Key]
        [Column("lp_partner_contact_id")]
        [StringLength(50)]
        public string LpPartnerContactId { get; set; }
        [Required]
        [Column("lp_lead_detail_id")]
        [StringLength(50)]
        public string LpLeadDetailId { get; set; }
        [Column("lp_company_id")]
        [StringLength(50)]
        public string LpCompanyId { get; set; }
        [Required]
        [Column("lp_partner_id")]
        [StringLength(50)]
        public string LpPartnerId { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("lp_cluster_id")]
        [StringLength(100)]
        public string LpClusterId { get; set; }
        [Column("lp_dealer_id")]
        [StringLength(100)]
        public string LpDealerId { get; set; }
    }
}
