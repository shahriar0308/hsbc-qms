﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_mstr_hdr")]
    public class LeadMstrHdr
    {
        [Column("ld_legal_entity_id")]
        public int LdLegalEntityId { get; set; }
        [Key]
        [Column("ld_lead_guid")]
        [StringLength(50)]
        public string LdLeadGuid { get; set; }
        [Required]
        [Column("ld_lead_id")]
        [StringLength(50)]
        public string LdLeadId { get; set; }
        [Required]
        [Column("ld_name")]
        [StringLength(100)]
        public string LdName { get; set; }
        [Column("ld_organization")]
        [StringLength(100)]
        public string LdOrganization { get; set; }
        [Column("ld_type")]
        [StringLength(100)]
        public string LdType { get; set; }
        [Column("ld_lead_opportunity")]
        [StringLength(50)]
        public string LdLeadOpportunity { get; set; }
        [Column("ld_date", TypeName = "datetime")]
        public DateTime? LdDate { get; set; }
        [Column("ld_priority")]
        [StringLength(100)]
        public string LdPriority { get; set; }
        [Column("ld_condition")]
        [StringLength(50)]
        public string LdCondition { get; set; }
        [Required]
        [Column("ld_stage")]
        [StringLength(100)]
        public string LdStage { get; set; }
        [Column("ld_location")]
        [StringLength(250)]
        public string LdLocation { get; set; }
        [Column("ld_description")]
        public string LdDescription { get; set; }
        [Column("ld_address_line1")]
        [StringLength(250)]
        public string LdAddressLine1 { get; set; }
        [Column("ld_address_line2")]
        [StringLength(250)]
        public string LdAddressLine2 { get; set; }
        [Column("ld_address_line3")]
        [StringLength(250)]
        public string LdAddressLine3 { get; set; }
        [Column("ld_city")]
        [StringLength(50)]
        public string LdCity { get; set; }
        [Column("ld_state")]
        [StringLength(50)]
        public string LdState { get; set; }
        [Column("ld_district")]
        public int? LdDistrict { get; set; }
        [Column("ld_pin")]
        [StringLength(50)]
        public string LdPin { get; set; }
        [Column("ld_police_station")]
        public int? LdPoliceStation { get; set; }
        [Column("ld_source")]
        [StringLength(100)]
        public string LdSource { get; set; }
        [Column("ld_primary_contact_number")]
        [StringLength(50)]
        public string LdPrimaryContactNumber { get; set; }
        [Column("ld_primary_email")]
        [StringLength(50)]
        public string LdPrimaryEmail { get; set; }
        [Column("ld_job_value", TypeName = "decimal(18, 2)")]
        public decimal? LdJobValue { get; set; }
        [Column("ld_material_value", TypeName = "decimal(18, 2)")]
        public decimal? LdMaterialValue { get; set; }
        [Column("ld_labour_value", TypeName = "decimal(18, 2)")]
        public decimal? LdLabourValue { get; set; }
        [Column("ld_job_value_actual", TypeName = "decimal(18, 2)")]
        public decimal? LdJobValueActual { get; set; }
        [Column("ld_material_value_actual", TypeName = "decimal(18, 2)")]
        public decimal? LdMaterialValueActual { get; set; }
        [Column("ld_labour_value_actual", TypeName = "decimal(18, 2)")]
        public decimal? LdLabourValueActual { get; set; }
        [Column("ld_remittance", TypeName = "decimal(18, 2)")]
        public decimal? LdRemittance { get; set; }
        [Column("ld_complete_date", TypeName = "datetime")]
        public DateTime? LdCompleteDate { get; set; }
        [Column("ld_csat")]
        [StringLength(50)]
        public string LdCsat { get; set; }
        [Column("ld_attribute1")]
        [StringLength(50)]
        public string LdAttribute1 { get; set; }
        [Column("ld_attribute2")]
        [StringLength(50)]
        public string LdAttribute2 { get; set; }
        [Column("ld_attribute3")]
        [StringLength(50)]
        public string LdAttribute3 { get; set; }
        [Column("ld_attribute4")]
        [StringLength(50)]
        public string LdAttribute4 { get; set; }
        [Column("ld_attribute5")]
        [StringLength(50)]
        public string LdAttribute5 { get; set; }
        [Column("ld_attribute6")]
        public int? LdAttribute6 { get; set; }
        [Column("ld_attribute7")]
        public int? LdAttribute7 { get; set; }
        [Column("ld_attribute8", TypeName = "decimal(18, 0)")]
        public decimal? LdAttribute8 { get; set; }
        [Column("ld_attribute9", TypeName = "decimal(18, 0)")]
        public decimal? LdAttribute9 { get; set; }
        [Column("ld_attribute10", TypeName = "decimal(18, 0)")]
        public decimal? LdAttribute10 { get; set; }
        [Column("ld_attribute11", TypeName = "datetime")]
        public DateTime? LdAttribute11 { get; set; }
        [Column("ld_attribute12", TypeName = "datetime")]
        public DateTime? LdAttribute12 { get; set; }
        [Column("ld_attribute13", TypeName = "datetime")]
        public DateTime? LdAttribute13 { get; set; }
        [Column("ld_attribute14", TypeName = "datetime")]
        public DateTime? LdAttribute14 { get; set; }
        [Column("ld_attribute15", TypeName = "datetime")]
        public DateTime? LdAttribute15 { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("ld_bibhag")]
        public int? LdBibhag { get; set; }


        //------------------------------------------
        public IList<LeadMstrDtl> LeadMstrDtls { get; set; }
        public LeadMstrHdr()
        {
            this.LeadMstrDtls = new List<LeadMstrDtl>();
        }
    }
}
