﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_application_requisition_dtl")]
    public partial class LeadApplicationRequisitionDtl
    {
        [Key]
        [Column("lardt_details_id")]
        public long LardtDetailsId { get; set; }
        [Column("lardt_header_id")]
        public long LardtHeaderId { get; set; }
        [Column("lardt_quotation_hdr_id")]
        public long LardtQuotationHdrId { get; set; }
        [Column("lardt_item_id")]
        [StringLength(50)]
        public string LardtItemId { get; set; }
        [Column("lardt_application_code")]
        [StringLength(50)]
        public string LardtApplicationCode { get; set; }
        [Column("lardt_application_cost", TypeName = "decimal(18, 2)")]
        public decimal? LardtApplicationCost { get; set; }
        [Column("lardt_area", TypeName = "decimal(18, 3)")]
        public decimal? LardtArea { get; set; }
        [Column("lardt_total_cost", TypeName = "decimal(18, 2)")]
        public decimal? LardtTotalCost { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
