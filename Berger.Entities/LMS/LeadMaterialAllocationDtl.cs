﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_material_allocation_dtl")]
    public partial class LeadMaterialAllocationDtl
    {
        [Key]
        [Column("lmad_details_id")]
        public long LmadDetailsId { get; set; }
        [Column("lmad_header_id")]
        public long LmadHeaderId { get; set; }
        [Column("lmad_requisition_details_id")]
        public long? LmadRequisitionDetailsId { get; set; }
        [Column("lmad_product_id")]
        [StringLength(50)]
        public string LmadProductId { get; set; }
        [Column("lmad_qty", TypeName = "decimal(18, 3)")]
        public decimal? LmadQty { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
