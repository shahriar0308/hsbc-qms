﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("mobile_applicable_menu")]
    public partial class MobileApplicableMenu
    {
        [Required]
        [Column("mam_division")]
        [StringLength(20)]
        public string MamDivision { get; set; }
        [Key]
        [Column("mam_user_group_id")]
        public int MamUserGroupId { get; set; }
        [Key]
        [Column("mam_user_id")]
        [StringLength(20)]
        public string MamUserId { get; set; }
        [Key]
        [Column("mam_menu_id")]
        public int MamMenuId { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
