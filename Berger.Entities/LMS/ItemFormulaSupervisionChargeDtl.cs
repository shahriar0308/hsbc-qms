﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("item_formula_supervision_charge_dtl")]
    public partial class ItemFormulaSupervisionChargeDtl
    {
        [Key]
        [Column("item_formula_application_id")]
        public long ItemFormulaApplicationId { get; set; }
        [Column("item_formula_id")]
        public long ItemFormulaId { get; set; }
        [Column("item_recipe_id")]
        public long ItemRecipeId { get; set; }
        [Column("item_supervision_code")]
        [StringLength(50)]
        public string ItemSupervisionCode { get; set; }
        [Column("item_supervision_cost", TypeName = "decimal(18, 2)")]
        public decimal? ItemSupervisionCost { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
