﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_visit_entry")]
    public partial class LeadVisitEntry
    {
        [Key]
        [Column("lve_id")]
        public long LveId { get; set; }
        [Column("lve_lead_dtl_guid")]
        [StringLength(50)]
        public string LveLeadDtlGuid { get; set; }
        [Column("lve_check_in_long")]
        [StringLength(50)]
        public string LveCheckInLong { get; set; }
        [Column("lve_check_in_lat")]
        [StringLength(50)]
        public string LveCheckInLat { get; set; }
        [Column("lve_check_in_time", TypeName = "datetime")]
        public DateTime? LveCheckInTime { get; set; }
        [Column("lve_check_in_accuracy")]
        [StringLength(50)]
        public string LveCheckInAccuracy { get; set; }
        [Column("lve_check_out_long")]
        [StringLength(50)]
        public string LveCheckOutLong { get; set; }
        [Column("lve_check_out_lat")]
        [StringLength(50)]
        public string LveCheckOutLat { get; set; }
        [Column("lve_check_out_time", TypeName = "datetime")]
        public DateTime? LveCheckOutTime { get; set; }
        [Column("lve_check_out_accuracy")]
        [StringLength(50)]
        public string LveCheckOutAccuracy { get; set; }
        [Column("lve_spent_min")]
        public int? LveSpentMin { get; set; }
        [Column("lve_activity_others")]
        [StringLength(100)]
        public string LveActivityOthers { get; set; }
        [Column("lve_points_discussed")]
        public string LvePointsDiscussed { get; set; }
        [Column("lve_activities_req")]
        public string LveActivitiesReq { get; set; }
        [Column("lve_comp_activity")]
        public string LveCompActivity { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("lve_next_followup_date", TypeName = "datetime")]
        public DateTime? LveNextFollowupDate { get; set; }
    }
}
