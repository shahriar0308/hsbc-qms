﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("visit_activity_mstr")]
    public partial class VisitActivityMstr
    {
        [Column("vam_legal_entity")]
        public int? VamLegalEntity { get; set; }
        [Key]
        [Column("vam_id")]
        public long VamId { get; set; }
        [Column("vam_type")]
        [StringLength(50)]
        public string VamType { get; set; }
        [Column("vam_type_stage")]
        [StringLength(50)]
        public string VamTypeStage { get; set; }
        [Column("vam_srl_no")]
        [StringLength(50)]
        public string VamSrlNo { get; set; }
        [Column("vam_type_name")]
        [StringLength(50)]
        public string VamTypeName { get; set; }
        [Column("vam_hard_coded")]
        public int? VamHardCoded { get; set; }
        [Column("vam_child_form")]
        [StringLength(30)]
        public string VamChildForm { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
