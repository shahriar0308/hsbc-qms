﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("user_otp")]
    public partial class UserOtp
    {
        [Key]
        [Column("o_id")]
        public long OId { get; set; }
        [Required]
        [Column("o_user_id")]
        [StringLength(20)]
        public string OUserId { get; set; }
        [Required]
        [Column("o_device_id")]
        [StringLength(200)]
        public string ODeviceId { get; set; }
        [Required]
        [Column("o_otp")]
        [StringLength(6)]
        public string OOtp { get; set; }
        [Column("o_email")]
        [StringLength(100)]
        public string OEmail { get; set; }
        [Column("o_mobile")]
        [StringLength(20)]
        public string OMobile { get; set; }
        [Column("o_otp_valid_till", TypeName = "datetime")]
        public DateTime? OOtpValidTill { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
    }
}
