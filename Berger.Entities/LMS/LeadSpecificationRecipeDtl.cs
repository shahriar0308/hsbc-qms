﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_specification_recipe_dtl")]
    public partial class LeadSpecificationRecipeDtl
    {
        [Key]
        [Column("lsrd_id")]
        public long LsrdId { get; set; }
        [Column("lsrd_hdr_id")]
        public long? LsrdHdrId { get; set; }
        [Column("lsrd_recipe_id")]
        public long? LsrdRecipeId { get; set; }
        [Column("lsrd_title")]
        public string LsrdTitle { get; set; }
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("lsrd_title2")]
        [StringLength(50)]
        public string LsrdTitle2 { get; set; }
    }
}
