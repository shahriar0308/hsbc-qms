﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("district_mstr")]
    public partial class DistrictMstr
    {
        [Column("dst_legal_entity_id")]
        public int? DstLegalEntityId { get; set; }
        [Column("dst_id")]
        public int DstId { get; set; }
        [Key]
        [Column("dst_name")]
        [StringLength(100)]
        public string DstName { get; set; }
        [Key]
        [Column("dst_bibhag_id")]
        public int DstBibhagId { get; set; }
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
