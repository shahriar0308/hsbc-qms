﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("cb_item_hdr")]
    public partial class CbItemHdr
    {
        [Column("cb_id")]
        public long CbId { get; set; }
        [Column("cb_item_name")]
        [StringLength(50)]
        public string CbItemName { get; set; }
        [Key]
        [Column("cb_item_code")]
        [StringLength(50)]
        public string CbItemCode { get; set; }
        [Key]
        [Column("cb_item_base")]
        [StringLength(50)]
        public string CbItemBase { get; set; }
        [Key]
        [Column("cb_item_group")]
        [StringLength(50)]
        public string CbItemGroup { get; set; }
        [Column("cb_item_shade")]
        [StringLength(50)]
        public string CbItemShade { get; set; }
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
