﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_supervision_dtl")]
    public partial class LeadSupervisionDtl
    {
        [Key]
        [Column("lsd_hdr_id")]
        public long LsdHdrId { get; set; }
        [Key]
        [Column("lsd_qns_id")]
        public long LsdQnsId { get; set; }
        [Column("lsd_entered_ans")]
        public string LsdEnteredAns { get; set; }
        [Column("lsd_selected_ans")]
        public long? LsdSelectedAns { get; set; }
        [Column("lsd_img")]
        public string LsdImg { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
