﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("user_tracking_history")]
    public partial class UserTrackingHistory
    {
        [Key]
        [Column("uth_id")]
        public long UthId { get; set; }
        [Required]
        [Column("uth_user_id")]
        [StringLength(20)]
        public string UthUserId { get; set; }
        [Required]
        [Column("uth_uuid")]
        [StringLength(200)]
        public string UthUuid { get; set; }
        [Column("uth_timestamp", TypeName = "datetime")]
        public DateTime? UthTimestamp { get; set; }
        [Column("uth_longitude")]
        [StringLength(50)]
        public string UthLongitude { get; set; }
        [Column("uth_latitude")]
        [StringLength(50)]
        public string UthLatitude { get; set; }
        [Column("uth_recorded_at")]
        [StringLength(50)]
        public string UthRecordedAt { get; set; }
        [Column("uth_speed")]
        [StringLength(50)]
        public string UthSpeed { get; set; }
        [Column("uth_accuracy")]
        [StringLength(50)]
        public string UthAccuracy { get; set; }
        [Column("uth_bearing")]
        [StringLength(50)]
        public string UthBearing { get; set; }
        [Column("uth_altitude")]
        [StringLength(50)]
        public string UthAltitude { get; set; }
        [Column("uth_activity_type")]
        [StringLength(50)]
        public string UthActivityType { get; set; }
        [Column("uth_activity_confidence")]
        [StringLength(50)]
        public string UthActivityConfidence { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("uth_is_battary_charging")]
        [StringLength(50)]
        public string UthIsBattaryCharging { get; set; }
        [Column("uth_battery_level")]
        [StringLength(50)]
        public string UthBatteryLevel { get; set; }
        [Column("uth_is_moving")]
        [StringLength(50)]
        public string UthIsMoving { get; set; }
    }
}
