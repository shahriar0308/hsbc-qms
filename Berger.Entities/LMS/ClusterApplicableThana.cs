﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("cluster_applicable_thana")]
    public partial class ClusterApplicableThana
    {
        [Key]
        [Column("cat_cl_code")]
        [StringLength(50)]
        public string CatClCode { get; set; }
        [Key]
        [Column("cat_thana_id")]
        public int CatThanaId { get; set; }
        [Key]
        [Column("cat_depot_code")]
        [StringLength(50)]
        public string CatDepotCode { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(10)]
        public string Active { get; set; }
    }
}
