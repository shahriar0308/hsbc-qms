﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("bibhag_mstr")]
    public partial class BibhagMstr
    {
        [Column("bbg_legal_entity_id")]
        public int? BbgLegalEntityId { get; set; }
        [Column("bbg_id")]
        public int BbgId { get; set; }
        [Key]
        [Column("bbg_name")]
        [StringLength(100)]
        public string BbgName { get; set; }
        [Column("created_user")]
        [StringLength(50)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
