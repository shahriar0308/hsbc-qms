﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("partner_docs")]
    public partial class PartnerDocs
    {
        [Required]
        [Column("pd_pm_code")]
        [StringLength(10)]
        public string PdPmCode { get; set; }
        [Key]
        [Column("pd_doc_id")]
        public int PdDocId { get; set; }
        [Column("pd_doc_type")]
        [StringLength(50)]
        public string PdDocType { get; set; }
        [Column("pd_doc_description")]
        [StringLength(200)]
        public string PdDocDescription { get; set; }
        [Column("pd_doc_path")]
        [StringLength(200)]
        public string PdDocPath { get; set; }
        [Column("pd_doc_extension")]
        [StringLength(50)]
        public string PdDocExtension { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
