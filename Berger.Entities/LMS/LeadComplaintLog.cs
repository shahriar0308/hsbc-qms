﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_complaint_log")]
    public partial class LeadComplaintLog
    {
        [Column("lcl_lcm_sr_id")]
        public long LclLcmSrId { get; set; }
        [Key]
        [Column("lcm_id")]
        public long LcmId { get; set; }
        [Column("lcm_action_taken")]
        public string LcmActionTaken { get; set; }
        [Column("lcm_action_taken_on", TypeName = "datetime")]
        public DateTime? LcmActionTakenOn { get; set; }
        [Column("lcm_action_teken_by")]
        [StringLength(20)]
        public string LcmActionTekenBy { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("lcl_action_taken_id")]
        [StringLength(100)]
        public string LclActionTakenId { get; set; }
    }
}
