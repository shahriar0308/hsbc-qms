﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("terms_conditions_master")]
    public partial class TermsConditionsMaster
    {
        [Key]
        [Column("tcm_id")]
        public int TcmId { get; set; }
        [Column("tcm_legal_entity")]
        public int TcmLegalEntity { get; set; }
        [Required]
        [Column("tcm_division")]
        [StringLength(50)]
        public string TcmDivision { get; set; }
        [Column("tcm_terms_conditions")]
        public string TcmTermsConditions { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(10)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(50)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(50)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
