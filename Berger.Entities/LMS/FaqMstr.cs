﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("faq_mstr")]
    public partial class FaqMstr
    {
        [Column("faq_division_code")]
        [StringLength(50)]
        public string FaqDivisionCode { get; set; }
        [Key]
        [Column("faq_id")]
        public int FaqId { get; set; }
        [Column("faq_question")]
        public string FaqQuestion { get; set; }
        [Column("faq_answer")]
        public string FaqAnswer { get; set; }
        [Column("faq_seq")]
        public int? FaqSeq { get; set; }
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
    }
}
