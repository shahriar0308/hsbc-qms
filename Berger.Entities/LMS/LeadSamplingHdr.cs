﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Berger.Entities.LMS
{
    [Table("lead_sampling_hdr")]
    public partial class LeadSamplingHdr
    {
        [Key]
        [Column("lsh_lead_detail_guid")]
        [StringLength(50)]
        public string LshLeadDetailGuid { get; set; }
        [Required]
        [Column("lsh_lead_detail_id")]
        [StringLength(50)]
        public string LshLeadDetailId { get; set; }
        [Key]
        [Column("lsh_header_id")]
        public long LshHeaderId { get; set; }
        [Required]
        [Column("lsh_type")]
        [StringLength(50)]
        public string LshType { get; set; }
        [Column("lsh_demo_date", TypeName = "datetime")]
        public DateTime LshDemoDate { get; set; }
        [Required]
        [Column("lsh_place")]
        [StringLength(100)]
        public string LshPlace { get; set; }
        [Column("lsh_contact_id")]
        [StringLength(50)]
        public string LshContactId { get; set; }
        [Column("lsh_stage")]
        [StringLength(50)]
        public string LshStage { get; set; }
        [Column("lsh_action_taken")]
        [StringLength(100)]
        public string LshActionTaken { get; set; }
        [Required]
        [Column("created_user")]
        [StringLength(20)]
        public string CreatedUser { get; set; }
        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Column("modified_user")]
        [StringLength(20)]
        public string ModifiedUser { get; set; }
        [Column("modified_date", TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [Column("deleted_user")]
        [StringLength(20)]
        public string DeletedUser { get; set; }
        [Column("deleted_date", TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [Required]
        [Column("active")]
        [StringLength(1)]
        public string Active { get; set; }
        [Column("lsh_surface_type")]
        [StringLength(50)]
        public string LshSurfaceType { get; set; }
        [Column("lsh_outcome")]
        [StringLength(50)]
        public string LshOutcome { get; set; }
        [Column("lsh_competetor")]
        [StringLength(50)]
        public string LshCompetetor { get; set; }
    }
}
