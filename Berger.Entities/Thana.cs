﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Berger.Entities
{
    public class Thana : AuditableEntity<int>
    {
        public string Name { get; set; }

        [ForeignKey("DistrictId")]
        public virtual District District { get; set; }
        public int DistrictId { get; set; }
    }
}
