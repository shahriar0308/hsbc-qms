﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Berger.Entities
{
    public class Dealer: AuditableEntity<int>
    {
        public string Name { get; set; }
        public string Address { get; set; }

        [ForeignKey("DivisionId")]
        public virtual Division Division { get; set; }
        public int DivisionId { get; set; }

        [ForeignKey("DistrictId")]
        public virtual District District { get; set; }
        public int DistrictId { get; set; }
        [Column(TypeName = "decimal(13,10)")]
        public decimal Latitude { get; set; }
        [Column(TypeName = "decimal(13,10)")]
        public decimal Longitude { get; set; }
    }
}
