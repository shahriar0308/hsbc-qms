﻿using Berger.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Entities
{
    public class LoginHistory : Entity<int>
    {
        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string FCMToken { get; set; }
        public bool IsLoggedIn { get; set; }
        public DateTime LoggedInDate { get; set; }
        public DateTime? LoggedOutDate { get; set; }
    }
}
