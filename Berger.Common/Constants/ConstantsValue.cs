﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Common.Constants
{
    public static class ConstantsValue
    {
        public static UserRoleName UserRoleName => new UserRoleName();
        public static RolePermission RolePermission => new RolePermission();
        public static string ProductImageLocation = "Resources/Images/Products/";
        public static string BannerImageLocation = "Resources/Images/Banners/";
    }

    public class UserRoleName
    {
        public string SuperAdmin => "SuperAdmin";
        public string Admin => "Admin";
        public string GeneralUser => "GeneralUser";
        public string AppUser => "AppUser";
    }

    public class RolePermission
    {
        public string Type => "Permission";
        public string Value => "Permissions.SuperAdmin";
    }
    public static class SingleValuedEntities
    {
        public static string TermsAndConditions => "TNC";
    }

    public static class LeadStages
    {
        public static string NEW => "NEW";
        public static string WIP => "WIP";
        public static string WON => "WON";
        public static string LOST => "LOST";
        public static string COMPLETED => "COMPLETED";
        public static string CLOSED => "CLOSED";
    }

    public static class NotificationTemplates
    {
        public static string ColorConsultantAssigned => "{0} has been assigned on your Service Request # {1}";
    }
}
