﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Common.Enums
{
    public enum EnumApplicationUserStatus
    {
        SuperAdmin = 1,
        GeneralUser = 2,
    }
}
