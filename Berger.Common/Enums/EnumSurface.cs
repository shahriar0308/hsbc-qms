

namespace Berger.Common.Enums
{
    public enum EnumSurfaceType
    {
        Interior = 1,
        Exterior = 2,
        Wood_Metal = 3
    }
    public enum EnumSurfaceCondition
    {
        New = 1,
        Old = 2
    }
}
