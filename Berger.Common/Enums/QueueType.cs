﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Common.Enums
{
    public enum QueueType
    {
        Exclusive=1,
        General=2
    }
}
