﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Common.Enums
{
    public enum CustomerType
    {
        Select=1,
        CEPS=2,
        Staff=3,
        Corporate,
        NRB,
        MASS

    }
}
