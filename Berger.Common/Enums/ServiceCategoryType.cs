﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Common.Enums
{
    public enum ServiceCategoryType
    {
        Account_Service=1,
        Card_Service=2,
        Bond_Service,
        PIB_Sevice,
        Endorsement_Service,
        Loan_Service

    }
}
