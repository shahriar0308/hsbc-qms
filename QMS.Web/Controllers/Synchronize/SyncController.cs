﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Berger.Services.LMS.Interfaces;
using Berger.Web.Controllers.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.Synchronize
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/sync")]

    public class SyncController : BaseController
    {
        private readonly ISyncService syncService;

        public SyncController(
            ISyncService syncService)
        {
            this.syncService = syncService;
        }

        [HttpPost("area")]
        public async Task<IActionResult> SyncArea()
        {
            try
            {
                var result = await syncService.AreaAsync();
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
