using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace Berger.Web.Controllers.FAQs
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/complain")]
    public class ComplainController : BaseController
    {
        private readonly IComplainService _complainService;
        private readonly IMapper _mapper;

        public ComplainController(
            IComplainService complainService,
            IMapper mapper)
        {
            this._complainService = complainService;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] ComplainQuery query)
        {
            try
            {
                var result = await _complainService.GetAllAsync(query);
                var queryResult = _mapper.Map<QueryResult<Berger.Entities.Complain>, QueryResult<ComplainModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var complain = await _complainService.GetByIdAsync(id);
                var result = _mapper.Map<Berger.Entities.Complain, ComplainModel>(complain);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _complainService.DeleteAsync(id);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
