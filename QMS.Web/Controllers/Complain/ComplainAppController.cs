using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace Berger.Web.Controllers.FAQs
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/app/complain")]
    public class ComplainAppController : BaseController
    {
        private readonly IComplainService _complainService;
        private readonly IMapper _mapper;

        public ComplainAppController(
            IComplainService complainService,
            IMapper mapper)
        {
            this._complainService = complainService;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] ComplainQuery query)
        {
            try
            {
                var result = await _complainService.GetAllAsync(query);
                var queryResult = _mapper.Map<QueryResult<Berger.Entities.Complain>, QueryResult<ComplainModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var complain = await _complainService.GetByIdAsync(id);
                var result = _mapper.Map<Berger.Entities.Complain, ComplainModel>(complain);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("GetComplainType")]
        public async Task<IActionResult> GetComplainType()
        {
            try
            {
                var result = await _complainService.GetComplainTypeSelectAsync();
    
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SaveComplainModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var complain = _mapper.Map<SaveComplainModel, Berger.Entities.Complain>(model);
                var result = await _complainService.AddAsync(complain);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Update(int id, [FromBody] SaveComplainModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var complain = _mapper.Map<SaveComplainModel, Berger.Entities.Complain>(model);
                await _complainService.UpdateAsync(complain);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _complainService.DeleteAsync(id);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
