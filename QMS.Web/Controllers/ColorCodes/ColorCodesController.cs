﻿using System;
using System.Threading.Tasks;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Berger.Web.Controllers.Common;
using Berger.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace Berger.Web.Controllers.ColorCodes
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/color-codes")]
    public class ColorCodesController : BaseController
    {
        private readonly IColorCodeService _colorCodeService;
        private readonly IFileImportService _fileImportService;
        private readonly IMapper _mapper;

        public ColorCodesController(
            IColorCodeService colorCodeService,
            IFileImportService fileImportService,
            IMapper mapper)
        {
            this._colorCodeService = colorCodeService;
            this._fileImportService = fileImportService;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] ColorCodeQuery query)
        {
            try
            {                
                var result = await _colorCodeService.GetAllAsync(query);
                var queryResult = _mapper.Map<QueryResult<ColorCode>, QueryResult<ColorCodeModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }            
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var colorCode = await _colorCodeService.GetByIdAsync(id);
                var result = _mapper.Map<ColorCode, ColorCodeModel>(colorCode);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }            
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SaveColorCodeModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }

            try
            {
                var colorCode = _mapper.Map<SaveColorCodeModel, ColorCode>(model);
                var result = await _colorCodeService.AddAsync(colorCode);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] SaveColorCodeModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }

            try
            {
                var colorCode = _mapper.Map<SaveColorCodeModel, ColorCode>(model);
                await _colorCodeService.UpdateAsync(colorCode);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }            
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _colorCodeService.DeleteAsync(id);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPost("excelImport")]
        public async Task<IActionResult> ExcelImport([FromForm] IFormFile excelFile)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return ValidationResult(ModelState);
                }
                else
                {
                    var result = await _fileImportService.ExcelImportColorCodeAsync(excelFile);
                    return OkResult(result.Data, result.Message);
                }
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
