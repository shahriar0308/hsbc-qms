﻿using System;
using System.Threading.Tasks;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Berger.Web.Controllers.Common;
using Berger.Entities;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;

namespace Berger.Web.Controllers.ColorCodes
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/app/color-codes")]
    public class ColorCodesAppController : BaseController
    {
        private readonly IColorCodeService _colorCodeService;
        private readonly IMapper _mapper;

        public ColorCodesAppController(
            IColorCodeService colorCodeService,
            IMapper mapper)
        {
            this._colorCodeService = colorCodeService;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {                
                var result = await _colorCodeService.GetAllActiveAsync();
                var queryResult = _mapper.Map<IList<ColorCode>, IList<ColorCodeModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }            
        }
    }
}
