﻿using System;
using System.Threading.Tasks;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Berger.Web.Controllers.Common;
using Berger.Entities;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;

namespace Berger.Web.Controllers.UserColorCodes
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/app/user-color-codes")]
    public class UserColorCodesAppController : BaseController
    {
        private readonly IUserColorCodeService _userColorCodeService;
        private readonly IMapper _mapper;

        public UserColorCodesAppController(
            IUserColorCodeService userColorCodeService,
            IMapper mapper)
        {
            this._userColorCodeService = userColorCodeService;
            this._mapper = mapper;
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> Get(Guid userId)
        {
            try
            {                
                var result = await _userColorCodeService.GetAllActiveByUserIdAsync(userId);
                var queryResult = _mapper.Map<IList<UserColorCode>, IList<UserColorCodeModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SaveUserColorCodeModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var userColorCode = _mapper.Map<SaveUserColorCodeModel, UserColorCode>(model);
                var result = await _userColorCodeService.AddAsync(userColorCode);
                return OkResult(result.Id);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _userColorCodeService.DeleteAsync(id);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
