﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.Divisions
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/app/divisions")]
    public class DivisionsAppController : BaseController
    {
        private readonly IDivisionService _divisionService;
        private readonly IMapper _mapper;

        public DivisionsAppController(
            IDivisionService divisionService,
            IMapper mapper)
        {
            this._divisionService = divisionService;
            this._mapper = mapper;
        }

        [HttpGet("select")]
        public async Task<IActionResult> GetSelect()
        {
            try
            {
                var result = await _divisionService.GetAllForSelectAsync();
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] DivisionQuery query)
        {
            try
            {
                var result = await _divisionService.GetAllAsync(query);
                var queryResult = _mapper.Map<QueryResult<Berger.Entities.Division>, QueryResult<DivisionModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("getwithdistrict")]
        public async Task<IActionResult> GetWithDistrict()
        {
            try
            {
                var result = await _divisionService.GetAllWithDistrictAsync();
                var queryResult = _mapper.Map<IList<Berger.Entities.Division>, IList<DivisionModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var division = await _divisionService.GetByIdAsync(id);
                var result = _mapper.Map<Berger.Entities.Division, DivisionModel>(division);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
