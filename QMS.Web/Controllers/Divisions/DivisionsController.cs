﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.Divisions
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/divisions")]
    public class DivisionsController : BaseController
    {
        private readonly IDivisionService _divisionService;
        private readonly IMapper _mapper;

        public DivisionsController(
            IDivisionService divisionService,
            IMapper mapper)
        {
            this._divisionService = divisionService;
            this._mapper = mapper;
        }

        [HttpGet("select")]
        public async Task<IActionResult> GetSelect()
        {
            try
            {
                var result = await _divisionService.GetAllForSelectAsync();
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] DivisionQuery query)
        {
            try
            {
                var result = await _divisionService.GetAllAsync(query);
                var queryResult = _mapper.Map<QueryResult<Berger.Entities.Division>, QueryResult<DivisionModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var division = await _divisionService.GetByIdAsync(id);
                var result = _mapper.Map<Berger.Entities.Division, DivisionModel>(division);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SaveDivisionModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var division = _mapper.Map<SaveDivisionModel, Berger.Entities.Division>(model);
                var result = await _divisionService.AddAsync(division);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Update(int id, [FromBody] SaveDivisionModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var division = _mapper.Map<SaveDivisionModel, Berger.Entities.Division>(model);
                await _divisionService.UpdateAsync(division);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _divisionService.DeleteAsync(id);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
