﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.Districts
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/districts")]
    public class DistrictsController : BaseController
    {
        private readonly IDistrictService _districtService;
        private readonly IMapper _mapper;

        public DistrictsController(
            IDistrictService districtService,
            IMapper mapper)
        {
            this._districtService = districtService;
            this._mapper = mapper;
        }

        [HttpGet("select")]
        public async Task<IActionResult> GetSelect(int divisionId = 0)
        {
            try
            {
                var result = (divisionId == 0) ? await _districtService.GetAllForSelectAsync()
                    : await _districtService.GetByDivisionSelectAsync(divisionId);

                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] DistrictQuery query)
        {
            try
            {
                var result = await _districtService.GetAllAsync(query);
                var queryResult = _mapper.Map<QueryResult<Berger.Entities.District>, QueryResult<DistrictModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var district = await _districtService.GetByIdAsync(id);
                var result = _mapper.Map<Berger.Entities.District, DistrictModel>(district);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SaveDistrictModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var district = _mapper.Map<SaveDistrictModel, Berger.Entities.District>(model);
                var result = await _districtService.AddAsync(district, model.DivisionId);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Update(int id, [FromBody] SaveDistrictModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var district = _mapper.Map<SaveDistrictModel, Berger.Entities.District>(model);
                await _districtService.UpdateAsync(district, model.DivisionId);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _districtService.DeleteAsync(id);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
