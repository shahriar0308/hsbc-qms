﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.Districts
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/app/districts")]
    public class DistrictsAppController : BaseController
    {
        private readonly IDistrictService _districtService;
        private readonly IMapper _mapper;

        public DistrictsAppController(
            IDistrictService districtService,
            IMapper mapper)
        {
            this._districtService = districtService;
            this._mapper = mapper;
        }

        [HttpGet("select")]
        public async Task<IActionResult> GetSelect(int divisionId = 0)
        {
            try
            {
                var result = (divisionId == 0) ? await _districtService.GetAllForSelectAsync()
                    : await _districtService.GetByDivisionSelectAsync(divisionId);

                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = await _districtService.GetAllAsync();
                var queryResult = _mapper.Map<IList<Berger.Entities.District>, IList<DistrictModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var district = await _districtService.GetByIdAsync(id);
                var result = _mapper.Map<Berger.Entities.District, DistrictModel>(district);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
