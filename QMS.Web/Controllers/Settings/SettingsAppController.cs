﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.Settings
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/app/settings")]
    public class SettingsAppController : BaseController
    {
        private readonly ISettingService _settingService;
        private readonly IMapper _mapper;

        public SettingsAppController(
            ISettingService settingService,
            IMapper mapper)
        {
            _settingService = settingService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = await _settingService.GetAllAsync();
                var queryResult = _mapper.Map<IList<Berger.Entities.Setting>, IList<SettingModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var setting = await _settingService.GetByIdAsync(id);
                var result = _mapper.Map<Berger.Entities.Setting, SettingModel>(setting);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("points/{id}")]
        public async Task<IActionResult> GetByPoints(int id)
        {
            try
            {
                var setting = await _settingService.GetByPoints(id);
                var result = _mapper.Map<Berger.Entities.Setting, SettingModel>(setting);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
