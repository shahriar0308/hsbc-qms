﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.Settings
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/[controller]")]
    public class SettingsController : BaseController
    {
        private readonly ISettingService _settingService;
        private readonly IMapper _mapper;

        public SettingsController(
            ISettingService settingService,
            IMapper mapper)
        {
            _settingService = settingService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] SettingQuery query)
        {
            try
            {
                var result = await _settingService.GetAllAsync(query);
                var queryResult = _mapper.Map<QueryResult<Berger.Entities.Setting>, QueryResult<SettingModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
        [HttpGet("select")]
        public async Task<IActionResult> GetSelect()
        {
            try
            {
                var result = await _settingService.GetAllSelectAsync();
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var setting = await _settingService.GetByIdAsync(id);
                var result = _mapper.Map<Berger.Entities.Setting, SettingModel>(setting);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SaveSettingModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var setting = _mapper.Map<SaveSettingModel, Berger.Entities.Setting>(model);
                var result = await _settingService.AddAsync(setting);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] SaveSettingModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var setting = _mapper.Map<SaveSettingModel, Berger.Entities.Setting>(model);
                await _settingService.UpdateAsync(setting);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _settingService.DeleteAsync(id);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
