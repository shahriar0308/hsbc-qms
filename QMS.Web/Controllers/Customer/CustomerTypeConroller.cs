﻿using AutoMapper;
using Berger.Web.Controllers.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using QMS.Entities;
using QMS.Services.Interfaces;
using QMS.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QMS.Web.Controllers.Customer
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/[controller]")]
    public class CustomerTypeConroller:BaseController
    {
        private readonly IMapper _mapper;
        private readonly ICustomerTypeService _customerTypeService;

        public CustomerTypeConroller(IMapper mapper,ICustomerTypeService customerTypeService)
        {
            _mapper = mapper;
            _customerTypeService = customerTypeService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return OkResult("");
        }
        [HttpGet("id")]
        public async Task<IActionResult> GetbyId(int id)         {
            return OkResult("");
        }

        [HttpPost("")]
        public async Task<IActionResult> CreateCustomerType([FromBody] CustomerTypeModel customerType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Model is not valid");
            }
            var customer = _mapper.Map<CustomerType>(customerType);
            var isCreated=await _customerTypeService.CreateAsync(customer);
            if (isCreated==null)
            {
                return BadRequest();
            }
            return OkResult("Customer type created successfully");
        }

        public async Task<IActionResult> UpdateCustomerType([FromBody] CustomerTypeModel customerType)
        {
            return OkResult("");
        }


        [HttpDelete]
        public async Task<IActionResult> DeleteCustomerType(int id)
        {
            return OkResult("");
        }

    }


}

