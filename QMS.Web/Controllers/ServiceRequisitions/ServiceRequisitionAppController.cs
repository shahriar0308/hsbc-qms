﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.Benefits
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/app/service-requisition")]
    public class ServiceRequisitionAppController : BaseController
    {
        private readonly IServiceRequisitionService serviceRequisitionService;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService currentUserService;
        public ServiceRequisitionAppController(
            IServiceRequisitionService serviceRequisitionService,
            ICurrentUserService currentUserService,
            IMapper mapper)
        {
            this.serviceRequisitionService = serviceRequisitionService;
            this.currentUserService = currentUserService;
            this._mapper = mapper;
        }

        //[HttpGet]
        //public async Task<IActionResult> Get()
        //{
        //    try
        //    {
        //        var result = await benefitService.GetAllAsync();
        //        var queryResult = _mapper.Map<IList<Entities.Benefit>, IList<BenefitModel>>(result);
        //        return OkResult(queryResult);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ExceptionResult(ex);
        //    }
        //}

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SaveServiceRequisitionModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var requisition = _mapper.Map<SaveServiceRequisitionModel, Entities.ServiceRequisition>(model);
                var result = await serviceRequisitionService.AddAsync(requisition);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = await serviceRequisitionService.GetByUserAsync(currentUserService.UserId);
                var queryResult = _mapper.Map<IList<Entities.ServiceRequisition>, IList<ServiceRequisitionModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            try
            {
                var result = await serviceRequisitionService.GetById(id);
                var queryResult = _mapper.Map<Entities.ServiceRequisition, ServiceRequisitionModel>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

    }
}
