﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.Benefits
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/service-requisition")]
    public class ServiceRequisitionController : BaseController
    {
        private readonly IServiceRequisitionService serviceRequisitionService;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService currentUserService;
        public ServiceRequisitionController(
            IServiceRequisitionService serviceRequisitionService,
            IMapper mapper)
        {
            this.serviceRequisitionService = serviceRequisitionService;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] bool workIdProvided)
        {
            try
            {
                var result = await serviceRequisitionService.GetAllAsync(workIdProvided);
                var queryResult = _mapper.Map<IList<Entities.ServiceRequisition>, IList<ServiceRequisitionModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("details")]
        public async Task<IActionResult> GetDetailList([FromQuery] Guid id)
        {
            try
            {
                var result = await serviceRequisitionService.GetDetailAsync(id);
                var queryResult = _mapper.Map<Entities.ServiceRequisition, ServiceRequisitionModel>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
