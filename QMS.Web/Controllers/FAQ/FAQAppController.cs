using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.FAQ
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/app/faqs")]
    public class FAQAppController : BaseController
    {
        private readonly IFAQService _FAQService;
        private readonly IMapper _mapper;

        public FAQAppController(
            IFAQService FAQService,
            IMapper mapper)
        {
            this._FAQService = FAQService;
            this._mapper = mapper;
        }

        [HttpGet("loyalty-program-faq")]
        public async Task<IActionResult> GetLoyaltyProgramFAQ()
        {
            try
            {
                var result = await _FAQService.GetAllLoyaltyProgramAsync();
                var queryResult = _mapper.Map<IList<Entities.FAQ>, IList<FAQAppModel>>(result);
                return OkResult(queryResult);
                
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("service-faq")]
        public async Task<IActionResult> GetServiceFAQ()
        {
            try
            {
                var result = await _FAQService.GetAllServiceAsync();
                var queryResult = _mapper.Map<IList<Entities.FAQ>, IList<FAQAppModel>>(result);
                return OkResult(queryResult);

            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("FAQType")]
        public IActionResult GetFAQType()
        {
            try
            {
                var result =  _FAQService.GetFAQTypeSelectAsync();

                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var faq = await _FAQService.GetByIdAsync(id);
                var result = _mapper.Map<Berger.Entities.FAQ, FAQModel>(faq);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SaveFAQModel model)
        {
            try
            {
                var faq = _mapper.Map<SaveFAQModel, Berger.Entities.FAQ>(model);
                var result = await _FAQService.AddAsync(faq);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Update(int id, [FromBody] SaveFAQModel model)
        {
            try
            {
                var faq = _mapper.Map<SaveFAQModel, Berger.Entities.FAQ>(model);
                await _FAQService.UpdateAsync(faq);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _FAQService.DeleteAsync(id);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
