using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.FAQ
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/[controller]")]
    public class FAQsController : BaseController
    {
        private readonly IFAQService _FAQService;
        private readonly IMapper _mapper;

        public FAQsController(
            IFAQService FAQService,
            IMapper mapper)
        {
            this._FAQService = FAQService;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] FAQQuery query)
        {
            try
            {
                var result = await _FAQService.GetAllAsync(query);
                var queryResult = _mapper.Map<QueryResult<Berger.Entities.FAQ>, QueryResult<FAQModel>>(result);
                return OkResult(queryResult);
                
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("FAQType")]
        public IActionResult GetFAQType()
        {
            try
            {
                var result =  _FAQService.GetFAQTypeSelectAsync();

                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var faq = await _FAQService.GetByIdAsync(id);
                var result = _mapper.Map<Berger.Entities.FAQ, FAQModel>(faq);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SaveFAQModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var faq = _mapper.Map<SaveFAQModel, Berger.Entities.FAQ>(model);
                var result = await _FAQService.AddAsync(faq);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Update(int id, [FromBody] SaveFAQModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var faq = _mapper.Map<SaveFAQModel, Berger.Entities.FAQ>(model);
                await _FAQService.UpdateAsync(faq);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _FAQService.DeleteAsync(id);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
