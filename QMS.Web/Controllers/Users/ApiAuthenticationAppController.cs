using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Linq;
using Berger.Models;
using Berger.Entities;
using Berger.Web.Core;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Berger.Web.Controllers.Common;
using Berger.Services.Exceptions;
using Berger.Common.Constants;
using Berger.Web.Services;
using Microsoft.AspNetCore.Identity.UI.Services;
using Berger.Services.Interfaces;
using Berger.Common.Enums;
using System.Transactions;

namespace Berger.Web.Controllers.Users
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/app/auth")]
    public class ApiAuthenticationAppController : BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        //private readonly IEmailSender _emailSender;
        private readonly IUserOTPService _userOTPService;
        private readonly ILoginService _loginService;
        private readonly ISMSService _smsService;
        private readonly IDateTime _dateTime;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly AuthSMSSenderOptions _authSMSSenderOptions;

        public ApiAuthenticationAppController(
            IOptionsSnapshot<AppSettings> options,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            //IEmailSender emailSender,
            IOptions<AuthSMSSenderOptions> authOptions,
            IUserOTPService userOTPService,
            ILoginService loginService,
            ISMSService smsService,
            IDateTime dateTime,
            IMapper mapper)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            //_emailSender = emailSender;
            _userOTPService = userOTPService;
            this._loginService = loginService;
            _smsService = smsService;
            _dateTime = dateTime;
            _mapper = mapper;
            _appSettings = options.Value;
            _authSMSSenderOptions = authOptions.Value;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] ApiAppLoginModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ValidationResult(ModelState);

                var header = HttpContext.Request?.Headers[nameof(_appSettings.AppHeaderSecretKey)];

                if (!header.HasValue || !header.Equals(_appSettings.AppHeaderSecretKey))
                {
                    ModelState.AddModelError("", "Invalid request.");
                    return ValidationResult(ModelState);
                }

                var user = await this._userManager.FindByNameAsync(model.PhoneNumber);
                if (user == null)
                {
                    ModelState.AddModelError("", "Phone Number or password is invalid.");
                    return ValidationResult(ModelState);
                }

                var isValidUser = await _userManager.CheckPasswordAsync(user, model.Password);
                if (!isValidUser)
                {
                    user.AccessFailedCount += 1;
                    var result = await _userManager.UpdateAsync(user);

                    ModelState.AddModelError("", "Phone Number or password is invalid.");
                    return ValidationResult(ModelState);
                }

                #region Login History
                //if(!string.IsNullOrWhiteSpace(model.FCMToken))
                //{
                    var logHistoryId = await this._loginService.UserLoggedInLogEntryAsync(user.Id, model.FCMToken);
                //}
                #endregion

                var authUser = _mapper.Map<ApplicationUser, ApiAuthenticateUserModel>(user);

                // authentication successful so generate jwt token
                authUser.Token = await this.BuildToken(user);

                return OkResult(authUser);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [AllowAnonymous]
        [HttpPost("registration")]
        public async Task<IActionResult> Registration([FromBody] ApiAppRegistrationModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ValidationResult(ModelState);

                var headerSK = HttpContext.Request?.Headers[nameof(_appSettings.AppHeaderSecretKey)];

                if (!headerSK.HasValue || !headerSK.Equals(_appSettings.AppHeaderSecretKey))
                {
                    ModelState.AddModelError("", "Invalid request.");
                    return ValidationResult(ModelState);
                }

                #region Check for validity or save
                var hasHeaderOTP = HttpContext.Request?.Headers.ContainsKey(nameof(_appSettings.AppRegistrationOTP));
                var headerOTP = HttpContext.Request?.Headers[nameof(_appSettings.AppRegistrationOTP)];
                var hasValidOTPData = hasHeaderOTP.HasValue && hasHeaderOTP.Value && headerOTP.HasValue && !string.IsNullOrWhiteSpace(headerOTP.Value);

                if (hasValidOTPData)
                {
                    var isValid = await _userOTPService.IsValidOTPByPhoneNumberAsync(model.PhoneNumber, int.Parse(headerOTP.Value), true, _authSMSSenderOptions.IsOTPExpires, _authSMSSenderOptions.OTPExpiresMinutes);
                    if (!isValid)
                    {
                        ModelState.AddModelError("", "OTP is invalid.");
                        return ValidationResult(ModelState);
                    }
                }
                #endregion

                var user = _mapper.Map<ApiAppRegistrationModel, ApplicationUser>(model);

                #region registration
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    try
                    {
                        var existingUser = (await _userManager.FindByNameAsync(user.UserName));
                        if (existingUser != null && !existingUser.IsDeleted)
                            throw new DuplicationException(nameof(user.PhoneNumber));

                        user.Status = EnumApplicationUserStatus.GeneralUser;
                        user.Created = _dateTime.Now;
                        var userSaveResult = await _userManager.CreateAsync(user, model.Password);

                        if (!userSaveResult.Succeeded)
                        {
                            throw new IdentityValidationException(userSaveResult.Errors);
                        };

                        // Add New User Role
                        var userRoleName = ConstantsValue.UserRoleName.AppUser;
                        var role = await _roleManager.FindByNameAsync(userRoleName);

                        if (role == null)
                        {
                            throw new NotFoundException(nameof(ApplicationRole), userRoleName);
                        }

                        var roleSaveResult = await _userManager.AddToRoleAsync(user, role.Name);

                        if (!roleSaveResult.Succeeded)
                        {
                            throw new IdentityValidationException(roleSaveResult.Errors);
                        };

                        #region Login History
                        //if (!string.IsNullOrWhiteSpace(model.FCMToken))
                        //{
                            var logHistoryId = await this._loginService.UserLoggedInLogEntryAsync(user.Id, model.FCMToken);
                        //}
                        #endregion

                        #region Send OTP when request for validity check
                        if (!hasValidOTPData)
                        {
                            scope.Dispose();

                            var otp = await _userOTPService.GenerateOTPByPhoneNumberAsync(model.PhoneNumber);
                            var isSend = await _smsService.SendOTPSMSAsync(model.PhoneNumber, otp);

                            return OkResult(true);
                        }
                        #endregion

                        scope.Complete();
                    }
                    catch (Exception ex)
                    {
                        scope.Dispose();
                        throw;
                    }
                }
                #endregion

                user = await this._userManager.FindByNameAsync(user.UserName);

                var authUser = _mapper.Map<ApplicationUser, ApiAuthenticateUserModel>(user);

                // authentication successful so generate jwt token
                authUser.Token = await this.BuildToken(user);

                return OkResult(authUser);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [AllowAnonymous]
        [HttpGet("forgot-password-otp/{phoneNumber}")]
        public async Task<IActionResult> ForgotPasswordOTP(string phoneNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ValidationResult(ModelState);

                var header = HttpContext.Request?.Headers[nameof(_appSettings.AppHeaderSecretKey)];

                if (!header.HasValue || !header.Equals(_appSettings.AppHeaderSecretKey))
                {
                    ModelState.AddModelError("", "Invalid request.");
                    return ValidationResult(ModelState);
                }

                var user = await this._userManager.FindByNameAsync(phoneNumber);
                if (user == null)
                {
                    ModelState.AddModelError("", "Phone Number is invalid.");
                    return ValidationResult(ModelState);
                }

                var otp = await _userOTPService.GenerateOTPAsync(user.Id);
                var isSend = await _smsService.SendOTPSMSAsync(phoneNumber, otp);

                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [AllowAnonymous]
        [HttpPost("forgot-password-otp-validity")]
        public async Task<IActionResult> ForgotPasswordOTPValidity([FromBody] ApiAppOTPValidityModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ValidationResult(ModelState);

                var header = HttpContext.Request?.Headers[nameof(_appSettings.AppHeaderSecretKey)];

                if (!header.HasValue || !header.Equals(_appSettings.AppHeaderSecretKey))
                {
                    ModelState.AddModelError("", "Invalid request.");
                    return ValidationResult(ModelState);
                }

                var user = await this._userManager.FindByNameAsync(model.PhoneNumber);
                if (user == null)
                {
                    ModelState.AddModelError("", "Phone Number is invalid.");
                    return ValidationResult(ModelState);
                }

                var isValid = await _userOTPService.IsValidOTPAsync(user.Id, model.OTP, true, _authSMSSenderOptions.IsOTPExpires, _authSMSSenderOptions.OTPExpiresMinutes);
                if (!isValid)
                {
                    ModelState.AddModelError("", "OTP is invalid.");
                    return ValidationResult(ModelState);
                }

                return OkResult(isValid);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [AllowAnonymous]
        [HttpPost("reset-password-otp")]
        public async Task<IActionResult> ResetPasswordOTP([FromBody] ApiAppResetPasswordOTPModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ValidationResult(ModelState);

                var header = HttpContext.Request?.Headers[nameof(_appSettings.AppHeaderSecretKey)];

                if (!header.HasValue || !header.Equals(_appSettings.AppHeaderSecretKey))
                {
                    ModelState.AddModelError("", "Invalid request.");
                    return ValidationResult(ModelState);
                }

                var user = await this._userManager.FindByNameAsync(model.PhoneNumber);
                if (user == null)
                {
                    ModelState.AddModelError("", "Phone Number is invalid.");
                    return ValidationResult(ModelState);
                }

                //var isValid = await _userOTPService.IsValidOTPAsync(user.Id, model.OTP, true);
                //if (!isValid)
                //{
                //    ModelState.AddModelError("", "OTP is invalid.");
                //    return ValidationResult(ModelState);
                //}

                var hashPassword = _userManager.PasswordHasher.HashPassword(user, model.NewPassword);
                //user.LastPassword = user.PasswordHash;
                user.PasswordHash = hashPassword;
                //user.PasswordChangedCount += 1;
                //user.LastPassChangeDate = _dateTime.Now;

                var userSaveResult = await _userManager.UpdateAsync(user);

                if (!userSaveResult.Succeeded)
                {
                    throw new IdentityValidationException(userSaveResult.Errors);
                };

                //var isReset = await _userOTPService.ResetOTPAsync(user.Id);
                
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        private async Task<string> BuildToken(ApplicationUser user)
        {
            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(this._appSettings.TokenSecretKey);

            var claims = new List<Claim>()
            {
                //new Claim(ClaimTypes.Name, user.Id.ToString())
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Email, user.Email??string.Empty),
                new Claim(ClaimTypes.Name, user.UserName??string.Empty)
            };

            var userRoles = await _userManager.GetRolesAsync(user);
            foreach (var userRole in userRoles)
            {
                var role = await _roleManager.FindByNameAsync(userRole);
                if (role == null) { continue; }
                claims.Add(new Claim(ClaimTypes.Role, role.Name));

                var roleClaims = await _roleManager.GetClaimsAsync(role);
                foreach (Claim roleClaim in roleClaims)
                {
                    claims.Add(roleClaim);
                }
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddHours(this._appSettings.TokenExpiresHours),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}