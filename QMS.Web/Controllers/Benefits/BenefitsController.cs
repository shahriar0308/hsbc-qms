﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.Benefits
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/benefits")]
    public class BenefitsController : BaseController
    {
        private readonly IBenefitService benefitService;
        private readonly IMapper _mapper;

        public BenefitsController(
            IBenefitService benefitService,
            IMapper mapper)
        {
            this.benefitService = benefitService;
            this._mapper = mapper;
        }

        [HttpGet("select")]
        public async Task<IActionResult> GetSelect()
        {
            try
            {
                var result = await benefitService.GetAllForSelectAsync();
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] BenefitQuery query)
        {
            try
            {
                var result = await benefitService.GetAllAsync(query);
                var queryResult = _mapper.Map<QueryResult<Entities.Benefit>, QueryResult<BenefitModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var benefit = await benefitService.GetByIdAsync(id);
                var result = _mapper.Map<Entities.Benefit, BenefitModel>(benefit);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SaveBenefitModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var benefit = _mapper.Map<SaveBenefitModel, Entities.Benefit>(model);
                var result = await benefitService.AddAsync(benefit, model.SettingId);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Update(int id, [FromBody] SaveBenefitModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var benefit = _mapper.Map<SaveBenefitModel, Berger.Entities.Benefit>(model);
                await benefitService.UpdateAsync(benefit, model.SettingId);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await benefitService.DeleteAsync(id);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
