﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.Benefits
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/app/benefits")]
    public class BenefitsAppController : BaseController
    {
        private readonly IBenefitService benefitService;
        private readonly IMapper _mapper;

        public BenefitsAppController(
            IBenefitService benefitService,
            IMapper mapper)
        {
            this.benefitService = benefitService;
            this._mapper = mapper;
        }

        [HttpGet("select")]
        public async Task<IActionResult> GetSelect()
        {
            try
            {
                var result = await benefitService.GetAllForSelectAsync();
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("category/{settingId}")]
        public async Task<IActionResult> GetByCategory(int settingId)
        {
            try
            {
                var benefit = await benefitService.GetByCategoryAsync(settingId);
                var result = _mapper.Map<IList<Entities.Benefit>, IList<BenefitModel>>(benefit);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = await benefitService.GetAllAsync();
                var queryResult = _mapper.Map<IList<Entities.Benefit>, IList<BenefitModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var benefit = await benefitService.GetByIdAsync(id);
                var result = _mapper.Map<Berger.Entities.Benefit, BenefitModel>(benefit);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
