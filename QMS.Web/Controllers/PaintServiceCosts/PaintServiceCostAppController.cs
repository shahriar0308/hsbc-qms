﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.PaintServiceCosts
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/app/paint-service-cost")]
    public class PaintServiceCostAppController : BaseController
    {
        private readonly IPaintService paintService;
        private readonly IMapper _mapper;

        public PaintServiceCostAppController(
            IPaintService paintService,
            IMapper mapper)
        {
            this.paintService = paintService;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] int surfaceType =0
            , [FromQuery] int surfaceCondition=0, [FromQuery] int productId=0)
        {
            try
            {
                var result = await paintService.Get(surfaceType, surfaceCondition, productId);
                var queryResult = _mapper.Map<Entities.PaintingServiceCost, PaintingServiceCostModel>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
