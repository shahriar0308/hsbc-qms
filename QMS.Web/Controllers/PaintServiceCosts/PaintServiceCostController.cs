﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.PaintServiceCosts
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/paint-service-cost")]
    public class PaintServiceCostController : BaseController
    {
        private readonly IPaintService paintService;
        private readonly IMapper _mapper;

        public PaintServiceCostController(
            IPaintService paintService,
            IMapper mapper)
        {
            this.paintService = paintService;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = await paintService.GetAllAsync();
                var queryResult = _mapper.Map<QueryResult<Entities.PaintingServiceCost>, QueryResult<PaintingServiceCostModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("grid")]
        public async Task<IActionResult> GetForGrid([FromQuery] int surfaceType, [FromQuery] int surfaceCondition)
        {
            try
            {
                var result = await paintService.GetForGrid(surfaceType, surfaceCondition);
                var queryResult = _mapper.Map<IList<Entities.PaintingServiceCost>, IList<PaintingServiceCostModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var paintingServiceCost = await paintService.GetByIdAsync(id);
                var result = _mapper.Map<Entities.PaintingServiceCost, PaintingServiceCostModel>(paintingServiceCost);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        //[HttpPost]
        //public async Task<IActionResult> Create([FromBody] SavePaintingServiceCostModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return ValidationResult(ModelState);
        //    }
        //    try
        //    {
        //        var paintingServiceCost = _mapper.Map<SavePaintingServiceCostModel, Entities.PaintingServiceCost>(model);
        //        var result = await paintService.AddAsync(paintingServiceCost, model.ProductId);
        //        return OkResult(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ExceptionResult(ex);
        //    }
        //}

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] IList<SavePaintingServiceCostModel> model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var paintingServiceCosts = _mapper.Map<IList<SavePaintingServiceCostModel>, 
                    IList<Entities.PaintingServiceCost>>(model);
                var result = await paintService.AddOrUpdateAsync(paintingServiceCosts);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Update(int id, [FromBody] SavePaintingServiceCostModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var paintingServiceCost = _mapper.Map<SavePaintingServiceCostModel, Entities.PaintingServiceCost>(model);
                await paintService.UpdateAsync(paintingServiceCost, model.ProductId);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await paintService.DeleteAsync(id);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
