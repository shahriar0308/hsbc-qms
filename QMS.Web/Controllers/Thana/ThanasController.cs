﻿using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Berger.Web.Controllers.Thana
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/thanas")]
    public class ThanasController : BaseController
    {
        private readonly IThanaService _thanaService;
        private readonly IMapper _mapper;

        public ThanasController(
            IThanaService thanaService,
            IMapper mapper)
        {
            this._thanaService = thanaService;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] ThanaQuery query)
        {
            try
            {
                var result = await _thanaService.GetAllAsync(query);
                var queryResult = _mapper.Map<QueryResult<Berger.Entities.Thana>, QueryResult<ThanaModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var thana = await _thanaService.GetByIdAsync(id);
                var result = _mapper.Map<Berger.Entities.Thana, ThanaModel>(thana);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SaveThanaModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var thana = _mapper.Map<SaveThanaModel, Berger.Entities.Thana>(model);
                var result = await _thanaService.AddAsync(thana, model.DistrictId);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Update(int id, [FromBody] SaveThanaModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var thana = _mapper.Map<SaveThanaModel, Berger.Entities.Thana>(model);
                await _thanaService.UpdateAsync(thana, model.DistrictId);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _thanaService.DeleteAsync(id);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}