﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.Thana
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/app/thanas")]
    public class ThanasAppController : BaseController
    {
        private readonly IThanaService _thanaService;
        private readonly IMapper _mapper;

        public ThanasAppController(
            IThanaService thanaService,
            IMapper mapper)
        {
            this._thanaService = thanaService;
            this._mapper = mapper;
        }

        [HttpGet("select")]
        public async Task<IActionResult> GetSelect(int districtId = 0)
        {
            try
            {
                var result = (districtId == 0) ? await _thanaService.GetAllForSelectAsync()
                    : await _thanaService.GetByDistrictSelectAsync(districtId);

                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = await _thanaService.GetAllAsync();
                var queryResult = _mapper.Map<IList<Berger.Entities.Thana>, IList<ThanaModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var thana = await _thanaService.GetByIdAsync(id);
                var result = _mapper.Map<Berger.Entities.Thana, ThanaModel>(thana);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }

}