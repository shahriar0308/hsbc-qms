﻿using AutoMapper;
using Berger.Web.Controllers.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Web.Controllers.Service
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/[controller]")]
    public class ServiceCategoryController :BaseController
    {
        
        private readonly IMapper _mapper;

        public ServiceCategoryController(IMapper mapper)
        {
            _mapper = mapper;
        }

        public async Task<IActionResult> GetAllService()
        {
            return OkResult("");
        }

        public async Task<IActionResult> GetById()
        {

            return OkResult("");
        }
        public async Task<IActionResult> Create() 
        {
            return OkResult("");
        }

        public async Task<IActionResult> Delete()
        {
            return OkResult("");
        }


    }
}
