﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.Benefits
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/single-value")]
    public class SingleValuedController : BaseController
    {
        private readonly ISingleValuedService singleValuedService;
        private readonly IMapper _mapper;

        public SingleValuedController(
            ISingleValuedService singleValuedService,
            IMapper mapper)
        {
            this.singleValuedService = singleValuedService;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] string name)
        {
            try
            {
                var result = await singleValuedService.GetByNameAsync(name);

                var queryResult = _mapper.Map<QueryResult<Entities.SingleValuedEntity>, QueryResult<SingleValuedModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var singleValuedEntity = await singleValuedService.GetByIdAsync(id);
                var result = _mapper.Map<Entities.SingleValuedEntity, SingleValuedModel>(singleValuedEntity);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] SaveSingleValuedModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var singleValuedEntity = _mapper.Map<SaveSingleValuedModel, Entities.SingleValuedEntity>(model);
                await singleValuedService.UpdateAsync(singleValuedEntity);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
