﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Berger.Web.Core;
using Berger.Web.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Berger.Web.Controllers.Common
{
    //[ApiController]
    public abstract class BaseController : ControllerBase
    {
        public IActionResult OkResult(object data)
        {
            var apiResult = new ApiResponse
            {
                StatusCode = 200,
                Status = "Success",
                Message = "Successful",
                Data = data
            };
            return ObjectResult(apiResult);
        }

        public IActionResult OkResult(object data, string message)
        {
            var apiResult = new ApiResponse
            {
                StatusCode = 200,
                Status = "Success",
                Message = message,
                Data = data
            };
            return ObjectResult(apiResult);
        }

        public IActionResult ValidationResult(ModelStateDictionary modelState)
        {
            var apiResult = new ApiResponse
            {
                StatusCode = 400,
                Status = "ValidationError",
                Message = "Validation Fail",
                Errors = modelState.GetErrors()
            };
            return ObjectResult(apiResult);
        }

        public IActionResult ExceptionResult(Exception ex)
        {
            ex.ToWriteLog();

            var apiResult = new ApiResponse
            {
                StatusCode = 500,
                Status = "Error",
                Message = ex.Message,
                Data = new object()
            };
            return ObjectResult(apiResult);
        }

        public IActionResult ObjectResult(ApiResponse model)
        {
            var result = new ObjectResult(model)
            {
                StatusCode = model.StatusCode
            };
            return result;
        }
    }
}
