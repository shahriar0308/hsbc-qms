﻿using System;
using System.Threading.Tasks;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Berger.Web.Controllers.Common;
using Berger.Entities;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;

namespace Berger.Web.Controllers.PromotionalBanners
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/app/promotional-banners")]
    public class PromotionalBannersAppController : BaseController
    {
        private readonly IPromotionalBannerService _promotionalBannerService;
        private readonly IMapper _mapper;

        public PromotionalBannersAppController(
            IPromotionalBannerService promotionalBannerService,
            IMapper mapper)
        {
            this._promotionalBannerService = promotionalBannerService;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {                
                var result = await _promotionalBannerService.GetAllActiveAsync();
                var queryResult = _mapper.Map<IList<PromotionalBanner>, IList<PromotionalBannerModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }            
        }
    }
}
