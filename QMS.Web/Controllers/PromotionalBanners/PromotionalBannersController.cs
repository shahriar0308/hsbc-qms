﻿using System;
using System.Threading.Tasks;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Berger.Web.Controllers.Common;
using Berger.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Berger.Web.Core;
using Microsoft.Extensions.Options;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Berger.Common.Enums;

namespace Berger.Web.Controllers.PromotionalBanners
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/promotional-banners")]
    public class PromotionalBannersController : BaseController
    {
        private readonly IPromotionalBannerService _promotionalBannerService;
        //private readonly IWebHostEnvironment _host;
        //private readonly PhotoSettings _photoSettings;
        private readonly IPhotoStorage _photoStorage;
        private readonly IMapper _mapper;

        public PromotionalBannersController(
            IPromotionalBannerService promotionalBannerService,
            //IWebHostEnvironment host,
            //IOptionsSnapshot<PhotoSettings> photoOptions,
            IPhotoStorage photoStorage,
            IMapper mapper)
        {
            this._promotionalBannerService = promotionalBannerService;
            //this._host = host;
            //this._photoSettings = photoOptions.Value;
            this._photoStorage = photoStorage;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] PromotionalBannerQuery query)
        {
            try
            {                
                var result = await _promotionalBannerService.GetAllAsync(query);
                var queryResult = _mapper.Map<QueryResult<PromotionalBanner>, QueryResult<PromotionalBannerModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }            
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var promotionalBanner = await _promotionalBannerService.GetByIdAsync(id);
                var result = _mapper.Map<PromotionalBanner, PromotionalBannerModel>(promotionalBanner);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }            
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromForm] SavePromotionalBannerModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }

            try
            {
                var promotionalBanner = _mapper.Map<SavePromotionalBannerModel, PromotionalBanner>(model);
                if(model.ImageFile != null)
                {
                    await this._photoStorage.IsValidImageAsync(model.ImageFile);
                    var fileName = model.Name + "_" + Guid.NewGuid().ToString();
                    var imageUrl = await _photoStorage.SaveImageAsync(model.ImageFile, fileName, EnumFileUploadFolderCode.PromotionalBanner);
                    promotionalBanner.ImageUrl = imageUrl;
                }
                var result = await _promotionalBannerService.AddAsync(promotionalBanner);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromForm] SavePromotionalBannerModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }

            try
            {
                var promotionalBanner = _mapper.Map<SavePromotionalBannerModel, PromotionalBanner>(model);
                if (model.ImageFile != null)
                {
                    await this._photoStorage.IsValidImageAsync(model.ImageFile);
                    var fileName = model.Name + "_" + Guid.NewGuid().ToString();
                    var imageUrl = await _photoStorage.SaveImageAsync(model.ImageFile, fileName, EnumFileUploadFolderCode.PromotionalBanner);
                    promotionalBanner.ImageUrl = imageUrl;
                }
                await _promotionalBannerService.UpdateAsync(promotionalBanner);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }            
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _promotionalBannerService.DeleteAsync(id);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
