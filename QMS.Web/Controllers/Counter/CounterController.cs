﻿using AutoMapper;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Web.Controllers.Counter
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/[controller]")]
    public class CounterController : BaseController
    {
        private readonly IMapper _mapper;

        public CounterController(IMapper mapper)
        {
            _mapper = mapper;
        }
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return OkResult("");
        }
        [HttpGet("id")]
        public async Task<IActionResult> GetbyId(int id)
        {
            return OkResult("");
        }

        [HttpPost("")]
        public async Task<IActionResult> CreateCounter([FromBody] CounterModel counterModel)
        {
            return OkResult("");
        }

        public async Task<IActionResult> UpdateCounter([FromBody] CounterModel counterModel)
        {
            return OkResult("");
        }


        [HttpDelete]
        public async Task<IActionResult> DeleteCounter(int id)
        {
            return OkResult("");
        }
    }
}
