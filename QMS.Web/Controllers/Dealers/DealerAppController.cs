﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.Dealers
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/app/dealer")]
    public class DealerAppController : BaseController
    {
        private readonly IDealerService _dealerService;
        private readonly IMapper _mapper;

        public DealerAppController(
            IDealerService dealerService,
            IMapper mapper)
        {
            _dealerService = dealerService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] int divisionId = 0, [FromQuery] int districtId = 0)
        {
            try
            {
                IList<Entities.Dealer> result;
                if (divisionId == 0 && districtId == 0)
                    result = await _dealerService.GetAllAsync();
                else if (divisionId != 0 && districtId == 0)
                    result = await _dealerService.GetByDivisionIdAsync(divisionId);
                else
                    result = await _dealerService.GetByDistrictIdAsync(districtId);
                var queryResult = _mapper.Map<IList<Berger.Entities.Dealer>, IList<DealerModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("range")]
        public async Task<IActionResult> GetByRange([FromQuery] decimal minLat = -90
            , [FromQuery] decimal maxLat = 90
            , [FromQuery] decimal minLong = -180
            , [FromQuery] decimal maxLong = 180)
        {
            try
            {
                var result = await _dealerService.GetByRangeAsync(minLat, maxLat, minLong, maxLong);
                var queryResult = _mapper.Map<IList<Entities.Dealer>, IList<DealerModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var dealer = await _dealerService.GetByIdAsync(id);
                var result = _mapper.Map<Berger.Entities.Dealer, DealerModel>(dealer);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("division/{divisionId}")]
        public async Task<IActionResult> GetByDivision(int divisionId)
        {
            try
            {
                var dealer = await _dealerService.GetByDivisionIdAsync(divisionId);
                var result = _mapper.Map<IList<Berger.Entities.Dealer>, IList<DealerModel>>(dealer);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("district/{districtId}")]
        public async Task<IActionResult> GetByDistrict(int districtId)
        {
            try
            {
                var dealer = await _dealerService.GetByDistrictIdAsync(districtId);
                var result = _mapper.Map<IList<Berger.Entities.Dealer>, IList<DealerModel>>(dealer);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
