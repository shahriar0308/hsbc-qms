﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.Dealers
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/[controller]")]
    public class DealerController : BaseController
    {
        private readonly IDealerService _dealerService;
        private readonly IMapper _mapper;

        public DealerController(
            IDealerService dealerService,
            IMapper mapper)
        {
            _dealerService = dealerService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] DealerQuery query)
        {
            try
            {
                var result = await _dealerService.GetAllAsync(query);
                var queryResult = _mapper.Map<QueryResult<Berger.Entities.Dealer>, QueryResult<DealerModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var dealer = await _dealerService.GetByIdAsync(id);
                var result = _mapper.Map<Berger.Entities.Dealer, DealerModel>(dealer);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SaveDealerModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var dealer = _mapper.Map<SaveDealerModel, Berger.Entities.Dealer>(model);
                var result = await _dealerService.AddAsync(dealer, model.DistrictId, model.DivisionId);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] SaveDealerModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var dealer = _mapper.Map<SaveDealerModel, Berger.Entities.Dealer>(model);
                await _dealerService.UpdateAsync(dealer, model.DistrictId, model.DivisionId);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _dealerService.DeleteAsync(id);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
