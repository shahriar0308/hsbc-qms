﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.TypeOfRequirements
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/app/typeOf-requirement")]
    public class TypeOfRequirementAppController : BaseController
    {
        private readonly ITypeOfRequirementService typeOfRequirementService;
        private readonly IMapper _mapper;

        public TypeOfRequirementAppController(
            ITypeOfRequirementService typeOfRequirementService,
            IMapper mapper)
        {
            this.typeOfRequirementService = typeOfRequirementService;
            this._mapper = mapper;
        }

        [HttpGet("select")]
        public async Task<IActionResult> GetSelect()
        {
            try
            {
                var result = await typeOfRequirementService.GetAllForSelectAsync();
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = await typeOfRequirementService.GetAllAsync();
                var queryResult = _mapper.Map<IList<Entities.TypeOfRequirement>, IList<TypeOfRequirementModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var typeOfRequirement = await typeOfRequirementService.GetByIdAsync(id);
                var result = _mapper.Map<Berger.Entities.TypeOfRequirement, TypeOfRequirementModel>(typeOfRequirement);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
