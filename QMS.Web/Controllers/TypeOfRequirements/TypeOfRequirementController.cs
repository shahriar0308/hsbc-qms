﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Berger.Web.Controllers.TypeOfRequirements
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/type-of-requirement")]
    public class TypeOfRequirementController : BaseController
    {
        private readonly ITypeOfRequirementService typeOfRequirementService;
        private readonly IMapper _mapper;

        public TypeOfRequirementController(
            ITypeOfRequirementService typeOfRequirementService,
            IMapper mapper)
        {
            this.typeOfRequirementService = typeOfRequirementService;
            this._mapper = mapper;
        }

        [HttpGet("select")]
        public async Task<IActionResult> GetSelect()
        {
            try
            {
                var result = await typeOfRequirementService.GetAllForSelectAsync();
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] TypeOfRequirementQuery query)
        {
            try
            {
                var result = await typeOfRequirementService.GetAllAsync(query);
                var queryResult = _mapper.Map<QueryResult<Entities.TypeOfRequirement>, QueryResult<TypeOfRequirementModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var typeOfRequirement = await typeOfRequirementService.GetByIdAsync(id);
                var result = _mapper.Map<Entities.TypeOfRequirement, TypeOfRequirementModel>(typeOfRequirement);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SaveServiceCategoryModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var typeOfRequirement = _mapper.Map<SaveServiceCategoryModel, Entities.TypeOfRequirement>(model);
                var result = await typeOfRequirementService.AddAsync(typeOfRequirement);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Update(int id, [FromBody] SaveServiceCategoryModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var typeOfRequirement = _mapper.Map<SaveServiceCategoryModel, Entities.TypeOfRequirement>(model);
                await typeOfRequirementService.UpdateAsync(typeOfRequirement);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await typeOfRequirementService.DeleteAsync(id);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
