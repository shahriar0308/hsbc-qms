﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Berger.Web.Controllers.LoyaltyProgram
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/loyalty-program-point")]
    public class LoyaltyProgramPointController : BaseController
    {
        private readonly ILoyaltyProgramPointService loyaltyProgramPointService;
        private readonly IMapper _mapper;

        public LoyaltyProgramPointController(
            ILoyaltyProgramPointService loyaltyProgramPointService,
            IMapper mapper)
        {
            this.loyaltyProgramPointService = loyaltyProgramPointService;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var loyalty = await loyaltyProgramPointService.GetFirstOrDefaultAsync();
                var result = _mapper.Map<LoyaltyProgramPoint, LoyaltyProgramPointModel>(loyalty);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] SaveLoyaltyProgramPointModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }

            try
            {
                var loyalty = _mapper.Map<SaveLoyaltyProgramPointModel, LoyaltyProgramPoint>(model);
                await loyaltyProgramPointService.UpdateAsync(loyalty);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
