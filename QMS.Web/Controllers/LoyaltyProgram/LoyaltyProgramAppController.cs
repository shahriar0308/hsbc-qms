﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Berger.Web.Controllers.LoyaltyProgram
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/app/loyalty-program")]
    public class LoyaltyProgramAppController : BaseController
    {
        private readonly ILoyaltyProgramInformationService loyaltyProgramInformationService;
        private readonly IMapper _mapper;

        public LoyaltyProgramAppController(
            ILoyaltyProgramInformationService loyaltyProgramInformationService,
            IMapper mapper)
        {
            this.loyaltyProgramInformationService = loyaltyProgramInformationService;
            this._mapper = mapper;
        }


        //[HttpGet("id/{userId}")]
        //public async Task<IActionResult> GetByUserId(string userId)
        //{
        //    try
        //    {
        //        var loyalty = await loyaltyProgramInformationService.GetByUserId(userId);
        //        var result = _mapper.Map<LoyaltyProgramInformation, LoyaltyProgramAppModel>(loyalty);
        //        return OkResult(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ExceptionResult(ex);
        //    }
        //}

        //[HttpGet("name/{userName}")]
        //public async Task<IActionResult> GetByUsername(string userName)
        //{
        //    try
        //    {
        //        var loyalty = await loyaltyProgramInformationService.GetByUsername(userName);
        //        var result = _mapper.Map<LoyaltyProgramInformation, LoyaltyProgramAppModel>(loyalty);
        //        return OkResult(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ExceptionResult(ex);
        //    }
        //}

        [HttpGet()]
        public async Task<IActionResult> Get([FromQuery] string phoneNo = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(phoneNo))
                {
                    var loyalty = await loyaltyProgramInformationService.Get(phoneNo);
                    var result = _mapper.Map<LoyaltyProgramInformation, LoyaltyProgramAppModel>(loyalty);
                    return OkResult(result);
                }
                else
                {
                    var loyalty = await loyaltyProgramInformationService.Get();
                    var result = _mapper.Map<LoyaltyProgramInformation, LoyaltyProgramAppModel>(loyalty);
                    return OkResult(result);
                }
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
