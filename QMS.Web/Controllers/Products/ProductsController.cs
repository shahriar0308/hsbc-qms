﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Common.Enums;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Core;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Berger.Web.Controllers.Products
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/products")]
    public class ProductsController : BaseController
    {
        private readonly IProductService _ProductService;
        private readonly IMapper _mapper;
        private readonly IPhotoStorage photoStorage;

        public ProductsController(
            IProductService ProductService,
            IMapper mapper, IPhotoStorage photoStorage)
        {
            this._ProductService = ProductService;
            this._mapper = mapper;
            this.photoStorage = photoStorage;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] ProductQuery query)
        {
            try
            {
                var result = await _ProductService.GetAllAsync(query);
                var queryResult = _mapper.Map<QueryResult<Berger.Entities.Product>, QueryResult<ProductModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("surface")]
        public async Task<IActionResult> GetBySurface([FromQuery] int surfaceType)
        {
            try
            {
                var result = await _ProductService.GetBySurfaceAsync(surfaceType);
                var queryResult = _mapper.Map<IList<Entities.Product>, IList<ProductModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("surface/select")]
        public async Task<IActionResult> GetBySurfaceSelect([FromQuery] int surfaceType)
        {
            try
            {
                var result = await _ProductService.GetBySurfaceSelectAsync(surfaceType);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("surface-type")]
        public IActionResult GetSurfaceType()
        {
            try
            {
                var result = _ProductService.GetSurfaceTypeSelectAsync();
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("surface-condition")]
        public IActionResult GetSurfaceCondition()
        {
            try
            {
                var result = _ProductService.GetSurfaceConditionSelectAsync();
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var Product = await _ProductService.GetByIdAsync(id);
                var result = _mapper.Map<Entities.Product, ProductModel>(Product);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        //[HttpPost]
        //public async Task<IActionResult> Create([FromForm] string data, [FromForm] IFormFile file)
        //{
        //    //TryValidateModel(model);

        //    if (!ModelState.IsValid)
        //    {
        //        return ValidationResult(ModelState);
        //    }
        //    try
        //    {
        //        SaveProductModel model = JsonConvert.DeserializeObject<SaveProductModel>(data);
        //        if (file != null)
        //        {
        //            var message = string.Empty;
        //            var fileName = string.Empty;
        //            var location = GenerateRequiredDirectories(Directory.GetCurrentDirectory());
        //            if (!FileUpload(file, location, out message, out fileName))
        //            {
        //                throw new Exception(message);
        //            }
        //            model.ImageUrl = fileName;
        //        }
        //        var Product = _mapper.Map<SaveProductModel, Entities.Product>(model);
        //        var result = await _ProductService.AddAsync(Product);
        //        return OkResult(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ExceptionResult(ex);
        //    }
        //}

        [HttpPost]
        public async Task<IActionResult> Create([FromForm] SaveProductModel model)
        {
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {

                var product = _mapper.Map<SaveProductModel, Entities.Product>(model);
                if (model.Image != null)
                {
                    await photoStorage.IsValidImageAsync(model.Image);
                    var fileName = model.Name + "_" + Guid.NewGuid().ToString();
                    var imageUrl = await photoStorage.SaveImageAsync(model.Image, fileName, EnumFileUploadFolderCode.Product);
                    product.ImageUrl = imageUrl;
                }
                var result = await _ProductService.AddAsync(product);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
        [HttpPut]
        public async Task<IActionResult> Update([FromForm] SaveProductModel model)
        {
            //model.ImageUrl = (!string.IsNullOrEmpty(model.ImageUrl)) ? model.ImageUrl.Split('/').Last(): "";
            if (!ModelState.IsValid)
            {
                return ValidationResult(ModelState);
            }
            try
            {
                var product = _mapper.Map<SaveProductModel, Entities.Product>(model);
                if (model.Image != null)
                {
                    await photoStorage.IsValidImageAsync(model.Image);
                    var fileName = model.Name + "_" + Guid.NewGuid().ToString();
                    var imageUrl = await photoStorage.SaveImageAsync(model.Image, fileName, EnumFileUploadFolderCode.Product);
                    product.ImageUrl = imageUrl;
                }
                await _ProductService.UpdateAsync(product);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var product = await _ProductService.GetByIdAsync(id);
                await _ProductService.DeleteAsync(id);
                await photoStorage.DeleteImageAsync(product.ImageUrl);
                return OkResult(true);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        //private string GenerateRequiredDirectories(string currentDirectory)
        //{
        //    try
        //    {
        //        var folderName = "wwwroot\\Resources";
        //        var subFolderName = "Images";
        //        var subSubFolderName = "Products";
        //        var location = GenerateDirectoryIfNotExist(currentDirectory, folderName);
        //        location = GenerateDirectoryIfNotExist(location, subFolderName);
        //        location = GenerateDirectoryIfNotExist(location, subSubFolderName);
        //        return location;
        //    }
        //    catch (Exception)
        //    {
        //        return "";
        //    }
        //}

        //private string GenerateDirectoryIfNotExist(string path, string folderName)
        //{
        //    var pathToSave = Path.Combine(path, folderName);
        //    if (!Directory.Exists(pathToSave))
        //    {
        //        Directory.CreateDirectory(pathToSave);
        //    }
        //    return pathToSave;
        //}

        //private bool DeleteExistingFile(string path)
        //{
        //    try
        //    {
        //        System.IO.File.Delete(path);
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

        //private bool FileUpload(IFormFile file, string uploadLocation, out string message, out string fileName)
        //{
        //    message = "";
        //    fileName = "";
        //    string[] exts = { "jpg", "png", "gif", "jpeg" };
        //    try
        //    {
        //        if (file != null && file.Length > 0)
        //        {
        //            var ofn = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
        //            var ext = ofn.Split('.').Last();

        //            if (!exts.Contains(ext.ToLower()))
        //            {
        //                message = "Unsupperted file type.";
        //                return false;
        //            }

        //            fileName = Guid.NewGuid().ToString() + "." + ext;
        //            var fullPath = Path.Combine(uploadLocation, fileName);

        //            using (var stream = new FileStream(fullPath, FileMode.Create))
        //            {
        //                file.CopyTo(stream);
        //            }

        //            return true;
        //        }
        //        message = "File not found";
        //        return false;
        //    }
        //    catch (Exception ex)
        //    {
        //        message = $"{ex.Message}";
        //        return false;
        //    }
        //}
    }
}
