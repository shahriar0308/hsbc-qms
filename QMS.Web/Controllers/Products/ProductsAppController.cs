﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AutoMapper;
using Berger.Entities.NotMapped;
using Berger.Services.Interfaces;
using Berger.Web.Controllers.Common;
using Berger.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Berger.Web.Controllers.Products
{
    [ApiVersion("1")]
    [Route("api/v{v:apiVersion}/app/products")]
    public class ProductsAppController : BaseController
    {
        private readonly IProductService _ProductService;
        private readonly IMapper _mapper;

        public ProductsAppController(
            IProductService ProductService,
            IMapper mapper)
        {
            this._ProductService = ProductService;
            this._mapper = mapper;
        }

        //[HttpGet]
        //public async Task<IActionResult> Get()
        //{
        //    try
        //    {
        //        var result = await _ProductService.GetAllAsync();
        //        var queryResult = _mapper.Map<IList<Entities.Product>, IList<ProductModel>>(result);
        //        return OkResult(queryResult);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ExceptionResult(ex);
        //    }
        //}

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] int surfaceType)
        {
            try
            {
                IList<Entities.Product> result;
                if (surfaceType == 0)
                    result = await _ProductService.GetAllAsync();
                else
                    result = await _ProductService.GetBySurfaceAsync(surfaceType);
                //IList<Entities.Product> result = await _ProductService.GetBySurfaceAsync(surfaceType, surfaceCondition);
                var queryResult = _mapper.Map<IList<Entities.Product>, IList<ProductModel>>(result);
                return OkResult(queryResult);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }



        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var Product = await _ProductService.GetByIdAsync(id);
                var result = _mapper.Map<Berger.Entities.Product, ProductModel>(Product);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("surface-type")]
        public IActionResult GetSurfaceType()
        {
            try
            {
                var result = _ProductService.GetSurfaceTypeSelectAsync();
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }

        [HttpGet("surface-condition")]
        public IActionResult GetSurfaceCondition()
        {
            try
            {
                var result = _ProductService.GetSurfaceConditionSelectAsync();
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return ExceptionResult(ex);
            }
        }
    }
}
