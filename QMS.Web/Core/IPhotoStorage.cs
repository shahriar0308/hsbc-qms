using System.Drawing;
using System.Threading.Tasks;
using Berger.Common.Enums;
using Microsoft.AspNetCore.Http;

namespace Berger.Web.Core
{
    public interface IPhotoStorage
    {
        Task<string> StorePhotoAsync(string uploadsFolderPath, IFormFile file);
        Task<string> SaveImageAsync(IFormFile file, string fileName, EnumFileUploadFolderCode type);
        Task<string> SaveImageAsync(IFormFile file, string fileName, EnumFileUploadFolderCode type, int width, int height);
        Task<string> SaveImageAsync(string base64String, string fileName, EnumFileUploadFolderCode type);
        Task<string> SaveImageAsync(string base64String, string fileName, EnumFileUploadFolderCode type, int width, int height);
        Task<Image> ResizeImageAsync(Image image, int width, int height);
        Task DeleteImageAsync(string filePath);
        Task<bool> IsValidImageAsync(IFormFile file);
        string ImageFolderPath(EnumFileUploadFolderCode type);
    }
}