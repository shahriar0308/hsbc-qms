﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Web.Core
{
    public class AuthSMSSenderOptions
    {
        public string UserId { get; set; }
        public string UserPassword { get; set; }
        public int RouteId { get; set; }
        public int SMSTypeId { get; set; }
        public string SMSSender { get; set; }
        public string SMSCategoryName { get; set; }
        public string SMSText { get; set; }
        public string SMSSendAPIUrl { get; set; }
        public bool IsOTPExpires { get; set; }
        public int OTPExpiresMinutes { get; set; }
    }
}
