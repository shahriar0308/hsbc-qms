﻿import { MapObject } from './map-object';

export enum EnumYesNo {
    No = 0,
    Yes = 1
}

export class EnumYesNoLabel {
    public static enumYesNoLabel: MapObject[] = [
        { id: 0, label: "No" },
        { id: 1, label: "Yes" }
    ];

    constructor() {
    }
}