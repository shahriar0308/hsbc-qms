import { MapObject } from './map-object';

export enum ComplainType {
    General = 1,
    NonGeneral = 2 
}

export class ComplainTypeObject{

    public static ComplainType :  MapObject[] = [
    { id : 1, label : "General" },
    { id : 2, label : "Non-general" }
    
    ];


}
