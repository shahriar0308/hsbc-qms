import { MapObject } from './map-object';

export enum EnumStatus {
    Inactive = 0,
    Active = 1
}

export class EnumStatusLabel {
    public static enumStatusLabel: MapObject[] = [
        { id: 0, label: "Inactive" },
        { id: 1, label: "Active" }
    ];

    constructor() {
    }
}
