import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../Common/common.service';
import { SaveUserRole } from '../../Entity/Users/role';
import { Guid } from 'guid-typescript';
import { APIResponse } from '../../Entity/Response/api-response';

@Injectable({
    providedIn: 'root'
})
export class DivisionService {
    public baseUrl: string;
    public divisionsEndpoint: string;

    constructor(
        private http: HttpClient, 
        @Inject('BASE_URL') baseUrl: string,
        private commonService: CommonService) {
            this.baseUrl = baseUrl + 'api/';
            this.divisionsEndpoint = this.baseUrl + 'v1/divisions';
    }

    getDivisionSelect () {
      return this.http.get<APIResponse>(`${this.divisionsEndpoint}/select`);
    }

    getDivisionList(query) {
        return this.http.get<APIResponse>(this.baseUrl + 'v1/divisions/'+'?'+this.commonService.toQueryString(query));
    }

    public getDivision(id: number) {
        return this.http.get<APIResponse>(this.baseUrl + 'v1/divisions/' + id);
    }

    public postDivision(model) {
        return this.http.post<APIResponse>(this.baseUrl + 'v1/divisions/', model);
    }

    public putDivision(model) {
        return this.http.put<APIResponse>(this.baseUrl + 'v1/divisions/', model);
    }

    public deleteDivision(id: number) {
        return this.http.delete<any>(`${this.baseUrl}v1/divisions/${id}`);
    }
}

