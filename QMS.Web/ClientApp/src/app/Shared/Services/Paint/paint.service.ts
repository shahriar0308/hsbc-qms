﻿import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../Common/common.service';
import { APIResponse } from '../../Entity/Response/api-response';
import { SavePaintingServiceCost } from '../../Entity/PaintingServiceCost/painting-service-cost';

@Injectable({
    providedIn: 'root'
})

export class PaintService {
    public baseUrl: string;
    public paintsEndpoint: string;

    constructor(
      private http: HttpClient,
      @Inject('BASE_URL') baseUrl: string,
      private commonService: CommonService) {
        this.baseUrl = baseUrl + 'api/';
        this.paintsEndpoint = this.baseUrl + 'v1/paint-service-cost';
    }
  
    getServiceCost(id) {
      return this.http.get<APIResponse>(`${this.paintsEndpoint}/${id}`);
    }

    getForGrid(surfaceType: number, surfaceCondition: number) {
      return this.http.get<APIResponse>(`${this.paintsEndpoint}/grid?surfaceType=${surfaceType}&surfaceCondition=${surfaceCondition}`);
    }
  
    getServiceCosts() {
      return this.http.get<APIResponse>(`${this.paintsEndpoint}`);
    }

    create(paintingServiceCost: SavePaintingServiceCost) {
      paintingServiceCost.id = 0;
      return this.http.post<APIResponse>(`${this.paintsEndpoint}`, paintingServiceCost);
    }

    createOrUpdate(paintingServiceCosts: SavePaintingServiceCost[]) {
      return this.http.post<APIResponse>(`${this.paintsEndpoint}`, paintingServiceCosts);
    }
  
    update(paintingServiceCost: SavePaintingServiceCost) {
      return this.http.put<APIResponse>(`${this.paintsEndpoint}`, paintingServiceCost);
    }
  
    delete(id) {
      return this.http.delete<APIResponse>(`${this.paintsEndpoint}/${id}`);
    }
}