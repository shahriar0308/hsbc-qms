﻿import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../Common/common.service';
import { Guid } from 'guid-typescript';
import { APIResponse } from '../../Entity/Response/api-response';
import { SavePromotionalBanner } from '../../Entity/PromotionalBanner/promotional-banner';

@Injectable({
    providedIn: 'root'
})

export class PromotionalBannerService {
    public baseUrl: string;
    public PromotionalBannersEndpoint: string;

    constructor(
      private http: HttpClient,
      @Inject('BASE_URL') baseUrl: string,
      private commonService: CommonService) {
        this.baseUrl = baseUrl + 'api/';
        this.PromotionalBannersEndpoint = this.baseUrl + 'v1/promotional-banners';
    }
  
    getPromotionalBanner(id) {
      return this.http.get<APIResponse>(`${this.PromotionalBannersEndpoint}/${id}`);
    }
  
    getPromotionalBanners(filter?) {
      return this.http.get<APIResponse>(`${this.PromotionalBannersEndpoint}?${this.commonService.toQueryString(filter)}`);
    }

    create(PromotionalBanner: SavePromotionalBanner) {
      PromotionalBanner.id = 0;
      return this.http.post<APIResponse>(`${this.PromotionalBannersEndpoint}`, this.commonService.toFormData( PromotionalBanner));
    }
  
    update(PromotionalBanner: SavePromotionalBanner) {
      return this.http.put<APIResponse>(`${this.PromotionalBannersEndpoint}/${PromotionalBanner.id}`, this.commonService.toFormData( PromotionalBanner));
    }
  
    delete(id) {
      return this.http.delete<APIResponse>(`${this.PromotionalBannersEndpoint}/${id}`);
    }
  
    activeInactive(id) {
      return this.http.post<APIResponse>(`${this.PromotionalBannersEndpoint}/activeInactive/${id}`, null);
    }
}