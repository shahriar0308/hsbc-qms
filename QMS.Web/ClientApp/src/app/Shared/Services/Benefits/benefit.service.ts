﻿import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../Common/common.service';
import { Guid } from 'guid-typescript';
import { APIResponse } from '../../Entity/Response/api-response';
import { SaveBenefit } from '../../Entity/Benefit/benefits';

@Injectable({
    providedIn: 'root'
})

export class BenefitService {
    public baseUrl: string;
    public benefitsEndpoint: string;

    constructor(
      private http: HttpClient,
      @Inject('BASE_URL') baseUrl: string,
      private commonService: CommonService) {
        this.baseUrl = baseUrl + 'api/';
        this.benefitsEndpoint = this.baseUrl + 'v1/benefits';
    }
  
    getBenefit(id) {
      return this.http.get<APIResponse>(`${this.benefitsEndpoint}/${id}`);
    }
  
    getBenefits(filter?) {
      return this.http.get<APIResponse>(`${this.benefitsEndpoint}?${this.commonService.toQueryString(filter)}`);
    }

    create(benefit: SaveBenefit) {
      benefit.id = 0;
      return this.http.post<APIResponse>(`${this.benefitsEndpoint}`, benefit);
    }
  
    update(product: SaveBenefit) {
      return this.http.put<APIResponse>(`${this.benefitsEndpoint}`, product);
    }
  
    delete(id) {
      return this.http.delete<APIResponse>(`${this.benefitsEndpoint}/${id}`);
    }
  
    activeInactive(id) {
      return this.http.post<APIResponse>(`${this.benefitsEndpoint}/activeInactive/${id}`, null);
    }
}