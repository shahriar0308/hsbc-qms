﻿import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../Common/common.service';
import { APIResponse } from '../../Entity/Response/api-response';
import { SaveTypeOfRequirement } from '../../Entity/TypeOfRequirement/type-of-requirement';

@Injectable({
    providedIn: 'root'
})

export class TypeOfRequirementService {
    public baseUrl: string;
    public typeOfRequirementsEndpoint: string;

    constructor(
      private http: HttpClient,
      @Inject('BASE_URL') baseUrl: string,
      private commonService: CommonService) {
        this.baseUrl = baseUrl + 'api/';
        this.typeOfRequirementsEndpoint = this.baseUrl + 'v1/type-of-requirement';
    }
  
    getTypeOfRequirement(id) {
      return this.http.get<APIResponse>(`${this.typeOfRequirementsEndpoint}/${id}`);
    }
  
    getTypeOfRequirements(filter?) {
      return this.http.get<APIResponse>(`${this.typeOfRequirementsEndpoint}?${this.commonService.toQueryString(filter)}`);
    }

    create(typeOfRequirement: SaveTypeOfRequirement) {
      typeOfRequirement.id = 0;
      return this.http.post<APIResponse>(`${this.typeOfRequirementsEndpoint}`, typeOfRequirement);
    }
  
    update(product: SaveTypeOfRequirement) {
      return this.http.put<APIResponse>(`${this.typeOfRequirementsEndpoint}`, product);
    }
  
    delete(id) {
      return this.http.delete<APIResponse>(`${this.typeOfRequirementsEndpoint}/${id}`);
    }
  
    activeInactive(id) {
      return this.http.post<APIResponse>(`${this.typeOfRequirementsEndpoint}/activeInactive/${id}`, null);
    }
}