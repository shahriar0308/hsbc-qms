﻿import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../Common/common.service';
import { Guid } from 'guid-typescript';
import { APIResponse } from '../../Entity/Response/api-response';
import { SaveSetting } from '../../Entity/Settings/setting';

@Injectable({
    providedIn: 'root'
})

export class PointSettingsService {
    public baseUrl: string;
    public settingsEndpoint: string;

    constructor(
      private http: HttpClient,
      @Inject('BASE_URL') baseUrl: string,
      private commonService: CommonService) {
        this.baseUrl = baseUrl + 'api/';
        this.settingsEndpoint = this.baseUrl + 'v1/settings';
    }
    getSelect () {
      return this.http.get<APIResponse>(`${this.settingsEndpoint}/select`);
    }
    getSetting(id) {
      return this.http.get<APIResponse>(`${this.settingsEndpoint}/${id}`);
    }
  
    getSettings(filter?) {
      return this.http.get<APIResponse>(`${this.settingsEndpoint}?${this.commonService.toQueryString(filter)}`);
    }

    create(setting: SaveSetting) {
      setting.id = 0;
      return this.http.post<APIResponse>(`${this.settingsEndpoint}`, setting);
    }
  
    update(setting: SaveSetting) {
      return this.http.put<APIResponse>(`${this.settingsEndpoint}/${setting.id}`, setting);
    }
  
    delete(id) {
      return this.http.delete<APIResponse>(`${this.settingsEndpoint}/${id}`);
    }
  
    activeInactive(id) {
      return this.http.post<APIResponse>(`${this.settingsEndpoint}/activeInactive/${id}`, null);
    }
}