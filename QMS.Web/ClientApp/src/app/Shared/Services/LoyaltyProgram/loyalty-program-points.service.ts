﻿import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../Common/common.service';
import { APIResponse } from '../../Entity/Response/api-response';
import { SaveLoyaltyProgramPoint } from '../../Entity/LoyaltyProgram/loyalty-program-point';

@Injectable({
    providedIn: 'root'
})

export class LoyaltyProgramPointService {
    public baseUrl: string;
    public loyaltyProgramPointsEndpoint: string;

    constructor(
      private http: HttpClient,
      @Inject('BASE_URL') baseUrl: string,
      private commonService: CommonService) {
        this.baseUrl = baseUrl + 'api/';
        this.loyaltyProgramPointsEndpoint = this.baseUrl + 'v1/loyalty-program-point';
    }
  
    getFirstOrDefaultLoyaltyProgramPoint() {
      return this.http.get<APIResponse>(`${this.loyaltyProgramPointsEndpoint}`);
    }
  
    update(loyaltyProgramPoint: SaveLoyaltyProgramPoint) {
      return this.http.put<APIResponse>(`${this.loyaltyProgramPointsEndpoint}/${loyaltyProgramPoint.id}`, loyaltyProgramPoint);
    }
}