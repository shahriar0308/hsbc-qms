﻿import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../Common/common.service';
import { Guid } from 'guid-typescript';
import { APIResponse } from '../../Entity/Response/api-response';
import { SaveColorCode } from '../../Entity/ColorCode/color-code';

@Injectable({
    providedIn: 'root'
})

export class ColorCodeService {
    public baseUrl: string;
    public colorCodesEndpoint: string;

    constructor(
      private http: HttpClient,
      @Inject('BASE_URL') baseUrl: string,
      private commonService: CommonService) {
        this.baseUrl = baseUrl + 'api/';
        this.colorCodesEndpoint = this.baseUrl + 'v1/color-codes';
    }
  
    getColorCode(id) {
      return this.http.get<APIResponse>(`${this.colorCodesEndpoint}/${id}`);
    }
  
    getColorCodes(filter?) {
      return this.http.get<APIResponse>(`${this.colorCodesEndpoint}?${this.commonService.toQueryString(filter)}`);
    }

    create(colorCode: SaveColorCode) {
      colorCode.id = 0;
      return this.http.post<APIResponse>(`${this.colorCodesEndpoint}`, colorCode);
    }
  
    update(colorCode: SaveColorCode) {
      return this.http.put<APIResponse>(`${this.colorCodesEndpoint}/${colorCode.id}`, colorCode);
    }
  
    delete(id) {
      return this.http.delete<APIResponse>(`${this.colorCodesEndpoint}/${id}`);
    }
  
    activeInactive(id) {
      return this.http.post<APIResponse>(`${this.colorCodesEndpoint}/activeInactive/${id}`, null);
    }

    excelImport(model) {
        return this.http.post(`${this.colorCodesEndpoint}/excelImport`, this.commonService.toFormData(model));
    }
}