﻿import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../Common/common.service';
import { APIResponse } from '../../Entity/Response/api-response';
import { SaveServiceRequisition } from '../../Entity/ServiceRequisition/service-requisition';
import { Guid } from 'guid-typescript';

@Injectable({
    providedIn: 'root'
})

export class ServiceRequisitionService {
    public baseUrl: string;
    public serviceRequisitionsEndpoint: string;

    constructor(
      private http: HttpClient,
      @Inject('BASE_URL') baseUrl: string,
      private commonService: CommonService) {
        this.baseUrl = baseUrl + 'api/';
        this.serviceRequisitionsEndpoint = this.baseUrl + 'v1/service-requisition';
    }
  
    getServiceRequisitions(workIdProvided) {
      return this.http.get<APIResponse>(`${this.serviceRequisitionsEndpoint}?workIdProvided=${workIdProvided}`);
    }

    getServiceRequisitionDetails(id: Guid) {
      return this.http.get<APIResponse>(`${this.serviceRequisitionsEndpoint}/details?id=${id}`);
    }
  
    update(product: SaveServiceRequisition) {
      return this.http.put<APIResponse>(`${this.serviceRequisitionsEndpoint}`, product);
    }
}