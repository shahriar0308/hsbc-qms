﻿import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../Common/common.service';
import { Guid } from 'guid-typescript';
import { APIResponse } from '../../Entity/Response/api-response';
import { SaveDealer } from '../../Entity/Dealers/dealer';
import { SaveProduct } from '../../Entity/Product/product';

@Injectable({
    providedIn: 'root'
})

export class ProductService {
    public baseUrl: string;
    public productsEndpoint: string;

    constructor(
      private http: HttpClient,
      @Inject('BASE_URL') baseUrl: string,
      private commonService: CommonService) {
        this.baseUrl = baseUrl + 'api/';
        this.productsEndpoint = this.baseUrl + 'v1/products';
    }
  
    getProduct(id) {
      return this.http.get<APIResponse>(`${this.productsEndpoint}/${id}`);
    }
  
    getProducts(filter?) {
      return this.http.get<APIResponse>(`${this.productsEndpoint}?${this.commonService.toQueryString(filter)}`);
    }

    getBySurfaceType(surfaceType: number) {
      return this.http.get<APIResponse>(this.productsEndpoint + '/surface/select?surfaceType=' + surfaceType);
    }

    getSurfaceTypes() {
      return this.http.get<APIResponse>(this.productsEndpoint + '/surface-type');
    }
    getSurfaceConditions() {
      return this.http.get<APIResponse>(this.productsEndpoint + '/surface-condition');
    }
    getPackages() {
      return this.http.get<APIResponse>(this.baseUrl + 'v1/package');
    }

    create(product: SaveProduct) {

      product.id = 0;
      var _product = this.commonService.toFormData(product);
      return this.http.post<APIResponse>(`${this.productsEndpoint}`, _product);
    }
  
    update(product: SaveProduct) {
      var _product = this.commonService.toFormData(product);
      return this.http.put<APIResponse>(`${this.productsEndpoint}`, _product);
    }
  
    delete(id) {
      return this.http.delete<APIResponse>(`${this.productsEndpoint}/${id}`);
    }
  
    activeInactive(id) {
      return this.http.post<APIResponse>(`${this.productsEndpoint}/activeInactive/${id}`, null);
    }
}