import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../Common/common.service';
import { APIResponse } from '../../Entity/Response/api-response';

@Injectable({
  providedIn: 'root'
})
export class ComplainService {

  public baseUrl: string;

    constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string, private commonService: CommonService) {
        console.log("baseUrl: ", baseUrl);
        this.baseUrl = baseUrl + 'api/';
    }

    getComplainList(query) {
        return this.http.get<APIResponse>(this.baseUrl + 'v1/complain/'+'?'+this.commonService.toQueryString(query));
    }

    public getComplain(id: number) {
        return this.http.get<APIResponse>(this.baseUrl + 'v1/complain/' + id);
    }

    public deleteComplain(id: number) {
        return this.http.delete<any>(`${this.baseUrl}v1/complain/${id}`);
    }
}
