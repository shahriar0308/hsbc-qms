﻿import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../Common/common.service';
import { Guid } from 'guid-typescript';
import { APIResponse } from '../../Entity/Response/api-response';
import { SaveDealer } from '../../Entity/Dealers/dealer';

@Injectable({
    providedIn: 'root'
})

export class DealerService {
    public baseUrl: string;
    public dealersEndpoint: string;

    constructor(
      private http: HttpClient,
      @Inject('BASE_URL') baseUrl: string,
      private commonService: CommonService) {
        this.baseUrl = baseUrl + 'api/';
        this.dealersEndpoint = this.baseUrl + 'v1/dealer';
    }
  
    getDealer(id) {
      return this.http.get<APIResponse>(`${this.dealersEndpoint}/${id}`);
    }
  
    getDealers(filter?) {
      return this.http.get<APIResponse>(`${this.dealersEndpoint}?${this.commonService.toQueryString(filter)}`);
    }

    create(dealer: SaveDealer) {
      dealer.id = 0;
      return this.http.post<APIResponse>(`${this.dealersEndpoint}`, dealer);
    }
  
    update(dealer: SaveDealer) {
      return this.http.put<APIResponse>(`${this.dealersEndpoint}/${dealer.id}`, dealer);
    }
  
    delete(id) {
      return this.http.delete<APIResponse>(`${this.dealersEndpoint}/${id}`);
    }
  
    activeInactive(id) {
      return this.http.post<APIResponse>(`${this.dealersEndpoint}/activeInactive/${id}`, null);
    }
}