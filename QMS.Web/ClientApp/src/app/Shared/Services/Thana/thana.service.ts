import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../Common/common.service';
import { APIResponse } from '../../Entity/Response/api-response';

@Injectable({
  providedIn: 'root'
})
export class ThanaService {
  public baseUrl: string;

  constructor(
    private http: HttpClient,
    @Inject('BASE_URL') baseUrl: string,
    private commonService: CommonService) {
    this.baseUrl = baseUrl + 'api/';
  }

  public getThana(id: number) {
    return this.http.get<APIResponse>(this.baseUrl + 'v1/thanas/' + id);
  }

  public getThanas(query) {
    return this.http.get<APIResponse>(this.baseUrl + 'v1/thanas' + '?' + this.commonService.toQueryString(query));
  }

  public getByDistrictSelect(districtId: number) {
    return this.http.get<APIResponse>(`${this.baseUrl}v1/thanas/select?districtId=${districtId}`);
  }

  public postThana(model) {
    return this.http.post<APIResponse>(this.baseUrl + 'v1/thanas/', model);
  }

  public putThana(model) {
    return this.http.put<APIResponse>(this.baseUrl + 'v1/thanas/', model);
  }

  public deleteThana(id: number) {
    return this.http.delete<any>(`${this.baseUrl}v1/thanas/${id}`);
  }
}
