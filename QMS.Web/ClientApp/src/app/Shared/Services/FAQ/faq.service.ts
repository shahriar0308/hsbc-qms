import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../Common/common.service';
import { APIResponse } from '../../Entity/Response/api-response';

@Injectable({
  providedIn: 'root'
})
export class FaqService {

  public baseUrl: string;

    constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string, private commonService: CommonService) {
        console.log("baseUrl: ", baseUrl);
        this.baseUrl = baseUrl + 'api/';
    }

    getFAQList(query) {
        return this.http.get<APIResponse>(this.baseUrl + 'v1/faqs/'+'?'+this.commonService.toQueryString(query));
    }

    getFAQTypes() {
        return this.http.get<APIResponse>(this.baseUrl + 'v1/faqs/faqtype');
    }

    public getFAQ(id: number) {
        return this.http.get<APIResponse>(this.baseUrl + 'v1/faqs/' + id);
    }

    public postFAQ(model) {
        return this.http.post<APIResponse>(this.baseUrl + 'v1/faqs/', model);
    }

    public putFAQ(model) {
        return this.http.put<APIResponse>(this.baseUrl + 'v1/faqs/', model);
    }

    public deleteFAQ(id: number) {
        return this.http.delete<any>(`${this.baseUrl}v1/faqs/${id}`);
    }
}
