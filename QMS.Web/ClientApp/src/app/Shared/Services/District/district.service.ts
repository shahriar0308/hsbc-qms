import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../Common/common.service';
import { APIResponse } from '../../Entity/Response/api-response';

@Injectable({
  providedIn: 'root'
})
export class DistrictService {

  public baseUrl: string;

    constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string, private commonService: CommonService) {
        console.log("baseUrl: ", baseUrl);
        this.baseUrl = baseUrl + 'api/';
    }

    public getDistrict(id: number) {
        return this.http.get<APIResponse>(this.baseUrl + 'v1/districts/' + id);
    }

    public getDistricts(query) {
        return this.http.get<APIResponse>(this.baseUrl + 'v1/districts'+'?'+this.commonService.toQueryString(query));
    }

    public getByDivisionSelect (divisionId: number = 0) {
        console.log(divisionId);
       return this.http.get<APIResponse>(`${this.baseUrl}v1/districts/select?divisionId=${divisionId}`);
    }
    
    public postDistrict(model) {
        return this.http.post<APIResponse>(this.baseUrl + 'v1/districts/', model);
    }

    public putDistrict(model) {
        return this.http.put<APIResponse>(this.baseUrl + 'v1/districts/', model);
    }

    public deleteDistrict(id: number) {
        return this.http.delete<any>(`${this.baseUrl}v1/districts/${id}`);
    }
}
