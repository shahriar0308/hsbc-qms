﻿import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../Common/common.service';
import { APIResponse } from '../../Entity/Response/api-response';
import { SingleValuedEntity } from '../../Entity/SingleValuedEntities/single-valued-entity';

@Injectable({
    providedIn: 'root'
})

export class SingleValuedService {
    public baseUrl: string;
    public singleValuesEndpoint: string;

    constructor(
      private http: HttpClient,
      @Inject('BASE_URL') baseUrl: string,
      private commonService: CommonService) {
        this.baseUrl = baseUrl + 'api/';
        this.singleValuesEndpoint = this.baseUrl + 'v1/single-value';
    }
  
    getValue(id) {
      return this.http.get<APIResponse>(`${this.singleValuesEndpoint}/${id}`);
    }
  
    getByName(name) {
      return this.http.get<APIResponse>(`${this.singleValuesEndpoint}?name=${name}`);
    }
 
    update(entity: SingleValuedEntity) {
      return this.http.put<APIResponse>(`${this.singleValuesEndpoint}`, entity);
    }
}