﻿import { Auditable } from '../Common/auditable';
import { Entity } from '../Common/entity';
import { QueryObject } from '../Common/query-object';

export class User extends Auditable {
    fullName: string;
    // userName: string;
    email: string;
    phoneNumber: string;
    gender: string;
    address: string;
    divisionName: string;
    dateOfBirth?: Date;
    userRoleName: string;
    divisionId: number;
    userRoleId: string;
    // imageUrl: string;
    isUnderAmbassadorCampaign: boolean;
    points: number;
    pointsCategory: string;

    // only for display
    isUnderAmbassadorCampaignText: string;
    userRoleDisplayName: string;
    userViewDetailBtn: string;
    
    constructor(init?: Partial<User>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.fullName = '';
        // this.userName = '';
        this.email = '';
        this.phoneNumber = '';
        this.gender = '';
        this.address = '';
        this.divisionName = '';
        this.dateOfBirth = null;
        this.userRoleName = '';
        this.divisionId = null;
        this.userRoleId = '';
        // this.imageUrl = '';
        this.isUnderAmbassadorCampaign = false;
        this.isUnderAmbassadorCampaignText = '';
        this.points = null;
        this.pointsCategory = '';
    }
}

export class SaveUser extends Entity {
    fullName: string;
    // userName: string;
    email: string;
    phoneNumber: string;
    gender: string;
    address: string;
    divisionId: number;
    dateOfBirth?: Date;
    userRoleId: string;
    // imageUrl: string;
    // inputFile: File;
    isUnderAmbassadorCampaign: boolean;
    
    constructor(init?: Partial<SaveUser>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.fullName = '';
        // this.userName = '';
        this.email = '';
        this.phoneNumber = '';
        this.gender = '';
        this.address = '';
        this.divisionId = null;
        this.dateOfBirth = null;
        this.userRoleId = '';
        // this.imageUrl = '';
        // this.inputFile = null;
        this.isUnderAmbassadorCampaign = false;
    }
}

export class UserQuery extends QueryObject {
    fullName: string;
    userName: string;
    email: string;
    phoneNumber: string;
    
    constructor(init?: Partial<UserQuery>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        this.fullName = '';
        this.userName = '';
        this.email = '';
        this.phoneNumber = '';
    }
}