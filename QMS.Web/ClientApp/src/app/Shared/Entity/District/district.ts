import { Auditable } from '../Common/auditable';
import { Entity } from '../Common/entity';
import { QueryObject } from '../Common/query-object';

export class District extends Auditable {
    name: string;
    divisionName: string;
    divisionId: number;
    isActiveCustom: string;

    constructor(init?: Partial<District>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.name = '';
        this.divisionName = '';
        this.divisionId = null;
        this.isActive = true;
    }
}

export class SaveDistrict extends Entity {
    name: string;
    divisionId: number;
    
    constructor(init?: Partial<SaveDistrict>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.name = '';
        this.divisionId = 0;
    }
}

export class DistrictQuery extends QueryObject {
    name: string;
    
    constructor(init?: Partial<DistrictQuery>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        this.name = '';
    }
}