import { Auditable } from '../Common/auditable';
import { Entity } from '../Common/entity';
import { QueryObject } from '../Common/query-object';

export class ColorCode extends Auditable {
    shadeSequence: string;
    shadeCode: string;
    r: string;
    g: string;
    b: string;
    
    constructor(init?: Partial<ColorCode>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.shadeSequence = '';
        this.shadeCode = '';
        this.r = '';
        this.g = '';
        this.b = '';
    }
}

export class SaveColorCode extends Entity {
    shadeSequence: string;
    shadeCode: string;
    r: string;
    g: string;
    b: string;
    
    constructor(init?: Partial<SaveColorCode>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.shadeSequence = '';
        this.shadeCode = '';
        this.r = '';
        this.g = '';
        this.b = '';
    }
}

export class ColorCodeQuery extends QueryObject {
    shadeSequence: string;
    shadeCode: string;
    r: string;
    g: string;
    b: string;
    
    constructor(init?: Partial<ColorCodeQuery>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        this.shadeSequence = '';
        this.shadeCode = '';
        this.r = '';
        this.g = '';
        this.b = '';
    }
}