import { Auditable } from '../Common/auditable';
import { Entity } from '../Common/entity';
import { QueryObject } from '../Common/query-object';

export class Dealer extends Auditable {
    name: string;
    address: string;
    isActiveCustom: string;
    divisionName: string;
    divisionId: number;

    districtName: string;
    districtId: number;
    latitude: number;
    longitude: number;

    constructor(init?: Partial<Dealer>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.name = '';
        this.address = '';
        this.isActive = true;
        this.divisionId = null;
        this.districtId = null;
    }
}

export class SaveDealer extends Entity {
    name: string;
    address: string;
    divisionId: number;
    districtId: number;
    latitude: number;
    longitude: number;
    
    constructor(init?: Partial<SaveDealer>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.name = '';
        this.address = '';
    }
}

export class DealerQuery extends QueryObject {
    name: string;
    address: string;
    
    constructor(init?: Partial<DealerQuery>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        this.name = '';
        this.address = '';
    }
}