import { Auditable } from '../Common/auditable';
import { Entity } from '../Common/entity';
import { QueryObject } from '../Common/query-object';

export class PromotionalBanner extends Auditable {
    sequence: string;
    description: string;
    name: string;
    imageUrl: string;

    // for display
    isActiveText: string;
    
    constructor(init?: Partial<PromotionalBanner>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.sequence = '';
        this.description = '';
        this.name = '';
        this.imageUrl = '';
        this.isActiveText = '';
    }
}

export class SavePromotionalBanner extends Entity {
    sequence: string;
    name: string;
    description: string;
    imageFile: File;
    
    constructor(init?: Partial<SavePromotionalBanner>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.sequence = '';
        this.name = '';
        this.description = '';
        this.imageFile = null;
    }
}

export class PromotionalBannerQuery extends QueryObject {
    name: string;
    
    constructor(init?: Partial<PromotionalBannerQuery>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        this.name = '';
    }
}