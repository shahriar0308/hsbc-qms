import { Auditable } from '../Common/auditable';
import { Entity } from '../Common/entity';
import { QueryObject } from '../Common/query-object';
import { SaveDistrict } from '../District/district';

export class SingleValuedEntity extends Auditable {
    name: string;
    value: string;

    constructor(init?: Partial<SingleValuedEntity>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.name = '';
        this.value = '';
    }
}