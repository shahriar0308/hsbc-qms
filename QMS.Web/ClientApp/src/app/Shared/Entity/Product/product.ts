import { Auditable } from '../Common/auditable';
import { Entity } from '../Common/entity';
import { QueryObject } from '../Common/query-object';
import { SaveDistrict } from '../District/district';

export class Product extends Auditable {
    name: string;
    description: string;
    isActiveCustom: string;
    details: string;
    imageUrl: string;
    image: File;
    surfaceType: number;
    surfaceTypeText: string;
    paintCost: number;
    sealerCost:number;
    puttyCost:number;
    topCoatCost: number
    baseCoatCost:number;
    primerCost: number;
    
    constructor(init?: Partial<Product>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.name = '';
        this.description = '';
        this.isActive = true;
    }
}

export class SaveProduct extends Entity {
    name: string;
    description: string;
    image: File;
    keepCurrentFile: boolean;
    surfaceType: number;
    paintCost: number;
    sealerCost:number;
    puttyCost:number;
    topCoatCost: number
    baseCoatCost:number;
    primerCost: number;
       
    constructor(init?: Partial<SaveDistrict>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.name = '';
        this.description = '';
        this.keepCurrentFile = false;
    }
}

export class ProductQuery extends QueryObject {
    name: string;
    
    constructor(init?: Partial<ProductQuery>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        this.name = '';
    }
}