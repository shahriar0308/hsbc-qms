import { Auditable } from '../Common/auditable';
import { Entity } from '../Common/entity';
import { QueryObject } from '../Common/query-object';
import { SaveDistrict } from '../District/district';

export class Benefit extends Auditable {
    name: string;
    description: string;
    isActiveCustom: string;
    categoryName: string;
    settingId: number;
    sequence: number;

    constructor(init?: Partial<Benefit>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.name = '';
        this.description = '';
        this.isActive = true;
    }
}

export class SaveBenefit extends Entity {
    name: string;
    description: string;
    settingId: string;
    sequence: number;
    constructor(init?: Partial<SaveDistrict>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.name = '';
        this.description = '';
    }
}

export class BenefitQuery extends QueryObject {
    name: string;
    
    constructor(init?: Partial<BenefitQuery>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        this.name = '';
    }
}