import { Auditable } from '../Common/auditable';
import { Entity } from '../Common/entity';
import { QueryObject } from '../Common/query-object';
import { SaveDistrict } from '../District/district';

export class TypeOfRequirement extends Auditable {
    name: string;
    description: string;
    isActiveCustom: string;

    constructor(init?: Partial<TypeOfRequirement>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.name = '';
        this.description = '';
        this.isActive = true;
    }
}

export class SaveTypeOfRequirement extends Entity {
    name: string;
    description: string;
    constructor(init?: Partial<SaveTypeOfRequirement>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.name = '';
        this.description = '';
    }
}

export class TypeOfRequirementQuery extends QueryObject {
    name: string;
    
    constructor(init?: Partial<TypeOfRequirementQuery>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        this.name = '';
    }
}