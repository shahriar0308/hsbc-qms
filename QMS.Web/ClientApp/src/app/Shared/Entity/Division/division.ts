export class Division {
    public id: number;
    public name: string;
    public isActiveCustom: string;
    public isActive: boolean;

    constructor(){
        this.id = 0;
    }
    clear(){
        this.isActive = true;
    }
}
