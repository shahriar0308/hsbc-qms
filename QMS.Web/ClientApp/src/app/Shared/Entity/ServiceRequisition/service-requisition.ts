import { Auditable } from '../Common/auditable';
import { Entity } from '../Common/entity';

export class ServiceRequisition extends Auditable {
    workId: string;
    typeOfRequirementId: number;
    typeOfRequirement: string;
    districtId: number;
    districtName: string;
    thanaId: number;
    thanaName: string;
    addressLine1: string;
    customerName: string;
    customerPhoneNo: string;
    requirementDetail: string;
    serviceRequisitionDetails: ServiceRequisitionDetail[];
    detailBtn: string;

    contactName: string;
    contactNo: string;

    constructor(init?: Partial<ServiceRequisition>) {
        super();
        Object.assign(this, init);
    }
}

export class ServiceRequisitionDetail extends Auditable {
    workDetailId: string;
    stage: string;
    financialEstimateUrl: string;
    paymentSlipUrl: string;
    colorConsultantName: string;
    colorConsultantPhoneNo: string;

    constructor(init?: Partial<ServiceRequisition>) {
        super();
        Object.assign(this, init);
    }
}

export class SaveServiceRequisition extends Entity {
    workId: string;
    
    clear() {
        this.workId = '';
        super.clear();
    }
}
