import { Auditable } from '../Common/auditable';
import { QueryObject } from '../Common/query-object';
import { Entity } from '../Common/entity';

export class FAQ extends Auditable{
    public id: number;
    public questionTitle: string;
    public questionAnswer: string;
    public faqType: number;
    public faqTypeCustom: boolean;
    public isActiveCustom: string;
    public sequence: number;

    constructor(init?: Partial<FAQ>){
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.questionAnswer = '';
        this.questionTitle = '';
        this.isActive = true;
    }

}

export class SaveFAQ extends Entity {
    questionTitle: string;
    questionAnswer: string;
    faqType: number;
    sequence: number;
    
    constructor(init?: Partial<SaveFAQ>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.questionTitle = '';
        this.questionAnswer = '';
    }
}

export class FAQQuery extends QueryObject {
    questionTitle: string;
    
    constructor(init?: Partial<FAQQuery>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        this.questionTitle = '';
    }
}