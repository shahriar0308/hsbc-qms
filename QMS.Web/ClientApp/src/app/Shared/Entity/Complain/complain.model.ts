import { QueryObject } from '../Common/query-object';

export class Complain {
    public id: number;
    public complainType : number;
    public complainDetails : string;
    public isDeleted: boolean;
    public isActive: boolean;

    // display

    public displayComplainType : string;
    public displayComplainDetails : string;
    public viewDetailsbutton : string;


    constructor(){
        this.id = 0;
    }

}


export class ComplainQuery extends QueryObject {
    complainType : number;
    complainDetails : string;
    created: Date;

    constructor(init?: Partial<ComplainQuery>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        this.complainDetails = '';        
    }
}