import { Auditable } from '../Common/auditable';
import { Entity } from '../Common/entity';

export class LoyaltyProgramPoint extends Auditable {
    productCost: number;
    painterCost: number;
    supervisionCharge: number;
    
    constructor(init?: Partial<LoyaltyProgramPoint>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.productCost = null;
        this.painterCost = null;
        this.supervisionCharge = null;
    }
}

export class SaveLoyaltyProgramPoint extends Entity {
    productCost: number;
    painterCost: number;
    supervisionCharge: number;
    
    constructor(init?: Partial<SaveLoyaltyProgramPoint>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.productCost = null;
        this.painterCost = null;
        this.supervisionCharge = null;
    }
}