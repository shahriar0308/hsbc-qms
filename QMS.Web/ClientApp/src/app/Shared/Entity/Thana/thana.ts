import { Auditable } from '../Common/auditable';
import { Entity } from '../Common/entity';
import { QueryObject } from '../Common/query-object';

export class Thana extends Auditable{
    name: string;
    districtName: string;
    districtId: number;
    isActiveCustom: string;

    constructor(init?: Partial<Thana>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.name = '';
        this.districtName = '';
        this.districtId = null;
        this.isActive = true;
    }
}

export class SaveThana extends Entity {
    name: string;
    districtId: number;
    
    constructor(init?: Partial<SaveThana>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.name = '';
        this.districtId = 0;
    }
}

export class ThanaQuery extends QueryObject {
    name: string;
    
    constructor(init?: Partial<ThanaQuery>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        this.name = '';
    }
}
