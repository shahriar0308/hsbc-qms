import { Auditable } from '../Common/auditable';
import { Entity } from '../Common/entity';
import { QueryObject } from '../Common/query-object';

export class Setting extends Auditable {
    category: string;
    points: number;
    isActiveText: string;

    constructor(init?: Partial<Setting>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.category = '';
        this.points = 0;
        this.isActive  = true;
    }
}

export class SaveSetting extends Entity {
    category: string;
    points: number;
    
    constructor(init?: Partial<SaveSetting>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.category = '';
        this.points = 0;
    }
}

export class SettingQuery extends QueryObject {
    category: string;
    
    constructor(init?: Partial<SettingQuery>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        this.category = '';
    }
}