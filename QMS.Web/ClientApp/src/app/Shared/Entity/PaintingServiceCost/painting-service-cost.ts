import { Auditable } from '../Common/auditable';
import { Entity } from '../Common/entity';
import { QueryObject } from '../Common/query-object';
import { SaveDistrict } from '../District/district';

export class PaintingServiceCost extends Auditable {
    surfaceType: number;
    surfaceTypeText: string;
    surfaceCondition:number;
    surfaceConditionText:string;
    productId: number;
    productName: string;
    platinumRatePerSft: number;
    platinumRatePerSftText: string;
    goldRatePerSft: number;
    goldRatePerSftText: string;
    silverRatePerSft: number;
    silverRatePerSftText: string;
    sevenToNine: number;
    aboveNine: number;
    isActive: boolean;
    isActiveText: string;
    vat: number;

    constructor(init?: Partial<PaintingServiceCost>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.platinumRatePerSft=0;
        this.silverRatePerSft=0;
        this.goldRatePerSft=0;
        this.sevenToNine=0;
        this.aboveNine=0;
        this.vat= 0;
        this.isActive = true;
    }
}

export class SavePaintingServiceCost extends Entity {
    surfaceType: number;
    surfaceCondition:number;
    productId: number;
    package: number;
    platinumRatePerSft: number;
    goldRatePerSft: number;
    silverRatePerSft: number;
    sevenToNine: number;
    aboveNine: number;
    vat: number;
    isActive: boolean;
    constructor(init?: Partial<SavePaintingServiceCost>) {
        super();
        Object.assign(this, init);
    }

    clear() {
        super.clear();
        this.platinumRatePerSft=0;
        this.silverRatePerSft=0;
        this.goldRatePerSft=0;
        this.sevenToNine=0;
        this.aboveNine=0;
    }
}