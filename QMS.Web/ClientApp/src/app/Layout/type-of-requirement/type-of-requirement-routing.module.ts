import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TypeOfRequirementComponent } from './type-of-requirement.component';
import { TypeOfRequirementListComponent } from './type-of-requirement-list/type-of-requirement-list.component';
import { TypeOfRequirementFormComponent } from './type-of-requirement-form/type-of-requirement-form.component';


const routes: Routes = [
  {
    path: '',
    component: TypeOfRequirementComponent,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: TypeOfRequirementListComponent
      },
      {
        path: 'new',
        component: TypeOfRequirementFormComponent
      },
      {
        path: 'edit/:id',
        component: TypeOfRequirementFormComponent,
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TypeOfRequirementRoutingModule { }
