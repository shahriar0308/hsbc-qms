import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TypeOfRequirementRoutingModule } from './type-of-requirement-routing.module';
import { TypeOfRequirementComponent } from './type-of-requirement.component';
import { TypeOfRequirementFormComponent } from './type-of-requirement-form/type-of-requirement-form.component';
import { TypeOfRequirementListComponent } from './type-of-requirement-list/type-of-requirement-list.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedMasterModule } from 'src/app/Shared/Modules/shared-master/shared-master.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [TypeOfRequirementComponent, TypeOfRequirementFormComponent, TypeOfRequirementListComponent],
  imports: [
    CommonModule,
    TypeOfRequirementRoutingModule,
    SharedMasterModule,
    AngularFontAwesomeModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule
  ]
})
export class TypeOfRequirementModule { }
