import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { finalize } from 'rxjs/operators';
import { TypeOfRequirement, SaveTypeOfRequirement } from 'src/app/Shared/Entity/TypeOfRequirement/type-of-requirement';
import { TypeOfRequirementService } from 'src/app/Shared/Services/TypeOfRequirements/type-of-requirement.service';

@Component({
  selector: 'app-type-of-requirement-form',
  templateUrl: './type-of-requirement-form.component.html',
  styleUrls: ['./type-of-requirement-form.component.css']
})
export class TypeOfRequirementFormComponent implements OnInit, OnDestroy {

  typeOfRequirement: TypeOfRequirement;
  typeOfRequirementForm: FormGroup;
  isEdit: boolean;
  private subscriptions: Subscription[] = [];


	constructor(private activatedRoute: ActivatedRoute,
		private router: Router,
    private typeOfRequirementFB: FormBuilder,
    private alertService: AlertService,
    private typeOfRequirementService: TypeOfRequirementService,
    private cd: ChangeDetectorRef) { }

	ngOnInit() {
		// this.alertService.fnLoading(true);
		const routeSubscription = this.activatedRoute.params.subscribe(params => {
      const id = params['id'];
      console.log(id);
			if (id) {
				this.isEdit = true;
				this.alertService.fnLoading(true);
				this.typeOfRequirementService.getTypeOfRequirement(id)
					.pipe(finalize(() => this.alertService.fnLoading(false)))
					.subscribe(res => {
						if (res) {
							this.typeOfRequirement = res.data as TypeOfRequirement;
							this.initTypeOfRequirement();
						}
					});
			} else {
				this.typeOfRequirement = new TypeOfRequirement();
				this.typeOfRequirement.clear();
				this.initTypeOfRequirement();
			}
		});
		this.subscriptions.push(routeSubscription);
	}

 	ngOnDestroy() {
		this.subscriptions.forEach(sb => sb.unsubscribe());
  }

	initTypeOfRequirement() {
		this.createForm();
	}

	createForm() {
		this.typeOfRequirementForm = this.typeOfRequirementFB.group({
			// userName: [this.user.userName, Validators.required],
			name: [this.typeOfRequirement.name, Validators.required],
			description: [this.typeOfRequirement.description],
			isActive: [this.typeOfRequirement.isActive]
		});
	}
	
	get dfControls() { return this.typeOfRequirementForm.controls; }

	onSubmit() {

    const controls = this.typeOfRequirementForm.controls;
		
		if (this.typeOfRequirementForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		const editedTypeOfRequirement = this.prepareTypeOfRequirement();
		if (editedTypeOfRequirement.id) {
			this.updateTypeOfRequirement(editedTypeOfRequirement);
		}
		else {
			this.createTypeOfRequirement(editedTypeOfRequirement);
		}
	}

	prepareTypeOfRequirement(): SaveTypeOfRequirement {
		const controls = this.typeOfRequirementForm.controls;
		const _typeOfRequirement = new SaveTypeOfRequirement();
		_typeOfRequirement.clear();
    _typeOfRequirement.id = this.typeOfRequirement.id;
		_typeOfRequirement.name = controls['name'].value;
		_typeOfRequirement.description = controls['description'].value;
		_typeOfRequirement.isActive = controls['isActive'].value;
		return _typeOfRequirement;
	}

	createTypeOfRequirement(_typeOfRequirement: SaveTypeOfRequirement) {
		this.alertService.fnLoading(true);
		const createSubscription = this.typeOfRequirementService.create(_typeOfRequirement)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`New requirement has been added successfully.`);
				this.goBack();
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.push(createSubscription);
	}

	updateTypeOfRequirement(_typeOfRequirement: SaveTypeOfRequirement) {
		this.alertService.fnLoading(true);
		const updateSubscription = this.typeOfRequirementService.update(_typeOfRequirement)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`Requirement has been saved successfully.`);
				this.goBack();
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.push(updateSubscription);
	}

	getComponentTitle() {
		let result = 'Create New Requirement';
		if (!this.typeOfRequirement || !this.typeOfRequirement.id) {
			return result;
		}

		result = `Edit requirement - ${this.typeOfRequirement.name}`;
		return result;
	}
	
	goBack() {
		this.router.navigate([`/type-of-requirement`], { relativeTo: this.activatedRoute });
	}

	stringToInt(value): number {
		return Number.parseInt(value);
	}

    private throwError(errorDetails: any) {
        // this.alertService.fnLoading(false);
        console.log("error", errorDetails);
        let errList = errorDetails.error.errors;
        if (errList.length) {
            console.log("error", errList, errList[0].errorList[0]);
            // this.alertService.tosterDanger(errList[0].errorList[0]);
        } else {
            //this.alertService.tosterDanger(errorDetails.error.message);
        }
    }

}
