import { Component, OnInit, OnDestroy } from '@angular/core';
import { TypeOfRequirementQuery, TypeOfRequirement } from 'src/app/Shared/Entity/TypeOfRequirement/type-of-requirement';
import { Subscription, of } from 'rxjs';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { TypeOfRequirementService } from 'src/app/Shared/Services/TypeOfRequirements/type-of-requirement.service';
import { take, delay, finalize } from 'rxjs/operators';
import { CommonService } from 'src/app/Shared/Services/Common/common.service';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-type-of-requirement-list',
  templateUrl: './type-of-requirement-list.component.html',
  styleUrls: ['./type-of-requirement-list.component.css']
})
export class TypeOfRequirementListComponent implements OnInit, OnDestroy {

  query: TypeOfRequirementQuery;
	PAGE_SIZE: number;
	typeOfRequirements: TypeOfRequirement[];

	// Subscriptions
	private subscriptions: Subscription[] = [];

	constructor(
		private router: Router,
    	private alertService: AlertService,
    	private typeOfRequirementService: TypeOfRequirementService,
		private commonService: CommonService,
		private modalService: NgbModal) {
		this.PAGE_SIZE = commonService.PAGE_SIZE;
	}

	ngOnInit() {
		of(undefined).pipe(take(1), delay(1000)).subscribe(() => {
			this.loadTypeOfRequirementsPage();
		});
	}

	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}
	
    loadTypeOfRequirementsPage() {
		this.searchConfiguration();
        this.alertService.fnLoading(true);
		const typeOfRequirementsSubscription = this.typeOfRequirementService.getTypeOfRequirements(this.query)
			.pipe(finalize(() => { this.alertService.fnLoading(false); }))
			.subscribe(
            (res) => {
				this.typeOfRequirements = res.data.items;
				this.typeOfRequirements.forEach(obj=>{
					obj.isActiveCustom = obj.isActive? 'Yes': 'No';
				});
            },
            (error) => {
                console.log(error);
            });
			this.subscriptions.push(typeOfRequirementsSubscription);
	}
	
	searchConfiguration() {
		this.query = new TypeOfRequirementQuery({
			page: 1,
			pageSize: this.PAGE_SIZE,
			sortBy: 'name',
			isSortAscending: true,
			name: '',
		});
	}

	toggleActiveInactive(id) {
		const actInSubscription = this.typeOfRequirementService.activeInactive(id).subscribe(res => {
			this.loadTypeOfRequirementsPage();
		});
		this.subscriptions.push(actInSubscription);
	}
	
	editTypeOfRequirement(id) {
		this.router.navigate(['/type-of-requirement/edit', id]);
	}
	
	newTypeOfRequirement() {
		this.router.navigate(['/type-of-requirement/new']);
	}
	
	deleteTypeOfRequirement(id) {
		this.alertService.confirm("Are you sure want to delete this type of requirement?", 
			() => {
				this.alertService.fnLoading(true);
				const deleteSubscription = this.typeOfRequirementService.delete(id)
					.pipe(finalize(() => { this.alertService.fnLoading(false); }))
					.subscribe((res: any) => {
						console.log('res from del func',res);          
						this.alertService.tosterSuccess("Type Of Requirement has been deleted successfully.");
						this.loadTypeOfRequirementsPage();
					},
					(error) => {
						console.log(error);
					});
					this.subscriptions.push(deleteSubscription);
				}, 
			() => {
			});
	}

    public ptableSettings: IPTableSetting = {
        tableID: "type-of-requirement-table",
        tableClass: "table table-border ",
        tableName: 'Type Of Requirements',
        tableRowIDInternalName: "id",
        tableColDef: [
			{ headerName: 'Requirements', width: '30%', internalName: 'name', sort: true, type: "" },
			{ headerName: 'Description', width: '60%', internalName: 'description', sort: false, type: "" },
			{ headerName: 'Active', width: '10%', internalName: 'isActiveCustom', sort: false, type: "" },
			// { headerName: 'Details', width: '20%', internalName: 'details', sort: false, type: "button", onClick: 'true', innerBtnIcon:"fa fa-action" },
        ],
        enabledSearch: true,
        enabledSerialNo: true,
        pageSize: 10,
        enabledPagination: true,
        enabledDeleteBtn: true,
        enabledEditBtn: true,
        enabledColumnFilter: true,
        enabledRadioBtn: false,
		enabledRecordCreateBtn: true,
		enabledCellClick: true,
		newRecordButtonText: 'New Requirement'
	};
	
    public fnCustomTrigger(event) {
        console.log("custom  click: ", event);

        if (event.action == "new-record") {
            this.newTypeOfRequirement();
        }
        else if (event.action == "edit-item") {
            this.editTypeOfRequirement(event.record.id);
        }
        else if (event.action == "delete-item") {
            this.deleteTypeOfRequirement(event.record.id);
        }
    }

}
