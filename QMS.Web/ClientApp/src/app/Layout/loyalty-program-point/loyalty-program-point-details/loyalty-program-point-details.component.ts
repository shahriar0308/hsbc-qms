import { Component, OnInit, OnDestroy } from '@angular/core';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';
import { finalize, take, delay } from 'rxjs/operators';
import { ColorCodeQuery, ColorCode } from 'src/app/Shared/Entity/ColorCode/color-code';
import { Subscription, of } from 'rxjs';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { CommonService } from 'src/app/Shared/Services/Common/common.service';
import { ColorCodeService } from 'src/app/Shared/Services/ColorCodes/color-codes.service';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { LoyaltyProgramPoint } from 'src/app/Shared/Entity/LoyaltyProgram/loyalty-program-point';
import { LoyaltyProgramPointService } from 'src/app/Shared/Services/LoyaltyProgram/loyalty-program-points.service';

@Component({
	selector: 'app-loyalty-program-point-details',
	templateUrl: './loyalty-program-point-details.component.html',
	styleUrls: ['./loyalty-program-point-details.component.css']
})
export class LoyaltyProgramPointDetailsComponent implements OnInit, OnDestroy {

	loyaltyProgramPoint: LoyaltyProgramPoint;

	// Subscriptions
	private subscriptions: Subscription[] = [];

	constructor(
		private router: Router,
		private alertService: AlertService,
		private loyaltyProgramPointService: LoyaltyProgramPointService,
		private commonService: CommonService) {
			this.loyaltyProgramPoint = new LoyaltyProgramPoint();
			this.loyaltyProgramPoint.clear();
		}

	ngOnInit() {
		this.alertService.fnLoading(true);
		const routeSubscription = this.loyaltyProgramPointService.getFirstOrDefaultLoyaltyProgramPoint()
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				if (res) {
					this.loyaltyProgramPoint = res.data as LoyaltyProgramPoint;
				}
			});
		this.subscriptions.push(routeSubscription);
	}

	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}

	editloyaltyProgramPoint() {
		this.router.navigate(['/loyalty-program-point/edit']);
	}
}
