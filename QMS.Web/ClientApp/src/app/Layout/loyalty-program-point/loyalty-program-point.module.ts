import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedMasterModule } from 'src/app/Shared/Modules/shared-master/shared-master.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoyaltyProgramPointRoutingModule } from './loyalty-program-point-routing.module';
import { LoyaltyProgramPointComponent } from './loyalty-program-point.component';
import { LoyaltyProgramPointFormComponent } from './loyalty-program-point-form/loyalty-program-point-form.component';
import { LoyaltyProgramPointDetailsComponent } from './loyalty-program-point-details/loyalty-program-point-details.component';

@NgModule({
  declarations: [
    LoyaltyProgramPointDetailsComponent,
    LoyaltyProgramPointFormComponent,
    LoyaltyProgramPointComponent
  ],
  imports: [
    CommonModule,
    LoyaltyProgramPointRoutingModule,
    SharedMasterModule,
    AngularFontAwesomeModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [
  ]
})
export class LoyaltyProgramPointModule { }
