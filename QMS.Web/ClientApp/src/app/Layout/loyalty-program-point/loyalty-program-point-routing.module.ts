import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoyaltyProgramPointDetailsComponent } from './loyalty-program-point-details/loyalty-program-point-details.component';
import { LoyaltyProgramPointComponent } from './loyalty-program-point.component';
import { LoyaltyProgramPointFormComponent } from './loyalty-program-point-form/loyalty-program-point-form.component';


const routes: Routes = [
  {
    path: '',
    component: LoyaltyProgramPointComponent,
    children: [
      {
        path: '',
        redirectTo: 'details',
        pathMatch: 'full',
      },
      {
        path: 'details',
        component: LoyaltyProgramPointDetailsComponent
      },
      // {
      //   path: 'new',
      //   component: ColorCodeFormComponent,
        // canActivate: [AuthorizeGuard],
        // data: { title: 'New User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Create] },
      // },
      {
        path: 'edit',
        component: LoyaltyProgramPointFormComponent,
        // canActivate: [AuthorizeGuard],
        // data: { title: 'Edit User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Edit] },
      },
    ],

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoyaltyProgramPointRoutingModule { }
