import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { Guid } from 'guid-typescript';
import { finalize } from 'rxjs/operators';
import { LoyaltyProgramPoint, SaveLoyaltyProgramPoint } from 'src/app/Shared/Entity/LoyaltyProgram/loyalty-program-point';
import { LoyaltyProgramPointService } from 'src/app/Shared/Services/LoyaltyProgram/loyalty-program-points.service';

@Component({
	selector: 'app-loyalty-program-point-form',
	templateUrl: './loyalty-program-point-form.component.html',
	styleUrls: ['./loyalty-program-point-form.component.css']
})
export class LoyaltyProgramPointFormComponent implements OnInit, OnDestroy {

	loyaltyProgramPoint: LoyaltyProgramPoint;
	loyaltyProgramPointForm: FormGroup;

	private subscriptions: Subscription[] = [];


	constructor(private activatedRoute: ActivatedRoute,
		private router: Router,
		private loyaltyProgramPointFB: FormBuilder,
		private alertService: AlertService,
		private loyaltyProgramPointService: LoyaltyProgramPointService) { 
			this.loyaltyProgramPoint = new LoyaltyProgramPoint();
			this.loyaltyProgramPoint.clear();
		}

	ngOnInit() {
		this.alertService.fnLoading(true);
		const routeSubscription = this.loyaltyProgramPointService.getFirstOrDefaultLoyaltyProgramPoint()
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				if (res) {
					this.loyaltyProgramPoint = res.data as LoyaltyProgramPoint;
					this.initLoyaltyProgramPoints();
				} else {
					this.alertService.tosterDanger('Something went wrong, please try again.');
					this.initLoyaltyProgramPoints();
				}
			});
		this.initLoyaltyProgramPoints();
		this.subscriptions.push(routeSubscription);
	}

	ngOnDestroy() {
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	initLoyaltyProgramPoints() {
		this.createForm();
	}

	createForm() {
		this.loyaltyProgramPointForm = this.loyaltyProgramPointFB.group({
			productCost: [this.loyaltyProgramPoint.productCost, [Validators.required, Validators.pattern(/^[0-9]+$/)]],
			painterCost: [this.loyaltyProgramPoint.painterCost, [Validators.required, Validators.pattern(/^[0-9]+$/)]],
			supervisionCharge: [this.loyaltyProgramPoint.supervisionCharge, [Validators.required, Validators.pattern(/^[0-9]+$/)]]
		});
	}

	get formControls() { return this.loyaltyProgramPointForm.controls; }

	onSubmit() {

		const controls = this.loyaltyProgramPointForm.controls;

		if (this.loyaltyProgramPointForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		const editedLoyaltyProgramPoints = this.prepareLoyaltyProgramPoints();
		if (editedLoyaltyProgramPoints.id) {
			this.updateLoyaltyProgramPoints(editedLoyaltyProgramPoints);
		} else {
			this.alertService.tosterDanger('Something went wrong, please try again.');
		}
	}

	prepareLoyaltyProgramPoints(): SaveLoyaltyProgramPoint {
		const controls = this.loyaltyProgramPointForm.controls;

		const _loyaltyProgramPoint = new SaveLoyaltyProgramPoint();
		_loyaltyProgramPoint.clear();
		_loyaltyProgramPoint.id = this.loyaltyProgramPoint.id;
		_loyaltyProgramPoint.productCost = Number.parseInt(controls['productCost'].value);
		_loyaltyProgramPoint.painterCost = Number.parseInt(controls['painterCost'].value);
		_loyaltyProgramPoint.supervisionCharge = Number.parseInt(controls['supervisionCharge'].value);
		
		return _loyaltyProgramPoint;
	}

	updateLoyaltyProgramPoints(_loyaltyProgramPoint: SaveLoyaltyProgramPoint) {
		this.alertService.fnLoading(true);
		const updateSubscription = this.loyaltyProgramPointService.update(_loyaltyProgramPoint)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`Loyalty Program Point has been saved successfully.`);
				this.goBack();
			},
				error => {
					this.throwError(error);
				});
		this.subscriptions.push(updateSubscription);
	}

	getComponentTitle() {
		let result = `Edit Loyalty Program Point Settings`;
		return result;
	}

	goBack() {
		this.router.navigate([`/loyalty-program-point/details`], { relativeTo: this.activatedRoute });
	}

	private throwError(errorDetails: any) {
		// this.alertService.fnLoading(false);
		console.log("error", errorDetails);
		let errList = errorDetails.error.errors;
		if (errList.length) {
			console.log("error", errList, errList[0].errorList[0]);
			// this.alertService.tosterDanger(errList[0].errorList[0]);
		} else {
			// this.alertService.tosterDanger(errorDetails.error.message);
		}
	}
}
