import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromotionalBannerRoutingModule } from './promotional-banner-routing.module';
import { PromotionalBannerListComponent } from './promotional-banner-list/promotional-banner-list.component';
import { PromotionalBannerFormComponent } from './promotional-banner-form/promotional-banner-form.component';
import { PromotionalBannerComponent } from './promotional-banner.component';
import { SharedMasterModule } from 'src/app/Shared/Modules/shared-master/shared-master.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    PromotionalBannerListComponent,
    PromotionalBannerFormComponent,
    PromotionalBannerComponent
  ],
  imports: [
    CommonModule,
    PromotionalBannerRoutingModule,
    SharedMasterModule,
    AngularFontAwesomeModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [
  ]
})
export class PromotionalBannerModule { }
