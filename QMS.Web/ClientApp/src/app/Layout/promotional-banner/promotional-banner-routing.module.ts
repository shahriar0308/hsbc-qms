import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PromotionalBannerComponent } from './promotional-banner.component';
import { PromotionalBannerListComponent } from './promotional-banner-list/promotional-banner-list.component';
import { PromotionalBannerFormComponent } from './promotional-banner-form/promotional-banner-form.component';


const routes: Routes = [
  {
    path: '',
    component: PromotionalBannerComponent,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: PromotionalBannerListComponent
      },
      {
        path: 'new',
        component: PromotionalBannerFormComponent,
        // canActivate: [AuthorizeGuard],
        // data: { title: 'New User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Create] },
      },
      {
        path: 'edit/:id',
        component: PromotionalBannerFormComponent,
        // canActivate: [AuthorizeGuard],
        // data: { title: 'Edit User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Edit] },
      },
    ],

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromotionalBannerRoutingModule { }
