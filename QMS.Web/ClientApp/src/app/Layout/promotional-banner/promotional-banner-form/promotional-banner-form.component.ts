import { Component, OnInit, OnDestroy } from '@angular/core';
import { PromotionalBanner, SavePromotionalBanner } from 'src/app/Shared/Entity/PromotionalBanner/promotional-banner';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { PromotionalBannerService } from 'src/app/Shared/Services/PromotionalBanners/promotional-banners.service';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { Guid } from 'guid-typescript';
import { finalize } from 'rxjs/operators';

@Component({
	selector: 'app-promotional-banner-form',
	templateUrl: './promotional-banner-form.component.html',
	styleUrls: ['./promotional-banner-form.component.css']
})
export class PromotionalBannerFormComponent implements OnInit, OnDestroy {

	promotionalBanner: PromotionalBanner;
	promotionalBannerForm: FormGroup;
	imageFile: File;

	private subscriptions: Subscription[] = [];


	constructor(private activatedRoute: ActivatedRoute,
		private router: Router,
		private promotionalBannerFB: FormBuilder,
		private alertService: AlertService,
		private promotionalBannerService: PromotionalBannerService) { }

	ngOnInit() {

		// this.alertService.fnLoading(true);
		const routeSubscription = this.activatedRoute.params.subscribe(params => {
			const id = params['id'];
			console.log(id);
			if (id) {

				this.alertService.fnLoading(true);
				this.promotionalBannerService.getPromotionalBanner(id)
					.pipe(finalize(() => this.alertService.fnLoading(false)))
					.subscribe(res => {
						if (res) {
							this.promotionalBanner = res.data as PromotionalBanner;
							this.initPromotionalBanners();
						}
					});
			} else {
				this.promotionalBanner = new PromotionalBanner();
				this.promotionalBanner.clear();
				this.initPromotionalBanners();
			}
		});
		this.subscriptions.push(routeSubscription);
	}

	ngOnDestroy() {
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	initPromotionalBanners() {
		this.createForm();
	}

	createForm() {
		this.promotionalBannerForm = this.promotionalBannerFB.group({
			name: [this.promotionalBanner.name, [Validators.required, Validators.pattern(/^(?!\s+$).+/)]],
			description: [this.promotionalBanner.description],
			sequence: [this.promotionalBanner.sequence, [Validators.required, Validators.pattern(/^[0-9]+$/)]],
			isActive: [this.promotionalBanner.isActive]
		});
	}

	get formControls() { return this.promotionalBannerForm.controls; }

	onSubmit() {

		const controls = this.promotionalBannerForm.controls;

		if (this.promotionalBannerForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		const editedPromotionalBanners = this.preparePromotionalBanners();
		if (editedPromotionalBanners.id) {
			this.updatePromotionalBanners(editedPromotionalBanners);
		}
		else {
			this.createPromotionalBanners(editedPromotionalBanners);
		}
	}

	preparePromotionalBanners(): SavePromotionalBanner {
		const controls = this.promotionalBannerForm.controls;

		const _promotionalBanner = new SavePromotionalBanner();
		_promotionalBanner.clear();
		_promotionalBanner.id = this.promotionalBanner.id;
		_promotionalBanner.name = controls['name'].value;
		_promotionalBanner.description = controls['description'].value;
		_promotionalBanner.sequence = controls['sequence'].value;
		_promotionalBanner.isActive = controls['isActive'].value;
		if(this.imageFile)
			_promotionalBanner.imageFile = this.imageFile;
		
		return _promotionalBanner;
	}

	createPromotionalBanners(_promotionalBanner: SavePromotionalBanner) {
		this.alertService.fnLoading(true);
		const createSubscription = this.promotionalBannerService.create(_promotionalBanner)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`New Promotional Banner has been added successfully.`);
				this.goBack();
			},
				error => {
					this.throwError(error);
				});
		this.subscriptions.push(createSubscription);
	}

	updatePromotionalBanners(_promotionalBanner: SavePromotionalBanner) {
		this.alertService.fnLoading(true);
		const updateSubscription = this.promotionalBannerService.update(_promotionalBanner)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`Promotional Banner has been saved successfully.`);
				this.goBack();
			},
				error => {
					this.throwError(error);
				});
		this.subscriptions.push(updateSubscription);
	}

	getComponentTitle() {
		let result = 'Create Promotional Banner';
		if (!this.promotionalBanner || !this.promotionalBanner.id) {
			return result;
		}

		result = `Edit Promotional Banner - ${this.promotionalBanner.name}`;
		return result;
	}

	goBack() {
		this.router.navigate([`/promotional-banner`], { relativeTo: this.activatedRoute });
	}

	stringToInt(value): number {
		return Number.parseInt(value);
	}

	// 	onAlertClose($event) {
	// 		this.resetErrors();
	// 	}

	private throwError(errorDetails: any) {
		// this.alertService.fnLoading(false);
		console.log("error", errorDetails);
		let errList = errorDetails.error.errors;
		if (errList.length) {
			console.log("error", errList, errList[0].errorList[0]);
			// this.alertService.tosterDanger(errList[0].errorList[0]);
		} else {
			// this.alertService.tosterDanger(errorDetails.error.message);
		}
	}

    onChangeFile(file: File) {
        console.log("image file", file);
        this.imageFile = file;
        if(this.imageFile == null)
        {
            this.promotionalBanner.imageUrl = '';
        }
    }
}
