import { Component, OnInit, OnDestroy } from '@angular/core';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';
import { finalize, take, delay } from 'rxjs/operators';
import { PromotionalBannerQuery, PromotionalBanner } from 'src/app/Shared/Entity/PromotionalBanner/promotional-banner';
import { Subscription, of } from 'rxjs';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { CommonService } from 'src/app/Shared/Services/Common/common.service';
import { PromotionalBannerService } from 'src/app/Shared/Services/PromotionalBanners/promotional-banners.service';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'app-promotional-banner-list',
	templateUrl: './promotional-banner-list.component.html',
	styleUrls: ['./promotional-banner-list.component.css']
})
export class PromotionalBannerListComponent implements OnInit, OnDestroy {

	query: PromotionalBannerQuery;
	PAGE_SIZE: number;
	promotionalBanners: PromotionalBanner[];

	// Subscriptions
	private subscriptions: Subscription[] = [];

	constructor(
		private router: Router,
		private alertService: AlertService,
		private promotionalBannerService: PromotionalBannerService,
		private modalService: NgbModal,
		private commonService: CommonService) {
		this.PAGE_SIZE = 5000;//commonService.PAGE_SIZE;
	}

	ngOnInit() {
		of(undefined).pipe(take(1), delay(1000)).subscribe(() => {
			this.loadPromotionalBannersPage();
		});
	}

	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}

	loadPromotionalBannersPage() {
		this.searchConfiguration();
		this.alertService.fnLoading(true);
		const promotionalBannersSubscription = this.promotionalBannerService.getPromotionalBanners(this.query)
			.pipe(finalize(() => { this.alertService.fnLoading(false); }))
			.subscribe(
				(res) => {
					console.log("res.data", res.data);
					this.promotionalBanners = res.data.items;
					this.promotionalBanners.forEach(obj => {
						obj.isActiveText = obj.isActive ? 'YES' : 'NO';
					});
				},
				(error) => {
					console.log(error);
				});
		this.subscriptions.push(promotionalBannersSubscription);
	}

	searchConfiguration() {
		this.query = new PromotionalBannerQuery({
			page: 1,
			pageSize: this.PAGE_SIZE,
			sortBy: 'name',
			isSortAscending: true,
			name: '',
		});
	}

	toggleActiveInactive(id) {
		const actInSubscription = this.promotionalBannerService.activeInactive(id).subscribe(res => {
			this.loadPromotionalBannersPage();
		});
		this.subscriptions.push(actInSubscription);
	}

	editPromotionalBanner(id) {
		this.router.navigate(['/promotional-banner/edit', id]);
	}

	newPromotionalBanner() {
		this.router.navigate(['/promotional-banner/new']);
	}

	deletePromotionalBanner(id) {
		this.alertService.confirm("Are you sure want to delete this Promotional Banner?",
			() => {
				this.alertService.fnLoading(true);
				const deleteSubscription = this.promotionalBannerService.delete(id)
					.pipe(finalize(() => { this.alertService.fnLoading(false); }))
					.subscribe((res: any) => {
						console.log('res from del func', res);
						this.alertService.tosterSuccess("Promotional Banner has been deleted successfully.");
						this.loadPromotionalBannersPage();
					},
						(error) => {
							console.log(error);
						});
				this.subscriptions.push(deleteSubscription);
			},
			() => {
			});
	}

	public ptableSettings: IPTableSetting = {
		tableID: "promotionalBanners-table",
		tableClass: "table table-border ",
		tableName: 'Promotional Banner List',
		tableRowIDInternalName: "id",
		tableColDef: [
			{ headerName: 'Name', width: '30%', internalName: 'name', sort: true, type: "" },
			{ headerName: 'Description', width: '40%', internalName: 'description', sort: false, type: "" },
			{ headerName: 'Sequence', width: '15%', internalName: 'sequence', sort: true, type: "" },
			{ headerName: 'Active', width: '15%', internalName: 'isActiveText', sort: true, type: "" },
		],
		enabledSearch: true,
		enabledSerialNo: true,
		pageSize: 10,
		enabledPagination: true,
		enabledDeleteBtn: true,
		enabledEditBtn: true,
		enabledColumnFilter: true,
		enabledRadioBtn: false,
		enabledRecordCreateBtn: true,
		newRecordButtonText: 'New Promotional Banner'
	};

	public fnCustomTrigger(event) {
		console.log("custom  click: ", event);

		if (event.action == "new-record") {
			this.newPromotionalBanner();
		}
		else if (event.action == "edit-item") {
			this.editPromotionalBanner(event.record.id);
		}
		else if (event.action == "delete-item") {
			this.deletePromotionalBanner(event.record.id);
		}
	}
}
