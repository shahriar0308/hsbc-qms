import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServiceRequestComponent } from './service-request.component';
import { ServiceRequestListComponent } from './service-request-list/service-request-list.component';
import { ServiceRequestDetailsListComponent } from './service-request-details-list/service-request-details-list.component';


const routes: Routes = [
  {
    path: '',
    component: ServiceRequestComponent,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: ServiceRequestListComponent
      },
      {
        path: 'child/:id',
        component: ServiceRequestDetailsListComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceRequestRoutingModule { }
