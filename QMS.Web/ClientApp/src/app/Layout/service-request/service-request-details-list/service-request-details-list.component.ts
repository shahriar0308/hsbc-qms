import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ServiceRequisitionDetail, ServiceRequisition } from 'src/app/Shared/Entity/ServiceRequisition/service-requisition';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { ServiceRequisitionService } from 'src/app/Shared/Services/ServiceRequisitions/service-requisition.service';
import { CommonService } from 'src/app/Shared/Services/Common/common.service';
import { finalize } from 'rxjs/operators';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';
import { Guid } from 'guid-typescript';

@Component({
  selector: 'app-service-request-details-list',
  templateUrl: './service-request-details-list.component.html',
  styleUrls: ['./service-request-details-list.component.css']
})
export class ServiceRequestDetailsListComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription = new Subscription();
  public serviceRequisition: ServiceRequisition = new ServiceRequisition();
  public leadId: string;
  id: Guid;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService,
    private requisitionService: ServiceRequisitionService,
		private commonService: CommonService
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id'];
    if(this.id){
      this.loadDetails();
    }
    else{
      this.goBack();
    }
  }
  ngOnDestroy() {
		this.subscriptions.unsubscribe();
  }

  loadDetails(){
    this.alertService.fnLoading(true);
		const productsSubscription = this.requisitionService.getServiceRequisitionDetails(this.id)
			.pipe(finalize(() => { this.alertService.fnLoading(false); }))
			.subscribe(
        (res) => {
          this.serviceRequisition = res.data;
        },
        (error) => {
            console.log(error);
        });
			this.subscriptions.add(productsSubscription);
  }

  public ptableSettings: IPTableSetting = {
    tableID: "service-requisition-details-table",
    tableClass: "table table-border ",
    tableName: 'Child Lead(s)',
    tableRowIDInternalName: "id",
    tableColDef: [
      { headerName: 'Work Detail Id', width: '20%', internalName: 'workDetailsId', type: "" },
      { headerName: 'Name', width: '20%', internalName: 'name', type: "" },
      { headerName: 'Stage', width: '10%', internalName: 'stage', type: "" },
      { headerName: 'Color Consultant', width: '20%', internalName: 'colorConsultantName', type: "" },
      { headerName: 'Color Consultant No', width: '20%', internalName: 'colorConsultantPhoneNo', type: ""},
    ],
    enabledPagination: false,
    // enabledEditBtn: true,
    enabledColumnFilter: false,
    tableHeaderVisibility: false,
    enabledRadioBtn: false,
    enabledCellClick: true,
    tableFooterVisibility: false
  };

  goBack(){
    this.router.navigate([`/service-request`]);
  }
}
