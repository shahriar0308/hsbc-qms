import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServiceRequestRoutingModule } from './service-request-routing.module';
import { ServiceRequestComponent } from './service-request.component';
import { ServiceRequestListComponent } from './service-request-list/service-request-list.component';
import { SharedMasterModule } from 'src/app/Shared/Modules/shared-master/shared-master.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceRequestDetailsListComponent } from './service-request-details-list/service-request-details-list.component';


@NgModule({
  declarations: [ServiceRequestComponent, ServiceRequestListComponent, ServiceRequestDetailsListComponent],
  imports: [
    CommonModule,
    ServiceRequestRoutingModule,
    SharedMasterModule,
    AngularFontAwesomeModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ServiceRequestModule { }
