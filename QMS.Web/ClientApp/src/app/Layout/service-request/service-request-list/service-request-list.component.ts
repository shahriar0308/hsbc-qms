import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { CommonService } from 'src/app/Shared/Services/Common/common.service';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { ServiceRequisitionService } from 'src/app/Shared/Services/ServiceRequisitions/service-requisition.service';
import { ServiceRequisition } from 'src/app/Shared/Entity/ServiceRequisition/service-requisition';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';

@Component({
  selector: 'app-service-request-list',
  templateUrl: './service-request-list.component.html',
  styleUrls: ['./service-request-list.component.css']
})
export class ServiceRequestListComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription = new Subscription();
  public workIdProvided: boolean = false;
  public text= "Showing only WorkId Unassigned";
  public serviceRequisitions: ServiceRequisition[];

	constructor(
		private router: Router,
    private alertService: AlertService,
    private requisitionService: ServiceRequisitionService,
		private commonService: CommonService) {}

	ngOnInit() {
    this.toggle();
			// this.loadRequisitions();
  }
  ngOnDestroy() {
		this.subscriptions.unsubscribe();
  }
  
  toggle(){
    this.workIdProvided = !this.workIdProvided;
    if(!this.workIdProvided){
      this.text = "Showing only WorkId Unassigned";
    }
    else{
      this.text = "Showing All";
    }
    this.loadRequisitions();
  }
  
  loadRequisitions(){
    this.alertService.fnLoading(true);
		const productsSubscription = this.requisitionService.getServiceRequisitions(this.workIdProvided)
			.pipe(finalize(() => { this.alertService.fnLoading(false); }))
			.subscribe(
        (res) => {
          this.serviceRequisitions = res.data;
          this.serviceRequisitions.forEach(elm=>{
            elm.detailBtn = 'Details';
          });
        },
        (error) => {
            console.log(error);
        });
			this.subscriptions.add(productsSubscription);
  }

  public ptableSettings: IPTableSetting = {
    tableID: "service-requisitions-table",
    tableClass: "table table-border ",
    tableName: 'Service Requisitions',
    tableRowIDInternalName: "id",
    tableColDef: [
      { headerName: 'Work Id', width: '10%', internalName: 'workId', sort: true, type: "" },
      { headerName: 'Type of Requirement', width: '10%', internalName: 'typeOfRequirementName', sort: true, type: "" },
      { headerName: 'Details', width: '10%', internalName: 'requirementDetail', sort: false, type: "" },
      { headerName: 'District', width: '10%', internalName: 'districtName', sort: false, type: "" },
      { headerName: 'Thana', width: '10%', internalName: 'thanaName', sort: false, type: "" },
      { headerName: 'Address', width: '10%', internalName: 'addressLine1', sort: false, type: "" },
      { headerName: 'Customer Name', width: '10%', internalName: 'customerName', sort: false, type: "" },
      { headerName: 'Phone No', width: '10%', internalName: 'customerPhoneNo', sort: false, type: "" },
      { headerName: 'Stage', width: '10%', internalName: 'stage', sort: false, type: "" },
      { headerName: 'Detail', width: '10%', internalName: 'detailBtn', sort: false, type: "button", onClick: 'true', innerBtnIcon:"fa fa-info" },
    ],
    enabledSearch: true,
    pageSize: 10,
    enabledPagination: true,
    // enabledEditBtn: true,
    enabledColumnFilter: true,
    enabledRadioBtn: false,
    enabledCellClick: true
  };

  fnViewDetails(event : any) {
    let id = event.record.id;
    console.log(event);
    if(!event.record.workId){
      this.alertService.tosterWarning("Details not available as Work Id is not assigned.");
    }
    else{
      this.router.navigate([`/service-request/child/${id}`]);
    }
  }
}
