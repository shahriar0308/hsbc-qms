import { Component, OnInit, OnDestroy } from '@angular/core';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';
import { finalize, take, delay } from 'rxjs/operators';
import { DealerQuery, Dealer } from 'src/app/Shared/Entity/Dealers/dealer';
import { Subscription, of } from 'rxjs';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { CommonService } from 'src/app/Shared/Services/Common/common.service';
import { DealerService } from 'src/app/Shared/Services/Dealers/dealer.service';

@Component({
  selector: 'app-dealer-list',
  templateUrl: './dealer-list.component.html',
  styleUrls: ['./dealer-list.component.css']
})
export class DealerListComponent implements OnInit, OnDestroy {

  query: DealerQuery;
	PAGE_SIZE: number;
	dealers: Dealer[];

	// Subscriptions
	private subscriptions: Subscription[] = [];

	constructor(
		private router: Router,
    private alertService: AlertService,
    private dealerService: DealerService,
		private commonService: CommonService) {
		this.PAGE_SIZE = commonService.PAGE_SIZE;
	}

	ngOnInit() {
		of(undefined).pipe(take(1), delay(1000)).subscribe(() => {
			this.loadDealersPage();
		});
	}

	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}
	
    loadDealersPage() {
		this.searchConfiguration();
        this.alertService.fnLoading(true);
		const dealersSubscription = this.dealerService.getDealers(this.query)
			.pipe(finalize(() => { this.alertService.fnLoading(false); }))
			.subscribe(
            (res) => {
				this.dealers = res.data.items;
				this.dealers.forEach(obj=>{
					obj.isActiveCustom = obj.isActive? 'Yes': 'No';
				});
            },
            (error) => {
                console.log(error);
            });
			this.subscriptions.push(dealersSubscription);
	}
	
	searchConfiguration() {
		this.query = new DealerQuery({
			page: 1,
			pageSize: this.PAGE_SIZE,
			sortBy: 'name',
			isSortAscending: true,
			name: '',
			address: ''
		});
	}

	toggleActiveInactive(id) {
		const actInSubscription = this.dealerService.activeInactive(id).subscribe(res => {
			this.loadDealersPage();
		});
		this.subscriptions.push(actInSubscription);
	}
	
	editDealer(id) {
		this.router.navigate(['/dealer/edit', id]);
	}
	
	newDealer() {
		this.router.navigate(['/dealer/new']);
	}
	
	deleteDealer(id) {
		this.alertService.confirm("Are you sure want to delete this dealer?", 
			() => {
				this.alertService.fnLoading(true);
				const deleteSubscription = this.dealerService.delete(id)
					.pipe(finalize(() => { this.alertService.fnLoading(false); }))
					.subscribe((res: any) => {
						console.log('res from del func',res);          
						this.alertService.tosterSuccess("Dealer has been deleted successfully.");
						this.loadDealersPage();
					},
					(error) => {
						console.log(error);
					});
					this.subscriptions.push(deleteSubscription);
				}, 
			() => {
			});
	}

    public ptableSettings: IPTableSetting = {
        tableID: "dealers-table",
        tableClass: "table table-border ",
        tableName: 'Dealer List',
        tableRowIDInternalName: "id",
        tableColDef: [
            { headerName: 'Dealer Name', width: '15%', internalName: 'name', sort: true, type: "" },
            { headerName: 'Division', width: '15%', internalName: 'divisionName', sort: false, type: "" },
            { headerName: 'District', width: '15%', internalName: 'districtName', sort: false, type: "" },
			{ headerName: 'Address', width: '25%', internalName: 'address', sort: true, type: "" },
			{ headerName: 'Latitude', width: '10%', internalName: 'latitude', sort: true, type: "" },
			{ headerName: 'Longitude', width: '10%', internalName: 'longitude', sort: true, type: "" },
			{ headerName: 'Active', width: '10%', internalName: 'isActiveCustom', sort: false, type: "", }
        ],
        enabledSearch: true,
        enabledSerialNo: true,
        pageSize: this.PAGE_SIZE,
        enabledPagination: true,
        enabledDeleteBtn: true,
        enabledEditBtn: true,
        enabledColumnFilter: true,
        enabledRadioBtn: false,
		enabledRecordCreateBtn: true,
		newRecordButtonText: 'New Dealer'
    };

    public fnCustomTrigger(event) {
        console.log("custom  click: ", event);

        if (event.action == "new-record") {
            this.newDealer();
        }
        else if (event.action == "edit-item") {
            this.editDealer(event.record.id);
        }
        else if (event.action == "delete-item") {
            this.deleteDealer(event.record.id);
        }
    }
}
