import { Component, OnInit, OnDestroy } from '@angular/core';
import { Dealer, SaveDealer } from 'src/app/Shared/Entity/Dealers/dealer';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { DealerService } from 'src/app/Shared/Services/Dealers/dealer.service';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { Guid } from 'guid-typescript';
import { finalize } from 'rxjs/operators';
import { KeyValuePair } from 'src/app/Shared/Entity/Common/key-value-pair';
import { DivisionService } from 'src/app/Shared/Services/Division/division.service';
import { DistrictService } from 'src/app/Shared/Services/District/district.service';

@Component({
  selector: 'app-dealer-form',
  templateUrl: './dealer-form.component.html',
  styleUrls: ['./dealer-form.component.css']
})
export class DealerFormComponent implements OnInit, OnDestroy {

	dealer: Dealer;
  	dealerForm: FormGroup;
	divisions: KeyValuePair[];
	districts: KeyValuePair[];
	private subscriptions: Subscription[] = [];


	constructor(private activatedRoute: ActivatedRoute,
		private router: Router,
    private dealerFB: FormBuilder,
    private alertService: AlertService,
		private dealerService: DealerService,
		private divisionService: DivisionService,
		private districtService: DistrictService) { }

	ngOnInit() {
		this.loadDivisions();
		// this.alertService.fnLoading(true);
		const routeSubscription = this.activatedRoute.params.subscribe(params => {
      const id = params['id'];
      console.log(id);
			if (id) {

				this.alertService.fnLoading(true);
				this.dealerService.getDealer(id)
					.pipe(finalize(() => this.alertService.fnLoading(false)))
					.subscribe(res => {
						if (res) {
							this.dealer = res.data as Dealer;
							this.loadDistrictsByDivison(this.dealer.divisionId);
							this.initDealer();
						}
					});
			} else {
				this.dealer = new Dealer();
				this.dealer.clear();
				this.initDealer();
			}
		});
		this.subscriptions.push(routeSubscription);
	}

 	ngOnDestroy() {
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	initDealer() {
		this.createForm();
	}

	loadDivisions() {
		this.alertService.fnLoading(true);
		const divisionSubscription = this.divisionService.getDivisionSelect()
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
        this.divisions = res.data;
        this.divisions.forEach(obj=>{
          obj.value = this.stringToInt(obj.value);
		});
		console.log(this.divisions);
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.push(divisionSubscription);
	}

	divisionChange(){
		let controls = this.dealerForm.controls;
		let divisionId = controls['divisionId'].value;
		console.log(divisionId);
		controls['districtId'].setValue(null);
		if(!divisionId)
			this.districts = [];
		this.loadDistrictsByDivison(divisionId);
	}

	loadDistrictsByDivison(divisionId: number) {
		this.alertService.fnLoading(true);
		const districtSubscription = this.districtService.getByDivisionSelect(divisionId)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
		this.districts = res.data;
		console.log(this.districts);
        this.districts.forEach(obj=>{
          obj.value = this.stringToInt(obj.value);
        });
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.push(districtSubscription);
	}

	createForm() {
		console.log(this.dealer);
		this.dealerForm = this.dealerFB.group({
			// userName: [this.user.userName, Validators.required],
			name: [this.dealer.name, Validators.required],
			address: [this.dealer.address, Validators.required],
			divisionId: [this.dealer.divisionId, Validators.required],
			districtId: [this.dealer.districtId, Validators.required],
			latitude: [this.dealer.latitude],
			longitude: [this.dealer.longitude],
			isActive: [this.dealer.isActive]
		});
	}
	
	get dfControls() { return this.dealerForm.controls; }

	onSubmit() {

    const controls = this.dealerForm.controls;
		
		if (this.dealerForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		const editedDealer = this.prepareDealer();
		if (editedDealer.id) {
			this.updateDealer(editedDealer);
		}
		else {
			this.createDealer(editedDealer);
		}
	}

	prepareDealer(): SaveDealer {
		const controls = this.dealerForm.controls;
		const _dealer = new SaveDealer();
		_dealer.clear();
    	_dealer.id = this.dealer.id;
		_dealer.name = controls['name'].value;
		_dealer.address = controls['address'].value;
		_dealer.divisionId = controls['divisionId'].value;
		_dealer.districtId = controls['districtId'].value;
		_dealer.latitude = controls['latitude'].value;
		_dealer.longitude = controls['longitude'].value;
		_dealer.isActive = controls['isActive'].value;
		return _dealer;
	}

	createDealer(_dealer: SaveDealer) {
		this.alertService.fnLoading(true);
		const createSubscription = this.dealerService.create(_dealer)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`New dealer has been added successfully.`);
				this.goBack();
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.push(createSubscription);
	}

	updateDealer(_dealer: SaveDealer) {
		this.alertService.fnLoading(true);
		const updateSubscription = this.dealerService.update(_dealer)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`Dealer has been saved successfully.`);
				this.goBack();
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.push(updateSubscription);
	}

	getComponentTitle() {
		let result = 'Create Dealer';
		if (!this.dealer || !this.dealer.id) {
			return result;
		}

		result = `Edit dealer - ${this.dealer.name}`;
		return result;
	}
	
	goBack() {
		this.router.navigate([`/dealer`], { relativeTo: this.activatedRoute });
	}

	stringToInt(value): number {
		return Number.parseInt(value);
	}

// 	onAlertClose($event) {
// 		this.resetErrors();
// 	}

    private throwError(errorDetails: any) {
        // this.alertService.fnLoading(false);
        console.log("error", errorDetails);
        let errList = errorDetails.error.errors;
        if (errList.length) {
            console.log("error", errList, errList[0].errorList[0]);
            // this.alertService.tosterDanger(errList[0].errorList[0]);
        } else {
            //this.alertService.tosterDanger(errorDetails.error.message);
        }
    }


}
