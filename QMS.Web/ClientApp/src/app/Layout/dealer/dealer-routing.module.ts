import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DealerComponent } from './dealer.component';
import { DealerListComponent } from './dealer-list/dealer-list.component';
import { DealerFormComponent } from './dealer-form/dealer-form.component';


const routes: Routes = [
  {
    path: '',
    component: DealerComponent,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: DealerListComponent
      },
      {
        path: 'new',
        component: DealerFormComponent,
        // canActivate: [AuthorizeGuard],
        // data: { title: 'New User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Create] },
      },
      {
        path: 'edit/:id',
        component: DealerFormComponent,
        // canActivate: [AuthorizeGuard],
        // data: { title: 'Edit User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Edit] },
      },
    ],

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DealerRoutingModule { }
