import { Component, OnInit } from '@angular/core';
import { Division } from 'src/app/Shared/Entity/Division/division';
import { Router, ActivatedRoute } from '@angular/router';
import { DivisionService } from 'src/app/Shared/Services/Division/division.service';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';

@Component({
  selector: 'app-division-add',
  templateUrl: './division-add.component.html',
  styleUrls: ['./division-add.component.css']
})
export class DivisionAddComponent implements OnInit {

  divisionModel: Division;

  constructor(
    private router: Router,
    private divisionService: DivisionService,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService
  ) {

   }

  ngOnInit() {
    this.createForm();
    if (Object.keys(this.activatedRoute.snapshot.params).length !== 0 && this.activatedRoute.snapshot.params.id !== 'undefined') {
      let divisionId = this.activatedRoute.snapshot.params.id;
      this.getDivision(divisionId);
      
    }
    else{
      this.divisionModel.clear();
    }
  }

  saveDivision(){

    if(this.divisionModel.id == 0)
    {
      this.insertDivision();
    }
    else{
      this.updateDivision();
    }

  }

  insertDivision(){
    this.divisionService.postDivision(this.divisionModel).subscribe((res) => {
      this.router.navigate(['/division/list']).then( () =>{
        this.alertService.titleTosterSuccess("Record has been saved successfully.");
      });
  },
      (error) => {
         this.displayError(error);
      }, () => this.alertService.fnLoading(false)


  );

  }

  updateDivision(){
    this.divisionService.putDivision(this.divisionModel).subscribe((res) => {
      this.router.navigate(['/division/list']).then( () =>{
        this.alertService.titleTosterSuccess("Record has been updated successfully.");
      });
  },
      (error) => {
         this.displayError(error);
      }, () => this.alertService.fnLoading(false)


  );

  }

  backToTheList(){

    this.router.navigate(['/division/list']);

  }

  getDivision(divisionId){
    this.divisionService.getDivision(divisionId).subscribe(
      (result: any) => {
       
        this.editForm(result.data);
      },
      (err: any) => console.log(err)
    );

  }


  createForm() {

    this.divisionModel = new Division();
   
  }

  editForm(divisionData: Division) {

    this.divisionModel.name = divisionData.name;
    this.divisionModel.id = divisionData.id;
    this.divisionModel.isActive = divisionData.isActive;

  }


  displayError(errorDetails: any) {
    // this.alertService.fnLoading(false);
    console.log("error", errorDetails);
    let errList = errorDetails.error.errors;
    if (errList.length) {
      console.log("error", errList, errList[0].errorList[0]);
      this.alertService.tosterDanger(errList[0].errorList[0]);
    } else {
      this.alertService.tosterDanger(errorDetails.error.msg);
    }
  }

}
