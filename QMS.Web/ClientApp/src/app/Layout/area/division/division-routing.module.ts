import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DivisionAddComponent } from './division-add/division-add.component';
import { DivisionListComponent } from './division-list/division-list.component';
import { DivisionComponent } from './division.component';


const routes: Routes = [
  {
    path: '',
    component: DivisionComponent,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: DivisionListComponent
      },
      {
        path: 'new',
        component: DivisionAddComponent,
        // canActivate: [AuthorizeGuard],
        // data: { title: 'New User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Create] },
      },
      {
        path: 'edit/:id',
        component: DivisionAddComponent,
        // canActivate: [AuthorizeGuard],
        // data: { title: 'Edit User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Edit] },
      },
    ],

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DivisionRoutingModule { }
