import { Component, OnInit } from '@angular/core';
import { Division } from 'src/app/Shared/Entity/Division/division';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { DivisionService } from 'src/app/Shared/Services/Division/division.service';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';

@Component({
  selector: 'app-division-list',
  templateUrl: './division-list.component.html',
  styleUrls: ['./division-list.component.css']
})
export class DivisionListComponent implements OnInit {

  heading = 'Create A New Work Flow';
  subheading = '';
  icon = 'pe-7s-drawer icon-gradient bg-happy-itmeo';

   tosterMsgDltSuccess: string = "Record has been deleted successfully.";
   tosterMsgError: string = "Something went wrong!";

   divisionList : Division[] = [];


  constructor(
    private router: Router,
    private divisionService: DivisionService,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService
  ) {

   }

   ngOnInit() {
     this.getDivisionList();
   
    
  }

  createDivision() {
    this.router.navigate(['/division/new']);
  }

  getDivisionList() {
    this.alertService.fnLoading(true);
    this.divisionService.getDivisionList({'page': 1, 'pageSize': 10}).subscribe(
      (res: any) => {
        this.divisionList = res.data.items; 
        this.divisionList.forEach(obj=>{
          obj.isActiveCustom = obj.isActive? 'Yes': 'No';
        });
      },
      (error) => {
        this.alertService.fnLoading(false);
        this.alertService.tosterDanger(this.tosterMsgError);
      },
      () => this.alertService.fnLoading(false));
  }

  edit(id: number) {
    this.router.navigate([`/division/edit/${id}`]);
  }

  delete(id: number) {
    this.alertService.confirm("Are you sure you want to delete this item?",
      () => {
        this.alertService.fnLoading(true);
        this.divisionService.deleteDivision(id).subscribe(
          (succ: any) => {
            this.alertService.tosterSuccess(this.tosterMsgDltSuccess);
            this.getDivisionList();
          },
          (error) => {
            this.alertService.fnLoading(false);
            this.alertService.tosterDanger(this.tosterMsgError);
          },
          () => this.alertService.fnLoading(false));
      }, () => { });
  }

  public ptableSettings: IPTableSetting = {
    tableID: "Division-table",
    tableClass: "table-responsive",
    tableName: 'Division List',
    tableRowIDInternalName: "Id",
    tableColDef: [
      
      { headerName: 'Division Name', width: '80%', internalName: 'name', sort: true, type: "" },
      { headerName: 'Active', width: '20%', internalName: 'isActiveCustom', sort: true, type: "" },
      
    
      
    ],
    enabledSearch: true,
    enabledSerialNo: true,
    pageSize: 10,
    enabledPagination: true,
    enabledEditBtn: true,
    enabledDeleteBtn: true,
    enabledColumnFilter: true, 
    enabledRecordCreateBtn: true,
		newRecordButtonText: 'New Division'
  };

  public fnCustomTrigger(event) {
    console.log("custom  click: ", event);

    if (event.action == "new-record") {
      this.createDivision();
    }
    else if (event.action == "edit-item") {
      this.edit(event.record.id);
    }
    else if (event.action == "delete-item") {
      this.delete(event.record.id);
    }
  }

}
