import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DivisionRoutingModule } from './division-routing.module';
import { DivisionAddComponent } from './division-add/division-add.component';
import { DivisionListComponent } from './division-list/division-list.component';
import { SharedMasterModule } from 'src/app/Shared/Modules/shared-master/shared-master.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';
import { DivisionComponent } from './division.component';


@NgModule({
  declarations: [DivisionAddComponent, DivisionListComponent, DivisionComponent],
  imports: [
    CommonModule,
    DivisionRoutingModule,
    SharedMasterModule,
    AngularFontAwesomeModule,
    FormsModule,
  ]
})
export class DivisionModule { }
