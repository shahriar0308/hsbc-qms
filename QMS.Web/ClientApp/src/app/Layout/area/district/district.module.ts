import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DistrictRoutingModule } from './district-routing.module';
import { DistrictFormComponent } from './district-form/district-form.component';
import { DistrictListComponent } from './district-list/district-list.component';
import { SharedMasterModule } from 'src/app/Shared/Modules/shared-master/shared-master.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DistrictComponent } from './district.component';
import { NgSelectModule } from '@ng-select/ng-select';


@NgModule({
  declarations: [DistrictFormComponent, DistrictListComponent, DistrictComponent],
  imports: [
    CommonModule,
    DistrictRoutingModule,
    SharedMasterModule,
    AngularFontAwesomeModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule
  ]
})
export class DistrictModule { }
