import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { DistrictService } from 'src/app/Shared/Services/District/district.service';
import { finalize } from 'rxjs/operators';
import { District, SaveDistrict } from 'src/app/Shared/Entity/District/district';
import { DivisionService } from 'src/app/Shared/Services/Division/division.service';
import { KeyValuePair } from 'src/app/Shared/Entity/Common/key-value-pair';

@Component({
	selector: 'app-district-form',
	templateUrl: './district-form.component.html',
	styleUrls: ['./district-form.component.css']
})
export class DistrictFormComponent implements OnInit, OnDestroy {

	district: District;
	districtForm: FormGroup;
	divisions: KeyValuePair[];
	private subscriptions: Subscription[] = [];


	constructor(private activatedRoute: ActivatedRoute,
		private router: Router,
		private districtFB: FormBuilder,
		private alertService: AlertService,
		private districtService: DistrictService,
		private divisionService: DivisionService) { }

	ngOnInit() {
		this.loadDivisions();
		// this.alertService.fnLoading(true);
		const routeSubscription = this.activatedRoute.params.subscribe(params => {
			const id = params['id'];
			console.log(id);
			if (id) {

				this.alertService.fnLoading(true);
				this.districtService.getDistrict(id)
					.pipe(finalize(() => this.alertService.fnLoading(false)))
					.subscribe(res => {
						if (res) {
							this.district = res.data as District;
							this.initDistrict(id);
						}
					});
			} else {
				this.district = new District();
				this.district.clear();
				this.initDistrict();
			}
		});
		this.subscriptions.push(routeSubscription);
	}

	ngOnDestroy() {
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	initDistrict(id = null) {
		this.createForm(id);
	}

	loadDivisions() {
		this.alertService.fnLoading(true);
		const divisionSubscription = this.divisionService.getDivisionSelect()
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.divisions = res.data;
				this.divisions.forEach(obj => {
					obj.value = this.stringToInt(obj.value);
				});
			},
				error => {
					this.throwError(error);
				});
		this.subscriptions.push(divisionSubscription);
	}

	createForm(id = null) {
		this.districtForm = this.districtFB.group({
			// userName: [this.user.userName, Validators.required],
			name: [this.district.name, Validators.required],
			divisionId: [this.district.divisionId, Validators.required],
			isActive: [this.district.isActive]
		});
	}

	get dfControls() { return this.districtForm.controls; }

	onSubmit() {

		const controls = this.districtForm.controls;

		if (this.districtForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		const editedDistrict = this.prepareDistrict();
		if (editedDistrict.id) {
			this.updateDistrict(editedDistrict);
		}
		else {
			this.createDistrict(editedDistrict);
		}
	}

	prepareDistrict(): SaveDistrict {
		const controls = this.districtForm.controls;
		const _district = new SaveDistrict();
		_district.clear();
		_district.id = this.district.id;
		_district.name = controls['name'].value;
		_district.divisionId = controls['divisionId'].value;
		_district.isActive = controls['isActive'].value;
		return _district;
	}

	createDistrict(_district: SaveDistrict) {
		this.alertService.fnLoading(true);
		const createSubscription = this.districtService.postDistrict(_district)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`New district has been added successfully.`);
				this.goBack();
			},
				error => {
					this.throwError(error);
				});
		this.subscriptions.push(createSubscription);
	}

	updateDistrict(_district: SaveDistrict) {
		this.alertService.fnLoading(true);
		const updateSubscription = this.districtService.putDistrict(_district)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`District has been saved successfully.`);
				this.goBack();
			},
				error => {
					this.throwError(error);
				});
		this.subscriptions.push(updateSubscription);
	}

	getComponentTitle() {
		let result = 'Create District';
		if (!this.district || !this.district.id) {
			return result;
		}

		result = `Edit district - ${this.district.name}`;
		return result;
	}

	goBack() {
		this.router.navigate([`/district`], { relativeTo: this.activatedRoute });
	}

	stringToInt(value): number {
		return Number.parseInt(value);
	}

	private throwError(errorDetails: any) {
		// this.alertService.fnLoading(false);
		console.log("error", errorDetails);
		let errList = errorDetails.error.errors;
		if (errList.length) {
			console.log("error", errList, errList[0].errorList[0]);
			this.alertService.tosterDanger(errList[0].errorList[0]);
		} else {
			this.alertService.tosterDanger(errorDetails.error.message);
		}
	}
}
