import { Component, OnInit, OnDestroy } from '@angular/core';
import { District, DistrictQuery } from 'src/app/Shared/Entity/District/district';
import { Subscription, of } from 'rxjs';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { DistrictService } from 'src/app/Shared/Services/District/district.service';
import { CommonService } from 'src/app/Shared/Services/Common/common.service';
import { take, delay, finalize } from 'rxjs/operators';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';

@Component({
  selector: 'app-district-list',
  templateUrl: './district-list.component.html',
  styleUrls: ['./district-list.component.css']
})
export class DistrictListComponent implements OnInit, OnDestroy {

  
  query: DistrictQuery;
	PAGE_SIZE: number;
	districts: District[];

	// Subscriptions
	private subscriptions: Subscription[] = [];

	constructor(
		private router: Router,
    private alertService: AlertService,
    private districtService: DistrictService,
		private commonService: CommonService) {
		this.PAGE_SIZE = commonService.PAGE_SIZE;
	}

	ngOnInit() {
		of(undefined).pipe(take(1), delay(1000)).subscribe(() => {
			this.loadDistrictsPage();
		});
	}

	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}
	
    loadDistrictsPage() {
		this.searchConfiguration();
        this.alertService.fnLoading(true);
		const districtsSubscription = this.districtService.getDistricts(this.query)
			.pipe(finalize(() => { this.alertService.fnLoading(false); }))
			.subscribe(
            (res) => {
				this.districts = res.data.items;
				this.districts.forEach(obj=>{
					obj.isActiveCustom = obj.isActive? 'Yes': 'No';
				});
            },
            (error) => {
                console.log(error);
            });
			this.subscriptions.push(districtsSubscription);
	}
	
	searchConfiguration() {
		this.query = new DistrictQuery({
			page: 1,
			pageSize: this.PAGE_SIZE,
			sortBy: 'Name',
			isSortAscending: true,
			name: ''
		});
	}
	
	editDistrict(id) {
		this.router.navigate(['/district/edit', id]);
	}
	
	newDistrict() {
		this.router.navigate(['/district/new']);
	}
	
	deleteDistrict(id) {
		this.alertService.confirm("Are you sure want to delete this district?", 
			() => {
				this.alertService.fnLoading(true);
				const deleteSubscription = this.districtService.deleteDistrict(id)
					.pipe(finalize(() => { this.alertService.fnLoading(false); }))
					.subscribe((res: any) => {
						console.log('res from del func',res);          
						this.alertService.tosterSuccess("District has been deleted successfully.");
						this.loadDistrictsPage();
					},
					(error) => {
						console.log(error);
					});
					this.subscriptions.push(deleteSubscription);
				}, 
			() => {
			});
	}

    public ptableSettings: IPTableSetting = {
        tableID: "districts-table",
        tableClass: "table table-border ",
        tableName: 'District List',
        tableRowIDInternalName: "id",
        tableColDef: [
            { headerName: 'District Name', width: '50%', internalName: 'name', sort: true, type: "" },
            { headerName: 'Division', width: '50%', internalName: 'divisionName', sort: false, type: "" },
            { headerName: 'Active', width: '50%', internalName: 'isActiveCustom', sort: false, type: "", }
        ],
        enabledSearch: true,
        enabledSerialNo: true,
        pageSize: 10,
        enabledPagination: true,
        enabledDeleteBtn: true,
        enabledEditBtn: true,
        enabledColumnFilter: true,
        enabledRadioBtn: false,
		enabledRecordCreateBtn: true,
		newRecordButtonText: 'New District'
    };

    public fnCustomTrigger(event) {
        console.log("custom  click: ", event);

        if (event.action == "new-record") {
            this.newDistrict();
        }
        else if (event.action == "edit-item") {
            this.editDistrict(event.record.id);
        }
        else if (event.action == "delete-item") {
            this.deleteDistrict(event.record.id);
        }
    }

}
