import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThanaRoutingModule } from './thana-routing.module';
import { ThanaFormComponent } from './thana-form/thana-form.component';
import { ThanaListComponent } from './thana-list/thana-list.component';
import { ThanaComponent } from './thana.component';
import { SharedMasterModule } from 'src/app/Shared/Modules/shared-master/shared-master.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';


@NgModule({
  declarations: [ThanaComponent, ThanaFormComponent, ThanaListComponent],
  imports: [
    CommonModule,
    ThanaRoutingModule,
    SharedMasterModule,
    ReactiveFormsModule,
    NgSelectModule
  ]
})
export class ThanaModule { }
