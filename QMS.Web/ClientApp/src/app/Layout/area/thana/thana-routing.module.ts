import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ThanaComponent } from './thana.component';
import { ThanaListComponent } from './thana-list/thana-list.component';
import { ThanaFormComponent } from './thana-form/thana-form.component';


const routes: Routes = [
  {
    path: '',
    component: ThanaComponent,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: ThanaListComponent
      },
      {
        path: 'new',
        component: ThanaFormComponent,
        // canActivate: [AuthorizeGuard],
        // data: { title: 'New User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Create] },
      },
      {
        path: 'edit/:id',
        component: ThanaFormComponent,
        // canActivate: [AuthorizeGuard],
        // data: { title: 'Edit User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Edit] },
      },
    ],

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThanaRoutingModule { }
