import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { DistrictService } from 'src/app/Shared/Services/District/district.service';
import { CommonService } from 'src/app/Shared/Services/Common/common.service';
import { Thana, ThanaQuery } from 'src/app/Shared/Entity/Thana/thana';
import { ThanaService } from 'src/app/Shared/Services/Thana/thana.service';
import { finalize } from 'rxjs/operators';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';

@Component({
  selector: 'app-thana-list',
  templateUrl: './thana-list.component.html',
  styleUrls: ['./thana-list.component.css']
})
export class ThanaListComponent implements OnInit {

  query: ThanaQuery;
  PAGE_SIZE: number;
  thanas: Thana[];

  constructor(
    private router: Router,
    private alertService: AlertService,
    private thanaService: ThanaService,
    private commonService: CommonService
  ) { }

  ngOnInit() {
    this.loadPage();
  }

  private loadPage() {
    this.searchConfiguration();
    this.alertService.fnLoading(true);
    this.getThanas();
  }

  private searchConfiguration() {
    this.query = new ThanaQuery({
      page: 1,
      pageSize: this.PAGE_SIZE,
      sortBy: 'Name',
      isSortAscending: true,
      name: ''
    });
  }

  private getThanas() {
    this.thanaService.getThanas(this.query)
      .pipe(finalize(() => { this.alertService.fnLoading(false); }))
      .subscribe(
        (res) => {
          this.thanas = res.data.items;
          this.thanas.forEach(obj => {
            obj.isActiveCustom = obj.isActive ? 'Yes' : 'No';
          });
        },
        (error) => {
          console.log(error);
        });
  }

  public ptableSettings: IPTableSetting = {
    tableID: "thanas-table",
    tableClass: "table table-border ",
    tableName: 'Thana List',
    tableRowIDInternalName: "id",
    tableColDef: [
      { headerName: 'Thana Name', width: '50%', internalName: 'name', sort: true, type: "" },
      { headerName: 'Dsitrict', width: '50%', internalName: 'districtName', sort: false, type: "" },
      { headerName: 'Active', width: '50%', internalName: 'isActiveCustom', sort: false, type: "", }
    ],
    enabledSearch: true,
    enabledSerialNo: true,
    pageSize: 10,
    enabledPagination: true,
    enabledDeleteBtn: true,
    enabledEditBtn: true,
    enabledColumnFilter: true,
    enabledRadioBtn: false,
    enabledRecordCreateBtn: true,
    newRecordButtonText: 'New Thana'
  };

  public fnCustomTrigger(event) {
    if (event.action == "new-record") {
      this.newThana();
    }
    else if (event.action == "edit-item") {
      this.editThana(event.record.id);
    }
    else if (event.action == "delete-item") {
      this.deleteThana(event.record.id);
    }
  }

  private editThana(id) {
		this.router.navigate(['/thana/edit', id]);
	}
	
	private newThana() {
		this.router.navigate(['/thana/new']);
	}
	
	deleteThana(id) {
		this.alertService.confirm("Are you sure want to delete this thana?", 
			() => {
				this.alertService.fnLoading(true);
				this.thanaService.deleteThana(id)
					.pipe(finalize(() => { this.alertService.fnLoading(false); }))
					.subscribe((res: any) => {         
						this.alertService.tosterSuccess("Thana has been deleted successfully.");
						this.loadPage();
					},
					(error) => {
						console.log(error);
					});
				}, 
			() => {
			});
	}
}


