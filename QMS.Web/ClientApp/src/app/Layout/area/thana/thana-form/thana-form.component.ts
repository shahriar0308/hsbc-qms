import { Component, OnInit } from '@angular/core';
import { Thana, SaveThana } from 'src/app/Shared/Entity/Thana/thana';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { KeyValuePair } from 'src/app/Shared/Entity/Common/key-value-pair';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { DistrictService } from 'src/app/Shared/Services/District/district.service';
import { ThanaService } from 'src/app/Shared/Services/Thana/thana.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-thana-form',
  templateUrl: './thana-form.component.html',
  styleUrls: ['./thana-form.component.css']
})
export class ThanaFormComponent implements OnInit {

  thana: Thana;
  thanaForm: FormGroup;
  districts: KeyValuePair[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private thanaFB: FormBuilder,
    private alertService: AlertService,
    private districtService: DistrictService,
    private thanaService: ThanaService
  ) { }

  ngOnInit() {
    this.loadDistricts();
    const id = this.activatedRoute.snapshot.params.id;
    if (id) {
      this.getThana(id);
    } else {
      this.thana = new Thana();
      this.thana.clear();
      this.createForm();
    }
  }

  private loadDistricts() {
    this.alertService.fnLoading(true);
    this.districtService.getByDivisionSelect()
      .pipe(finalize(() => this.alertService.fnLoading(false)))
      .subscribe(res => {
        this.districts = res.data;
        this.districts.forEach(obj => {
          obj.value = this.stringToInt(obj.value);
        });
      },
        error => {
          this.throwError(error);
        });
  }

  private getThana(id: number) {
    this.alertService.fnLoading(true);
    this.thanaService.getThana(id)
      .pipe(finalize(() => this.alertService.fnLoading(false)))
      .subscribe(res => {
        if (res) {
          this.thana = res.data as Thana;
          this.createForm();
        }
      });
  }

  private createForm() {
    this.thanaForm = this.thanaFB.group({
      name: [this.thana.name, Validators.required],
      districtId: [this.thana.districtId, Validators.required],
      isActive: [this.thana.isActive]
    });
  }

  get tfControls() { return this.thanaForm.controls; }

  public getComponentTitle() {
    let result = 'Create Thana';
    if (!this.thana || !this.thana.id) {
      return result;
    }
    result = `Edit Thana - ${this.thana.name}`;
    return result;
  }

  public goBack() {
    this.router.navigate([`/thana`], { relativeTo: this.activatedRoute });
  }

  public onSubmit() {
    const controls = this.thanaForm.controls;

    if (this.thanaForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    const editedThana = this.prepareThana();
    if (editedThana.id) {
      this.updateThana(editedThana);
    }
    else {
      this.createThana(editedThana);
    }
  }

  private prepareThana(): SaveThana {
    const controls = this.thanaForm.controls;
    const _thana = new SaveThana();
    _thana.clear();
    _thana.id = this.thana.id;
    _thana.name = controls['name'].value;
    _thana.districtId = controls['districtId'].value;
    _thana.isActive = controls['isActive'].value;
    return _thana;
  }

  private updateThana(model: SaveThana) {
    this.alertService.fnLoading(true);
		this.thanaService.putThana(model)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`Thana has been saved successfully.`);
				this.goBack();
			},
				error => {
					this.throwError(error);
				});
  }

  private createThana(model: SaveThana) {
    this.alertService.fnLoading(true);
    this.thanaService.postThana(model)
      .pipe(finalize(() => this.alertService.fnLoading(false)))
      .subscribe(res => {
        this.alertService.tosterSuccess(`New Thana has been added successfully.`);
        this.goBack();
      },
        error => {
          this.throwError(error);
        });
  }

  private stringToInt(value): number {
    return Number.parseInt(value);
  }

  private throwError(errorDetails: any) {
    console.log("error", errorDetails);
    let errList = errorDetails.error.errors;
    if (errList.length) {
      console.log("error", errList, errList[0].errorList[0]);
      this.alertService.tosterDanger(errList[0].errorList[0]);
    } else {
      this.alertService.tosterDanger(errorDetails.error.message);
    }
  }
}
