import { Component, OnInit, OnDestroy } from '@angular/core';
import { SingleValuedEntity } from 'src/app/Shared/Entity/SingleValuedEntities/single-valued-entity';
import { Subscription, of } from 'rxjs';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { SingleValuedService } from 'src/app/Shared/Services/SingleValues/single-value.service';
import { CommonService } from 'src/app/Shared/Services/Common/common.service';
import { take, delay, finalize } from 'rxjs/operators';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';
import { SingleValuedEntities } from 'src/app/Shared/Constants/single-valued-entity';

@Component({
  selector: 'app-terms-conditions-list',
  templateUrl: './terms-conditions-list.component.html',
  styleUrls: ['./terms-conditions-list.component.css']
})
export class TermsConditionsListComponent implements OnInit, OnDestroy {

	PAGE_SIZE: number;
	terms: SingleValuedEntity[];

	// Subscriptions
	private subscriptions: Subscription[] = [];

	constructor(
		private router: Router,
    	private alertService: AlertService,
    	private singleValuedService: SingleValuedService,
		private commonService: CommonService) {
		this.PAGE_SIZE = commonService.PAGE_SIZE;
	}

	ngOnInit() {
		of(undefined).pipe(take(1), delay(1000)).subscribe(() => {
			this.loadTermsAndConditions();
		});
	}

	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}
	
  loadTermsAndConditions() {
    this.alertService.fnLoading(true);
		const termsSubscription = this.singleValuedService.getByName(SingleValuedEntities.TermsAndConditions)
			.pipe(finalize(() => { this.alertService.fnLoading(false); }))
			.subscribe(
        (res) => {
          this.terms = res.data.items;
          },
          (error) => {
              console.log(error);
          });
			this.subscriptions.push(termsSubscription);
	}
	
	editTerms(id) {
		this.router.navigate(['/terms-conditions/edit', id]);
	}
	
  public ptableSettings: IPTableSetting = {
    tableID: "terms-conditions-table",
    tableClass: "table table-border ",
    tableName: 'Terms & Conditions',
    tableRowIDInternalName: "id",
    tableColDef: [
      { headerName: 'Description', width: '100%', internalName: 'value', sort: false, type: "" }
    ],
    enabledEditBtn: true,
    enabledColumnFilter: true
	};
	
  public fnCustomTrigger(event) {   
    if (event.action == "edit-item") {
        this.editTerms(event.record.id);
    }
  }
}
