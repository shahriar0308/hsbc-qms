import { Component, OnInit, OnDestroy } from '@angular/core';
import { SingleValuedEntity } from 'src/app/Shared/Entity/SingleValuedEntities/single-valued-entity';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { SingleValuedService } from 'src/app/Shared/Services/SingleValues/single-value.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-terms-conditions-form',
  templateUrl: './terms-conditions-form.component.html',
  styleUrls: ['./terms-conditions-form.component.css']
})
export class TermsConditionsFormComponent implements OnInit,OnDestroy {

  
  term: SingleValuedEntity;
  termsConditionsForm: FormGroup;
  isEdit: boolean;
  private subscriptions: Subscription[] = [];


	constructor(private activatedRoute: ActivatedRoute,
		private router: Router,
    private termsConditionsFB: FormBuilder,
    private alertService: AlertService,
    private singleValuedService: SingleValuedService) { }

	ngOnInit() {
		const routeSubscription = this.activatedRoute.params.subscribe(params => {
      const id = params['id'];
      console.log(id);
			if (id) {
				this.isEdit = true;
				this.alertService.fnLoading(true);
				this.singleValuedService.getValue(id)
					.pipe(finalize(() => this.alertService.fnLoading(false)))
					.subscribe(res => {
						if (res) {
							this.term = res.data as SingleValuedEntity;
							this.initTerms();
						}
					});
			}
		});
		this.subscriptions.push(routeSubscription);
	}

 	ngOnDestroy() {
		this.subscriptions.forEach(sb => sb.unsubscribe());
  }

	initTerms() {
		this.createForm();
	}

	createForm() {
		this.termsConditionsForm = this.termsConditionsFB.group({
			// userName: [this.user.userName, Validators.required],
			description: [this.term.value, Validators.required]
		});
	}
	
	get dfControls() { return this.termsConditionsForm.controls; }

	onSubmit() {
    const controls = this.termsConditionsForm.controls;
		
		if (this.termsConditionsForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		const editedTerms = this.prepareTerms();
		if (editedTerms.id) {
			this.updateTerms(editedTerms);
		}
	}

	prepareTerms(): SingleValuedEntity {
		const controls = this.termsConditionsForm.controls;
		const _term = new SingleValuedEntity();
		_term.clear();
		_term.id = this.term.id;
		_term.name = this.term.name;
		_term.value = controls['description'].value;
		return _term;
	}

	updateTerms(_term: SingleValuedEntity) {
		this.alertService.fnLoading(true);
		const updateSubscription = this.singleValuedService.update(_term)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`Terms & Conditions has been saved successfully.`);
				this.goBack();
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.push(updateSubscription);
	}

	getComponentTitle() {
		return `Update Terms & Conditions`;
	}
	
	goBack() {
		this.router.navigate([`/terms-conditions`], { relativeTo: this.activatedRoute });
	}

  private throwError(errorDetails: any) {
      console.log("error", errorDetails);
      let errList = errorDetails.error.errors;
      if (errList.length) {
          console.log("error", errList, errList[0].errorList[0]);
      }
  }
}
