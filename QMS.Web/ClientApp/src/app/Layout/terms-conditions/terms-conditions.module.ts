import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TermsConditionsRoutingModule } from './terms-conditions-routing.module';
import { TermsConditionsComponent } from './terms-conditions.component';
import { TermsConditionsFormComponent } from './terms-conditions-form/terms-conditions-form.component';
import { TermsConditionsListComponent } from './terms-conditions-list/terms-conditions-list.component';
import { SharedMasterModule } from 'src/app/Shared/Modules/shared-master/shared-master.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [TermsConditionsComponent, TermsConditionsFormComponent, TermsConditionsListComponent],
  imports: [
    CommonModule,
    TermsConditionsRoutingModule,
    SharedMasterModule,
    AngularFontAwesomeModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class TermsConditionsModule { }
