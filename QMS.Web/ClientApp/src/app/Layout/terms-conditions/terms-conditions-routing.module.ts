import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TermsConditionsComponent } from './terms-conditions.component';
import { TermsConditionsListComponent } from './terms-conditions-list/terms-conditions-list.component';
import { TermsConditionsFormComponent } from './terms-conditions-form/terms-conditions-form.component';


const routes: Routes = [
  {
    path: '',
    component: TermsConditionsComponent,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: TermsConditionsListComponent
      },
      {
        path: 'edit/:id',
        component: TermsConditionsFormComponent,
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TermsConditionsRoutingModule { }
