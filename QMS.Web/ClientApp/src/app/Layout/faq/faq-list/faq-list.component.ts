import { Component, OnInit, OnDestroy } from '@angular/core';
import { FAQ, FAQQuery } from 'src/app/Shared/Entity/FAQ/FAQ';
import { Router, ActivatedRoute } from '@angular/router';
import { FaqService } from 'src/app/Shared/Services/FAQ/faq.service';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { of, Subscription } from 'rxjs';
import { take, delay, finalize } from 'rxjs/operators';
import { CommonService } from 'src/app/Shared/Services/Common/common.service';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';

@Component({
  selector: 'app-faq-list',
  templateUrl: './faq-list.component.html',
  styleUrls: ['./faq-list.component.css']
})
export class FaqListComponent implements OnInit, OnDestroy {

  query: FAQQuery;
  tosterMsgDltSuccess: string = "Record has been deleted successfully.";
  tosterMsgError: string = "Something went wrong!";
  PAGE_SIZE: number;
  faqList : FAQ[] = [];
  private subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private faqService: FaqService,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService,
    private commonService: CommonService
  ) {
    this.PAGE_SIZE = commonService.PAGE_SIZE;
   }

  ngOnInit() {
    of(undefined).pipe(take(1), delay(1000)).subscribe(() => {
			this.loadFAQs();
		});
  }

  ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}

  loadFAQs(){
    this.searchConfiguration();
    this.alertService.fnLoading(true);
    const dealersSubscription = this.faqService.getFAQList(this.query)
      .pipe(finalize(() => { this.alertService.fnLoading(false); }))
      .subscribe(
            (res) => {
        this.faqList = res.data.items;
        this.faqList.forEach(obj=>{
          obj.isActiveCustom = obj.isActive? 'Yes': 'No';
        });
      },
      (error) => {
          console.log(error);
      });
    this.subscriptions.push(dealersSubscription);
  }

  searchConfiguration() {
		this.query = new FAQQuery({
			page: 1,
			pageSize: this.PAGE_SIZE,
			sortBy: 'sequence',
			isSortAscending: true,
			questionTitle: ''
		});
	}

  createFAQ() {
    this.router.navigate(['/faq/new']);
  }


  edit(id: number) {
    this.router.navigate([`/faq/edit/${id}`]);
  }

  delete(id: number) {
    this.alertService.confirm("Are you sure you want to delete this item?",
      () => {
        this.alertService.fnLoading(true);
        this.faqService.deleteFAQ(id).subscribe(
          (succ: any) => {
            this.alertService.tosterSuccess(this.tosterMsgDltSuccess);
            this.loadFAQs();
          },
          (error) => {
            this.alertService.fnLoading(false);
            this.alertService.tosterDanger(this.tosterMsgError);
          },
          () => this.alertService.fnLoading(false));
      }, () => { });
  }

  public ptableSettings: IPTableSetting = {
    tableID: "FAQs-table",
    tableClass: "table-responsive",
    tableName: 'Frequently Asked Questions List',
    tableRowIDInternalName: "Id",
    tableColDef: [
      { headerName: 'FAQ Type', width: '15%', internalName: 'faqTypeCustom', sort: true, type: "" },
      { headerName: 'Question Title', width: '15%', internalName: 'questionTitle', sort: true, type: "" },
      { headerName: 'Answer', width: '50%', internalName: 'questionAnswer', sort: false, type: "" },
      { headerName: 'Sequence', width: '10%', internalName: 'sequence', sort: false, type: "" },
      { headerName: 'Active', width: '10%', internalName: 'isActiveCustom', sort: false, type: "" }      
    ],
    enabledSearch: true,
    enabledSerialNo: true,
    pageSize: 10,
    enabledPagination: true,
    enabledEditBtn: true,
    enabledDeleteBtn: true,
    enabledColumnFilter: true, 
    enabledRecordCreateBtn: true,
		newRecordButtonText: 'New FAQ'
  };

  public fnCustomTrigger(event) {
    console.log("custom  click: ", event);

    if (event.action == "new-record") {
      this.createFAQ();
    }
    else if (event.action == "edit-item") {
      this.edit(event.record.id);
    }
    else if (event.action == "delete-item") {
      this.delete(event.record.id);
    }
  }

}
