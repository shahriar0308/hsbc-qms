import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FaqListComponent } from './faq-list/faq-list.component';
import { FaqAddComponent } from './faq-add/faq-add.component';


const routes: Routes = [
  {
      path: '',
      children: [
          { path: '', redirectTo: 'list', pathMatch: 'full' },
          { path: 'new', component: FaqAddComponent },
          { path: 'list', component: FaqListComponent },
          { path: 'edit/:id', component: FaqAddComponent }
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FaqRoutingModule { }
