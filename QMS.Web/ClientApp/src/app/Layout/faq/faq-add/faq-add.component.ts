import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { FAQ, SaveFAQ, FAQQuery } from 'src/app/Shared/Entity/FAQ/FAQ';
import { FaqService } from 'src/app/Shared/Services/FAQ/faq.service';
import { finalize } from 'rxjs/operators';
import { KeyValuePair } from 'src/app/Shared/Entity/Common/key-value-pair';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { APIResponse } from 'src/app/Shared/Entity/Response/api-response';

@Component({
	selector: 'app-faq-add',
	templateUrl: './faq-add.component.html',
	styleUrls: ['./faq-add.component.css']
})
export class FaqAddComponent implements OnInit {

	faqModel: FAQ;
	faqForm: FormGroup;
	faqTypes: KeyValuePair[];
	query: FAQQuery = new FAQQuery({
		page: 1
	});
	faqList: FAQ[] = [];
	faqTypeMaxSequencePair = {};
	faqEditId: number;

	constructor(
		private router: Router,
		private faqService: FaqService,
		private faqFB: FormBuilder,
		private activatedRoute: ActivatedRoute,
		private alertService: AlertService
	) {

	}

	ngOnInit() {
		this.faqEditId = this.activatedRoute.snapshot.params['id'] || 0;
		this.loadFAQTypes();
	}

	private prepareEditOrCreateForm() {
		if (this.faqEditId > 0) {

			this.getFaqById(this.faqEditId);
		}
		else {
			this.faqModel = new FAQ();
			this.faqModel.clear();
			this.initDealer();
			//this.getFaqList();
		}
		this.getFaqList();
	}

	initDealer() {
		this.createForm();
	}

	loadFAQTypes() {
		this.alertService.fnLoading(true);
		this.faqService.getFAQTypes()
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.faqTypes = res.data;
				this.faqTypes.forEach(obj => {
					obj.value = this.stringToInt(obj.value);
				});

				this.prepareEditOrCreateForm();
			},
				error => {
					this.throwError(error);
				});
	}

	private getFaqById(id: number) {
		this.alertService.fnLoading(true);
		this.faqService.getFAQ(id)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				if (res) {
					this.faqModel = res.data as FAQ;
					this.initDealer();
				}
			});
	}

	goBack() {
		this.router.navigate([`/faq`], { relativeTo: this.activatedRoute });
	}

	insertFAQ() {

		this.alertService.fnLoading(true);
		this.faqService.postFAQ(this.faqModel)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`New FAQ has been added successfully.`);
				this.goBack();
			},
				error => {
					this.throwError(error);
				});

	}

	createForm() {
		this.faqForm = this.faqFB.group({
			faqType: [this.faqModel.faqType, Validators.required],
			questionTitle: [this.faqModel.questionTitle, Validators.required],
			questionAnswer: [this.faqModel.questionAnswer, Validators.required],
			isActive: [this.faqModel.isActive],
			sequence: [this.faqModel.sequence, [Validators.required, Validators.min(1)]]
		});
	}

	get dfControls() { return this.faqForm.controls; }

	onSubmit() {

		const controls = this.faqForm.controls;

		if (this.faqForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		const editedFAQ = this.prepareFAQ();
		if (editedFAQ.id) {
			this.updateFAQ(editedFAQ);
		}
		else {
			this.createFAQ(editedFAQ);
		}
	}

	prepareFAQ(): SaveFAQ {
		const controls = this.faqForm.controls;
		const _faq = new SaveFAQ();
		_faq.clear();
		_faq.id = this.faqModel.id;
		_faq.faqType = controls['faqType'].value;
		_faq.questionTitle = controls['questionTitle'].value;
		_faq.questionAnswer = controls['questionAnswer'].value;
		_faq.isActive = controls['isActive'].value;
		_faq.sequence = controls['sequence'].value;
		return _faq;
	}

	createFAQ(_faq: SaveFAQ) {
		this.alertService.fnLoading(true);
		this.faqService.postFAQ(_faq)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`New FAQ has been added successfully.`);
				this.goBack();
			},
				error => {
					this.throwError(error);
				});
	}

	updateFAQ(_faq: SaveFAQ) {
		this.alertService.fnLoading(true);
		this.faqService.putFAQ(_faq)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`FAQ has been saved successfully.`);
				this.goBack();
			},
				error => {
					this.throwError(error);
				});
	}

	getComponentTitle() {
		let result = 'Create FAQ';
		if (!this.faqModel || !this.faqModel.id) {
			return result;
		}

		result = `Edit FAQ - ${this.faqModel.questionTitle}`;
		return result;
	}

	stringToInt(value): number {
		return Number.parseInt(value);
	}

	private throwError(errorDetails: any) {
		// this.alertService.fnLoading(false);
		console.log("error", errorDetails);
		let errList = errorDetails.error.errors;
		if (errList.length) {
			console.log("error", errList, errList[0].errorList[0]);
			// this.alertService.tosterDanger(errList[0].errorList[0]);
		} else {
			//this.alertService.tosterDanger(errorDetails.error.message);
		}
	}

	private getFaqList() {
		this.alertService.fnLoading(true);
		this.faqService.getFAQList(this.query)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe((res: APIResponse) => {
				this.faqList = res.data.items;
				this.prepareTypeSequence();

			},
				error => {
					this.throwError(error);
				});
	}

	onFaqTypeChange(seqType: string | number) {		
		let seq = this.faqTypeMaxSequencePair[seqType] + 1;
		if (this.faqEditId > 0 && this.faqModel.faqType == seqType) {
			seq = this.faqTypeMaxSequencePair[seqType];
		}
		this.faqForm.controls['sequence'].setValue(seq);
	}

	private prepareTypeSequence() {
		let sequenceList: number[] = [];
		this.faqTypes.forEach((type) => {
			sequenceList = this.faqList.filter(faq => faq.faqType === type.value).map(t => t.sequence);

			if (this.faqEditId > 0 && this.faqModel.faqType == type.value) {
				this.faqTypeMaxSequencePair[type.value] = this.faqModel.sequence;
			} else {
				this.faqTypeMaxSequencePair[type.value] = Math.max(...sequenceList);
			}
		});
		console.log("Test Object", this.faqTypeMaxSequencePair);
	}
}
