import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FaqRoutingModule } from './faq-routing.module';
import { FaqListComponent } from './faq-list/faq-list.component';
import { SharedMasterModule } from 'src/app/Shared/Modules/shared-master/shared-master.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { FaqAddComponent } from './faq-add/faq-add.component';


@NgModule({
  declarations: [FaqAddComponent, FaqListComponent],
  imports: [
    CommonModule,
    FaqRoutingModule,
    SharedMasterModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule,
  ]
})
export class FaqModule { }
