import { Component, OnInit } from '@angular/core';
import { Complain, ComplainQuery } from 'src/app/Shared/Entity/Complain/complain.model';
import { MapObject } from 'src/app/Shared/Enums/map-object';
import { ComplainTypeObject } from 'src/app/Shared/Enums/complainType';
import { Router, ActivatedRoute } from '@angular/router';
import { ComplainService } from 'src/app/Shared/Services/Complain/complain.service';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';

@Component({
  selector: 'app-complain-list',
  templateUrl: './complain-list.component.html',
  styleUrls: ['./complain-list.component.css']
})
export class ComplainListComponent implements OnInit {

  heading = 'Create A New Work Flow';
  subheading = ''; 
  icon = 'pe-7s-drawer icon-gradient bg-happy-itmeo';

   tosterMsgDltSuccess: string = "Record has been deleted successfully.";
   tosterMsgError: string = "Something went wrong!";

   complainList : Complain[] = [];
   enumComplainType : MapObject[] =  ComplainTypeObject.ComplainType;
   query: ComplainQuery;


  constructor(
    private router: Router,
    private complainService: ComplainService,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService
  ) {

   }

   ngOnInit() {
    this.getComplainList();
   
    
  }


  getComplainList() {
    this.alertService.fnLoading(true);
    this.searchConfiguration();
    this.complainService.getComplainList(this.query).subscribe(
      (res: any) => {
        this.complainList = res.data.items;
        
        this.complainList.forEach(element =>{

          let data = this.enumComplainType.filter(a => a.id == element.complainType)[0];

          if(data){
            element.displayComplainType = data.label;
          }

          element.displayComplainDetails = element.complainDetails.substr(0,200) + ' ...';
          element.viewDetailsbutton = "View Details"


        });

        
      },
      (error) => {
        this.alertService.fnLoading(false);
        this.alertService.tosterDanger(this.tosterMsgError);
      },
      () => this.alertService.fnLoading(false));
  }

  searchConfiguration() {
		this.query = new ComplainQuery({
			page: 1,
			pageSize: 10,
			sortBy: 'Created',
			isSortAscending: false,
		});
	}

  fnViewComplain(event : any) {
    let id = event.record.id;
    this.router.navigate([`/complain/complain-view/${id}`]);
  }

  delete(id: number) {
    this.alertService.confirm("Are you sure you want to delete this item?",
      () => {
        this.alertService.fnLoading(true);
        this.complainService.deleteComplain(id).subscribe(
          (succ: any) => {
            this.alertService.tosterSuccess(this.tosterMsgDltSuccess);
            this.getComplainList();
          },
          (error) => {
            this.alertService.fnLoading(false);
            this.alertService.tosterDanger(this.tosterMsgError);
          },
          () => this.alertService.fnLoading(false));
      }, () => { });
  }

  public ptableSettings = {
    tableID: "Complain-table",
    tableClass: "table-responsive",
    tableName: 'Complain List',
    tableRowIDInternalName: "Id",
    tableColDef: [
      
      { headerName: 'Complain Type', width: '25%', internalName: 'displayComplainType', sort: false, type: "" },
      { headerName: 'Complain Details', width: '55%', internalName: 'displayComplainDetails', sort: false, type: "" },
      { headerName: 'View Details', width: '20%', internalName: 'viewDetailsbutton', sort: false, type: "button", onClick: 'true',innerBtnIcon:"fa fa-action" },
    
      
    ],
    enabledSearch: true,
    enabledSerialNo: true,
    pageSize: 10,
    enabledPagination: true,
    enabledDeleteBtn: true,
    enabledColumnFilter: true, 
    enabledCellClick: true,
  };

  public fnCustomTrigger(event) {
    console.log("custom  click: ", event);

    if (event.action == "delete-item") {
      this.delete(event.record.id);
    }
  }

}
