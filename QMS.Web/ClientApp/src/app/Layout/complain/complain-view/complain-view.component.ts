import { Component, OnInit } from '@angular/core';
import { Complain } from 'src/app/Shared/Entity/Complain/complain.model';
import { MapObject } from 'src/app/Shared/Enums/map-object';
import { ComplainTypeObject } from 'src/app/Shared/Enums/complainType';
import { Router, ActivatedRoute } from '@angular/router';
import { ComplainService } from 'src/app/Shared/Services/Complain/complain.service';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';

@Component({
  selector: 'app-complain-view',
  templateUrl: './complain-view.component.html',
  styleUrls: ['./complain-view.component.css']
})
export class ComplainViewComponent implements OnInit {

  complainModel: Complain;
  enumComplainType : MapObject[] =  ComplainTypeObject.ComplainType;

  constructor(
    private router: Router,
    private complainService: ComplainService,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService
  ) {

   }

  ngOnInit() {
    this.createForm();
    if (Object.keys(this.activatedRoute.snapshot.params).length !== 0 && this.activatedRoute.snapshot.params.id !== 'undefined') {
      let complainId = this.activatedRoute.snapshot.params.id;
      this.getComplain(complainId);
      
    }
  }



  backToTheList(){

    this.router.navigate(['/complain/complain-list']);

  }

  getComplain(complainId){
    this.complainService.getComplain(complainId).subscribe(
      (result: any) => {
       
        this.editForm(result.data);
      },
      (err: any) => console.log(err)
    );

  }


  createForm() {

    this.complainModel = new Complain();
   
  }

  editForm(complainData: Complain) {

    this.complainModel.complainDetails = complainData.complainDetails;
    this.complainModel.complainType = complainData.complainType;
    this.complainModel.id = complainData.id;

    let data = this.enumComplainType.filter(a => a.id == complainData.complainType)[0];

    if(data){
      this.complainModel.displayComplainType = data.label;
    }


  }


  displayError(errorDetails: any) {
    // this.alertService.fnLoading(false);
    console.log("error", errorDetails);
    let errList = errorDetails.error.errors;
    if (errList.length) {
      console.log("error", errList, errList[0].errorList[0]);
      this.alertService.tosterDanger(errList[0].errorList[0]);
    } else {
      this.alertService.tosterDanger(errorDetails.error.msg);
    }
  }

}
