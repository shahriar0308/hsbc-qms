import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComplainRoutingModule } from './complain-routing.module';
import { ComplainListComponent } from './complain-list/complain-list.component';
import { ComplainViewComponent } from './complain-view/complain-view.component';
import { SharedMasterModule } from 'src/app/Shared/Modules/shared-master/shared-master.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';


@NgModule({
  declarations: [ComplainListComponent, ComplainViewComponent],
  imports: [
    CommonModule,
    ComplainRoutingModule,
    SharedMasterModule,
    AngularFontAwesomeModule,
    FormsModule,
    NgSelectModule,
  ]
})
export class ComplainModule { }
