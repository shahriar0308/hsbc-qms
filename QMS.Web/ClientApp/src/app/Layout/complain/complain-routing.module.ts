import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComplainViewComponent } from './complain-view/complain-view.component';
import { ComplainListComponent } from './complain-list/complain-list.component';

const routes: Routes = [
  {
      path: '',
      children: [
          { path: '', redirectTo: 'complain-list' },
          { path: 'complain-view/:id', component: ComplainViewComponent },
          { path: 'complain-list', component: ComplainListComponent },
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComplainRoutingModule { }
