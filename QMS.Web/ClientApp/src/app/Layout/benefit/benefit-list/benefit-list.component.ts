import { Component, OnInit, OnDestroy } from '@angular/core';
import { BenefitQuery, Benefit } from 'src/app/Shared/Entity/Benefit/benefits';
import { Subscription, of } from 'rxjs';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { BenefitService } from 'src/app/Shared/Services/Benefits/benefit.service';
import { take, delay, finalize } from 'rxjs/operators';
import { CommonService } from 'src/app/Shared/Services/Common/common.service';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-benefit-list',
  templateUrl: './benefit-list.component.html',
  styleUrls: ['./benefit-list.component.css']
})
export class BenefitListComponent implements OnInit, OnDestroy {

  query: BenefitQuery;
	PAGE_SIZE: number;
	benefits: Benefit[];

	// Subscriptions
	private subscriptions: Subscription[] = [];

	constructor(
		private router: Router,
    	private alertService: AlertService,
    	private benefitService: BenefitService,
		private commonService: CommonService,
		private modalService: NgbModal) {
		this.PAGE_SIZE = commonService.PAGE_SIZE;
	}

	ngOnInit() {
		of(undefined).pipe(take(1), delay(1000)).subscribe(() => {
			this.loadBenefitsPage();
		});
	}

	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}
	
    loadBenefitsPage() {
		this.searchConfiguration();
        this.alertService.fnLoading(true);
		const benefitsSubscription = this.benefitService.getBenefits(this.query)
			.pipe(finalize(() => { this.alertService.fnLoading(false); }))
			.subscribe(
            (res) => {
				this.benefits = res.data.items;
				this.benefits.forEach(obj=>{
					obj.isActiveCustom = obj.isActive? 'Yes': 'No';
				});
            },
            (error) => {
                console.log(error);
            });
			this.subscriptions.push(benefitsSubscription);
	}
	
	searchConfiguration() {
		this.query = new BenefitQuery({
			page: 1,
			pageSize: this.PAGE_SIZE,
			sortBy: 'sequence',
			isSortAscending: true,
			name: '',
		});
	}

	toggleActiveInactive(id) {
		const actInSubscription = this.benefitService.activeInactive(id).subscribe(res => {
			this.loadBenefitsPage();
		});
		this.subscriptions.push(actInSubscription);
	}
	
	editBenefit(id) {
		this.router.navigate(['/benefit/edit', id]);
	}
	
	newBenefit() {
		this.router.navigate(['/benefit/new']);
	}
	
	deleteBenefit(id) {
		this.alertService.confirm("Are you sure want to delete this benefit?", 
			() => {
				this.alertService.fnLoading(true);
				const deleteSubscription = this.benefitService.delete(id)
					.pipe(finalize(() => { this.alertService.fnLoading(false); }))
					.subscribe((res: any) => {
						console.log('res from del func',res);          
						this.alertService.tosterSuccess("Benefit has been deleted successfully.");
						this.loadBenefitsPage();
					},
					(error) => {
						console.log(error);
					});
					this.subscriptions.push(deleteSubscription);
				}, 
			() => {
			});
	}

    public ptableSettings: IPTableSetting = {
        tableID: "benefits-table",
        tableClass: "table table-border ",
        tableName: 'Benefit List',
        tableRowIDInternalName: "id",
        tableColDef: [
			{ headerName: 'Benefit Name', width: '20%', internalName: 'name', sort: true, type: "" },
			{ headerName: 'Category', width: '10%', internalName: 'categoryName', sort: true, type: "" },
			{ headerName: 'Description', width: '50%', internalName: 'description', sort: false, type: "" },
			{ headerName: 'Sequence', width: '10%', internalName: 'sequence', sort: false, type: "" },
			{ headerName: 'Active', width: '10%', internalName: 'isActiveCustom', sort: false, type: "" }
			// { headerName: 'Details', width: '20%', internalName: 'details', sort: false, type: "button", onClick: 'true', innerBtnIcon:"fa fa-action" },
        ],
        enabledSearch: true,
        enabledSerialNo: true,
        pageSize: 10,
        enabledPagination: true,
        enabledDeleteBtn: true,
        enabledEditBtn: true,
        enabledColumnFilter: true,
        enabledRadioBtn: false,
		enabledRecordCreateBtn: true,
		enabledCellClick: true,
		newRecordButtonText: 'New Benefit'
	};
	
    public fnCustomTrigger(event) {
        console.log("custom  click: ", event);

        if (event.action == "new-record") {
            this.newBenefit();
        }
        else if (event.action == "edit-item") {
            this.editBenefit(event.record.id);
        }
        else if (event.action == "delete-item") {
            this.deleteBenefit(event.record.id);
        }
    }

}
