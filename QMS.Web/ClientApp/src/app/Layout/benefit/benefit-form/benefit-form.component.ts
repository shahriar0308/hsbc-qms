import { Component, OnInit, OnDestroy } from '@angular/core';
import { Benefit, SaveBenefit } from 'src/app/Shared/Entity/Benefit/benefits';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { BenefitService } from 'src/app/Shared/Services/Benefits/benefit.service';
import { finalize } from 'rxjs/operators';
import { Setting } from 'src/app/Shared/Entity/Settings/setting';
import { PointSettingsService } from 'src/app/Shared/Services/Settings/point-setting.service';

@Component({
  selector: 'app-benefit-form',
  templateUrl: './benefit-form.component.html',
  styleUrls: ['./benefit-form.component.css']
})
export class BenefitFormComponent implements OnInit, OnDestroy {

  benefit: Benefit;
  benefitForm: FormGroup;
  settings: Setting[];
  isEdit: boolean;
  private subscriptions: Subscription[] = [];


	constructor(private activatedRoute: ActivatedRoute,
		private router: Router,
    private benefitFB: FormBuilder,
    private alertService: AlertService,
    private benefitService: BenefitService, private settingService: PointSettingsService) { }

	ngOnInit() {
		// this.alertService.fnLoading(true);
		this.loadCategories();
		const routeSubscription = this.activatedRoute.params.subscribe(params => {
		const id = params['id'];
		console.log(id);
			if (id) {
				this.isEdit = true;
				this.alertService.fnLoading(true);
				this.benefitService.getBenefit(id)
					.pipe(finalize(() => this.alertService.fnLoading(false)))
					.subscribe(res => {
						if (res) {
							this.benefit = res.data as Benefit;
							this.initBenefit();
						}
					});
			} else {
				this.benefit = new Benefit();
				this.benefit.clear();
				this.initBenefit();
			}
		});
		this.subscriptions.push(routeSubscription);
	}

 	ngOnDestroy() {
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	loadCategories(){
		this.alertService.fnLoading(true);
		const catSubscription = this.settingService.getSelect()
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.settings = res.data;
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.push(catSubscription);
	}

	initBenefit() {
		this.createForm();
	}

	createForm() {
		console.log(this.benefit);
		this.benefitForm = this.benefitFB.group({
			// userName: [this.user.userName, Validators.required],
			name: [this.benefit.name, Validators.required],
			description: [this.benefit.description],
			settingId: [this.benefit.settingId, Validators.required],
			isActive: [this.benefit.isActive],
			sequence: [this.benefit.sequence, [Validators.required, Validators.min(1)]]
		});
	}
	
	get dfControls() { return this.benefitForm.controls; }

	onSubmit() {

    const controls = this.benefitForm.controls;
		
		if (this.benefitForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		const editedBenefit = this.prepareBenefit();
		if (editedBenefit.id) {
			this.updateBenefit(editedBenefit);
		}
		else {
			this.createBenefit(editedBenefit);
		}
	}

	prepareBenefit(): SaveBenefit {
		const controls = this.benefitForm.controls;
		const _benefit = new SaveBenefit();
		_benefit.clear();
    	_benefit.id = this.benefit.id;
		_benefit.name = controls['name'].value;
		_benefit.settingId = controls['settingId'].value;
		_benefit.description = controls['description'].value;
		_benefit.isActive = controls['isActive'].value;
		_benefit.sequence = controls['sequence'].value;
		return _benefit;
	}

	createBenefit(_benefit: SaveBenefit) {
		this.alertService.fnLoading(true);
		const createSubscription = this.benefitService.create(_benefit)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`New benefit has been added successfully.`);
				this.goBack();
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.push(createSubscription);
	}

	updateBenefit(_benefit: SaveBenefit) {
		this.alertService.fnLoading(true);
		const updateSubscription = this.benefitService.update(_benefit)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`Benefit has been saved successfully.`);
				this.goBack();
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.push(updateSubscription);
	}

	getComponentTitle() {
		let result = 'Create Benefit';
		if (!this.benefit || !this.benefit.id) {
			return result;
		}

		result = `Edit benefit - ${this.benefit.name}`;
		return result;
	}
	
	goBack() {
		this.router.navigate([`/benefit`], { relativeTo: this.activatedRoute });
	}

	stringToInt(value): number {
		return Number.parseInt(value);
	}

    private throwError(errorDetails: any) {
        // this.alertService.fnLoading(false);
        console.log("error", errorDetails);
        let errList = errorDetails.error.errors;
        if (errList.length) {
            console.log("error", errList, errList[0].errorList[0]);
            // this.alertService.tosterDanger(errList[0].errorList[0]);
        } else {
            //this.alertService.tosterDanger(errorDetails.error.message);
        }
    }

}
