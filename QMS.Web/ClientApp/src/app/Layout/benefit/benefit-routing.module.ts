import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BenefitComponent } from './benefit.component';
import { BenefitListComponent } from './benefit-list/benefit-list.component';
import { BenefitFormComponent } from './benefit-form/benefit-form.component';


const routes: Routes = [
  {
    path: '',
    component: BenefitComponent,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: BenefitListComponent
      },
      {
        path: 'new',
        component: BenefitFormComponent,
        // canActivate: [AuthorizeGuard],
        // data: { title: 'New User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Create] },
      },
      {
        path: 'edit/:id',
        component: BenefitFormComponent,
        // canActivate: [AuthorizeGuard],
        // data: { title: 'Edit User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Edit] },
      },
    ],

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BenefitRoutingModule { }
