import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingsComponent } from './settings.component';
import { PointSettingsListComponent } from './point/point-settings-list/point-settings-list.component';
import { PointSettingsFormComponent } from './point/point-settings-form/point-settings-form.component';


const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      {
        path: 'point',
        redirectTo: 'point/list',
        pathMatch: 'full',
      },
      {
        path: 'point/list',
        component: PointSettingsListComponent
      },
      {
        path: 'point/new',
        component: PointSettingsFormComponent,
        // canActivate: [AuthorizeGuard],
        // data: { title: 'New User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Create] },
      },
      {
        path: 'point/edit/:id',
        component: PointSettingsFormComponent,
        // canActivate: [AuthorizeGuard],
        // data: { title: 'Edit User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Edit] },
      },
    ],

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
