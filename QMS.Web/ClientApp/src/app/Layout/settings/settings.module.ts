import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { SharedMasterModule } from 'src/app/Shared/Modules/shared-master/shared-master.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SettingsComponent } from './settings.component';
import { PointSettingsListComponent } from './point/point-settings-list/point-settings-list.component';
import { PointSettingsFormComponent } from './point/point-settings-form/point-settings-form.component';


@NgModule({
  declarations: [SettingsComponent,PointSettingsListComponent, PointSettingsFormComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    SharedMasterModule,
    AngularFontAwesomeModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SettingsModule { }
