import { Component, OnInit } from '@angular/core';
import { SettingQuery, Setting } from 'src/app/Shared/Entity/Settings/setting';
import { Subscription, of } from 'rxjs';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { PointSettingsService } from 'src/app/Shared/Services/Settings/point-setting.service';
import { CommonService } from 'src/app/Shared/Services/Common/common.service';
import { take, delay, finalize } from 'rxjs/operators';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';

@Component({
  selector: 'app-settings-list',
  templateUrl: './point-settings-list.component.html',
  styleUrls: ['./point-settings-list.component.css']
})
export class PointSettingsListComponent implements OnInit {

  query: SettingQuery;
	PAGE_SIZE: number;
	settings: Setting[];

	// Subscriptions
	private subscriptions: Subscription[] = [];

	constructor(
		private router: Router,
    private alertService: AlertService,
    private settingService: PointSettingsService,
		private commonService: CommonService) {
		this.PAGE_SIZE = commonService.PAGE_SIZE;
	}

	ngOnInit() {
		of(undefined).pipe(take(1), delay(1000)).subscribe(() => {
			this.loadSettingsPage();
		});
	}

	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}
	
    loadSettingsPage() {
		this.searchConfiguration();
        this.alertService.fnLoading(true);
		const settingsSubscription = this.settingService.getSettings(this.query)
			.pipe(finalize(() => { this.alertService.fnLoading(false); }))
			.subscribe(
            (res) => {
				this.settings = res.data.items;
            },
            (error) => {
                console.log(error);
            });
			this.subscriptions.push(settingsSubscription);
	}
	
	searchConfiguration() {
		this.query = new SettingQuery({
			page: 1,
			pageSize: this.PAGE_SIZE,
			sortBy: 'category',
			isSortAscending: true,
			category: ''
		});
	}
	
	editSettings(id) {
		this.router.navigate(['/settings/point/edit', id]);
	}
	
	newSettings() {
		this.router.navigate(['/settings/point/new']);
	}
	
	deleteSetting(id) {
		this.alertService.confirm("Are you sure want to delete this point setting?", 
			() => {
				this.alertService.fnLoading(true);
				const deleteSubscription = this.settingService.delete(id)
					.pipe(finalize(() => { this.alertService.fnLoading(false); }))
					.subscribe((res: any) => {
						console.log('res from del func',res);          
						this.alertService.tosterSuccess("Point setting has been deleted successfully.");
						this.loadSettingsPage();
					},
					(error) => {
						console.log(error);
					});
					this.subscriptions.push(deleteSubscription);
				}, 
			() => {
			});
	}

    public ptableSettings: IPTableSetting = {
        tableID: "settings-table",
        tableClass: "table table-border ",
        tableName: 'Point Setting List',
        tableRowIDInternalName: "id",
        tableColDef: [
            { headerName: 'Category', width: '40%', internalName: 'category', sort: true, type: "" },
            { headerName: 'Points', width: '50%', internalName: 'points', sort: true, type: "" },
			{ headerName: 'Active', width: '10%', internalName: 'isActiveText', sort: false, type: "" },
        ],
        enabledSearch: true,
        enabledSerialNo: true,
        pageSize: this.PAGE_SIZE,
        enabledPagination: true,
        enabledDeleteBtn: true,
        enabledEditBtn: true,
        enabledColumnFilter: true,
        enabledRadioBtn: false,
		enabledRecordCreateBtn: true,
		newRecordButtonText: 'New Point Setting'
    };

    public fnCustomTrigger(event) {
        console.log("custom  click: ", event);

        if (event.action == "new-record") {
            this.newSettings();
        }
        else if (event.action == "edit-item") {
            this.editSettings(event.record.id);
        }
        else if (event.action == "delete-item") {
            this.deleteSetting(event.record.id);
        }
    }

}
