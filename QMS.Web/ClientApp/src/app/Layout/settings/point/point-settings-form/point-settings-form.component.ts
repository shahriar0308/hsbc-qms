import { Component, OnInit } from '@angular/core';
import { Setting, SaveSetting } from 'src/app/Shared/Entity/Settings/setting';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { finalize } from 'rxjs/operators';
import { PointSettingsService } from 'src/app/Shared/Services/Settings/point-setting.service';

@Component({
  selector: 'app-settings-form',
  templateUrl: './point-settings-form.component.html',
  styleUrls: ['./point-settings-form.component.css']
})
export class PointSettingsFormComponent implements OnInit {

  
	setting: Setting;
  settingForm: FormGroup;
  
	private subscriptions: Subscription[] = [];


	constructor(private activatedRoute: ActivatedRoute,
		private router: Router,
    private settingFB: FormBuilder,
    private alertService: AlertService,
		private settingService: PointSettingsService) { }

	ngOnInit() {
		
		// this.alertService.fnLoading(true);
		const routeSubscription = this.activatedRoute.params.subscribe(params => {
      const id = params['id'];
      console.log(id);
			if (id) {

				this.alertService.fnLoading(true);
				this.settingService.getSetting(id)
					.pipe(finalize(() => this.alertService.fnLoading(false)))
					.subscribe(res => {
						if (res) {
							this.setting = res.data as Setting;
							console.log(this.setting);
							this.initSetting();
						}
					});
			} else {
				this.setting = new Setting();
				this.setting.clear();
				this.initSetting();
			}
		});
		this.subscriptions.push(routeSubscription);
	}

 	ngOnDestroy() {
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	initSetting() {
		this.createForm();
	}

	createForm() {
		this.settingForm = this.settingFB.group({
			category: [this.setting.category, Validators.required],
			points: [this.setting.points, Validators.required],
			isActive: [this.setting.isActive]
		});
	} 
	
	get sfControls() { return this.settingForm.controls; }

	onSubmit() {

    const controls = this.settingForm.controls;
		
		if (this.settingForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		const editedSetting = this.prepareSetting();
		if (editedSetting.id) {
			this.updateSetting(editedSetting);
		}
		else {
			this.createSetting(editedSetting);
		}
	}

	prepareSetting(): SaveSetting {
		const controls = this.settingForm.controls;
		const _setting = new SaveSetting();
		_setting.clear();
    _setting.id = this.setting.id;
		_setting.category = controls['category'].value;
		_setting.points = controls['points'].value;
		_setting.isActive = controls['isActive'].value;
		return _setting;
	}

	createSetting(_setting: SaveSetting) {
		this.alertService.fnLoading(true);
		const createSubscription = this.settingService.create(_setting)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`New Point settings has been added successfully.`);
				this.goBack();
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.push(createSubscription);
	}

	updateSetting(_setting: SaveSetting) {
		this.alertService.fnLoading(true);
		const updateSubscription = this.settingService.update(_setting)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`Point settings has been saved successfully.`);
				this.goBack();
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.push(updateSubscription);
	}

	getComponentTitle() {
		let result = 'Create Point setting';
		if (!this.setting || !this.setting.id) {
			return result;
		}

		result = `Edit point setting for ${this.setting.category} category`;
		return result;
	}
	
	goBack() {
		this.router.navigate([`/settings/point`], { relativeTo: this.activatedRoute });
	}

	stringToInt(value): number {
		return Number.parseInt(value);
	}


    private throwError(errorDetails: any) {
        // this.alertService.fnLoading(false);
        console.log("error", errorDetails);
        let errList = errorDetails.error.errors;
        if (errList.length) {
            console.log("error", errList, errList[0].errorList[0]);
            this.alertService.tosterDanger(errList[0].errorList[0]);
        } else {
            this.alertService.tosterDanger(errorDetails.error.message);
        }
    }



}
