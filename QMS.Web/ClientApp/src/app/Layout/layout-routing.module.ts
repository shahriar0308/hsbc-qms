import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BaseLayoutComponent } from './LayoutComponent/base-layout/base-layout.component';
import { AuthGuard } from '../Shared/Guards/auth.guard';

const routes: Routes = [
    {
        path: '',
        component: BaseLayoutComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'dashboard' },
            { path: 'demo', loadChildren: () => import('./DemoPages/demo.module').then(m => m.DemoModule) },
            { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule) },
            { path: 'users', loadChildren: () => import('./user/user.module').then(m => m.UserModule) },
            { path: 'roles', loadChildren: () => import('./user-role/user-role.module').then(m => m.UserRoleModule) },
            { path: 'dealer', loadChildren: () => import('./Dealer/dealer.module').then(m => m.DealerModule) },
            { path: 'settings', loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule) },
            { path: 'faq', loadChildren: () => import('./faq/faq.module').then(m => m.FaqModule) },
            { path: 'complain', loadChildren: () => import('./complain/complain.module').then(m => m.ComplainModule) },
            { path: 'division', loadChildren: () => import('./area/division/division.module').then(m => m.DivisionModule) },
            { path: 'district', loadChildren: () => import('./area/district/district.module').then(m => m.DistrictModule) },
            { path: 'thana', loadChildren: () => import('./area/thana/thana.module').then(m => m.ThanaModule) },
            { path: 'color-code', loadChildren: () => import('./color-code/color-code.module').then(m => m.ColorCodeModule) },
            { path: 'product', loadChildren: () => import('./product/product.module').then(m => m.ProductModule) },
            { path: 'benefit', loadChildren: () => import('./benefit/benefit.module').then(m => m.BenefitModule) },
            { path: 'type-of-requirement', loadChildren: () => import('./type-of-requirement/type-of-requirement.module').then(m => m.TypeOfRequirementModule) },
            { path: 'terms-conditions', loadChildren: () => import('./terms-conditions/terms-conditions.module').then(m => m.TermsConditionsModule) },
            { path: 'promotional-banner', loadChildren: () => import('./promotional-banner/promotional-banner.module').then(m => m.PromotionalBannerModule) },
            { path: 'loyalty-program-point', loadChildren: () => import('./loyalty-program-point/loyalty-program-point.module').then(m => m.LoyaltyProgramPointModule) },
            { path: 'painting-service-price', loadChildren: () => import('./paint-service-price/paint-service-price.module').then(m => m.PaintServicePriceModule) },
            { path: 'service-request', loadChildren: () => import('./service-request/service-request.module').then(m => m.ServiceRequestModule) }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
