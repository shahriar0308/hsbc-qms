import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { ColorCodeService } from 'src/app/Shared/Services/ColorCodes/color-codes.service';

@Component({
  selector: 'app-modal-excel-import-color-code',
  templateUrl: './modal-excel-import-color-code.component.html',
  styleUrls: ['./modal-excel-import-color-code.component.css']
})
export class ModalExcelImportColorCodeComponent implements OnInit {

  isFormValid: boolean = false;
  selectedFile: File = null;
	selectedFileName: string;
  tosterMsgError: string = "Something went wrong";
  fileError: string = '';

  constructor(public activeModal: NgbActiveModal,
    private userService: ColorCodeService, private alertService: AlertService) { }

  ngOnInit() {
  }

	onChangeInputFile(event: any) {
		if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      this.selectedFile = file;
			if (!this.isValidFile(file)) return;
      this.isFormValid = true;
      this.selectedFileName = file.name;
    } else {
      this.clearFile();
		}
	}

	isValidFile(file) {
		const fileExt = file.name.split('.').pop().toLowerCase();
		if (!(['xlsx'].indexOf(fileExt) > -1)) {
			this.fileError = 'Invalid file type';
      this.clearFile();
			return false;
		}
		this.fileError = '';
		return true;
	}
  
  clearFile() { 
    this.selectedFile = null;
    this.isFormValid = false;
    this.selectedFileName = '';
  }

  submit() {
    if(!this.selectedFile) {
      console.log('File is not selected');
      return;
    }

    const model = {'excelFile': this.selectedFile};
    console.log(model);
    this.userService.excelImport(model).subscribe(
      (res: any) => {
        console.log(res.data);
        this.alertService.tosterSuccess(`Color code has been imported successfully,<br/> ${res.message}`);
        this.activeModal.close(`Color code has been imported successfully, ${res.message}`);
      },
      (err) => {
        console.log(err);
        this.showError();
      }
    );
  }

  showError(msg: string = null) {
    this.activeModal.close(msg ? msg : this.tosterMsgError);
  }

}
