import { Component, OnInit, OnDestroy } from '@angular/core';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';
import { finalize, take, delay } from 'rxjs/operators';
import { ColorCodeQuery, ColorCode } from 'src/app/Shared/Entity/ColorCode/color-code';
import { Subscription, of } from 'rxjs';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { CommonService } from 'src/app/Shared/Services/Common/common.service';
import { ColorCodeService } from 'src/app/Shared/Services/ColorCodes/color-codes.service';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ModalExcelImportColorCodeComponent } from '../modal-excel-import-color-code/modal-excel-import-color-code.component';

@Component({
	selector: 'app-color-code-list',
	templateUrl: './color-code-list.component.html',
	styleUrls: ['./color-code-list.component.css']
})
export class ColorCodeListComponent implements OnInit, OnDestroy {

	query: ColorCodeQuery;
	PAGE_SIZE: number;
	colorCodes: ColorCode[];

	// Subscriptions
	private subscriptions: Subscription[] = [];

	constructor(
		private router: Router,
		private alertService: AlertService,
		private colorCodeService: ColorCodeService,
		private modalService: NgbModal,
		private commonService: CommonService) {
		this.PAGE_SIZE = 5000;//commonService.PAGE_SIZE;
	}

	ngOnInit() {
		of(undefined).pipe(take(1), delay(1000)).subscribe(() => {
			this.loadColorCodesPage();
		});
	}

	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}

	loadColorCodesPage() {
		this.searchConfiguration();
		this.alertService.fnLoading(true);
		const colorCodesSubscription = this.colorCodeService.getColorCodes(this.query)
			.pipe(finalize(() => { this.alertService.fnLoading(false); }))
			.subscribe(
				(res) => {
					console.log("res.data", res.data);
					this.colorCodes = res.data.items;
				},
				(error) => {
					console.log(error);
				});
		this.subscriptions.push(colorCodesSubscription);
	}

	searchConfiguration() {
		this.query = new ColorCodeQuery({
			page: 1,
			pageSize: this.PAGE_SIZE,
			sortBy: 'shadeSequence',
			isSortAscending: true,
			shadeSequence: '',
			shadeCode: '',
			r: '',
			g: '',
			b: '',
		});
	}

	toggleActiveInactive(id) {
		const actInSubscription = this.colorCodeService.activeInactive(id).subscribe(res => {
			this.loadColorCodesPage();
		});
		this.subscriptions.push(actInSubscription);
	}

	editColorCode(id) {
		this.router.navigate(['/color-code/edit', id]);
	}

	newColorCode() {
		this.router.navigate(['/color-code/new']);
	}

	deleteColorCode(id) {
		this.alertService.confirm("Are you sure want to delete this Color Code?",
			() => {
				this.alertService.fnLoading(true);
				const deleteSubscription = this.colorCodeService.delete(id)
					.pipe(finalize(() => { this.alertService.fnLoading(false); }))
					.subscribe((res: any) => {
						console.log('res from del func', res);
						this.alertService.tosterSuccess("Color Code has been deleted successfully.");
						this.loadColorCodesPage();
					},
						(error) => {
							console.log(error);
						});
				this.subscriptions.push(deleteSubscription);
			},
			() => {
			});
	}

	public ptableSettings: IPTableSetting = {
		tableID: "colorCodes-table",
		tableClass: "table table-border ",
		tableName: '',
		tableRowIDInternalName: "id",
		tableColDef: [
			{ headerName: 'Shade Sequence', width: '20%', internalName: 'shadeSequence', sort: true, type: "" },
			{ headerName: 'Shade Code', width: '35%', internalName: 'shadeCode', sort: true, type: "" },
			{ headerName: 'R', width: '15%', internalName: 'r', sort: false, type: "" },
			{ headerName: 'G', width: '15%', internalName: 'g', sort: false, type: "" },
			{ headerName: 'B', width: '15%', internalName: 'b', sort: false, type: "" }
		],
		enabledSearch: true,
		enabledSerialNo: true,
		pageSize: 10,
		enabledPagination: true,
		enabledDeleteBtn: true,
		enabledEditBtn: true,
		enabledColumnFilter: true,
		enabledRadioBtn: false,
		// enabledRecordCreateBtn: true,
		newRecordButtonText: 'New Color Code'
	};

	public fnCustomTrigger(event) {
		console.log("custom  click: ", event);

		if (event.action == "new-record") {
			this.newColorCode();
		}
		else if (event.action == "edit-item") {
			this.editColorCode(event.record.id);
		}
		else if (event.action == "delete-item") {
			this.deleteColorCode(event.record.id);
		}
	}
	
	openExcelImportModal() {
        let ngbModalOptions: NgbModalOptions = {
          backdrop: 'static',
          keyboard: false
        };
        const modalRef = this.modalService.open(ModalExcelImportColorCodeComponent, ngbModalOptions);
    
        modalRef.result.then((result) => {
          console.log(result);
          this.router.navigate(['/color-code/list']);
        },
        (reason) => {
            console.log(reason);
        });
    }
}
