import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ColorCodeComponent } from './color-code.component';
import { ColorCodeListComponent } from './color-code-list/color-code-list.component';
import { ColorCodeFormComponent } from './color-code-form/color-code-form.component';


const routes: Routes = [
  {
    path: '',
    component: ColorCodeComponent,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: ColorCodeListComponent
      },
      {
        path: 'new',
        component: ColorCodeFormComponent,
        // canActivate: [AuthorizeGuard],
        // data: { title: 'New User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Create] },
      },
      {
        path: 'edit/:id',
        component: ColorCodeFormComponent,
        // canActivate: [AuthorizeGuard],
        // data: { title: 'Edit User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Edit] },
      },
    ],

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ColorCodeRoutingModule { }
