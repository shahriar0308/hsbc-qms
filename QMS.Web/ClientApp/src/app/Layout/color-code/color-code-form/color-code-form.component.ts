import { Component, OnInit, OnDestroy } from '@angular/core';
import { ColorCode, SaveColorCode } from 'src/app/Shared/Entity/ColorCode/color-code';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ColorCodeService } from 'src/app/Shared/Services/ColorCodes/color-codes.service';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { Guid } from 'guid-typescript';
import { finalize } from 'rxjs/operators';

@Component({
	selector: 'app-color-code-form',
	templateUrl: './color-code-form.component.html',
	styleUrls: ['./color-code-form.component.css']
})
export class ColorCodeFormComponent implements OnInit, OnDestroy {

	colorCode: ColorCode;
	colorCodeForm: FormGroup;

	private subscriptions: Subscription[] = [];


	constructor(private activatedRoute: ActivatedRoute,
		private router: Router,
		private colorCodeFB: FormBuilder,
		private alertService: AlertService,
		private colorCodeService: ColorCodeService) { }

	ngOnInit() {

		// this.alertService.fnLoading(true);
		const routeSubscription = this.activatedRoute.params.subscribe(params => {
			const id = params['id'];
			console.log(id);
			if (id) {

				this.alertService.fnLoading(true);
				this.colorCodeService.getColorCode(id)
					.pipe(finalize(() => this.alertService.fnLoading(false)))
					.subscribe(res => {
						if (res) {
							this.colorCode = res.data as ColorCode;
							this.initColorCodes();
						}
					});
			} else {
				this.colorCode = new ColorCode();
				this.colorCode.clear();
				this.initColorCodes();
			}
		});
		this.subscriptions.push(routeSubscription);
	}

	ngOnDestroy() {
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	initColorCodes() {
		this.createForm();
	}

	createForm() {
		this.colorCodeForm = this.colorCodeFB.group({
			shadeSequence: [this.colorCode.shadeSequence, [Validators.required, Validators.pattern(/^(?!\s+$).+/)]],
			shadeCode: [this.colorCode.shadeCode, [Validators.required, Validators.pattern(/^(?!\s+$).+/)]],
			r: [this.colorCode.r, [Validators.required, Validators.pattern(/^[0-9]+([.][0-9]+)*$/)]],
			g: [this.colorCode.g, [Validators.required, Validators.pattern(/^[0-9]+([.][0-9]+)*$/)]],
			b: [this.colorCode.b, [Validators.required, Validators.pattern(/^[0-9]+([.][0-9]+)*$/)]]
		});
	}

	get formControls() { return this.colorCodeForm.controls; }

	onSubmit() {

		const controls = this.colorCodeForm.controls;

		if (this.colorCodeForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		const editedColorCodes = this.prepareColorCodes();
		if (editedColorCodes.id) {
			this.updateColorCodes(editedColorCodes);
		}
		else {
			this.createColorCodes(editedColorCodes);
		}
	}

	prepareColorCodes(): SaveColorCode {
		const controls = this.colorCodeForm.controls;

		const _colorCode = new SaveColorCode();
		_colorCode.clear();
		_colorCode.id = this.colorCode.id;
		_colorCode.shadeSequence = controls['shadeSequence'].value;
		_colorCode.shadeCode = controls['shadeCode'].value;
		_colorCode.r = controls['r'].value;
		_colorCode.g = controls['g'].value;
		_colorCode.b = controls['b'].value;
		
		return _colorCode;
	}

	createColorCodes(_colorCode: SaveColorCode) {
		this.alertService.fnLoading(true);
		const createSubscription = this.colorCodeService.create(_colorCode)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`New color code has been added successfully.`);
				this.goBack();
			},
				error => {
					this.throwError(error);
				});
		this.subscriptions.push(createSubscription);
	}

	updateColorCodes(_colorCode: SaveColorCode) {
		this.alertService.fnLoading(true);
		const updateSubscription = this.colorCodeService.update(_colorCode)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`Color code has been saved successfully.`);
				this.goBack();
			},
				error => {
					this.throwError(error);
				});
		this.subscriptions.push(updateSubscription);
	}

	getComponentTitle() {
		let result = 'Create Color Code';
		if (!this.colorCode || !this.colorCode.id) {
			return result;
		}

		result = `Edit Color Code - ${this.colorCode.shadeSequence}`;
		return result;
	}

	goBack() {
		this.router.navigate([`/color-code`], { relativeTo: this.activatedRoute });
	}

	stringToInt(value): number {
		return Number.parseInt(value);
	}

	// 	onAlertClose($event) {
	// 		this.resetErrors();
	// 	}

	private throwError(errorDetails: any) {
		// this.alertService.fnLoading(false);
		console.log("error", errorDetails);
		let errList = errorDetails.error.errors;
		if (errList.length) {
			console.log("error", errList, errList[0].errorList[0]);
			// this.alertService.tosterDanger(errList[0].errorList[0]);
		} else {
			// this.alertService.tosterDanger(errorDetails.error.message);
		}
	}


}
