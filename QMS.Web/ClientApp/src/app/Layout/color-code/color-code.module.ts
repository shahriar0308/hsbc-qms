import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ColorCodeRoutingModule } from './color-code-routing.module';
import { ColorCodeListComponent } from './color-code-list/color-code-list.component';
import { ColorCodeFormComponent } from './color-code-form/color-code-form.component';
import { ColorCodeComponent } from './color-code.component';
import { SharedMasterModule } from 'src/app/Shared/Modules/shared-master/shared-master.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalExcelImportColorCodeComponent } from './modal-excel-import-color-code/modal-excel-import-color-code.component';


@NgModule({
  declarations: [
    ColorCodeListComponent,
    ColorCodeFormComponent,
    ColorCodeComponent,
    ModalExcelImportColorCodeComponent
  ],
  imports: [
    CommonModule,
    ColorCodeRoutingModule,
    SharedMasterModule,
    AngularFontAwesomeModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    ModalExcelImportColorCodeComponent
  ]
})
export class ColorCodeModule { }
