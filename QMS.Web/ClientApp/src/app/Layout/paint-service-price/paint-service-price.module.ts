import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaintServicePriceRoutingModule } from './paint-service-price-routing.module';
import { PaintServicePriceComponent } from './paint-service-price.component';
import { PaintingServicePriceFormComponent } from './painting-service-price-form/painting-service-price-form.component';
import { PaintingServicePriceListComponent } from './painting-service-price-list/painting-service-price-list.component';
import { SharedMasterModule } from 'src/app/Shared/Modules/shared-master/shared-master.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { PaintingServicePriceListBackupComponent } from './painting-service-price-list-backup/painting-service-price-list-backup.component';


@NgModule({
  declarations: [PaintServicePriceComponent, PaintingServicePriceFormComponent, PaintingServicePriceListComponent, PaintingServicePriceListBackupComponent],
  imports: [
    CommonModule,
    PaintServicePriceRoutingModule,
    SharedMasterModule,
    AngularFontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule
  ]
})
export class PaintServicePriceModule { }
