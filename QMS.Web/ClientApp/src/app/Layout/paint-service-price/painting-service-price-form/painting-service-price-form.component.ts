import { Component, OnInit, OnDestroy } from '@angular/core';
import { PaintingServiceCost, SavePaintingServiceCost } from 'src/app/Shared/Entity/PaintingServiceCost/painting-service-cost';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { KeyValuePair } from 'src/app/Shared/Entity/Common/key-value-pair';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { PaintService } from 'src/app/Shared/Services/Paint/paint.service';
import { ProductService } from 'src/app/Shared/Services/Product/product.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-painting-service-price-form',
  templateUrl: './painting-service-price-form.component.html',
  styleUrls: ['./painting-service-price-form.component.css']
})
export class PaintingServicePriceFormComponent implements OnInit, OnDestroy {

  paintingServiceCost: PaintingServiceCost;
  paintingServiceForm: FormGroup;
  isEdit: boolean;
  surfaceTypes: KeyValuePair[];
  surfaceConditions: KeyValuePair[];
  packages: KeyValuePair[];
  products: KeyValuePair[];
  private subscriptions: Subscription = new Subscription();

  constructor(private activatedRoute: ActivatedRoute,
		private router: Router,
    private paintingServiceFB: FormBuilder,
    private alertService: AlertService,
    private paintService: PaintService,
    private productService: ProductService) { }

  ngOnInit() {
    this.loadSurfaceTypes();
    this.loadSurfaceConditions();
    this.loadPackages();
    const id = this.activatedRoute.snapshot.params['id'] || 0;
    if (id) {
      this.isEdit = true;
      this.alertService.fnLoading(true);
      this.paintService.getServiceCost(id)
        .pipe(finalize(() => this.alertService.fnLoading(false)))
        .subscribe(res => {
          if (res) {
			this.paintingServiceCost = res.data as PaintingServiceCost;
			this.loadProducts(this.paintingServiceCost.surfaceType);
            this.initPaintingServiceCost();
          }
        });
    } else {
      this.paintingServiceCost = new PaintingServiceCost();
      this.paintingServiceCost.clear();
      this.initPaintingServiceCost();
    }
  }

  ngOnDestroy() {
		this.subscriptions.unsubscribe();
	}

  loadSurfaceTypes() {
		this.alertService.fnLoading(true);
		const surfaceTypeSubscription = this.productService.getSurfaceTypes()
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.surfaceTypes = res.data;
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.add(surfaceTypeSubscription);
  }

  loadSurfaceConditions() {
		this.alertService.fnLoading(true);
		const surfaceConditionSubscription = this.productService.getSurfaceConditions()
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.surfaceConditions = res.data;
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.add(surfaceConditionSubscription);
  }

  loadPackages() {
		this.alertService.fnLoading(true);
		const packageSubscription = this.productService.getPackages()
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.packages = res.data;
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.add(packageSubscription);
  }

  changeProduct(){
	let controls = this.paintingServiceForm.controls;
    let surfaceType = controls['surfaceType'].value;
	controls['productId'].setValue(null);
	this.loadProducts(surfaceType);
    
  }

  loadProducts(surfaceType){
    this.alertService.fnLoading(true);
		const productSubscription = this.productService.getBySurfaceType(surfaceType)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
        console.log(res);
				this.products = res.data;
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.add(productSubscription);
  }

  initPaintingServiceCost() {
		this.createForm();
	}
  
  createForm() {
		this.paintingServiceForm = this.paintingServiceFB.group({
			// userName: [this.user.userName, Validators.required],
			surfaceType: [this.paintingServiceCost.surfaceType, Validators.required],
			surfaceCondition: [this.paintingServiceCost.surfaceCondition, Validators.required],
			// package: [this.paintingServiceCost.package, Validators.required],
			productId: [this.paintingServiceCost.productId, Validators.required],
			ratePerSft: [this.paintingServiceCost.platinumRatePerSft, Validators.required],
			sevenToNine: [this.paintingServiceCost.sevenToNine, Validators.required],
			aboveNine: [this.paintingServiceCost.aboveNine, Validators.required],
			isActive: [this.paintingServiceCost.isActive]
		});
  }
  
	get dfControls() { return this.paintingServiceForm.controls; }

	onSubmit() {

    const controls = this.paintingServiceForm.controls;
		
		if (this.paintingServiceForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		const editedPaintingServiceCost = this.preparePaintingServiceCost();
		if (editedPaintingServiceCost.id) {
			this.updatePaintingServiceCost(editedPaintingServiceCost);
		}
		else {
			this.createPaintingServiceCost(editedPaintingServiceCost);
		}
	}

	preparePaintingServiceCost(): SavePaintingServiceCost {
		const controls = this.paintingServiceForm.controls;
		const _paintingServiceCost = new SavePaintingServiceCost();
    _paintingServiceCost.clear();
    _paintingServiceCost.id = this.paintingServiceCost.id;
    _paintingServiceCost.surfaceType = controls['surfaceType'].value;;
    _paintingServiceCost.surfaceCondition = controls['surfaceCondition'].value;;
    _paintingServiceCost.package = controls['package'].value;;
    _paintingServiceCost.productId = controls['productId'].value;;
    _paintingServiceCost.platinumRatePerSft = controls['ratePerSft'].value;;
    _paintingServiceCost.sevenToNine = controls['sevenToNine'].value;;
    _paintingServiceCost.aboveNine = controls['aboveNine'].value;;
    _paintingServiceCost.isActive = controls['isActive'].value;;
    
		return _paintingServiceCost;
	}

	createPaintingServiceCost(_paintingServiceCost: SavePaintingServiceCost) {
    this.alertService.fnLoading(true);
    console.log(_paintingServiceCost);
		const createSubscription = this.paintService.create(_paintingServiceCost)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`New Painting Service Price has been added successfully.`);
				this.goBack();
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.add(createSubscription);
	}

	updatePaintingServiceCost(_paintingServiceCost: SavePaintingServiceCost) {
		this.alertService.fnLoading(true);
		const updateSubscription = this.paintService.update(_paintingServiceCost)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`Painting Service Cost has been saved successfully.`);
				this.goBack();
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.add(updateSubscription);
	}

	getComponentTitle() {
		let result = 'Create Express Painting Service';
		if (!this.paintingServiceCost || !this.paintingServiceCost.id) {
			return result;
		}

    result = `Edit Express Painting Service - 
    ${this.paintingServiceCost.surfaceTypeText} | ${this.paintingServiceCost.surfaceConditionText} |
    ${this.paintingServiceCost.productName}`;
		return result;
	}
	
	goBack() {
		this.router.navigate([`/painting-service-price`], { relativeTo: this.activatedRoute });
	}

  private throwError(errorDetails: any) {
    let errList = errorDetails.error.errors;
    if (errList.length) {
        console.log("error", errList, errList[0].errorList[0]);
    } else { }
  }

}
