import { Component, OnInit } from '@angular/core';
import { PaintingServiceCost, SavePaintingServiceCost } from 'src/app/Shared/Entity/PaintingServiceCost/painting-service-cost';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { PaintService } from 'src/app/Shared/Services/Paint/paint.service';
import { CommonService } from 'src/app/Shared/Services/Common/common.service';
import { finalize } from 'rxjs/operators';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';
import { ProductService } from 'src/app/Shared/Services/Product/product.service';
import { KeyValuePair } from 'src/app/Shared/Entity/Common/key-value-pair';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-painting-service-price-list',
  templateUrl: './painting-service-price-list.component.html',
  styleUrls: ['./painting-service-price-list.component.css']
})
export class PaintingServicePriceListComponent implements OnInit {

  paintingServiceCost: PaintingServiceCost;
	paintingServiceCosts: PaintingServiceCost[]= [];
  private subscriptions: Subscription = new Subscription();
  surfaceTypes: KeyValuePair[];
  surfaceConditions: KeyValuePair[];
  paintingServiceForm: FormGroup;
  showGrid: boolean;
  showBottomRow: boolean;
  showAdditionalColums: boolean;
  showSurfaceCondition: boolean;
	constructor(
    private alertService: AlertService,
    private paintService: PaintService,
    private paintingServiceFB: FormBuilder,
    private productService: ProductService) { }

    ngOnInit() {
      this.loadSurfaceTypes();
      this.loadSurfaceConditions();
      // var painting = new PaintingServiceCost();
      // painting.clear();
      // this.paintingServiceCosts.push(painting);

      this.paintingServiceCost = new PaintingServiceCost();
      this.paintingServiceCost.clear();
      this.initPaintingServiceCost();
    }

    ngOnDestroy() {
      this.subscriptions.unsubscribe();
    }

    loadSurfaceTypes() {
      this.alertService.fnLoading(true);
      const surfaceTypeSubscription = this.productService.getSurfaceTypes()
        .pipe(finalize(() => this.alertService.fnLoading(false)))
        .subscribe(res => {
          this.surfaceTypes = res.data;
        },
        error => {
          this.throwError(error);
        });
      this.subscriptions.add(surfaceTypeSubscription);
    }
  
    loadSurfaceConditions() {
      this.alertService.fnLoading(true);
      const surfaceConditionSubscription = this.productService.getSurfaceConditions()
        .pipe(finalize(() => this.alertService.fnLoading(false)))
        .subscribe(res => {
          this.surfaceConditions = res.data;
        },
        error => {
          this.throwError(error);
        });
      this.subscriptions.add(surfaceConditionSubscription);
    }

    loadProducts(){
      let controls = this.paintingServiceForm.controls;
      let surfaceType = controls['surfaceType'].value;
      let surfaceCondition = controls['surfaceCondition'].value;
      this.showSurfaceCondition = true;
      if(surfaceType == 3) {this.showSurfaceCondition = false; surfaceCondition = 0;}
      this.showBottomRow = false;
      this.showGrid = false;
      if(surfaceType && (surfaceCondition || !this.showSurfaceCondition )){
        this.showBottomRow = true;
        this.showAdditionalColums = false;
        if(surfaceType ==2 ){
          this.showAdditionalColums = true;
        }
        const paintSubscription = this.paintService.getForGrid(surfaceType, surfaceCondition)
        .pipe(finalize(() => this.alertService.fnLoading(false)))
        .subscribe(res => {
          this.paintingServiceCosts = res.data;
          this.showGrid = true;
          controls['aboveNine'].setValue(this.paintingServiceCosts[0].aboveNine);
          controls['sevenToNine'].setValue(this.paintingServiceCosts[0].sevenToNine);
          controls['vat'].setValue(this.paintingServiceCosts[0].vat);
          this.rControls.clear();
          this.paintingServiceCosts.forEach(element => {
            this.rControls.push(this.paintingServiceFB.group({
              id: [element.id],
              productId: [element.productId],
              productName: [element.productName],
              platinumRatePerSft: [element.platinumRatePerSft],
              goldRatePerSft: [element.goldRatePerSft],
              silverRatePerSft: [element.silverRatePerSft]
            }));
          });
        },
        error => {
          this.throwError(error);
        });
        this.subscriptions.add(paintSubscription);
      }
    }

    getComponentTitle() {
      let result = 'Price List for Express Painting Service';
      return result;
    }
    initPaintingServiceCost() {
      this.createForm();
    }

    createForm() {
      this.paintingServiceForm = this.paintingServiceFB.group({
        surfaceType: [this.paintingServiceCost.surfaceType, Validators.required],
        surfaceCondition: [this.paintingServiceCost.surfaceCondition],
        sevenToNine: [this.paintingServiceCost.sevenToNine, Validators.required],
        aboveNine: [this.paintingServiceCost.aboveNine, Validators.required],
        vat: [this.paintingServiceCost.vat, Validators.required],
        rates: this.paintingServiceFB.array([])
      });
    }

    onSubmit(){
      const controls = this.paintingServiceForm.controls;
      if (this.paintingServiceForm.invalid) {
        Object.keys(controls).forEach(controlName =>
          controls[controlName].markAsTouched()
        );
        return;
      }

      const paintingServiceCosts = this.preparePaintingServiceCost();
      console.log(paintingServiceCosts);
      this.createOrUpdate(paintingServiceCosts);
    }

    preparePaintingServiceCost(): SavePaintingServiceCost[]{
      var paintingServiceCosts : SavePaintingServiceCost[] = [];
      const controls = this.paintingServiceForm.controls;
      var rates = controls['rates'].value;
      console.log(rates);
      rates.forEach(element => {
        const _paintingServiceCost = new SavePaintingServiceCost();
        _paintingServiceCost.clear();
        _paintingServiceCost.id = element.id;
        _paintingServiceCost.surfaceType = controls['surfaceType'].value;
        _paintingServiceCost.surfaceCondition = _paintingServiceCost.surfaceType == 3 ? 0 : controls['surfaceCondition'].value;
        _paintingServiceCost.productId = element.productId;
        _paintingServiceCost.platinumRatePerSft = element.platinumRatePerSft;
        _paintingServiceCost.goldRatePerSft = element.goldRatePerSft;
        _paintingServiceCost.silverRatePerSft = element.silverRatePerSft;
        _paintingServiceCost.sevenToNine = controls['sevenToNine'].value;
        _paintingServiceCost.aboveNine = controls['aboveNine'].value;
        _paintingServiceCost.vat = controls['vat'].value;
        paintingServiceCosts.push(_paintingServiceCost);  
      });
      return paintingServiceCosts;
    }

    createOrUpdate(_paintingServiceCosts: SavePaintingServiceCost[]) {
      this.alertService.fnLoading(true);
      const createSubscription = this.paintService.createOrUpdate(_paintingServiceCosts)
        .pipe(finalize(() => this.alertService.fnLoading(false)))
        .subscribe(res => {
          this.alertService.tosterSuccess(`Express Painting Service Price List has been updated.`);
          this.loadProducts();
        },
        error => {
          this.throwError(error);
        });
      this.subscriptions.add(createSubscription);
    }

    // createTableRow() {
    //   return this.paintingServiceFB.group({
    //     productId: [this.paintingServiceCost.productId],
    //     productName: [this.paintingServiceCost.productName],
    //     platinumRatePerSft: [this.paintingServiceCost.platinumRatePerSft],
    //     goldRatePerSft: [this.paintingServiceCost.goldRatePerSft],
    //     silverRatePerSft: [this.paintingServiceCost.silverRatePerSft]
    //   });
    // }

    get dfControls() { return this.paintingServiceForm.controls; }
    get rControls() { return this.dfControls.rates as FormArray; }

    private throwError(errorDetails: any) {
      let errList = errorDetails.error.errors;
      if (errList.length) {
          console.log("error", errList, errList[0].errorList[0]);
      } else { }
    }
}
