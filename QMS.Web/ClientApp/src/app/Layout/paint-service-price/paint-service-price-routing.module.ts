import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaintServicePriceComponent } from './paint-service-price.component';
import { PaintingServicePriceListComponent } from './painting-service-price-list/painting-service-price-list.component';
import { PaintingServicePriceFormComponent } from './painting-service-price-form/painting-service-price-form.component';


const routes: Routes = [
  {
    path: '',
    component: PaintServicePriceComponent,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: PaintingServicePriceListComponent
      },
      {
        path: 'new',
        component: PaintingServicePriceFormComponent,
      },
      {
        path: 'edit/:id',
        component: PaintingServicePriceFormComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaintServicePriceRoutingModule { }
