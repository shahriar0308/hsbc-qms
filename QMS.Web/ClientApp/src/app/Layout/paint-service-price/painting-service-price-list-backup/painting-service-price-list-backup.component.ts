import { Component, OnInit } from '@angular/core';
import { PaintingServiceCost } from 'src/app/Shared/Entity/PaintingServiceCost/painting-service-cost';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { PaintService } from 'src/app/Shared/Services/Paint/paint.service';
import { CommonService } from 'src/app/Shared/Services/Common/common.service';
import { finalize } from 'rxjs/operators';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';

@Component({
  selector: 'app-painting-service-price-list-backup',
  templateUrl: './painting-service-price-list-backup.component.html',
  styleUrls: ['./painting-service-price-list-backup.component.css']
})
export class PaintingServicePriceListBackupComponent implements OnInit {

  
	paintingServiceCosts: PaintingServiceCost[];

	// Subscriptions
	private subscriptions: Subscription[] = [];

	constructor(
		private router: Router,
    private alertService: AlertService,
    private paintService: PaintService) { }

    ngOnInit() {
        this.loadPaintServiceCosts();
    }

    ngOnDestroy() {
      this.subscriptions.forEach(el => el.unsubscribe());
    }
    
  loadPaintServiceCosts() {
    this.alertService.fnLoading(true);
    const paintSubscription = this.paintService.getServiceCosts()
    .pipe(finalize(() => { this.alertService.fnLoading(false); }))
    .subscribe(
      (res) => {
        this.paintingServiceCosts = res.data.items;
      },
      (error) => {
        console.log(error);
      }
    );
    this.subscriptions.push(paintSubscription);
  }

  editPaintingService(id) {
    this.router.navigate(['/painting-service-price/edit', id]);
  }
  
  newPaintingService() {
    this.router.navigate(['/painting-service-price/new']);
  }


  deletePaintingService(id) {
		this.alertService.confirm("Are you sure want to delete this express painting service?", 
			() => {
				this.alertService.fnLoading(true);
				const deleteSubscription = this.paintService.delete(id)
					.pipe(finalize(() => { this.alertService.fnLoading(false); }))
					.subscribe((res: any) => {
						console.log('res from del func',res);          
						this.alertService.tosterSuccess("Product has been deleted successfully.");
						this.loadPaintServiceCosts();
					},
					(error) => {
						console.log(error);
					});
					this.subscriptions.push(deleteSubscription);
				}, 
			() => {
			});
	}

  public ptableSettings: IPTableSetting = {
    tableID: "paingting-service-price-table",
    tableClass: "table table-border ",
    tableName: 'Price List of Express Painting Service',
    tableRowIDInternalName: "id",
    tableColDef: [
      { headerName: 'Surface Type', width: '20%', internalName: 'surfaceTypeText', sort: true, type: "" },
      { headerName: 'Surface Condition', width: '20%', internalName: 'surfaceConditionText', sort: true, type: "" },
      { headerName: 'Package', width: '10%', internalName: 'packageText', sort: true, type: "" },
      { headerName: 'Product Name', width: '30%', internalName: 'productName', sort: true, type: "" },
      { headerName: 'Rate', width: '20%', internalName: 'ratePerSftText', sort: false, type: "" }
    ],
    enabledSearch: true,
    enabledSerialNo: true,
    pageSize: 10,
    enabledPagination: true,
    enabledDeleteBtn: true,
    newRecordButtonText: 'New Express Painting Service',
    enabledEditBtn: true,
    enabledColumnFilter: true,
    enabledRadioBtn: false,
    enabledRecordCreateBtn: true,
    enabledCellClick: true
	};

  public fnCustomTrigger(event) {
    if (event.action == "new-record") {
        this.newPaintingService();
    }
    else if (event.action == "edit-item") {
        this.editPaintingService(event.record.id);
    }
    else if (event.action == "delete-item") {
        this.deletePaintingService(event.record.id);
    }
  }

  

}
