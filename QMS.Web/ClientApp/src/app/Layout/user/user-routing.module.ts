import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserListAppComponent } from './user-list-app/user-list-app.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserDetailsAppComponent } from './user-details-app/user-details-app.component';


const routes: Routes = [{
  path: '',
  component: UserComponent,
  // canActivate: [AuthorizeGuard],
  children: [
    {
      path: '',
      redirectTo: 'users',
      pathMatch: 'full',
    },
    {
      path: 'users',
      component: UserListComponent,
      // canActivate: [AuthorizeGuard],
      // data: { title: 'Users', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.ListView] },
    },
    {
      path: 'app-users',
      component: UserListAppComponent,
      // canActivate: [AuthorizeGuard],
      // data: { title: 'Users', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.ListView] },
    },
    {
      path: 'new',
      component: UserFormComponent,
      // canActivate: [AuthorizeGuard],
      // data: { title: 'New User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Create] },
    },
    {
      path: 'edit/:id',
      component: UserFormComponent,
      // canActivate: [AuthorizeGuard],
      // data: { title: 'Edit User', permissions: [RolePermissions.SuperAdmin, RolePermissions.Users.Edit] },
    },
    {
      path: 'details/:id',
      component: UserDetailsComponent
    },
    {
      path: 'app-details/:id',
      component: UserDetailsAppComponent
    },
  ],
}];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class UserRoutingModule { }
