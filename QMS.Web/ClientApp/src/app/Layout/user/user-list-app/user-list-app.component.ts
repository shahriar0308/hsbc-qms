import { Component, OnInit, OnDestroy, ElementRef, ViewChild, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { merge, fromEvent, Subscription, of } from 'rxjs';
import { tap, debounceTime, distinctUntilChanged, take, delay, finalize } from 'rxjs/operators';
import { UserQuery, User } from 'src/app/Shared/Entity/Users/user';
import { UserService } from 'src/app/Shared/Services/Users/user.service';
import { CommonService } from 'src/app/Shared/Services/Common/common.service';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { MapObject } from 'src/app/Shared/Enums/map-object';
import { EnumUserRoleTypeDisplayName } from 'src/app/Shared/Enums/userRoleType';

@Component({
	selector: 'app-user-list-app',
	templateUrl: './user-list-app.component.html',
	styleUrls: ['./user-list-app.component.css']
})
export class UserListAppComponent implements OnInit, OnDestroy {

	query: UserQuery;
	PAGE_SIZE: number;
	users: User[];
	enumUserTypes: MapObject[] = EnumUserRoleTypeDisplayName.EnumUserRoleTypeDisplayName;

	// Subscriptions
	private subscriptions: Subscription[] = [];

	constructor(
		private router: Router,
		private userService: UserService,
		private alertService: AlertService,
		private commonService: CommonService) {
		this.PAGE_SIZE = commonService.PAGE_SIZE;
	}

	ngOnInit() {
		// First Load
		of(undefined).pipe(take(1), delay(1000)).subscribe(() => {
			this.loadUsersPage();
		});
	}

	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}

	loadUsersPage() {
		this.searchConfiguration();
		this.alertService.fnLoading(true);
		const usersSubscription = this.userService.getAppUsers(this.query)
			.pipe(finalize(() => { this.alertService.fnLoading(false); }))
			.subscribe(
				(res) => {
					this.users = res.data.items;
					this.users.forEach(x => {
						x.isUnderAmbassadorCampaignText = x.isUnderAmbassadorCampaign ? 'YES' : 'NO';
						x.userViewDetailBtn = "View User";

						let role = this.enumUserTypes.filter(a => a.id == x.userRoleName)[0];
						x.userRoleDisplayName = (role) ? role.label : "";
					});
				},
				(error) => {
					console.log(error);
				});
		this.subscriptions.push(usersSubscription);
	}

	searchConfiguration() {
		this.query = new UserQuery({
			page: 1,
			pageSize: this.PAGE_SIZE,
			sortBy: 'fullName',
			isSortAscending: true,
			fullName: '',
			userName: '',
			email: '',
		});
	}

	toggleActiveInactive(id) {
		const actInSubscription = this.userService.activeInactive(id).subscribe(res => {
			this.loadUsersPage();
		});
		this.subscriptions.push(actInSubscription);
	}

	viewUser(id) {
		console.log(`view ${id}`);
	}

	editUser(id) {
		this.router.navigate(['/users/edit', id]);
	}

	newUser() {
		this.router.navigate(['/users/new']);
	}

	deleteUser(id) {
		console.log(`delete ${id}`);
		this.alertService.confirm("Are you sure want to delete this user?",
			() => {
				this.alertService.fnLoading(true);
				const deleteSubscription = this.userService.delete(id)
					.pipe(finalize(() => { this.alertService.fnLoading(false); }))
					.subscribe((res: any) => {
						console.log('res from del func', res);
						this.alertService.tosterSuccess("User has been deleted successfully.");
						this.loadUsersPage();
					},
						(error) => {
							console.log(error);
						});
				this.subscriptions.push(deleteSubscription);
			},
			() => {
			});
	}

	public ptableSettings: IPTableSetting = {
		tableID: "users-table",
		tableClass: "table table-border ",
		tableName: 'App Users List',
		tableRowIDInternalName: "id",
		tableColDef: [
			{ headerName: 'Full Name', width: '15%', internalName: 'fullName', sort: true, type: "" },
			{ headerName: 'Email', width: '15%', internalName: 'email', sort: true, type: "" },
			{ headerName: 'Phone Number ', width: '15%', internalName: 'phoneNumber', sort: false, type: "" },
			// { headerName: 'Gender', width: '10%', internalName: 'gender', sort: true, type: "" },
			// { headerName: 'Role Name', width: '10%', internalName: 'userRoleDisplayName', sort: true, type: "" },
			{ headerName: 'Points', width: '10%', internalName: 'points', sort: true, type: "" },
			{ headerName: 'Category', width: '10%', internalName: 'pointsCategory', sort: true, type: "" },
			{ headerName: 'Division', width: '10%', internalName: 'divisionName', sort: true, type: "" },
			{ headerName: 'Under Campaign', width: '10%', internalName: 'isUnderAmbassadorCampaignText', sort: false, type: "" },
			{ headerName: 'Details', width: '15%', internalName: 'userViewDetailBtn', sort: false, type: "button", onClick: 'true', innerBtnIcon: "fa fa-user" },
		],
		enabledSearch: true,
		enabledSerialNo: true,
		pageSize: 10,
		enabledPagination: true,
		//enabledAutoScrolled:true,
		// enabledDeleteBtn: true,
		// enabledEditBtn: true,
		enabledCellClick: true,
		enabledColumnFilter: true,
		// enabledDataLength:true,
		// enabledColumnResize:true,
		// enabledReflow:true,
		// enabledPdfDownload:true,
		// enabledExcelDownload:true,
		// enabledPrint:true,
		// enabledColumnSetting:true,
		// enabledRecordCreateBtn: true,
		// enabledTotal:true,
	};

	public fnCustomTrigger(event) {
		console.log("custom  click: ", event);

		if (event.action == "new-record") {
			this.newUser();
		}
		else if (event.action == "edit-item") {
			this.editUser(event.record.id);
		}
		else if (event.action == "delete-item") {
			this.deleteUser(event.record.id);
		}
	}

	public fnViewUser(event: any) {
		console.log(event);
		let id = event.record.id;
		this.router.navigate([`/users/app-details/${id}`]);
	}
}
