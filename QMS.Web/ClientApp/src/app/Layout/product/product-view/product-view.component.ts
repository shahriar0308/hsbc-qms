import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Product } from 'src/app/Shared/Entity/Product/product';
import { ProductService } from 'src/app/Shared/Services/Product/product.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent implements OnInit, OnDestroy {
  @Input() productId: any;
  private subscriptions: Subscription[] = [];
  product: Product;
  constructor(private productService: ProductService,public activeModal: NgbActiveModal) {
    this.product = new Product();
    this.product.clear();
   }

  ngOnInit() {
    if (this.productId) {
      const productSubscriptions = this.productService.getProduct(this.productId).subscribe(res => {
        this.product = res.data;
        this.product.imageUrl = this.productService.baseUrl.replace('/api','') + this.product.imageUrl.replace('wwwroot/','');
        console.log(this.product);
      });
      this.subscriptions.push(productSubscriptions);
    }
  }

  ngOnDestroy(){
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }

}
