import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Product, SaveProduct } from 'src/app/Shared/Entity/Product/product';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { ProductService } from 'src/app/Shared/Services/Product/product.service';
import { finalize } from 'rxjs/operators';
import { KeyValuePair } from 'src/app/Shared/Entity/Common/key-value-pair';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit, OnDestroy {

  product: Product;
  productForm: FormGroup;
  imageFile: File;
  isEdit: boolean;
  keepCurrentFile: boolean;
  surfaceTypes: KeyValuePair[];
  private subscriptions: Subscription = new Subscription();


	constructor(private activatedRoute: ActivatedRoute,
		private router: Router,
    private productFB: FormBuilder,
    private alertService: AlertService,
    private productService: ProductService,
    private cd: ChangeDetectorRef) { }

	ngOnInit() {
		this.loadSurfaceTypes();
		// const routeSubscription = this.activatedRoute.params.subscribe(params => {
			const id = this.activatedRoute.snapshot.params['id'] || 0;
			if (id) {
				this.isEdit = true;
				this.alertService.fnLoading(true);
				this.productService.getProduct(id)
					.pipe(finalize(() => this.alertService.fnLoading(false)))
					.subscribe(res => {
						if (res) {
							this.product = res.data as Product;
							// this.product.imageUrl = this.product.imageUrl ? this.productService.baseUrl.replace('/api','') 
							// + (this.product.imageUrl).replace('wwwroot/','') : '';

							this.initProduct();
						}
					});
			} else {
				this.product = new Product();
				this.product.clear();
				this.initProduct();
			}
		// });
		// this.subscriptions.add(routeSubscription);
	}
	loadSurfaceTypes() {
		this.alertService.fnLoading(true);
		const surfaceTypeSubscription = this.productService.getSurfaceTypes()
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.surfaceTypes = res.data;
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.add(surfaceTypeSubscription);
	}

 	ngOnDestroy() {
		this.subscriptions.unsubscribe();
	}
  
//   onFileChange(event) {
//     let reader = new FileReader();
   
//     if(event.target.files && event.target.files.length) {
//       const [file] = event.target.files;
//       this.imageFile = file;
//       reader.readAsDataURL(file);
    
//       reader.onload = () => {
//         this.productForm.patchValue({
//           file: reader.result
//         });
//         // this.imageFile = reader.result;
//         // need to run CD since file load runs outside of zone
//         this.cd.markForCheck();
//       };
//     }
//   }
	onChangeFile(file: File) {
		// this.product.image = this.imageFile = file;	
		this.imageFile = file;
        if(this.imageFile == null)
        {
            this.product.imageUrl = '';
		}
	}

	initProduct() {
		this.createForm();
	}

	createForm() {
		console.log(this.product);
		this.productForm = this.productFB.group({
			// userName: [this.user.userName, Validators.required],
			name: [this.product.name, Validators.required],
			description: [this.product.description, Validators.required],
			image: [null],
			isActive: [this.product.isActive],
			keepCurrentFile: [this.keepCurrentFile],
			surfaceType: [this.product.surfaceType],
			paintCost: [this.product.paintCost],
			sealerCost: [this.product.sealerCost],
			puttyCost: [this.product.puttyCost],
			topCoatCost: [this.product.topCoatCost],
			baseCoatCost: [this.product.baseCoatCost],
			primerCost: [this.product.primerCost]
		});
	}
	
	get dfControls() { return this.productForm.controls; }

	onSubmit() {

    const controls = this.productForm.controls;
		
		if (this.productForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		const editedProduct = this.prepareProduct();
		if (editedProduct.id) {
			this.updateProduct(editedProduct);
		}
		else {
			this.createProduct(editedProduct);
		}
	}

	prepareProduct(): SaveProduct {
		const controls = this.productForm.controls;
		const _product = new SaveProduct();
		_product.clear();
    	_product.id = this.product.id;
		_product.name = controls['name'].value;
		if(this.imageFile)
			_product.image = this.imageFile;
		// _product.imageUrl = this.product.imageUrl;
		_product.description = controls['description'].value;
		_product.isActive = controls['isActive'].value;
		// _product.keepCurrentFile = controls['keepCurrentFile'].value;
		_product.surfaceType = controls['surfaceType'].value;
		_product.paintCost = controls['paintCost'].value;
		_product.sealerCost = controls['sealerCost'].value;
		_product.puttyCost = controls['puttyCost'].value;
		_product.baseCoatCost = controls['baseCoatCost'].value;
		_product.topCoatCost = controls['topCoatCost'].value;
		_product.primerCost = controls['primerCost'].value;
		return _product;
	}

	createProduct(_product: SaveProduct) {
    console.log(_product);
		this.alertService.fnLoading(true);
		const createSubscription = this.productService.create(_product)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`New product has been added successfully.`);
				this.goBack();
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.add(createSubscription);
	}

	updateProduct(_product: SaveProduct) {
		console.log(_product);
		this.alertService.fnLoading(true);
		const updateSubscription = this.productService.update(_product)
			.pipe(finalize(() => this.alertService.fnLoading(false)))
			.subscribe(res => {
				this.alertService.tosterSuccess(`Product has been saved successfully.`);
				this.goBack();
			},
			error => {
				this.throwError(error);
			});
		this.subscriptions.add(updateSubscription);
	}

	getComponentTitle() {
		let result = 'Create Product';
		if (!this.product || !this.product.id) {
			return result;
		}

		result = `Edit product - ${this.product.name}`;
		return result;
	}
	
	goBack() {
		this.router.navigate([`/product`], { relativeTo: this.activatedRoute });
	}

	// stringToInt(value): number {
	// 	return Number.parseInt(value);
	// }

	toggle() {
		this.keepCurrentFile = !this.keepCurrentFile;
		if((!this.keepCurrentFile && this.isEdit) || !this.isEdit) {
			 this.productForm.get("image").enable();
		} else {
			 this.productForm.get("image").disable();
		}
	}

    private throwError(errorDetails: any) {
        // this.alertService.fnLoading(false);
        console.log("error", errorDetails);
        let errList = errorDetails.error.errors;
        if (errList.length) {
            console.log("error", errList, errList[0].errorList[0]);
            // this.alertService.tosterDanger(errList[0].errorList[0]);
        } else {
            //this.alertService.tosterDanger(errorDetails.error.message);
        }
    }

}
