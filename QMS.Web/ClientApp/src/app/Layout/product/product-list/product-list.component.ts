import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductQuery, Product } from 'src/app/Shared/Entity/Product/product';
import { Subscription, of } from 'rxjs';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Shared/Modules/alert/alert.service';
import { ProductService } from 'src/app/Shared/Services/Product/product.service';
import { take, delay, finalize } from 'rxjs/operators';
import { CommonService } from 'src/app/Shared/Services/Common/common.service';
import { IPTableSetting } from 'src/app/Shared/Modules/p-table';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ProductViewComponent } from '../product-view/product-view.component';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit, OnDestroy {

	query: ProductQuery;
	PAGE_SIZE: number;
	products: Product[];

	// Subscriptions
	private subscriptions: Subscription[] = [];

	constructor(
		private router: Router,
    	private alertService: AlertService,
    	private productService: ProductService,
		private commonService: CommonService,
		private modalService: NgbModal) {
		this.PAGE_SIZE = commonService.PAGE_SIZE;
	}

	ngOnInit() {
		// of(undefined).pipe(take(1), delay(1000)).subscribe(() => {
			this.loadProductsPage();
		// });
	}

	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}
	
    loadProductsPage() {
		this.searchConfiguration();
        this.alertService.fnLoading(true);
		const productsSubscription = this.productService.getProducts(this.query)
			.pipe(finalize(() => { this.alertService.fnLoading(false); }))
			.subscribe(
            (res) => {
				this.products = res.data.items;
				this.products.forEach(obj=>{
					obj.isActiveCustom = obj.isActive? 'Yes': 'No';
					obj.details = "Details";
				});
            },
            (error) => {
                console.log(error);
            });
			this.subscriptions.push(productsSubscription);
	}
	
	searchConfiguration() {
		this.query = new ProductQuery({
			page: 1,
			pageSize: this.PAGE_SIZE,
			sortBy: 'name',
			isSortAscending: true,
			name: '',
		});
	}

	toggleActiveInactive(id) {
		const actInSubscription = this.productService.activeInactive(id).subscribe(res => {
			this.loadProductsPage();
		});
		this.subscriptions.push(actInSubscription);
	}
	
	editProduct(id) {
		this.router.navigate(['/product/edit', id]);
	}
	
	newProduct() {
		this.router.navigate(['/product/new']);
	}
	
	deleteProduct(id) {
		this.alertService.confirm("Are you sure want to delete this product?", 
			() => {
				this.alertService.fnLoading(true);
				const deleteSubscription = this.productService.delete(id)
					.pipe(finalize(() => { this.alertService.fnLoading(false); }))
					.subscribe((res: any) => {
						console.log('res from del func',res);          
						this.alertService.tosterSuccess("Product has been deleted successfully.");
						this.loadProductsPage();
					},
					(error) => {
						console.log(error);
					});
					this.subscriptions.push(deleteSubscription);
				}, 
			() => {
			});
	}

    public ptableSettings: IPTableSetting = {
        tableID: "products-table",
        tableClass: "table table-border ",
        tableName: 'Product List',
        tableRowIDInternalName: "id",
        tableColDef: [
			{ headerName: 'Surface Type', width: '10%', internalName: 'surfaceTypeText', sort: true, type: "" },
			{ headerName: 'Product Name', width: '10%', internalName: 'name', sort: true, type: "" },
			{ headerName: 'Description', width: '10%', internalName: 'description', sort: false, type: "" },
			{ headerName: 'Paint Cost', width: '10%', internalName: 'paintCost', sort: false, type: "" },
			{ headerName: 'Sealer Cost', width: '10%', internalName: 'sealerCost', sort: false, type: "" },
			{ headerName: 'Putty Cost', width: '10%', internalName: 'puttyCost', sort: false, type: "" },
			{ headerName: 'Top Coat Cost', width: '10%', internalName: 'topCoatCost', sort: false, type: "" },
			{ headerName: 'Base Coat Cost', width: '10%', internalName: 'baseCoatCost', sort: false, type: "" },
			{ headerName: 'Primer Cost', width: '10%', internalName: 'primerCost', sort: false, type: "" },
			{ headerName: 'Active', width: '10%', internalName: 'isActiveCustom', sort: false, type: "" },
			// { headerName: 'Details', width: '20%', internalName: 'details', sort: false, type: "button", onClick: 'true', innerBtnIcon:"fa fa-action" },
        ],
        enabledSearch: true,
        enabledSerialNo: true,
        pageSize: 10,
        enabledPagination: true,
		enabledDeleteBtn: true,
		newRecordButtonText: 'New Product',
        enabledEditBtn: true,
        enabledColumnFilter: true,
        enabledRadioBtn: false,
		enabledRecordCreateBtn: true,
		enabledCellClick: true
	};
	
	fnViewProduct(event : any) {
		let id = event.record.id;
		let ngbModalOptions: NgbModalOptions = {
			  backdrop: "static",
			  keyboard: false,
			  size: "lg",
		};
		const modalRef = this.modalService.open(
			ProductViewComponent,
			ngbModalOptions
		);
		modalRef.componentInstance.productId = id;

		modalRef.result.then(
			(result) => {
				console.log(result);
			},
			(reason) => {
				console.log(reason);
			});
	}

    public fnCustomTrigger(event) {
        console.log("custom  click: ", event);

        if (event.action == "new-record") {
            this.newProduct();
        }
        else if (event.action == "edit-item") {
            this.editProduct(event.record.id);
        }
        else if (event.action == "delete-item") {
            this.deleteProduct(event.record.id);
        }
    }

}
