﻿using Berger.Services.Interfaces;
using Berger.Web.Core;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Berger.Web.Services
{
    public class SMSService : ISMSService
    {
        public SMSService(IOptions<AuthSMSSenderOptions> optionsAccessor)
        {
            Options = optionsAccessor.Value;
        }

        public AuthSMSSenderOptions Options { get; } //set only via Secret Manager

        public async Task<bool> SendOTPSMSAsync(string phoneNumber, int otp)
        {
            var smsCategoryName = string.IsNullOrWhiteSpace(this.Options.SMSCategoryName) ? "OTP" : this.Options.SMSCategoryName;
            var smsText = string.IsNullOrWhiteSpace(this.Options.SMSText) ? string.Format("Berger OTP token is : {0}", otp) : 
                                                                            string.Format(this.Options.SMSText, otp);
            phoneNumber = phoneNumber.StartsWith("88") ? phoneNumber : "88"+phoneNumber;

            string URL = $"{this.Options.SMSSendAPIUrl}?user_id={this.Options.UserId}&user_password={this.Options.UserPassword}"+
                $"&route_id ={this.Options.RouteId}&sms_type_id={this.Options.SMSTypeId}&sms_sender={this.Options.SMSSender}"+
                $"&sms_receiver={phoneNumber}&sms_text={smsText}&sms_category_name={smsCategoryName}";

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(URL);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
            StreamReader sr = new StreamReader(resp.GetResponseStream());
            string results = sr.ReadToEnd();
            sr.Close();

            return true;
        }
    }
}
