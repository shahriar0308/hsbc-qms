﻿using Berger.Services.Interfaces;
using System;

namespace Berger.Web.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
