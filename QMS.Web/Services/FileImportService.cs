﻿using Berger.Entities;
using Berger.Services.Interfaces;
using ExcelDataReader;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Berger.Web.Services
{
    public class FileImportService : IFileImportService
    {
        private readonly IWebHostEnvironment _host;
        private readonly IColorCodeService _userService;

        public FileImportService(IWebHostEnvironment host,
            IColorCodeService userService)
        {
            _host = host;
            _userService = userService;
        }

        public async Task<(IList<ColorCode> Data, string Message)> ExcelImportColorCodeAsync(object fileObj)
        {
            var file = fileObj as IFormFile;
            using (var stream = file.OpenReadStream())
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var dataset = reader.AsDataSet(new ExcelDataSetConfiguration()
                    {
                        ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                        {
                            UseHeaderRow = true
                        }
                    });

                    DataTable datatable = dataset.Tables[0];

                    foreach (DataColumn dataColumn in datatable.Columns)
                    {
                        dataColumn.ColumnName = Regex.Replace(dataColumn.ColumnName, @"\s+", "").ToUpper();
                    }

                    return await _userService.DataTableSaveToDatabaseAsync(datatable);
                }
            }
        }
    }
}
