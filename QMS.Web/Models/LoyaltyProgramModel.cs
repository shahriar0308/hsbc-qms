using Berger.Entities;
using Berger.Mappings;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Berger.Common.Enums;
using System.ComponentModel.DataAnnotations;

namespace Berger.Web.Models
{
    public class LoyaltyProgramAppModel : IMapFrom<Entities.NotMapped.LoyaltyProgramInformation>
    {
        public UserModel User { get; set; }
        public SettingModel Setting { get; set; }
        public IList<PromotionalBannerModel> PromotionalBanners { get; set; }
        public IList<BenefitModel> Benefits { get; set; }
        public IList<FAQAppModel> LoyaltyProgramFAQs { get; set; }
        public SingleValuedEntity TermsConditions { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap< Entities.NotMapped.LoyaltyProgramInformation, LoyaltyProgramAppModel >()
                .ForMember(dest => dest.User, opt => opt.MapFrom(src => src.User))
                .ForMember(dest => dest.Setting, opt => opt.MapFrom(src => src.Setting))
                .ForMember(dest => dest.PromotionalBanners, opt => opt.MapFrom(src => src.PromotionalBanners))
                .ForMember(dest => dest.Benefits, opt => opt.MapFrom(src => src.Benefits))
                .ForMember(dest => dest.LoyaltyProgramFAQs, opt => opt.MapFrom(src => src.LoyaltyProgramFAQs))
                .ForMember(dest => dest.TermsConditions, opt => opt.MapFrom(src => src.TermsConditions));
        }
    }
}
