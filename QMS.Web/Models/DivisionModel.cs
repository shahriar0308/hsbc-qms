
using AutoMapper;
using Berger.Common.Enums;
using Berger.Entities;
using Berger.Mappings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Web.Models
{
    public class DivisionModel : IMapFrom<Division>
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public bool IsActive { get; set; }

        public IList<DistrictModel> Districts { get; set; }

        public DivisionModel()
        {
            this.Districts = new List<DistrictModel>();
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Division, DivisionModel>();
        }
    }

    public class SaveDivisionModel : IMapFrom<Division>
    {
        public int Id { get; set; }
        [Required]
        //[RegularExpression("^[a-zA-Z]+([\\s,.'-][a-zA-Z]+)*$",
        //    ErrorMessage = "Only takes alphabets, spaces and characters (,.'-)")]
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<SaveDivisionModel, Division>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src =>
                    src.Name.Trim()));
            profile.CreateMap<Division, SaveDivisionModel>();
        }
    }
}
