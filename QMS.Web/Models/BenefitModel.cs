
using AutoMapper;
using Berger.Common.Enums;
using Berger.Entities;
using Berger.Mappings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Web.Models
{
    public class BenefitModel : IMapFrom<Benefit>
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string CategoryName { get; set; }
        public int SettingId { get; set; }
        public int Sequence { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Benefit, BenefitModel>()
                .ForMember(dest => dest.CategoryName, opt => opt.MapFrom(src =>
                    src.Setting != null ? src.Setting.Category : null));
        }
    }

    public class SaveBenefitModel : IMapFrom<Benefit>
    {
        public int Id { get; set; }
        [Required]
        //[RegularExpression("^[a-zA-Z]+([\\s,.'-][a-zA-Z]+)*$",
        //    ErrorMessage = "Only takes alphabets, spaces and characters (,.'-)")]
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public int SettingId { get; set; }
        public int Sequence { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<SaveBenefitModel, Benefit>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src =>
                    src.Name.Trim()));
            profile.CreateMap<Benefit, SaveBenefitModel>();
        }
    }
}
