﻿using AutoMapper;
using Berger.Entities;
using Berger.Mappings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Berger.Web.Models
{
    public class SettingModel : IMapFrom<Setting>
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public long Points { get; set; }
        public string IsActiveText { get; set; }
        public bool IsActive { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Setting, SettingModel>()
                .ForMember(m=>m.IsActiveText, opt=>opt.MapFrom(a=>a.IsActive ? "Yes" : "No"));
        }
    }

    public class SaveSettingModel : IMapFrom<Setting>
    {
        public int Id { get; set; }
        [Required]
        public string Category { get; set; }
        [Required]
        public long Points { get; set; }
        public bool IsActive { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<SaveSettingModel, Setting>();
            profile.CreateMap<Setting, SaveSettingModel>();
        }
    }
}
