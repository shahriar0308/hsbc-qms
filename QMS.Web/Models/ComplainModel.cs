
using AutoMapper;
using Berger.Common.Enums;
using Berger.Entities;
using Berger.Mappings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Web.Models
{
    public class ComplainModel : IMapFrom<Complain>
    {
        public int Id { get; set; }
        public EnumComplainType ComplainType { get; set; }
        public string ComplainDetails { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Complain, ComplainModel>();
        }
    }

    public class SaveComplainModel : IMapFrom<Complain>
    {
        public int Id { get; set; }
        public EnumComplainType ComplainType { get; set; }
        public string ComplainDetails { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<SaveComplainModel, Complain>();
            profile.CreateMap<Complain, SaveComplainModel>();
        }
    }
}
