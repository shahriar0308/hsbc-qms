
using AutoMapper;
using Berger.Common.Enums;
using Berger.Entities;
using Berger.Mappings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Web.Models
{
    public class PaintingServiceCostModel : IMapFrom<PaintingServiceCost>
    {
        public int Id { get; set; }
        public EnumSurfaceType SurfaceType { get; set; }
        public string SurfaceTypeText { get; set; }
        public EnumSurfaceCondition SurfaceCondition { get; set; }
        public string SurfaceConditionText { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal PlatinumRatePerSft { get; set; }
        public string PlatinumRatePerSftText { get; set; }
        public decimal GoldRatePerSft { get; set; }
        public string GoldRatePerSftText { get; set; }
        public decimal SilverRatePerSft { get; set; }
        public string SilverRatePerSftText { get; set; }
        public decimal SevenToNine { get; set; }
        public decimal AboveNine { get; set; }
        public bool IsActive { get; set; }
        public string IsActiveText { get; set; }
        public decimal Vat { get; set; }
        public decimal PaintCost { get; set; }
        public decimal SealerCost { get; set; }
        public decimal PuttyCost { get; set; }
        public decimal TopCoatCost { get; set; }
        public decimal BaseCoatCost { get; set; }
        public decimal PrimerCost { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<PaintingServiceCost, PaintingServiceCostModel>()
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src =>
                    src.Product != null ? src.Product.Name : null))
                .ForMember(dest => dest.SurfaceTypeText,
                opt => opt.MapFrom(src => Enum.GetName(typeof(EnumSurfaceType), src.SurfaceType).Replace('_', '/')))
                .ForMember(dest => dest.SurfaceConditionText,
                opt => opt.MapFrom(src => Enum.GetName(typeof(EnumSurfaceCondition), src.SurfaceCondition)))
                .ForMember(m => m.IsActiveText, opt => opt.MapFrom(a => a.IsActive ? "Yes" : "No"))
                .ForMember(m => m.PlatinumRatePerSftText, opt => opt.MapFrom(a => a.PlatinumRatePerSft.ToString() + " BDT"))
                .ForMember(m => m.GoldRatePerSftText, opt => opt.MapFrom(a => a.GoldRatePerSft.ToString() + " BDT"))
                .ForMember(m => m.SilverRatePerSftText, opt => opt.MapFrom(a => a.SilverRatePerSft.ToString() + " BDT"))
                .ForMember(m => m.PaintCost, opt => opt.MapFrom(a => a.Product != null ? a.Product.PaintCost : 0M))
                .ForMember(m => m.SealerCost, opt => opt.MapFrom(a => a.Product != null ? a.Product.SealerCost : 0M))
                .ForMember(m => m.PuttyCost, opt => opt.MapFrom(a => a.Product != null ? a.Product.PuttyCost : 0M))
                .ForMember(m => m.TopCoatCost, opt => opt.MapFrom(a => a.Product != null ? a.Product.TopCoatCost : 0M))
                .ForMember(m => m.BaseCoatCost, opt => opt.MapFrom(a => a.Product != null ? a.Product.BaseCoatCost : 0M))
                .ForMember(m => m.PrimerCost, opt => opt.MapFrom(a => a.Product != null ? a.Product.PrimerCost : 0M));
        }
    }

    public class SavePaintingServiceCostModel : IMapFrom<PaintingServiceCost>
    {
        public int Id { get; set; }
        public EnumSurfaceType SurfaceType { get; set; }
        public EnumSurfaceCondition SurfaceCondition { get; set; }
        public int ProductId { get; set; }
        public decimal PlatinumRatePerSft { get; set; }
        public decimal GoldRatePerSft { get; set; }
        public decimal SilverRatePerSft { get; set; }
        public decimal SevenToNine { get; set; }
        public decimal AboveNine { get; set; }
        public decimal Vat { get; set; }
        public bool IsActive { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<SavePaintingServiceCostModel, PaintingServiceCost>();
            profile.CreateMap<PaintingServiceCost, SavePaintingServiceCostModel>();
        }
    }
}
