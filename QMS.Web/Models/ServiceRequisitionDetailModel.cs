
using AutoMapper;
using Berger.Common.Enums;
using Berger.Entities;
using Berger.Mappings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Web.Models
{
    public class ServiceRequisitionDetailModel : IMapFrom<ServiceRequisition>
    {
        public Guid Id { get; set; }
        public Guid ServiceRequisitionId { get; set; }
        public string WorkDetailsId { get; set; }
        public string Stage { get; set; }
        public string FinancialEstimateUrl { get; set; }
        public string PaymentSlipUrl { get; set; }
        public string ColorConsultantName { get; set; }
        public string ColorConsultantPhoneNo { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<ServiceRequisitionDetail, ServiceRequisitionDetailModel>();
            profile.CreateMap<ServiceRequisitionDetailModel, ServiceRequisitionDetail>();
        }
    }
}
