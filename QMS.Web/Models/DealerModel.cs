﻿using AutoMapper;
using Berger.Entities;
using Berger.Mappings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Berger.Web.Models
{
    public class DealerModel : IMapFrom<Dealer>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public bool IsActive { get; set; }
        public string DivisionName { get; set; }
        public int DivisionId { get; set; }
        public string DistrictName { get; set; }
        public int DistrictId { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<Dealer, DealerModel>()
                .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src =>
                    src.Division != null ? src.Division.Name : null))
                .ForMember(dest => dest.DistrictName, opt => opt.MapFrom(src =>
                    src.District != null ? src.District.Name : null));
        }
    }

    public class SaveDealerModel : IMapFrom<Dealer>
    {
        public int Id { get; set; }
        [Required]
        //[RegularExpression("^[a-zA-Z]+(([\\s,.'-/&]*)[a-zA-Z]+)*$", 
        //    ErrorMessage ="Only takes alphabets, spaces and characters (,.'-)")]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }
        public bool IsActive { get; set; }
        public int DivisionId { get; set; }
        public int DistrictId { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<SaveDealerModel, Dealer>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src =>
                    src.Name.Trim()))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src =>
                    src.Address.Trim()));
            profile.CreateMap<Dealer, SaveDealerModel>();
        }
    }
}
