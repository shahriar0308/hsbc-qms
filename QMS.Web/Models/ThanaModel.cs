﻿using AutoMapper;
using Berger.Entities;
using Berger.Mappings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Berger.Web.Models
{
    public class ThanaModel : IMapFrom<Thana>
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string DistrictName { get; set; }
        public int DistrictId { get; set; }
        public bool IsActive { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Thana, ThanaModel>()
                .ForMember(dest => dest.DistrictName, opt => opt.MapFrom(src =>
                    src.District != null ? src.District.Name : null));
        }
    }

    public class SaveThanaModel : IMapFrom<Thana>
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Range(minimum: 1, maximum: int.MaxValue, ErrorMessage = "Division is required")]
        [Required(ErrorMessage = "District is required")]
        public int DistrictId { get; set; }
        public bool IsActive { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<SaveThanaModel, Thana>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src =>
                    src.Name.Trim()));
            profile.CreateMap<Thana, SaveThanaModel>();
        }
    }
}
