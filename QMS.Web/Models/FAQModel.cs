using Berger.Entities;
using Berger.Mappings;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Berger.Common.Enums;
using System.ComponentModel.DataAnnotations;

namespace Berger.Web.Models
{
    public class FAQModel : IMapFrom<FAQ>
    {
        public int Id { get; set; }
        public string QuestionTitle { get; set; }
        public string QuestionAnswer { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public string FAQTypeCustom { get; set; }
        public EnumFAQType FAQType { get; set; }
        public int Sequence { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<FAQ, FAQModel>()
                .ForMember(dest => dest.FAQTypeCustom, opt => opt.MapFrom(src => Enum.GetName(typeof(EnumFAQType), src.FAQType).Replace('_',' ')));
        }
    }

    public class SaveFAQModel : IMapFrom<FAQ>
    {
        public int Id { get; set; }
        [Required]
        public string QuestionTitle { get; set; }
        [Required]
        public string QuestionAnswer { get; set; }
        public EnumFAQType FAQType { get; set; }
        [Required]
        [Range(1,int.MaxValue, ErrorMessage ="Sequence cannot be negetive")]
        public int Sequence { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<SaveFAQModel, FAQ>()
                .ForMember(dest => dest.QuestionTitle, opt => opt.MapFrom(src =>
                    src.QuestionTitle.Trim()))
                .ForMember(dest => dest.QuestionAnswer, opt => opt.MapFrom(src =>
                    src.QuestionAnswer.Trim()));
            profile.CreateMap<FAQ, SaveFAQModel>();
        }
    }

    public class FAQAppModel : IMapFrom<FAQ>
    {
        public int Id { get; set; }
        public string QuestionTitle { get; set; }
        public string QuestionAnswer { get; set; }
        public int Sequence { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<FAQ, FAQAppModel>();
        }
    }


}
