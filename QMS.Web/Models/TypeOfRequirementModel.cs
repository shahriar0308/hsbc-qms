
using AutoMapper;
using Berger.Common.Enums;
using Berger.Entities;
using Berger.Mappings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Web.Models
{
    public class TypeOfRequirementModel : IMapFrom<TypeOfRequirement>
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<TypeOfRequirement, TypeOfRequirementModel>();
        }
    }

    public class SaveServiceCategoryModel : IMapFrom<TypeOfRequirement>
    {
        public int Id { get; set; }
        [Required]
        //[RegularExpression("^[a-zA-Z]+([\\s,.'-][a-zA-Z]+)*$",
        //    ErrorMessage = "Only takes alphabets, spaces and characters (,.'-)")]
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<SaveServiceCategoryModel, TypeOfRequirement>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src =>
                    src.Name.Trim()));
            profile.CreateMap<TypeOfRequirement, SaveServiceCategoryModel>();
        }
    }
}
