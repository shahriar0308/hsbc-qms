﻿using Berger.Entities;
using Berger.Mappings;
using AutoMapper;
using System.ComponentModel.DataAnnotations;

namespace Berger.Web.Models
{
    public class ColorCodeModel : IMapFrom<ColorCode>
    {
        public int Id { get; set; }
        public string ShadeSequence { get; set; }
        public string ShadeCode { get; set; }
        public string R { get; set; }
        public string G { get; set; }
        public string B { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<ColorCode, ColorCodeModel>();
        }
    }

    public class SaveColorCodeModel : IMapFrom<ColorCode>
    {
        public int Id { get; set; }
        [Required]
        public string ShadeSequence { get; set; }
        [Required]
        public string ShadeCode { get; set; }
        [Required]
        public string R { get; set; }
        [Required]
        public string G { get; set; }
        [Required]
        public string B { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<SaveColorCodeModel, ColorCode>();
            profile.CreateMap<ColorCode, SaveColorCodeModel>();
        }
    }
}
