﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Web.Models
{
    public class CounterModel
    {
        public  int ID { get; set; }

        public int BankUserId { get; set; }

        public int BranchId { get; set; }

        public bool IsPriority { get; set; }
    }
}
