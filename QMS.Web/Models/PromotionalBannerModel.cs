﻿using Berger.Entities;
using Berger.Mappings;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Berger.Web.Models
{
    public class PromotionalBannerModel : IMapFrom<PromotionalBanner>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Sequence { get; set; }
        public string ImageUrl { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<PromotionalBanner, PromotionalBannerModel>();
        }
    }

    public class SavePromotionalBannerModel : IMapFrom<PromotionalBanner>
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public int Sequence { get; set; }
        public bool IsActive { get; set; }
        public IFormFile ImageFile { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<SavePromotionalBannerModel, PromotionalBanner>();
            profile.CreateMap<PromotionalBanner, SavePromotionalBannerModel>();
        }
    }
}
