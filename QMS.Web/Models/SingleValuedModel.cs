
using AutoMapper;
using Berger.Common.Enums;
using Berger.Entities;
using Berger.Mappings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Web.Models
{
    public class SingleValuedModel : IMapFrom<SingleValuedEntity>
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Value { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<SingleValuedEntity, SingleValuedModel>();
        }
    }

    public class SaveSingleValuedModel : IMapFrom<SingleValuedEntity>
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Value { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<SaveSingleValuedModel, SingleValuedEntity>()
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src =>
                    src.Value.Trim()));
            profile.CreateMap<SingleValuedEntity, SaveSingleValuedModel>();
        }
    }
}
