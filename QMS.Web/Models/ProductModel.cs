using Berger.Entities;
using Berger.Mappings;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Berger.Common.Enums;
using Berger.Common.Constants;
using Microsoft.AspNetCore.Http;

namespace Berger.Web.Models
{
    public class ProductModel : IMapFrom<Product>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public string ImageUrl { get; set; }

        public string SurfaceTypeText { get; set; }
        public EnumSurfaceType SurfaceType { get; set; }
        public decimal PaintCost { get; set; }
        public decimal SealerCost { get; set; }
        public decimal PuttyCost { get; set; }
        public decimal TopCoatCost { get; set; }
        public decimal BaseCoatCost { get; set; }
        public decimal PrimerCost { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<Product, ProductModel>()
                .ForMember(dest => dest.SurfaceTypeText, 
                opt => opt.MapFrom(src => Enum.GetName(typeof(EnumSurfaceType), src.SurfaceType).Replace('_','/')));
            //profile.CreateMap<Product, ProductModel>()
            //    .ForMember(dest=>dest.ImageUrl, 
            //    opt=>opt.MapFrom(
            //        src=> string.IsNullOrEmpty(src.ImageUrl) ? src.ImageUrl :
            //        (ConstantsValue.ProductImageLocation + src.ImageUrl))
            //    );
        }
    }

    public class SaveProductModel : IMapFrom<Product>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public bool IsActive { get; set; }
        public EnumSurfaceType SurfaceType { get; set; }
        public IFormFile Image { get; set; }
        public decimal PaintCost { get; set; }
        public decimal SealerCost { get; set; }
        public decimal PuttyCost { get; set; }
        public decimal TopCoatCost { get; set; }
        public decimal BaseCoatCost { get; set; }
        public decimal PrimerCost { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<SaveProductModel, Product>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src =>
                    src.Name.Trim()))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src =>
                    src.Description.Trim()));
            profile.CreateMap<Product, SaveProductModel>();
        }
    }
}
