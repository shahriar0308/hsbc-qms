
using AutoMapper;
using Berger.Common.Enums;
using Berger.Entities;
using Berger.Mappings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Berger.Web.Models
{
    public class ServiceRequisitionModel: IMapFrom<ServiceRequisition>
    {
        public Guid Id { get; set; }
        public string WorkId { get; set; }
        public int TypeOfRequirementId { get; set; }
        public string TypeOfRequirementName { get; set; }
        public int ThanaId { get; set; }
        public string ThanaName { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string AddressLine1 { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhoneNo { get; set; }
        public string Stage { get; set; }
        public string RequirementDetail { get; set; }
        public IList<ServiceRequisitionDetailModel> ServiceRequisitionDetails { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<ServiceRequisition, ServiceRequisitionModel>()
                .ForMember(dest => dest.ThanaName, opt => opt.MapFrom(src =>
                    src.Thana != null ? src.Thana.Name : null))
                .ForMember(dest => dest.DistrictName, opt => opt.MapFrom(src =>
                    src.District != null ? src.District.Name : null))
                .ForMember(dest => dest.TypeOfRequirementName, opt => opt.MapFrom(src =>
                    src.TypeOfRequirement != null ? src.TypeOfRequirement.Name : null))
                .ForMember(dest => dest.ServiceRequisitionDetails, opt => opt.MapFrom(src =>
                    src.ServiceRequisitionDetails));
        }
    }
    
    public class SaveServiceRequisitionModel : IMapFrom<ServiceRequisition>
    {
        [Required]
        public int TypeOfRequirementId { get; set; }
        [Required]
        public int ThanaId { get; set; }
        [Required]
        public int DistrictId { get; set; }
        [Required]
        public string AddressLine1 { get; set; }
        [Required]
        public string CustomerName { get; set; }
        [Required]
        public string CustomerPhoneNo { get; set; }
        public string RequirementDetail { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<SaveServiceRequisitionModel, ServiceRequisition>();
            profile.CreateMap<ServiceRequisition, SaveServiceRequisitionModel>();
        }
    }

    //public class UpdateServiceRequisitionModel : IMapFrom<ServiceRequisition>
    //{
    //    public string WorkId { get; set; }
    //    public Guid Id { get; set; }
    //    public string Stage { get; set; }
    //    public string FinancialEstimateUrl { get; set; }
    //    public string PaymentSlipUrl { get; set; }
    //    public string ColorConsultantName { get; set; }
    //    public string ColorConsultantPhoneNo { get; set; }
    //    public void Mapping(Profile profile)
    //    {
    //        profile.CreateMap<UpdateServiceRequisitionModel, ServiceRequisition>();
    //        profile.CreateMap<ServiceRequisition, UpdateServiceRequisitionModel>();
    //    }
    //}
}
