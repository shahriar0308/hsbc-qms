﻿using Berger.Entities;
using Berger.Mappings;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Berger.Web.Models
{
    public class LoyaltyProgramPointModel : IMapFrom<LoyaltyProgramPoint>
    {
        public int Id { get; set; }
        public int ProductCost { get; set; }
        public int PainterCost { get; set; }
        public int SupervisionCharge { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<LoyaltyProgramPoint, LoyaltyProgramPointModel>();
        }
    }

    public class SaveLoyaltyProgramPointModel : IMapFrom<LoyaltyProgramPoint>
    {
        public int Id { get; set; }
        [Required]
        public int ProductCost { get; set; }
        [Required]
        public int PainterCost { get; set; }
        [Required]
        public int SupervisionCharge { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<SaveLoyaltyProgramPointModel, LoyaltyProgramPoint>();
            profile.CreateMap<LoyaltyProgramPoint, SaveLoyaltyProgramPointModel>();
        }
    }
}
