
using AutoMapper;
using Berger.Common.Enums;
using Berger.Entities;
using Berger.Mappings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Web.Models
{
    public class DistrictModel : IMapFrom<District>
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string DivisionName { get; set; }
        public int DivisionId { get; set; }
        public bool IsActive { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<District, DistrictModel>()
                .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src =>
                    src.Division != null ? src.Division.Name : null));
        }
    }

    public class SaveDistrictModel : IMapFrom<District>
    {
        public int Id { get; set; }
        [Required]
        //[RegularExpression("^[a-zA-Z]+([\\s,.'-][a-zA-Z]+)*$",
        //    ErrorMessage = "Only takes alphabets, spaces and characters (,.'-)")]
        public string Name { get; set; }
        [Range(minimum: 1, maximum: int.MaxValue, ErrorMessage = "Division is required")]
        [Required(ErrorMessage = "Division is required")]
        public int DivisionId { get; set; }
        public bool IsActive { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<SaveDistrictModel, District>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src =>
                    src.Name.Trim()));
            profile.CreateMap<District, SaveDistrictModel>();
        }
    }
}
