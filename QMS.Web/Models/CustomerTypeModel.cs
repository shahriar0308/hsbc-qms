﻿using QMS.Mappings;
using QMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace QMS.Web.Models
{
    public class CustomerTypeModel : IMapFrom<CustomerType>
    {
        public int Id { get; set; }
        public string CustomerType { get; set; }


        public void Mapping(Profile profile)
        {
            profile.CreateMap<CustomerTypeModel, CustomerType>()
                .ReverseMap();
        }

    }
}
