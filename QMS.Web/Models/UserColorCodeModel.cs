﻿using Berger.Entities;
using Berger.Mappings;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using System;

namespace Berger.Web.Models
{
    public class UserColorCodeModel : IMapFrom<UserColorCode>
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public int ColorCodeId { get; set; }
        public string Name { get; set; }
        public string ShadeSequence { get; set; }
        public string ShadeCode { get; set; }
        public string R { get; set; }
        public string G { get; set; }
        public string B { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<UserColorCode, UserColorCodeModel>()
                .ForMember(dest => dest.ShadeSequence, opt => opt.MapFrom(src => src.ColorCode.ShadeSequence))
                .ForMember(dest => dest.ShadeCode, opt => opt.MapFrom(src => src.ColorCode.ShadeCode))
                .ForMember(dest => dest.R, opt => opt.MapFrom(src => src.ColorCode.R))
                .ForMember(dest => dest.G, opt => opt.MapFrom(src => src.ColorCode.G))
                .ForMember(dest => dest.B, opt => opt.MapFrom(src => src.ColorCode.B));
        }
    }

    public class SaveUserColorCodeModel : IMapFrom<UserColorCode>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [Required]
        public Guid UserId { get; set; }
        [Required]
        public int ColorCodeId { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<SaveUserColorCodeModel, UserColorCode>();
            profile.CreateMap<UserColorCode, SaveUserColorCodeModel>();
        }
    }
}
