﻿using Berger.Common.Enums;
using Berger.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using QMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Repository.Context
{
    public class ApplicationDbContext : DbContext
    {
        // Add DbSet here

      
     

        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<ApplicationUserRole>(userRole =>
            //{
            //    userRole.HasKey(ur => new { ur.UserId, ur.RoleId });

            //    userRole.HasOne(ur => ur.Role)
            //        .WithMany(r => r.UserRoles)
            //        .HasForeignKey(ur => ur.RoleId)
            //        .IsRequired();

            //    userRole.HasOne(ur => ur.User)
            //        .WithMany(r => r.UserRoles)
            //        .HasForeignKey(ur => ur.UserId)
            //        .IsRequired();
            //});

            //#region Seed Data

            //#region Divisions
            //modelBuilder.Entity<Division>().HasData(
            //    new Division { Name = "Dhaka", Id = 1, IsActive = true, IsDeleted = false, Created = new DateTime(2020, 07, 01) },
            //    new Division { Name = "Sylhet", Id = 2, IsActive = true, IsDeleted = false, Created = new DateTime(2020, 07, 01) }
            //    );
            //#endregion

            
            //#endregion

     
        }
        public DbSet<CustomerType> CustomerTypes { get; set; }
    }
}