﻿using System;
using Berger.Entities.LMS;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Berger.Repository.Context
{
    public partial class LMSDbContext : DbContext
    {
        public LMSDbContext(DbContextOptions<LMSDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AppContactMstr> AppContactMstr { get; set; }
        public virtual DbSet<AppMessageAssignment> AppMessageAssignment { get; set; }
        public virtual DbSet<AppMessageMstr> AppMessageMstr { get; set; }
        public virtual DbSet<AppUserLog> AppUserLog { get; set; }
        public virtual DbSet<AppVersion> AppVersion { get; set; }
        public virtual DbSet<AppVersionUpdateLog> AppVersionUpdateLog { get; set; }
        public virtual DbSet<BibhagMstr> BibhagMstr { get; set; }
        public virtual DbSet<BusinessCallDtls> BusinessCallDtls { get; set; }
        public virtual DbSet<BusinessCallHdr> BusinessCallHdr { get; set; }
        public virtual DbSet<BusinessCallMstr> BusinessCallMstr { get; set; }
        public virtual DbSet<CbItemDtls> CbItemDtls { get; set; }
        public virtual DbSet<CbItemHdr> CbItemHdr { get; set; }
        public virtual DbSet<ClusterApplicableThana> ClusterApplicableThana { get; set; }
        public virtual DbSet<ClusterDtls> ClusterDtls { get; set; }
        public virtual DbSet<ClusterHdr> ClusterHdr { get; set; }
        public virtual DbSet<CompanyAssociates> CompanyAssociates { get; set; }
        public virtual DbSet<CompanyDealer> CompanyDealer { get; set; }
        public virtual DbSet<CompanyDivision> CompanyDivision { get; set; }
        public virtual DbSet<CompanyDocs> CompanyDocs { get; set; }
        public virtual DbSet<CompanyHierarchy> CompanyHierarchy { get; set; }
        public virtual DbSet<CompanyMstr> CompanyMstr { get; set; }
        public virtual DbSet<CustomerFeedbackOtp> CustomerFeedbackOtp { get; set; }
        public virtual DbSet<DashboardLead> DashboardLead { get; set; }
        public virtual DbSet<DealerPositionRegistration> DealerPositionRegistration { get; set; }
        public virtual DbSet<DealerPositionRegistrationUpdatedDeleted> DealerPositionRegistrationUpdatedDeleted { get; set; }
        public virtual DbSet<DistrictMstr> DistrictMstr { get; set; }
        public virtual DbSet<DivisionMapping> DivisionMapping { get; set; }
        public virtual DbSet<FaqMstr> FaqMstr { get; set; }
        public virtual DbSet<FinYear> FinYear { get; set; }
        public virtual DbSet<FlashNews> FlashNews { get; set; }
        public virtual DbSet<FormMenuMstr> FormMenuMstr { get; set; }
        public virtual DbSet<HdItemMstr> HdItemMstr { get; set; }
        public virtual DbSet<HelpLine> HelpLine { get; set; }
        public virtual DbSet<ItemBrandMapping> ItemBrandMapping { get; set; }
        public virtual DbSet<ItemFormulaApplicationDtl> ItemFormulaApplicationDtl { get; set; }
        public virtual DbSet<ItemFormulaDtl> ItemFormulaDtl { get; set; }
        public virtual DbSet<ItemFormulaHdr> ItemFormulaHdr { get; set; }
        public virtual DbSet<ItemFormulaSupervisionChargeDtl> ItemFormulaSupervisionChargeDtl { get; set; }
        public virtual DbSet<ItemPackSize> ItemPackSize { get; set; }
        public virtual DbSet<ItemRecipeApplicationDtl> ItemRecipeApplicationDtl { get; set; }
        public virtual DbSet<ItemRecipeDtl> ItemRecipeDtl { get; set; }
        public virtual DbSet<ItemRecipeHdr> ItemRecipeHdr { get; set; }
        public virtual DbSet<ItemRecipeSupervisionChargeDtl> ItemRecipeSupervisionChargeDtl { get; set; }
        public virtual DbSet<JourneyPlanningDetails> JourneyPlanningDetails { get; set; }
        public virtual DbSet<JourneyPlanningHeader> JourneyPlanningHeader { get; set; }
        public virtual DbSet<LeadApplicableCluster> LeadApplicableCluster { get; set; }
        public virtual DbSet<LeadApplicableClusterAddtional> LeadApplicableClusterAddtional { get; set; }
        public virtual DbSet<LeadApplicationPainterPayment> LeadApplicationPainterPayment { get; set; }
        public virtual DbSet<LeadApplicationRequirement> LeadApplicationRequirement { get; set; }
        public virtual DbSet<LeadApplicationRequisitionDtl> LeadApplicationRequisitionDtl { get; set; }
        public virtual DbSet<LeadApplicationRequisitionHdr> LeadApplicationRequisitionHdr { get; set; }
        public virtual DbSet<LeadAssignmentHistory> LeadAssignmentHistory { get; set; }
        public virtual DbSet<LeadComplaintLog> LeadComplaintLog { get; set; }
        public virtual DbSet<LeadComplaintMstr> LeadComplaintMstr { get; set; }
        public virtual DbSet<LeadContacts> LeadContacts { get; set; }
        public virtual DbSet<LeadCustomerFeedbackDtl> LeadCustomerFeedbackDtl { get; set; }
        public virtual DbSet<LeadCustomerFeedbackHdr> LeadCustomerFeedbackHdr { get; set; }
        public virtual DbSet<LeadCustomerFeedbackQuestion> LeadCustomerFeedbackQuestion { get; set; }
        public virtual DbSet<LeadCustomerFeedbackQuestionAnswer> LeadCustomerFeedbackQuestionAnswer { get; set; }
        public virtual DbSet<LeadDocs> LeadDocs { get; set; }
        public virtual DbSet<LeadEstimateDtl> LeadEstimateDtl { get; set; }
        public virtual DbSet<LeadEstimateHdr> LeadEstimateHdr { get; set; }
        public virtual DbSet<LeadEstimationPaymentTerms> LeadEstimationPaymentTerms { get; set; }
        public virtual DbSet<LeadEstimationTermsConditions> LeadEstimationTermsConditions { get; set; }
        public virtual DbSet<LeadEvaluationActivity> LeadEvaluationActivity { get; set; }
        public virtual DbSet<LeadEvaluationDtl> LeadEvaluationDtl { get; set; }
        public virtual DbSet<LeadEvaluationHdr> LeadEvaluationHdr { get; set; }
        public virtual DbSet<LeadGeoLocationRegistration> LeadGeoLocationRegistration { get; set; }
        public virtual DbSet<LeadGeoLocationUpdation> LeadGeoLocationUpdation { get; set; }
        public virtual DbSet<LeadInvoiceLink> LeadInvoiceLink { get; set; }
        public virtual DbSet<LeadLostValue> LeadLostValue { get; set; }
        public virtual DbSet<LeadMaterialAllocationDtl> LeadMaterialAllocationDtl { get; set; }
        public virtual DbSet<LeadMaterialAllocationHdr> LeadMaterialAllocationHdr { get; set; }
        public virtual DbSet<LeadMaterialRequirementDtl> LeadMaterialRequirementDtl { get; set; }
        public virtual DbSet<LeadMaterialRequirementHdr> LeadMaterialRequirementHdr { get; set; }
        public virtual DbSet<LeadMaterialRequisionDtl> LeadMaterialRequisionDtl { get; set; }
        public virtual DbSet<LeadMaterialRequisionHdr> LeadMaterialRequisionHdr { get; set; }
        public virtual DbSet<LeadMaterialRequisionItemReceivedDtl> LeadMaterialRequisionItemReceivedDtl { get; set; }
        public virtual DbSet<LeadMaterialRequisionItemReceivedHdr> LeadMaterialRequisionItemReceivedHdr { get; set; }
        public virtual DbSet<LeadMaterialSupplier> LeadMaterialSupplier { get; set; }
        public virtual DbSet<LeadMaterialSupplierBillDtl> LeadMaterialSupplierBillDtl { get; set; }
        public virtual DbSet<LeadMaterialSupplierBillHdr> LeadMaterialSupplierBillHdr { get; set; }
        public virtual DbSet<LeadMaterialSupplierPayment> LeadMaterialSupplierPayment { get; set; }
        public virtual DbSet<LeadMaterialsupplyDtl> LeadMaterialsupplyDtl { get; set; }
        public virtual DbSet<LeadMaterialsupplyHdr> LeadMaterialsupplyHdr { get; set; }
        public virtual DbSet<LeadMstrCompetitor> LeadMstrCompetitor { get; set; }
        public virtual DbSet<LeadMstrDtl> LeadMstrDtl { get; set; }
        public virtual DbSet<LeadMstrDtlSurface> LeadMstrDtlSurface { get; set; }
        public virtual DbSet<LeadMstrHdr> LeadMstrHdr { get; set; }
        public virtual DbSet<LeadNotifcation> LeadNotifcation { get; set; }
        public virtual DbSet<LeadNotifcationDtl> LeadNotifcationDtl { get; set; }
        public virtual DbSet<LeadNotifcationHdr> LeadNotifcationHdr { get; set; }
        public virtual DbSet<LeadPartners> LeadPartners { get; set; }
        public virtual DbSet<LeadPartnersJobAmount> LeadPartnersJobAmount { get; set; }
        public virtual DbSet<LeadPartnersPayment> LeadPartnersPayment { get; set; }
        public virtual DbSet<LeadPaymentApplication> LeadPaymentApplication { get; set; }
        public virtual DbSet<LeadPaymentReceipt> LeadPaymentReceipt { get; set; }
        public virtual DbSet<LeadPaymentReceiptHistory> LeadPaymentReceiptHistory { get; set; }
        public virtual DbSet<LeadPoDtl> LeadPoDtl { get; set; }
        public virtual DbSet<LeadPoHdr> LeadPoHdr { get; set; }
        public virtual DbSet<LeadProspectMstr> LeadProspectMstr { get; set; }
        public virtual DbSet<LeadQuotationDtl> LeadQuotationDtl { get; set; }
        public virtual DbSet<LeadQuotationHdr> LeadQuotationHdr { get; set; }
        public virtual DbSet<LeadQuotationPaymentTerms> LeadQuotationPaymentTerms { get; set; }
        public virtual DbSet<LeadQuotationSilverDtl> LeadQuotationSilverDtl { get; set; }
        public virtual DbSet<LeadQuotationTermsConditions> LeadQuotationTermsConditions { get; set; }
        public virtual DbSet<LeadRequestMaster> LeadRequestMaster { get; set; }
        public virtual DbSet<LeadSamplingDtl> LeadSamplingDtl { get; set; }
        public virtual DbSet<LeadSamplingHdr> LeadSamplingHdr { get; set; }
        public virtual DbSet<LeadSpecificationHdr> LeadSpecificationHdr { get; set; }
        public virtual DbSet<LeadSpecificationItemDtl> LeadSpecificationItemDtl { get; set; }
        public virtual DbSet<LeadSpecificationRecipeDtl> LeadSpecificationRecipeDtl { get; set; }
        public virtual DbSet<LeadSupervisionDtl> LeadSupervisionDtl { get; set; }
        public virtual DbSet<LeadSupervisionHdr> LeadSupervisionHdr { get; set; }
        public virtual DbSet<LeadSupervisionQusetionAns> LeadSupervisionQusetionAns { get; set; }
        public virtual DbSet<LeadSupervisionQusetionMstr> LeadSupervisionQusetionMstr { get; set; }
        public virtual DbSet<LeadSupervisionType> LeadSupervisionType { get; set; }
        public virtual DbSet<LeadValuationDtl> LeadValuationDtl { get; set; }
        public virtual DbSet<LeadValuationHdr> LeadValuationHdr { get; set; }
        public virtual DbSet<LeadVisitActivities> LeadVisitActivities { get; set; }
        public virtual DbSet<LeadVisitActivitiesProspect> LeadVisitActivitiesProspect { get; set; }
        public virtual DbSet<LeadVisitActivityMstr> LeadVisitActivityMstr { get; set; }
        public virtual DbSet<LeadVisitActivityMstrApplicableDivision> LeadVisitActivityMstrApplicableDivision { get; set; }
        public virtual DbSet<LeadVisitActivityMstrApplicableStage> LeadVisitActivityMstrApplicableStage { get; set; }
        public virtual DbSet<LeadVisitEntry> LeadVisitEntry { get; set; }
        public virtual DbSet<LeadVisitEntryProspect> LeadVisitEntryProspect { get; set; }
        public virtual DbSet<LmsDivision> LmsDivision { get; set; }
        public virtual DbSet<LovDetails> LovDetails { get; set; }
        public virtual DbSet<LovMstr> LovMstr { get; set; }
        public virtual DbSet<MobileApplicableMenu> MobileApplicableMenu { get; set; }
        public virtual DbSet<MobileMenuMstr> MobileMenuMstr { get; set; }
        public virtual DbSet<NonDecorPriceDtl> NonDecorPriceDtl { get; set; }
        public virtual DbSet<NonDecorPriceHdr> NonDecorPriceHdr { get; set; }
        public virtual DbSet<PartnerAssociates> PartnerAssociates { get; set; }
        public virtual DbSet<PartnerCompanyLinking> PartnerCompanyLinking { get; set; }
        public virtual DbSet<PartnerDocs> PartnerDocs { get; set; }
        public virtual DbSet<PartnerMstr> PartnerMstr { get; set; }
        public virtual DbSet<PaymentTermsMaster> PaymentTermsMaster { get; set; }
        public virtual DbSet<PositionRegistrationCompany> PositionRegistrationCompany { get; set; }
        public virtual DbSet<PositionRegistrationCompanyUpdated> PositionRegistrationCompanyUpdated { get; set; }
        public virtual DbSet<ProspectLeadGeoLocationRegistration> ProspectLeadGeoLocationRegistration { get; set; }
        public virtual DbSet<RegularShade> RegularShade { get; set; }
        public virtual DbSet<RelationshipCallDtls> RelationshipCallDtls { get; set; }
        public virtual DbSet<RelationshipCallHdr> RelationshipCallHdr { get; set; }
        public virtual DbSet<RelationshipCallQuestionAnsMstr> RelationshipCallQuestionAnsMstr { get; set; }
        public virtual DbSet<RelationshipCallQuestionMstr> RelationshipCallQuestionMstr { get; set; }
        public virtual DbSet<SrlControlMstr> SrlControlMstr { get; set; }
        public virtual DbSet<StandardParameter> StandardParameter { get; set; }
        public virtual DbSet<TempOtpLog> TempOtpLog { get; set; }
        public virtual DbSet<TermsConditionsMaster> TermsConditionsMaster { get; set; }
        public virtual DbSet<ThanaMstr> ThanaMstr { get; set; }
        public virtual DbSet<UserApplicableAppMstr> UserApplicableAppMstr { get; set; }
        public virtual DbSet<UserApplicableCluster> UserApplicableCluster { get; set; }
        public virtual DbSet<UserApplicableClusterBackup> UserApplicableClusterBackup { get; set; }
        public virtual DbSet<UserApplicableDivision> UserApplicableDivision { get; set; }
        public virtual DbSet<UserApplicableDivisionBackup> UserApplicableDivisionBackup { get; set; }
        public virtual DbSet<UserApplicableLegalEntity> UserApplicableLegalEntity { get; set; }
        public virtual DbSet<UserApplicableLegalEntityBackup> UserApplicableLegalEntityBackup { get; set; }
        public virtual DbSet<UserDeactivationLog> UserDeactivationLog { get; set; }
        public virtual DbSet<UserDeviceRegistration> UserDeviceRegistration { get; set; }
        public virtual DbSet<UserDeviceRegistrationDelete> UserDeviceRegistrationDelete { get; set; }
        public virtual DbSet<UserFcmToken> UserFcmToken { get; set; }
        public virtual DbSet<UserFormsAccess> UserFormsAccess { get; set; }
        public virtual DbSet<UserGroup> UserGroup { get; set; }
        public virtual DbSet<UserHistory> UserHistory { get; set; }
        public virtual DbSet<UserOtp> UserOtp { get; set; }
        public virtual DbSet<UserProfile> UserProfile { get; set; }
        public virtual DbSet<UserProfileBackup> UserProfileBackup { get; set; }
        public virtual DbSet<UserSessionAll> UserSessionAll { get; set; }
        public virtual DbSet<UserSessionCurrent> UserSessionCurrent { get; set; }
        public virtual DbSet<UserTrackingHistory> UserTrackingHistory { get; set; }
        public virtual DbSet<VisitActivityApplicableGroup> VisitActivityApplicableGroup { get; set; }
        public virtual DbSet<VisitActivityMstr> VisitActivityMstr { get; set; }
        public virtual DbSet<VisitCompanyDtl> VisitCompanyDtl { get; set; }
        public virtual DbSet<VisitCompanyHdr> VisitCompanyHdr { get; set; }
        public virtual DbSet<VwCompanyPartnerMstr> VwCompanyPartnerMstr { get; set; }
        public virtual DbSet<XxxxBusinessCallQuestionAnsMstr> XxxxBusinessCallQuestionAnsMstr { get; set; }
        public virtual DbSet<XxxxBusinessCallQuestionMstr> XxxxBusinessCallQuestionMstr { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Server=BS-336;Database=LMS_ACTUAL_DB;User ID=sa;Password=Admin@123;");
//            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppContactMstr>(entity =>
            {
                entity.HasKey(e => new { e.ContactName, e.ContactNumber })
                    .HasName("PK_contact_mstr");

                entity.Property(e => e.ContactName).IsUnicode(false);

                entity.Property(e => e.ContactNumber).IsUnicode(false);
            });

            modelBuilder.Entity<AppMessageAssignment>(entity =>
            {
                entity.HasKey(e => new { e.AmaMsgId, e.AmaAssignmentType, e.AmaAssignTo });

                entity.HasIndex(e => e.AmaAssignmentType)
                    .HasName("IX2_app_message_assignment");

                entity.HasIndex(e => new { e.AmaAssignmentType, e.AmaAssignTo })
                    .HasName("IX1_app_message_assignment");

                entity.Property(e => e.AmaAssignmentType)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.AmaAssignTo).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<AppMessageMstr>(entity =>
            {
                entity.HasIndex(e => new { e.MsgId, e.MsgSubject, e.MsgContent })
                    .HasName("IX2_app_message_mstr");

                entity.HasIndex(e => new { e.MsgValidFrom, e.MsgId, e.Active })
                    .HasName("IX3_app_message_mstr");

                entity.HasIndex(e => new { e.MsgId, e.MsgSubject, e.MsgContent, e.MsgValidTo })
                    .HasName("IX1_app_message_mstr");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.MsgContent).IsUnicode(false);

                entity.Property(e => e.MsgSubject).IsUnicode(false);
            });

            modelBuilder.Entity<AppUserLog>(entity =>
            {
                entity.Property(e => e.LocAction).IsUnicode(false);

                entity.Property(e => e.LogMsg).IsUnicode(false);

                entity.Property(e => e.LogUserId).IsUnicode(false);
            });

            modelBuilder.Entity<AppVersion>(entity =>
            {
                entity.HasIndex(e => e.VrCode)
                    .HasName("IX1_app_version");

                entity.Property(e => e.VrCode).ValueGeneratedNever();

                entity.Property(e => e.VrBitaVrYn)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.VrMsg).IsUnicode(false);

                entity.Property(e => e.VrNumber).IsUnicode(false);

                entity.Property(e => e.VrObsoleteYn)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<AppVersionUpdateLog>(entity =>
            {
                entity.Property(e => e.AvlUserId).IsUnicode(false);

                entity.Property(e => e.AvlUuid).IsUnicode(false);

                entity.Property(e => e.AvlVersion).IsUnicode(false);

                entity.Property(e => e.AvlVersionCode).IsUnicode(false);
            });

            modelBuilder.Entity<BibhagMstr>(entity =>
            {
                entity.HasIndex(e => e.BbgId)
                    .HasName("IX1_bibhag_mstr");

                entity.Property(e => e.BbgName).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.BbgId).ValueGeneratedOnAdd();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<BusinessCallDtls>(entity =>
            {
                entity.HasKey(e => new { e.BcdId, e.BcdHdrId });

                entity.HasIndex(e => e.BcdHdrId)
                    .HasName("IX2_business_call_dtls");

                entity.HasIndex(e => new { e.BcdHdrId, e.BcdQusId })
                    .HasName("IX1_business_call_dtls");

                entity.HasIndex(e => new { e.BcdQusId, e.BcdSelectedYn, e.BcdHdrId })
                    .HasName("IX3_business_call_dtls");

                entity.Property(e => e.BcdId).ValueGeneratedOnAdd();

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.BcdAttributeChar1).IsUnicode(false);

                entity.Property(e => e.BcdAttributeChar2).IsUnicode(false);

                entity.Property(e => e.BcdImgPath).IsUnicode(false);

                entity.Property(e => e.BcdSelectedYn)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<BusinessCallHdr>(entity =>
            {
                entity.HasIndex(e => e.BchDealerCode)
                    .HasName("IX3_business_call_hdr");

                entity.HasIndex(e => e.BchId)
                    .HasName("IX1_business_call_hdr");

                entity.HasIndex(e => e.BchTypeId)
                    .HasName("IX5_business_call_hdr");

                entity.HasIndex(e => e.BchUser)
                    .HasName("IX4_business_call_hdr");

                entity.HasIndex(e => e.CreatedUser)
                    .HasName("IX2_business_call_hdr");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.BchCheckInAccuracy).IsUnicode(false);

                entity.Property(e => e.BchCheckInLat).IsUnicode(false);

                entity.Property(e => e.BchCheckInLong).IsUnicode(false);

                entity.Property(e => e.BchCheckOutAccuracy).IsUnicode(false);

                entity.Property(e => e.BchCheckOutLat).IsUnicode(false);

                entity.Property(e => e.BchCheckOutLong).IsUnicode(false);

                entity.Property(e => e.BchDealerCode).IsUnicode(false);

                entity.Property(e => e.BchMode).IsUnicode(false);

                entity.Property(e => e.BchTypeId).IsUnicode(false);

                entity.Property(e => e.BchUser).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<BusinessCallMstr>(entity =>
            {
                entity.HasIndex(e => e.BcId)
                    .HasName("IX1_business_call_mstr");

                entity.HasIndex(e => e.BcType)
                    .HasName("IX2_business_call_mstr");

                entity.HasIndex(e => new { e.BcType, e.BcCatg })
                    .HasName("IX3_business_call_mstr");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.BcAttrtibuteChar1Desc).IsUnicode(false);

                entity.Property(e => e.BcAttrtibuteChar2Desc).IsUnicode(false);

                entity.Property(e => e.BcAttrtibuteDate1Desc).IsUnicode(false);

                entity.Property(e => e.BcAttrtibuteDate2Desc).IsUnicode(false);

                entity.Property(e => e.BcAttrtibuteDecimal1Desc).IsUnicode(false);

                entity.Property(e => e.BcAttrtibuteDecimal2Desc).IsUnicode(false);

                entity.Property(e => e.BcAttrtibuteInt1Desc).IsUnicode(false);

                entity.Property(e => e.BcAttrtibuteInt2Desc).IsUnicode(false);

                entity.Property(e => e.BcCatg).IsUnicode(false);

                entity.Property(e => e.BcDesc).IsUnicode(false);

                entity.Property(e => e.BcImgReqiredYn)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.BcType).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<CbItemDtls>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.CbFgItemCode)
                    .HasName("IX2_cb_item_dtls");

                entity.HasIndex(e => e.CbHdrId)
                    .HasName("IX1_cb_item_dtls");

                entity.HasIndex(e => e.CbItemBase)
                    .HasName("IX4_cb_item_dtls");

                entity.HasIndex(e => e.CbItemCode)
                    .HasName("IX3_cb_item_dtls");

                entity.HasIndex(e => new { e.CbItemCode, e.CbFgItemCode })
                    .HasName("IX5_cb_item_dtls");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.CbDetailsId).ValueGeneratedOnAdd();

                entity.Property(e => e.CbFgItemCode).IsUnicode(false);

                entity.Property(e => e.CbItemBase).IsUnicode(false);

                entity.Property(e => e.CbItemCode).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<CbItemHdr>(entity =>
            {
                entity.HasKey(e => new { e.CbItemCode, e.CbItemBase, e.CbItemGroup });

                entity.HasIndex(e => e.CbItemBase)
                    .HasName("IX2_cb_item_hdr");

                entity.HasIndex(e => e.CbItemCode)
                    .HasName("IX1_cb_item_hdr");

                entity.HasIndex(e => e.CbItemGroup)
                    .HasName("IX3_cb_item_hdr");

                entity.HasIndex(e => e.CbItemShade)
                    .HasName("IX4_cb_item_hdr");

                entity.HasIndex(e => new { e.CbItemGroup, e.CbItemBase })
                    .HasName("IX5_cb_item_hdr");

                entity.HasIndex(e => new { e.CbItemBase, e.CbItemGroup, e.Active })
                    .HasName("IX6_cb_item_hdr");

                entity.Property(e => e.CbItemCode).IsUnicode(false);

                entity.Property(e => e.CbItemBase).IsUnicode(false);

                entity.Property(e => e.CbItemGroup).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.CbId).ValueGeneratedOnAdd();

                entity.Property(e => e.CbItemName).IsUnicode(false);

                entity.Property(e => e.CbItemShade).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<ClusterApplicableThana>(entity =>
            {
                entity.HasKey(e => new { e.CatClCode, e.CatThanaId, e.CatDepotCode });

                entity.HasIndex(e => e.CatClCode)
                    .HasName("IX2_cluster_applicable_thana");

                entity.HasIndex(e => e.CatThanaId)
                    .HasName("IX3_cluster_applicable_thana");

                entity.HasIndex(e => new { e.CatClCode, e.CatThanaId })
                    .HasName("IX1_cluster_applicable_thana");

                entity.HasIndex(e => new { e.CatThanaId, e.CatClCode })
                    .HasName("IX4_cluster_applicable_thana");

                entity.Property(e => e.CatClCode).IsUnicode(false);

                entity.Property(e => e.CatDepotCode).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<ClusterDtls>(entity =>
            {
                entity.HasKey(e => new { e.CldCode, e.CldDepotCode, e.CldDealerCode })
                    .HasName("PK_cluster_dtls_1");

                entity.HasIndex(e => e.CldCode)
                    .HasName("IX1_cluster_dtls");

                entity.HasIndex(e => e.CldDealerCode)
                    .HasName("IX5_cluster_dtls");

                entity.HasIndex(e => e.CldDepotCode)
                    .HasName("IX2_cluster_dtls");

                entity.HasIndex(e => e.CldTerritoryCode)
                    .HasName("IX3_cluster_dtls");

                entity.HasIndex(e => e.CldZoneCode)
                    .HasName("IX4_cluster_dtls");

                entity.HasIndex(e => new { e.CldCode, e.CldDepotCode, e.CldTerritoryCode, e.CldZoneCode })
                    .HasName("IX6_cluster_dtls");

                entity.HasIndex(e => new { e.CldDepotCode, e.CldTerritoryCode, e.CldZoneCode, e.CldCode, e.CldDealerCode })
                    .HasName("IX7_cluster_dtls");

                entity.Property(e => e.CldCode).IsUnicode(false);

                entity.Property(e => e.CldDepotCode).IsUnicode(false);

                entity.Property(e => e.CldDealerCode).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CldTerritoryCode).IsUnicode(false);

                entity.Property(e => e.CldZoneCode).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<ClusterHdr>(entity =>
            {
                entity.HasIndex(e => e.ClCode)
                    .HasName("IX1_cluster_hdr");

                entity.HasIndex(e => e.ClDivision)
                    .HasName("IX2_cluster_hdr");

                entity.HasIndex(e => new { e.ClDivision, e.Active })
                    .HasName("IX3_cluster_hdr");

                entity.Property(e => e.ClCode).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ClDescription).IsUnicode(false);

                entity.Property(e => e.ClDivision).IsUnicode(false);

                entity.Property(e => e.ClLegalEntity).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<CompanyAssociates>(entity =>
            {
                entity.HasKey(e => new { e.CaId, e.CaCmId, e.CaLvlCode })
                    .HasName("PK_chain_mstr");

                entity.Property(e => e.CaId).IsUnicode(false);

                entity.Property(e => e.CaCmId).IsUnicode(false);

                entity.Property(e => e.CaLvlCode).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CaDesignation).IsUnicode(false);

                entity.Property(e => e.CaEmail).IsUnicode(false);

                entity.Property(e => e.CaFirstName).IsUnicode(false);

                entity.Property(e => e.CaGender).IsUnicode(false);

                entity.Property(e => e.CaLastName).IsUnicode(false);

                entity.Property(e => e.CaLinkCode).IsUnicode(false);

                entity.Property(e => e.CaMiddleName).IsUnicode(false);

                entity.Property(e => e.CaPrimaryContactNo).IsUnicode(false);

                entity.Property(e => e.CaSecondaryContactNo).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<CompanyDealer>(entity =>
            {
                entity.HasKey(e => new { e.CdlCmCode, e.CdlDealer });

                entity.Property(e => e.CdlCmCode).IsUnicode(false);

                entity.Property(e => e.CdlDealer).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CdlId).ValueGeneratedOnAdd();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<CompanyDivision>(entity =>
            {
                entity.HasIndex(e => e.CdvCmCode)
                    .HasName("IX1_company_division");

                entity.HasIndex(e => e.CdvDivision)
                    .HasName("IX2_company_division");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CdvCmCode).IsUnicode(false);

                entity.Property(e => e.CdvDivision).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<CompanyDocs>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CdCmCode).IsUnicode(false);

                entity.Property(e => e.CdDocDescription).IsUnicode(false);

                entity.Property(e => e.CdDocExtension).IsUnicode(false);

                entity.Property(e => e.CdDocPath).IsUnicode(false);

                entity.Property(e => e.CdDocType).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<CompanyHierarchy>(entity =>
            {
                entity.HasKey(e => new { e.LvlId, e.LvlCmId });

                entity.Property(e => e.LvlId).ValueGeneratedOnAdd();

                entity.Property(e => e.LvlCmId).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LvlCode).IsUnicode(false);

                entity.Property(e => e.LvlLowerCode).IsUnicode(false);

                entity.Property(e => e.LvlName).IsUnicode(false);

                entity.Property(e => e.LvlUpperCode).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<CompanyMstr>(entity =>
            {
                entity.HasKey(e => e.CmId)
                    .HasName("PK_company_mstr_1");

                entity.HasIndex(e => e.CmId)
                    .HasName("IX1_company_mstr");

                entity.HasIndex(e => e.CmType)
                    .HasName("IX2_company_mstr");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CmAccountNo).IsUnicode(false);

                entity.Property(e => e.CmAccountTitle).IsUnicode(false);

                entity.Property(e => e.CmAccountType).IsUnicode(false);

                entity.Property(e => e.CmAddressLine1).IsUnicode(false);

                entity.Property(e => e.CmAddressLine2).IsUnicode(false);

                entity.Property(e => e.CmAddressLine3).IsUnicode(false);

                entity.Property(e => e.CmBankBranch).IsUnicode(false);

                entity.Property(e => e.CmBankName).IsUnicode(false);

                entity.Property(e => e.CmBusinessStatus).IsUnicode(false);

                entity.Property(e => e.CmBusinessSubVerticals).IsUnicode(false);

                entity.Property(e => e.CmBusinessVerticals).IsUnicode(false);

                entity.Property(e => e.CmCategory).IsUnicode(false);

                entity.Property(e => e.CmCity).IsUnicode(false);

                entity.Property(e => e.CmDescription).IsUnicode(false);

                entity.Property(e => e.CmFax).IsUnicode(false);

                entity.Property(e => e.CmLegalEntiry).IsUnicode(false);

                entity.Property(e => e.CmMotherCategory).IsUnicode(false);

                entity.Property(e => e.CmMultiLocation).IsUnicode(false);

                entity.Property(e => e.CmName).IsUnicode(false);

                entity.Property(e => e.CmPin).IsUnicode(false);

                entity.Property(e => e.CmPrimaryContactNumber).IsUnicode(false);

                entity.Property(e => e.CmPrimaryEmail).IsUnicode(false);

                entity.Property(e => e.CmRating).IsUnicode(false);

                entity.Property(e => e.CmSapid).IsUnicode(false);

                entity.Property(e => e.CmState).IsUnicode(false);

                entity.Property(e => e.CmTradeLicense).IsUnicode(false);

                entity.Property(e => e.CmType).IsUnicode(false);

                entity.Property(e => e.CmVatno).IsUnicode(false);

                entity.Property(e => e.CmWebsite).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<CustomerFeedbackOtp>(entity =>
            {
                entity.Property(e => e.CfoConsumed)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CfoLeadGuid).IsUnicode(false);

                entity.Property(e => e.CfoMobileNo).IsUnicode(false);

                entity.Property(e => e.CfoOtp).IsUnicode(false);

                entity.Property(e => e.CreatedBy).IsUnicode(false);
            });

            modelBuilder.Entity<DashboardLead>(entity =>
            {
                entity.HasKey(e => new { e.DlYear, e.DlMonth, e.DlUser, e.DlStage, e.DlDivision })
                    .HasName("PK_dashboard_lead_1");

                entity.Property(e => e.DlYear).IsUnicode(false);

                entity.Property(e => e.DlMonth).IsUnicode(false);

                entity.Property(e => e.DlUser).IsUnicode(false);

                entity.Property(e => e.DlStage).IsUnicode(false);

                entity.Property(e => e.DlDivision).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<DealerPositionRegistration>(entity =>
            {
                entity.Property(e => e.DprDealerId).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.DprAccuracy).IsUnicode(false);

                entity.Property(e => e.DprLat).IsUnicode(false);

                entity.Property(e => e.DprLong).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<DealerPositionRegistrationUpdatedDeleted>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.DprAccuracy).IsUnicode(false);

                entity.Property(e => e.DprDealerId).IsUnicode(false);

                entity.Property(e => e.DprId).ValueGeneratedOnAdd();

                entity.Property(e => e.DprLat).IsUnicode(false);

                entity.Property(e => e.DprLong).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<DistrictMstr>(entity =>
            {
                entity.HasKey(e => new { e.DstName, e.DstBibhagId })
                    .HasName("PK_district_mstr_1");

                entity.Property(e => e.DstName).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DstId).ValueGeneratedOnAdd();

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<DivisionMapping>(entity =>
            {
                entity.HasKey(e => new { e.DmLmsDivisionCode, e.DmMstrDivisionCode });

                entity.Property(e => e.DmLmsDivisionCode).IsUnicode(false);

                entity.Property(e => e.DmMstrDivisionCode).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.DmId).ValueGeneratedOnAdd();

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<FaqMstr>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.FaqDivisionCode).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<FinYear>(entity =>
            {
                entity.Property(e => e.FinYear1).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.FinCurrent)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<FlashNews>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.FlashMsg).IsUnicode(false);

                entity.Property(e => e.FlashUserid).IsUnicode(false);

                entity.Property(e => e.LdDivision).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<FormMenuMstr>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.FafaIcon).IsUnicode(false);

                entity.Property(e => e.FmmLink).IsUnicode(false);

                entity.Property(e => e.FmmName).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<HdItemMstr>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.ItemBrand)
                    .HasName("IX3_hd_item_mstr");

                entity.HasIndex(e => e.ItemFloor)
                    .HasName("IX2_hd_item_mstr");

                entity.HasIndex(e => e.ItemSurface)
                    .HasName("IX1_hd_item_mstr");

                entity.HasIndex(e => new { e.ItemPackage, e.ItemSurface, e.ItemFloor, e.ItemBase, e.ItemBrand })
                    .HasName("IX4_hd_item_mstr");

                entity.Property(e => e.ItemBase).IsUnicode(false);

                entity.Property(e => e.ItemBrand).IsUnicode(false);

                entity.Property(e => e.ItemFloor).IsUnicode(false);

                entity.Property(e => e.ItemId).IsUnicode(false);

                entity.Property(e => e.ItemPackage).IsUnicode(false);

                entity.Property(e => e.ItemSurface).IsUnicode(false);
            });

            modelBuilder.Entity<HelpLine>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.HlpEmail).IsUnicode(false);

                entity.Property(e => e.HlpPhone).IsUnicode(false);

                entity.Property(e => e.HplName).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<ItemBrandMapping>(entity =>
            {
                entity.HasKey(e => e.ItemBrandCode)
                    .HasName("PK_item_brand_mapping_1");

                entity.Property(e => e.ItemBrandCode).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ItemGroupCode).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<ItemFormulaApplicationDtl>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ItemApplicationCode).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<ItemFormulaDtl>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ItemId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<ItemFormulaHdr>(entity =>
            {
                entity.HasKey(e => e.ItemFormulaCode)
                    .HasName("PK_item_formula_hdr_1");

                entity.Property(e => e.ItemFormulaCode).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ItemFormulaAttribute1).IsUnicode(false);

                entity.Property(e => e.ItemFormulaId).ValueGeneratedOnAdd();

                entity.Property(e => e.ItemFormulaName).IsUnicode(false);

                entity.Property(e => e.ItemFormulaPackage).IsUnicode(false);

                entity.Property(e => e.ItemFormulaShade).IsUnicode(false);

                entity.Property(e => e.ItemFormulaSurface).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<ItemFormulaSupervisionChargeDtl>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ItemSupervisionCode).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<ItemPackSize>(entity =>
            {
                entity.HasIndex(e => e.ItemCategory)
                    .HasName("IX9_item_pack_size");

                entity.HasIndex(e => e.ItemId)
                    .HasName("IX6_item_pack_size");

                entity.HasIndex(e => e.ItemShadeCode)
                    .HasName("IX5_item_pack_size");

                entity.HasIndex(e => new { e.Active, e.ItemHdIndicator, e.ItemCategory })
                    .HasName("IX11_item_pack_size");

                entity.HasIndex(e => new { e.ItemGroupCode, e.ItemCategory, e.ItemHdIndicator })
                    .HasName("IX10_item_pack_size");

                entity.HasIndex(e => new { e.ItemGroupCode, e.ItemShadeCode, e.Active })
                    .HasName("IX7_item_pack_size");

                entity.HasIndex(e => new { e.ItemHdIndicator, e.ItemGroupCode, e.Active })
                    .HasName("IX2_item_pack_size");

                entity.HasIndex(e => new { e.ItemCategory, e.ItemGroupCode, e.ItemHdIndicator, e.Active })
                    .HasName("IX3_item_pack_size");

                entity.HasIndex(e => new { e.ItemGroupCode, e.ItemShadeCode, e.Active, e.ItemHdIndicator })
                    .HasName("IX8_item_pack_size");

                entity.HasIndex(e => new { e.ItemHdIndicator, e.Active, e.ItemGroupCode, e.ItemShadeName })
                    .HasName("IX1_item_pack_size");

                entity.Property(e => e.ItemId).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ItemCategory).IsUnicode(false);

                entity.Property(e => e.ItemGroupCode).IsUnicode(false);

                entity.Property(e => e.ItemHdIndicator).IsUnicode(false);

                entity.Property(e => e.ItemHdrId).ValueGeneratedOnAdd();

                entity.Property(e => e.ItemShadeCode).IsUnicode(false);

                entity.Property(e => e.ItemShadeName).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<ItemRecipeApplicationDtl>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ItemApplicationCode).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<ItemRecipeDtl>(entity =>
            {
                entity.HasKey(e => e.ItemRecipeLineId)
                    .HasName("PK_item_recipe_1");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ItemBrandCode).IsUnicode(false);

                entity.Property(e => e.ItemGroupCode).IsUnicode(false);

                entity.Property(e => e.ItemId).IsUnicode(false);

                entity.Property(e => e.ItemTypeCode).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<ItemRecipeHdr>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ItemRecipeCode).IsUnicode(false);

                entity.Property(e => e.ItemRecipeName).IsUnicode(false);

                entity.Property(e => e.ItemRecipePackage).IsUnicode(false);

                entity.Property(e => e.ItemRecipeShade).IsUnicode(false);

                entity.Property(e => e.ItemRecipeSurface).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<ItemRecipeSupervisionChargeDtl>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ItemSupervisionCode).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<JourneyPlanningDetails>(entity =>
            {
                entity.HasKey(e => new { e.JpdDate, e.JpdUser, e.JpdSrl });

                entity.Property(e => e.JpdUser).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.JpdActId).IsUnicode(false);

                entity.Property(e => e.JpdActType).IsUnicode(false);

                entity.Property(e => e.JpdVisited)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<JourneyPlanningHeader>(entity =>
            {
                entity.HasKey(e => new { e.JphDate, e.JphUser });

                entity.Property(e => e.JphUser).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.JphApprovalRemarks).IsUnicode(false);

                entity.Property(e => e.JphApprovalStatus).IsUnicode(false);

                entity.Property(e => e.JphApprovedBy).IsUnicode(false);

                entity.Property(e => e.JphDeviationOccured)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.JphRemarks).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadApplicableCluster>(entity =>
            {
                entity.HasKey(e => new { e.LacLeadDetailGuid, e.LacLeadDetailId, e.LacClCode, e.LacGroupId, e.LacActivatedDate });

                entity.HasIndex(e => e.LacLeadDetailGuid)
                    .HasName("IX1_lead_applicable_cluster");

                entity.HasIndex(e => new { e.LacClCode, e.LacGroupId })
                    .HasName("IX3_lead_applicable_cluster");

                entity.HasIndex(e => new { e.LacClCode, e.LacLeadDetailGuid })
                    .HasName("IX2_lead_applicable_cluster");

                entity.Property(e => e.LacLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LacLeadDetailId).IsUnicode(false);

                entity.Property(e => e.LacClCode).IsUnicode(false);

                entity.Property(e => e.LacGroupId).IsUnicode(false);

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.LacId).ValueGeneratedOnAdd();

                entity.Property(e => e.LacUserId).IsUnicode(false);
            });

            modelBuilder.Entity<LeadApplicableClusterAddtional>(entity =>
            {
                entity.HasKey(e => new { e.LacaLeadDetailGuid, e.LacaLeadDetailId, e.LacaDivision, e.LacaDepotCode, e.LacaClCode, e.LacaGroupId, e.LacaUserId });

                entity.HasIndex(e => e.LacaLeadDetailGuid)
                    .HasName("IX1_lead_applicable_cluster_addtional");

                entity.HasIndex(e => new { e.LacaDivision, e.LacaGroupId, e.LacaUserId })
                    .HasName("IX3_lead_applicable_cluster_addtional");

                entity.HasIndex(e => new { e.LacaDivision, e.LacaDepotCode, e.LacaClCode, e.LacaGroupId, e.LacaUserId })
                    .HasName("IX2_lead_applicable_cluster_addtional");

                entity.Property(e => e.LacaLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LacaLeadDetailId).IsUnicode(false);

                entity.Property(e => e.LacaDivision).IsUnicode(false);

                entity.Property(e => e.LacaDepotCode).IsUnicode(false);

                entity.Property(e => e.LacaClCode).IsUnicode(false);

                entity.Property(e => e.LacaUserId).IsUnicode(false);

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.LacaId).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<LeadApplicationPainterPayment>(entity =>
            {
                entity.HasIndex(e => e.LappLeadDetailGuid)
                    .HasName("IX1_lead_application_painter_payment");

                entity.HasIndex(e => e.LappPainterId)
                    .HasName("IX3_lead_application_painter_payment");

                entity.HasIndex(e => e.LappPaymentId)
                    .HasName("IX2_lead_application_painter_payment");

                entity.HasIndex(e => e.LappRequisitionId)
                    .HasName("IX4_lead_application_painter_payment");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LappInstrumentNo).IsUnicode(false);

                entity.Property(e => e.LappInvoiceNo).IsUnicode(false);

                entity.Property(e => e.LappLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LappLeadDetailId).IsUnicode(false);

                entity.Property(e => e.LappPainterId).IsUnicode(false);

                entity.Property(e => e.LappPaymentMode).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadApplicationRequirement>(entity =>
            {
                entity.HasIndex(e => e.LarQuotationHdrId)
                    .HasName("IX1_lead_application_requirement");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LarItemApplicationCode).IsUnicode(false);
            });

            modelBuilder.Entity<LeadApplicationRequisitionDtl>(entity =>
            {
                entity.HasIndex(e => e.LardtHeaderId)
                    .HasName("IX1_lead_application_requisition_dtl");

                entity.HasIndex(e => e.LardtQuotationHdrId)
                    .HasName("IX2_lead_application_requisition_dtl");

                entity.HasIndex(e => new { e.LardtHeaderId, e.LardtQuotationHdrId })
                    .HasName("IX3_lead_application_requisition_dtl");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LardtApplicationCode).IsUnicode(false);

                entity.Property(e => e.LardtItemId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadApplicationRequisitionHdr>(entity =>
            {
                entity.HasIndex(e => e.LarhId)
                    .HasName("IX2_lead_application_requisition_hdr");

                entity.HasIndex(e => e.LarhLeadDetailGuid)
                    .HasName("IX1_lead_application_requisition_hdr");

                entity.HasIndex(e => e.LarhPaintnerId)
                    .HasName("IX4_lead_application_requisition_hdr");

                entity.HasIndex(e => e.LarhQuotationHdrId)
                    .HasName("IX3_lead_application_requisition_hdr");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LarhLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LarhLeadDetailId).IsUnicode(false);

                entity.Property(e => e.LarhPaintnerId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadAssignmentHistory>(entity =>
            {
                entity.HasKey(e => new { e.LahLeadDetailGuid, e.LahClCode });

                entity.HasIndex(e => e.LahClCode)
                    .HasName("IX2_lead_assignment_history");

                entity.HasIndex(e => e.LahLeadDetailGuid)
                    .HasName("IX1_lead_assignment_history");

                entity.Property(e => e.LahLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LahClCode).IsUnicode(false);

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.LahLeadDetailId).IsUnicode(false);
            });

            modelBuilder.Entity<LeadComplaintLog>(entity =>
            {
                entity.HasIndex(e => e.LclLcmSrId)
                    .HasName("IX1_lead_complaint_log");

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.LcmActionTaken).IsUnicode(false);
            });

            modelBuilder.Entity<LeadComplaintMstr>(entity =>
            {
                entity.HasKey(e => e.LcmSrId)
                    .HasName("PK_lead_complaint_hdr");

                entity.HasIndex(e => e.LcmLeadDetailGuid)
                    .HasName("IX1_lead_complaint_mstr");

                entity.HasIndex(e => e.LcmSrId)
                    .HasName("IX2_lead_complaint_mstr");

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.LcmDescription).IsUnicode(false);

                entity.Property(e => e.LcmLastActionTaken).IsUnicode(false);

                entity.Property(e => e.LcmLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LcmLeadDetailId).IsUnicode(false);

                entity.Property(e => e.LcmSrCode).IsUnicode(false);

                entity.Property(e => e.LcmStatus).IsUnicode(false);

                entity.Property(e => e.LcmSubType).IsUnicode(false);

                entity.Property(e => e.LcmType).IsUnicode(false);
            });

            modelBuilder.Entity<LeadContacts>(entity =>
            {
                entity.HasKey(e => new { e.LdcLeadGuid, e.LdcSrl, e.LdcPrimaryNo })
                    .HasName("PK_lead_contacts_1");

                entity.HasIndex(e => e.LdcLeadGuid)
                    .HasName("IX1_lead_contacts");

                entity.Property(e => e.LdcLeadGuid).IsUnicode(false);

                entity.Property(e => e.LdcPrimaryNo).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LdcContactType).IsUnicode(false);

                entity.Property(e => e.LdcDesignation).IsUnicode(false);

                entity.Property(e => e.LdcEmailId).IsUnicode(false);

                entity.Property(e => e.LdcLeadId).IsUnicode(false);

                entity.Property(e => e.LdcName).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadCustomerFeedbackDtl>(entity =>
            {
                entity.HasKey(e => new { e.LcfdLcfHdrId, e.LcfdQuesId })
                    .HasName("PK_lead_customer_feedback_dtl_1");

                entity.HasIndex(e => e.LcfdLcfHdrId)
                    .HasName("IX1_lead_customer_feedback_dtl");

                entity.HasIndex(e => new { e.LcfdLcfHdrId, e.LcfdQuesId })
                    .HasName("IX2_lead_customer_feedback_dtl");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LcfdRemarksCust).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadCustomerFeedbackHdr>(entity =>
            {
                entity.HasKey(e => e.LcfHdrId)
                    .HasName("PK_lead_customer_feedback_hdr_1");

                entity.HasIndex(e => e.LcfHdrId)
                    .HasName("IX2_lead_customer_feedback_hdr");

                entity.HasIndex(e => e.LcfLeadDetailGuid)
                    .HasName("IX1_lead_customer_feedback_hdr");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LcfLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LcfLeadDetailId).IsUnicode(false);

                entity.Property(e => e.LcfMode).IsUnicode(false);

                entity.Property(e => e.LcfOtp).IsUnicode(false);

                entity.Property(e => e.LcfRemarksCust).IsUnicode(false);

                entity.Property(e => e.LcfRemarksVerified).IsUnicode(false);

                entity.Property(e => e.LcfVerified).IsUnicode(false);

                entity.Property(e => e.LcfVerifiedBy).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadCustomerFeedbackQuestion>(entity =>
            {
                entity.HasKey(e => e.QuesId)
                    .HasName("PK_customer_feedback_question");

                entity.HasIndex(e => e.QuesDivision)
                    .HasName("NonClusteredIndex-20181030-193422");

                entity.HasIndex(e => e.QuesId)
                    .HasName("IX3_lead_customer_feedback_question");

                entity.HasIndex(e => new { e.QuesDivision, e.Active })
                    .HasName("IX2_lead_customer_feedback_question");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.QuesDesc).IsUnicode(false);

                entity.Property(e => e.QuesDivision).IsUnicode(false);
            });

            modelBuilder.Entity<LeadCustomerFeedbackQuestionAnswer>(entity =>
            {
                entity.HasKey(e => e.AnsId)
                    .HasName("PK_customer_feedback_question_answer");

                entity.HasIndex(e => e.AnsQuesId)
                    .HasName("IX1_lead_customer_feedback_question_answer");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.AnsDesc).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadDocs>(entity =>
            {
                entity.HasKey(e => new { e.LdLeadDetailGuid, e.LdDocSrlNo });

                entity.HasIndex(e => e.LdLeadDetailGuid)
                    .HasName("IX1_lead_docs");

                entity.Property(e => e.LdLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LdDocSrlNo).ValueGeneratedOnAdd();

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LdDocDescription).IsUnicode(false);

                entity.Property(e => e.LdDocExtension).IsUnicode(false);

                entity.Property(e => e.LdDocPath).IsUnicode(false);

                entity.Property(e => e.LdDocType).IsUnicode(false);

                entity.Property(e => e.LdLeadDetailId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadEstimateDtl>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LedLocationCode).IsUnicode(false);

                entity.Property(e => e.LedProductId).IsUnicode(false);

                entity.Property(e => e.LedSurfaceCode).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadEstimateHdr>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LehAttributeId).IsUnicode(false);

                entity.Property(e => e.LehLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LehLeadDetailId).IsUnicode(false);

                entity.Property(e => e.LehPackageId).IsUnicode(false);

                entity.Property(e => e.LehSurfaceId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadEstimationPaymentTerms>(entity =>
            {
                entity.HasIndex(e => e.LeptHeaderId)
                    .HasName("IX1_lead_estimation_payment_terms");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LeptPaymentTerms).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadEstimationTermsConditions>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LetcTermsConditions).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadEvaluationActivity>(entity =>
            {
                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.LemActivityDesc).IsUnicode(false);
            });

            modelBuilder.Entity<LeadEvaluationDtl>(entity =>
            {
                entity.HasKey(e => new { e.LedHdrId, e.LedActivityId });

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.LedPresentCondition).IsUnicode(false);

                entity.Property(e => e.LedStandardRange).IsUnicode(false);

                entity.Property(e => e.LedTreatmentRequired).IsUnicode(false);
            });

            modelBuilder.Entity<LeadEvaluationHdr>(entity =>
            {
                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.LehLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LehLeadDetailId).IsUnicode(false);

                entity.Property(e => e.LehPaintingSurface).IsUnicode(false);

                entity.Property(e => e.LehPaintingType).IsUnicode(false);
            });

            modelBuilder.Entity<LeadGeoLocationRegistration>(entity =>
            {
                entity.HasKey(e => new { e.GlrLeadDtlGuid, e.GlrTypeId });

                entity.HasIndex(e => e.GlrLeadDtlGuid)
                    .HasName("IX1_lead_geo_location_registration");

                entity.Property(e => e.GlrLeadDtlGuid).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.GlrAccuracy).IsUnicode(false);

                entity.Property(e => e.GlrLatitude).IsUnicode(false);

                entity.Property(e => e.GlrLongitude).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadGeoLocationUpdation>(entity =>
            {
                entity.HasIndex(e => e.GluLeadDtlGuid)
                    .HasName("IX1_lead_geo_location_updation");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.GluAccuracy).IsUnicode(false);

                entity.Property(e => e.GluLatitude).IsUnicode(false);

                entity.Property(e => e.GluLeadDtlGuid).IsUnicode(false);

                entity.Property(e => e.GluLongitude).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadInvoiceLink>(entity =>
            {
                entity.HasKey(e => e.LilTrxId)
                    .HasName("PK_lead_invoice_link_1");

                entity.HasIndex(e => e.LilLeadDetailGuid)
                    .HasName("IX1_lead_invoice_link");

                entity.Property(e => e.LilTrxId).IsUnicode(false);

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LilId).ValueGeneratedOnAdd();

                entity.Property(e => e.LilLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LilLeadDetailId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadLostValue>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LlvItemGroup).IsUnicode(false);

                entity.Property(e => e.LlvLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMaterialAllocationDtl>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LmadProductId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMaterialAllocationHdr>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LmahDlrId).IsUnicode(false);

                entity.Property(e => e.LmahLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LmahReceivedStatus).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMaterialRequirementDtl>(entity =>
            {
                entity.HasKey(e => e.ImrdSrlId)
                    .HasName("PK_lead_material_requirement_dtl_1");

                entity.HasIndex(e => e.ImrdSrlId)
                    .HasName("IX2_lead_material_requirement_dtl");

                entity.HasIndex(e => e.LmrdQuoutationHdrId)
                    .HasName("IX1_lead_material_requirement_dtl");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LmrdBaseShade).IsUnicode(false);

                entity.Property(e => e.LmrdItemGroupCode).IsUnicode(false);

                entity.Property(e => e.LmrdItemId).IsUnicode(false);

                entity.Property(e => e.LmrdItemShadeCode).IsUnicode(false);

                entity.Property(e => e.LmrdQuotationShadeCode).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMaterialRequirementHdr>(entity =>
            {
                entity.HasIndex(e => e.LqidLeadDetailGuid)
                    .HasName("IX1_lead_material_requirement_hdr");

                entity.HasIndex(e => e.LqidQuoutationHdrId)
                    .HasName("IX2_lead_material_requirement_hdr");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LqidBase).IsUnicode(false);

                entity.Property(e => e.LqidFgShadeCode).IsUnicode(false);

                entity.Property(e => e.LqidItemGroupCode).IsUnicode(false);

                entity.Property(e => e.LqidItemId).IsUnicode(false);

                entity.Property(e => e.LqidItemTypeCode).IsUnicode(false);

                entity.Property(e => e.LqidLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LqidLeadDetailId).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMaterialRequisionDtl>(entity =>
            {
                entity.HasIndex(e => e.LmrdtHeaderId)
                    .HasName("IX1_lead_material_requision_dtl");

                entity.HasIndex(e => e.LmrdtQuotationHdrId)
                    .HasName("IX2_lead_material_requision_dtl");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LmrdtBase).IsUnicode(false);

                entity.Property(e => e.LmrdtItemGroupCode).IsUnicode(false);

                entity.Property(e => e.LmrdtItemId).IsUnicode(false);

                entity.Property(e => e.LmrdtItemPackSize).IsUnicode(false);

                entity.Property(e => e.LmrdtItemShadeCode).IsUnicode(false);

                entity.Property(e => e.LmrdtQuotationShadeCode).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMaterialRequisionHdr>(entity =>
            {
                entity.HasIndex(e => e.LmrhLeadDetailGuid)
                    .HasName("IX1_lead_material_requision_hdr");

                entity.HasIndex(e => e.LmrhQuotationHdrId)
                    .HasName("IX2_lead_material_requision_hdr");

                entity.HasIndex(e => new { e.LmrhId, e.LmrhLeadDetailGuid })
                    .HasName("IX4_lead_material_requision_hdr");

                entity.HasIndex(e => new { e.LmrhLeadDetailGuid, e.LmrhDlrId })
                    .HasName("IX3_lead_material_requision_hdr");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LmrhDlrId).IsUnicode(false);

                entity.Property(e => e.LmrhLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LmrhLeadDetailId).IsUnicode(false);

                entity.Property(e => e.LmrhReceivedStatus).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMaterialRequisionItemReceivedDtl>(entity =>
            {
                entity.HasIndex(e => e.LmrirdHeaderId)
                    .HasName("IX1_lead_material_requision_item_received_dtl");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LmrirdBillStatus).IsUnicode(false);

                entity.Property(e => e.LmrirdProductId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMaterialRequisionItemReceivedHdr>(entity =>
            {
                entity.HasIndex(e => e.LmrirhLeadDetailGuid)
                    .HasName("IX1_lead_material_requision_item_received_hdr");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LmrirhAllocationId).IsUnicode(false);

                entity.Property(e => e.LmrirhDlrId).IsUnicode(false);

                entity.Property(e => e.LmrirhLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LmrirhLeadDetailId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMaterialSupplier>(entity =>
            {
                entity.HasKey(e => new { e.LmsLeadDetailGuid, e.LmsDlrId });

                entity.HasIndex(e => e.LmsLeadDetailGuid)
                    .HasName("IX1_lead_material_supplier");

                entity.HasIndex(e => new { e.LmsLeadDetailGuid, e.LmsClusterId })
                    .HasName("IX3_lead_material_supplier");

                entity.HasIndex(e => new { e.LmsLeadDetailGuid, e.LmsDlrId })
                    .HasName("IX2_lead_material_supplier");

                entity.Property(e => e.LmsLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LmsDlrId).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LmsClusterId).IsUnicode(false);

                entity.Property(e => e.LmsLeadDetailId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMaterialSupplierBillDtl>(entity =>
            {
                entity.HasIndex(e => e.LmsbdHeaderId)
                    .HasName("IX1_lead_material_supplier_bill_dtl");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LmsbdProductId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMaterialSupplierBillHdr>(entity =>
            {
                entity.HasIndex(e => e.LmsbhLeadDetailGuid)
                    .HasName("IX1_lead_material_supplier_bill_hdr");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LmsbhDlrId).IsUnicode(false);

                entity.Property(e => e.LmsbhDocExtension).IsUnicode(false);

                entity.Property(e => e.LmsbhDocGuid).IsUnicode(false);

                entity.Property(e => e.LmsbhDocPath).IsUnicode(false);

                entity.Property(e => e.LmsbhInvoiceNo).IsUnicode(false);

                entity.Property(e => e.LmsbhLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LmsbhLeadDetailId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMaterialSupplierPayment>(entity =>
            {
                entity.HasIndex(e => e.LmspLeadDetailGuid)
                    .HasName("IX1_lead_material_supplier_payment");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LmspAccountNo).IsUnicode(false);

                entity.Property(e => e.LmspBankName).IsUnicode(false);

                entity.Property(e => e.LmspBranchName).IsUnicode(false);

                entity.Property(e => e.LmspDlrId).IsUnicode(false);

                entity.Property(e => e.LmspIfscCode).IsUnicode(false);

                entity.Property(e => e.LmspInstrumentNo).IsUnicode(false);

                entity.Property(e => e.LmspLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LmspLeadDetailId).IsUnicode(false);

                entity.Property(e => e.LmspPaymentMode).IsUnicode(false);

                entity.Property(e => e.LmspStatus).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMaterialsupplyDtl>(entity =>
            {
                entity.HasKey(e => new { e.LvdHdrId, e.LvdItemId });

                entity.HasIndex(e => e.LvdHdrId)
                    .HasName("IX1_lead_materialsupply_dtl");

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.BaseCode).IsUnicode(false);

                entity.Property(e => e.CategoryCode).IsUnicode(false);

                entity.Property(e => e.ItemGroupCode).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMaterialsupplyHdr>(entity =>
            {
                entity.HasIndex(e => e.LvhLeadDetailGuid)
                    .HasName("IX1_lead_materialsupply_hdr");

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.LvhDealerCode).IsUnicode(false);

                entity.Property(e => e.LvhLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LvhLeadDetailId).IsUnicode(false);

                entity.Property(e => e.LvhWarehouse).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMstrCompetitor>(entity =>
            {
                entity.HasIndex(e => e.LcLeadGuid)
                    .HasName("IX1_lead_mstr_competitor");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LcCompetitorCode).IsUnicode(false);

                entity.Property(e => e.LcLeadGuid).IsUnicode(false);

                entity.Property(e => e.LcLeadId).IsUnicode(false);

                entity.Property(e => e.LcPositionCode).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMstrDtl>(entity =>
            {
                entity.HasIndex(e => e.LdDivision)
                    .HasName("IX5_lead_mstr_dtl");

                entity.HasIndex(e => e.LdLeadDetailGuid)
                    .HasName("IX3_lead_mstr_dtl");

                entity.HasIndex(e => e.LdLeadDetailId)
                    .HasName("IX4_lead_mstr_dtl");

                entity.HasIndex(e => e.LdLeadGuid)
                    .HasName("IX1_lead_mstr_dtl");

                entity.HasIndex(e => e.LdLeadId)
                    .HasName("IX2_lead_mstr_dtl");

                entity.Property(e => e.LdLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LdAttribute1).IsUnicode(false);

                entity.Property(e => e.LdAttribute2).IsUnicode(false);

                entity.Property(e => e.LdAttribute3).IsUnicode(false);

                entity.Property(e => e.LdAttribute4).IsUnicode(false);

                entity.Property(e => e.LdAttribute5).IsUnicode(false);

                entity.Property(e => e.LdClCode).IsUnicode(false);

                entity.Property(e => e.LdCsat).IsUnicode(false);

                entity.Property(e => e.LdDescription).IsUnicode(false);

                entity.Property(e => e.LdDivision).IsUnicode(false);

                entity.Property(e => e.LdJobLostCompetitor).IsUnicode(false);

                entity.Property(e => e.LdJobLostKeyInfluencer).IsUnicode(false);

                entity.Property(e => e.LdJobLostReason).IsUnicode(false);

                entity.Property(e => e.LdJobRemarks).IsUnicode(false);

                entity.Property(e => e.LdJobType).IsUnicode(false);

                entity.Property(e => e.LdLeadDetailId).IsUnicode(false);

                entity.Property(e => e.LdLeadGuid).IsUnicode(false);

                entity.Property(e => e.LdLeadId).IsUnicode(false);

                entity.Property(e => e.LdPriority).IsUnicode(false);

                entity.Property(e => e.LdStage).IsUnicode(false);

                entity.Property(e => e.LdSupplyMode).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMstrDtlSurface>(entity =>
            {
                entity.HasIndex(e => e.LdsLeadDetailGuid)
                    .HasName("IX1_lead_mstr_dtl_surface");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LdsLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LdsLeadDetailId).IsUnicode(false);

                entity.Property(e => e.LdsSurfaceSqft).IsUnicode(false);

                entity.Property(e => e.LdsSurfaceType).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadMstrHdr>(entity =>
            {
                entity.HasIndex(e => e.LdLeadGuid)
                    .HasName("IX1_lead_mstr_hdr");

                entity.HasIndex(e => e.LdLeadId)
                    .HasName("IX2_lead_mstr_hdr");

                entity.Property(e => e.LdLeadGuid).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LdAddressLine1).IsUnicode(false);

                entity.Property(e => e.LdAddressLine2).IsUnicode(false);

                entity.Property(e => e.LdAddressLine3).IsUnicode(false);

                entity.Property(e => e.LdAttribute1).IsUnicode(false);

                entity.Property(e => e.LdAttribute2).IsUnicode(false);

                entity.Property(e => e.LdAttribute3).IsUnicode(false);

                entity.Property(e => e.LdAttribute4).IsUnicode(false);

                entity.Property(e => e.LdAttribute5).IsUnicode(false);

                entity.Property(e => e.LdCity).IsUnicode(false);

                entity.Property(e => e.LdCondition).IsUnicode(false);

                entity.Property(e => e.LdCsat).IsUnicode(false);

                entity.Property(e => e.LdDescription).IsUnicode(false);

                entity.Property(e => e.LdLeadId).IsUnicode(false);

                entity.Property(e => e.LdLeadOpportunity).IsUnicode(false);

                entity.Property(e => e.LdLocation).IsUnicode(false);

                entity.Property(e => e.LdName).IsUnicode(false);

                entity.Property(e => e.LdOrganization).IsUnicode(false);

                entity.Property(e => e.LdPin).IsUnicode(false);

                entity.Property(e => e.LdPrimaryContactNumber).IsUnicode(false);

                entity.Property(e => e.LdPrimaryEmail).IsUnicode(false);

                entity.Property(e => e.LdPriority).IsUnicode(false);

                entity.Property(e => e.LdSource).IsUnicode(false);

                entity.Property(e => e.LdStage).IsUnicode(false);

                entity.Property(e => e.LdState).IsUnicode(false);

                entity.Property(e => e.LdType).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadNotifcation>(entity =>
            {
                entity.HasKey(e => new { e.LnId, e.LnLeadDetailsGuid, e.LnLeadDetailsId });

                entity.HasIndex(e => e.LnLeadDetailsGuid)
                    .HasName("IX1_lead_notifcation");

                entity.Property(e => e.LnId).ValueGeneratedOnAdd();

                entity.Property(e => e.LnLeadDetailsGuid).IsUnicode(false);

                entity.Property(e => e.LnLeadDetailsId).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LnDesc).IsUnicode(false);

                entity.Property(e => e.LnPushed)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LnType).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadNotifcationDtl>(entity =>
            {
                entity.HasKey(e => new { e.LndId, e.LndLeadDetailsGuid, e.LndLeadDetailsId });

                entity.HasIndex(e => e.LndLeadDetailsGuid)
                    .HasName("IX2_lead_notifcation_dtl");

                entity.HasIndex(e => e.LnhId)
                    .HasName("IX1_lead_notifcation_dtl");

                entity.Property(e => e.LndId).ValueGeneratedOnAdd();

                entity.Property(e => e.LndLeadDetailsGuid).IsUnicode(false);

                entity.Property(e => e.LndLeadDetailsId).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LndCluster).IsUnicode(false);

                entity.Property(e => e.LndDivision).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadNotifcationHdr>(entity =>
            {
                entity.HasKey(e => new { e.LnhId, e.LnhLeadDetailsGuid, e.LnhLeadDetailsId });

                entity.HasIndex(e => e.LnhLeadDetailsGuid)
                    .HasName("IX1_lead_notifcation_hdr");

                entity.Property(e => e.LnhId).ValueGeneratedOnAdd();

                entity.Property(e => e.LnhLeadDetailsGuid).IsUnicode(false);

                entity.Property(e => e.LnhLeadDetailsId).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LnhDesc).IsUnicode(false);

                entity.Property(e => e.LnhDetails).IsUnicode(false);

                entity.Property(e => e.LnhPushed)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LnhType).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadPartners>(entity =>
            {
                entity.HasKey(e => new { e.LpLeadDetailGuid, e.LpPartnerContactId });

                entity.HasIndex(e => e.LpLeadDetailGuid)
                    .HasName("IX1_lead_partners");

                entity.Property(e => e.LpLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LpPartnerContactId).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LpClusterId).IsUnicode(false);

                entity.Property(e => e.LpCompanyId).IsUnicode(false);

                entity.Property(e => e.LpDealerId).IsUnicode(false);

                entity.Property(e => e.LpLeadDetailId).IsUnicode(false);

                entity.Property(e => e.LpPartnerId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadPartnersJobAmount>(entity =>
            {
                entity.HasIndex(e => e.LpjaLeadDetailsGuid)
                    .HasName("IX1_lead_partners_job_amount");

                entity.HasIndex(e => new { e.LpjaLeadDetailsGuid, e.LpjaLeadPartnerId })
                    .HasName("IX2_lead_partners_job_amount");

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LpjaLeadDetailsGuid).IsUnicode(false);

                entity.Property(e => e.LpjaLeadDetailsId).IsUnicode(false);

                entity.Property(e => e.LpjaLeadPartnerId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadPartnersPayment>(entity =>
            {
                entity.HasIndex(e => e.LppLeadDetailsGuid)
                    .HasName("IX1_lead_partners_payment");

                entity.HasIndex(e => new { e.LppLeadDetailsGuid, e.LppLeadPartnerId })
                    .HasName("IX2_lead_partners_payment");

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LppAccountNo).IsUnicode(false);

                entity.Property(e => e.LppBankName).IsUnicode(false);

                entity.Property(e => e.LppBranchName).IsUnicode(false);

                entity.Property(e => e.LppIfscCode).IsUnicode(false);

                entity.Property(e => e.LppInstrumentNo).IsUnicode(false);

                entity.Property(e => e.LppLeadDetailsGuid).IsUnicode(false);

                entity.Property(e => e.LppLeadDetailsId).IsUnicode(false);

                entity.Property(e => e.LppLeadPartnerId).IsUnicode(false);

                entity.Property(e => e.LppPaymentMode).IsUnicode(false);

                entity.Property(e => e.LppStatus).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadPaymentApplication>(entity =>
            {
                entity.HasKey(e => e.LpaId)
                    .HasName("PK_Table_1");

                entity.HasIndex(e => e.LpaLeadDetailGuid)
                    .HasName("IX1_lead_payment_application");

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LpaLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LpaLeadDetailId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadPaymentReceipt>(entity =>
            {
                entity.HasKey(e => e.LprId)
                    .HasName("PK_lead_payment_receipt_1");

                entity.HasIndex(e => e.LprLdLeadGuid)
                    .HasName("IX1_lead_payment_receipt");

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LprAccountNo).IsUnicode(false);

                entity.Property(e => e.LprBankName).IsUnicode(false);

                entity.Property(e => e.LprBranchName).IsUnicode(false);

                entity.Property(e => e.LprIfscCode).IsUnicode(false);

                entity.Property(e => e.LprInstrumentNo).IsUnicode(false);

                entity.Property(e => e.LprLdLeadGuid).IsUnicode(false);

                entity.Property(e => e.LprLdLeadId).IsUnicode(false);

                entity.Property(e => e.LprPaymentMode).IsUnicode(false);

                entity.Property(e => e.LprRemarks).IsUnicode(false);

                entity.Property(e => e.LprStatus).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadPaymentReceiptHistory>(entity =>
            {
                entity.HasIndex(e => e.LprhId)
                    .HasName("IX1_lead_payment_receipt_history");

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LprhRemarks).IsUnicode(false);

                entity.Property(e => e.LprhStatus).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadPoDtl>(entity =>
            {
                entity.HasIndex(e => e.LpodHdrId)
                    .HasName("IX1_lead_po_dtl");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LpodItemId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadPoHdr>(entity =>
            {
                entity.HasIndex(e => e.LpoLeadDetailGuid)
                    .HasName("IX1_lead_po_hdr");

                entity.HasIndex(e => e.LpoSpecificationId)
                    .HasName("IX3_lead_po_hdr");

                entity.HasIndex(e => new { e.LpoSpecificationId, e.LpoLeadDetailGuid })
                    .HasName("IX2_lead_po_hdr");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LpoLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LpoSubject).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadProspectMstr>(entity =>
            {
                entity.HasIndex(e => e.LdpDivision)
                    .HasName("IX2_lead_prospect_mstr");

                entity.HasIndex(e => e.LdpLeadGuid)
                    .HasName("IX1_lead_prospect_mstr");

                entity.Property(e => e.LdpLeadGuid).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LdpAddressLine1).IsUnicode(false);

                entity.Property(e => e.LdpAddressLine2).IsUnicode(false);

                entity.Property(e => e.LdpAddressLine3).IsUnicode(false);

                entity.Property(e => e.LdpCondition).IsUnicode(false);

                entity.Property(e => e.LdpDescription).IsUnicode(false);

                entity.Property(e => e.LdpDivision).IsUnicode(false);

                entity.Property(e => e.LdpLeadId).IsUnicode(false);

                entity.Property(e => e.LdpLeadOpportunity).IsUnicode(false);

                entity.Property(e => e.LdpLocation).IsUnicode(false);

                entity.Property(e => e.LdpName).IsUnicode(false);

                entity.Property(e => e.LdpNationalAc).IsUnicode(false);

                entity.Property(e => e.LdpOrganization).IsUnicode(false);

                entity.Property(e => e.LdpPin).IsUnicode(false);

                entity.Property(e => e.LdpPrimaryContactDesignation).IsUnicode(false);

                entity.Property(e => e.LdpPrimaryContactName).IsUnicode(false);

                entity.Property(e => e.LdpPrimaryContactNumber).IsUnicode(false);

                entity.Property(e => e.LdpPrimaryContactType).IsUnicode(false);

                entity.Property(e => e.LdpPrimaryEmail).IsUnicode(false);

                entity.Property(e => e.LdpPriority).IsUnicode(false);

                entity.Property(e => e.LdpRegionalAc).IsUnicode(false);

                entity.Property(e => e.LdpSource).IsUnicode(false);

                entity.Property(e => e.LdpStage).IsUnicode(false);

                entity.Property(e => e.LdpType).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadQuotationDtl>(entity =>
            {
                entity.HasIndex(e => e.LqdHeaderId)
                    .HasName("IX1_lead_quotation_dtl");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LqdBaseShade).IsUnicode(false);

                entity.Property(e => e.LqdBrand).IsUnicode(false);

                entity.Property(e => e.LqdFgShadeCode).IsUnicode(false);

                entity.Property(e => e.LqdItemId).IsUnicode(false);

                entity.Property(e => e.LqdLocationCode).IsUnicode(false);

                entity.Property(e => e.LqdProductId).IsUnicode(false);

                entity.Property(e => e.LqdShadeCode).IsUnicode(false);

                entity.Property(e => e.LqdSurfaceCode).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadQuotationHdr>(entity =>
            {
                entity.HasIndex(e => e.LqhId)
                    .HasName("IX2_lead_quotation_hdr");

                entity.HasIndex(e => e.LqhLeadDetailGuid)
                    .HasName("IX1_lead_quotation_hdr");

                entity.HasIndex(e => new { e.LqhLeadDetailGuid, e.LqhStatus })
                    .HasName("IX3_lead_quotation_hdr");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LqhAttributeId).IsUnicode(false);

                entity.Property(e => e.LqhDealer).IsUnicode(false);

                entity.Property(e => e.LqhDealerAllocationType).IsUnicode(false);

                entity.Property(e => e.LqhLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LqhLeadDetailId).IsUnicode(false);

                entity.Property(e => e.LqhPackageId).IsUnicode(false);

                entity.Property(e => e.LqhRemarks).IsUnicode(false);

                entity.Property(e => e.LqhStatus).IsUnicode(false);

                entity.Property(e => e.LqhSurfaceId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadQuotationPaymentTerms>(entity =>
            {
                entity.HasIndex(e => e.LqptHeaderId)
                    .HasName("IX1_lead_quotation_payment_terms");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LqptPaymentTerms).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadQuotationSilverDtl>(entity =>
            {
                entity.HasIndex(e => e.LqsdHeaderId)
                    .HasName("IX1_lead_quotation_silver_dtl");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LqsdBase).IsUnicode(false);

                entity.Property(e => e.LqsdBrand).IsUnicode(false);

                entity.Property(e => e.LqsdCategory).IsUnicode(false);

                entity.Property(e => e.LqsdLocationCode).IsUnicode(false);

                entity.Property(e => e.LqsdShadeCode).IsUnicode(false);

                entity.Property(e => e.LqsdSurfaceCode).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadQuotationTermsConditions>(entity =>
            {
                entity.HasIndex(e => e.LqtcHeaderId)
                    .HasName("IX1_lead_quotation_terms_conditions");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LqtcTermsConditions).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadRequestMaster>(entity =>
            {
                entity.HasKey(e => new { e.LrmId, e.LrmLeadDetailGuid });

                entity.HasIndex(e => e.LrmLeadDetailGuid)
                    .HasName("IX1_lead_request_master");

                entity.Property(e => e.LrmId).ValueGeneratedOnAdd();

                entity.Property(e => e.LrmLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LrmAction).IsUnicode(false);

                entity.Property(e => e.LrmActionAssignedCluster).IsUnicode(false);

                entity.Property(e => e.LrmActionTakenBy).IsUnicode(false);

                entity.Property(e => e.LrmDescription).IsUnicode(false);

                entity.Property(e => e.LrmRemark).IsUnicode(false);

                entity.Property(e => e.LrmRequestTo).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadSamplingDtl>(entity =>
            {
                entity.HasKey(e => new { e.LsdLeadDetailGuid, e.LsdHeaderId, e.LsdDetailId, e.LsdItemId });

                entity.HasIndex(e => e.LsdHeaderId)
                    .HasName("IX2_lead_sampling_dtl");

                entity.HasIndex(e => e.LsdLeadDetailGuid)
                    .HasName("IX1_lead_sampling_dtl");

                entity.Property(e => e.LsdLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LsdItemId).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LsdItemGroupCode).IsUnicode(false);

                entity.Property(e => e.LsdItemName).IsUnicode(false);

                entity.Property(e => e.LsdLeadDetailId).IsUnicode(false);

                entity.Property(e => e.LsdRemarks).IsUnicode(false);

                entity.Property(e => e.LsdResult).IsUnicode(false);

                entity.Property(e => e.LsdShadeCode).IsUnicode(false);

                entity.Property(e => e.LsdShadeName).IsUnicode(false);

                entity.Property(e => e.LsdShadeType).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadSamplingHdr>(entity =>
            {
                entity.HasKey(e => new { e.LshLeadDetailGuid, e.LshHeaderId });

                entity.HasIndex(e => e.LshLeadDetailGuid)
                    .HasName("IX1_lead_sampling_hdr");

                entity.Property(e => e.LshLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LshActionTaken).IsUnicode(false);

                entity.Property(e => e.LshCompetetor).IsUnicode(false);

                entity.Property(e => e.LshContactId).IsUnicode(false);

                entity.Property(e => e.LshLeadDetailId).IsUnicode(false);

                entity.Property(e => e.LshOutcome).IsUnicode(false);

                entity.Property(e => e.LshPlace).IsUnicode(false);

                entity.Property(e => e.LshStage).IsUnicode(false);

                entity.Property(e => e.LshSurfaceType).IsUnicode(false);

                entity.Property(e => e.LshType).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadSpecificationHdr>(entity =>
            {
                entity.HasIndex(e => e.LshLeadDetailGuid)
                    .HasName("IX1_lead_specification_hdr");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LshLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LshSpecification).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadSpecificationItemDtl>(entity =>
            {
                entity.HasIndex(e => e.LsidHdrId)
                    .HasName("IX1_lead_specification_item_dtl");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LsidCategory).IsUnicode(false);

                entity.Property(e => e.LsidItemId).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadSpecificationRecipeDtl>(entity =>
            {
                entity.HasIndex(e => e.LsrdHdrId)
                    .HasName("IX2_lead_specification_recipe_dtl");

                entity.HasIndex(e => e.LsrdRecipeId)
                    .HasName("IX3_lead_specification_recipe_dtl");

                entity.HasIndex(e => new { e.LsrdHdrId, e.LsrdRecipeId })
                    .HasName("IX1_lead_specification_recipe_dtl");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.LsrdTitle).IsUnicode(false);

                entity.Property(e => e.LsrdTitle2).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadSupervisionDtl>(entity =>
            {
                entity.HasKey(e => new { e.LsdHdrId, e.LsdQnsId });

                entity.HasIndex(e => e.LsdHdrId)
                    .HasName("IX1_lead_supervision_dtl");

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.LsdEnteredAns).IsUnicode(false);

                entity.Property(e => e.LsdImg).IsUnicode(false);
            });

            modelBuilder.Entity<LeadSupervisionHdr>(entity =>
            {
                entity.HasIndex(e => e.LshLeadDetailGuid)
                    .HasName("IX1_lead_supervision_hdr");

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.LshLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LshLeadDetailId).IsUnicode(false);
            });

            modelBuilder.Entity<LeadSupervisionQusetionAns>(entity =>
            {
                entity.HasIndex(e => e.LsaQnsId)
                    .HasName("IX1_lead_supervision_qusetion_ans");

                entity.Property(e => e.LsaAnsId).ValueGeneratedNever();

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LsaAnsDesc).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadSupervisionQusetionMstr>(entity =>
            {
                entity.HasIndex(e => e.LsqDivision)
                    .HasName("IX1_lead_supervision_qusetion_mstr");

                entity.HasIndex(e => e.LsqQnsId)
                    .HasName("IX2_lead_supervision_qusetion_mstr");

                entity.Property(e => e.LsqQnsId).ValueGeneratedNever();

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.IsqQnsDesc).IsUnicode(false);

                entity.Property(e => e.LsqDivision).IsUnicode(false);

                entity.Property(e => e.LsqImgRequiredYn)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LsqInputType).IsUnicode(false);

                entity.Property(e => e.LsqSubDivision).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadSupervisionType>(entity =>
            {
                entity.HasKey(e => e.LstTypeId)
                    .HasName("PK_lead_site_supervision_type_1");

                entity.Property(e => e.LstTypeId).ValueGeneratedNever();

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LstTypeDesc).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadValuationDtl>(entity =>
            {
                entity.HasKey(e => new { e.LvdHdrId, e.LvdItemId });

                entity.HasIndex(e => e.LvdHdrId)
                    .HasName("IX1_lead_valuation_dtl");

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.BaseCode).IsUnicode(false);

                entity.Property(e => e.CategoryCode).IsUnicode(false);

                entity.Property(e => e.ItemGroupCode).IsUnicode(false);
            });

            modelBuilder.Entity<LeadValuationHdr>(entity =>
            {
                entity.HasIndex(e => e.LvhLeadDetailGuid)
                    .HasName("IX1_lead_valuation_hdr");

                entity.Property(e => e.Active).IsUnicode(false);

                entity.Property(e => e.LvhLeadDetailGuid).IsUnicode(false);

                entity.Property(e => e.LvhLeadDetailId).IsUnicode(false);
            });

            modelBuilder.Entity<LeadVisitActivities>(entity =>
            {
                entity.HasKey(e => new { e.LvaActivityId, e.LvaActivityMstrId })
                    .HasName("PK_lead_visit_activities_1");

                entity.HasIndex(e => e.LvaActivityId)
                    .HasName("IX1_lead_visit_activities");

                entity.HasIndex(e => e.LvaActivityMstrId)
                    .HasName("IX2_lead_visit_activities");

                entity.HasIndex(e => new { e.LvaId, e.LvaActivityId })
                    .HasName("IX3_lead_visit_activities");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LvaId).ValueGeneratedOnAdd();

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadVisitActivitiesProspect>(entity =>
            {
                entity.HasKey(e => new { e.LvaActivityId, e.LvaActivityMstrId });

                entity.HasIndex(e => e.LvaActivityId)
                    .HasName("IX2_lead_visit_activities_prospect");

                entity.HasIndex(e => e.LvaActivityMstrId)
                    .HasName("IX3_lead_visit_activities_prospect");

                entity.HasIndex(e => new { e.LvaActivityId, e.LvaActivityMstrId })
                    .HasName("IX1_lead_visit_activities_prospect");

                entity.Property(e => e.LvaActivityMstrId).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LvaId).ValueGeneratedOnAdd();

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadVisitActivityMstr>(entity =>
            {
                entity.HasIndex(e => e.VamId)
                    .HasName("IX1_lead_visit_activity_mstr");

                entity.Property(e => e.VamId).ValueGeneratedNever();

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.VamChildForm).IsUnicode(false);

                entity.Property(e => e.VamTypeName).IsUnicode(false);
            });

            modelBuilder.Entity<LeadVisitActivityMstrApplicableDivision>(entity =>
            {
                entity.HasKey(e => new { e.VamdId, e.VamdLmsDivision });

                entity.Property(e => e.VamdLmsDivision).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadVisitActivityMstrApplicableStage>(entity =>
            {
                entity.HasKey(e => new { e.VamsId, e.VamsLeadStageId });

                entity.HasIndex(e => e.VamsId)
                    .HasName("IX1_lead_visit_activity_mstr_applicable_stage");

                entity.Property(e => e.VamsLeadStageId).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadVisitEntry>(entity =>
            {
                entity.HasIndex(e => e.LveLeadDtlGuid)
                    .HasName("IX1_lead_visit_entry");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LveActivitiesReq).IsUnicode(false);

                entity.Property(e => e.LveActivityOthers).IsUnicode(false);

                entity.Property(e => e.LveCheckInAccuracy).IsUnicode(false);

                entity.Property(e => e.LveCheckInLat).IsUnicode(false);

                entity.Property(e => e.LveCheckInLong).IsUnicode(false);

                entity.Property(e => e.LveCheckOutAccuracy).IsUnicode(false);

                entity.Property(e => e.LveCheckOutLat).IsUnicode(false);

                entity.Property(e => e.LveCheckOutLong).IsUnicode(false);

                entity.Property(e => e.LveCompActivity).IsUnicode(false);

                entity.Property(e => e.LveLeadDtlGuid).IsUnicode(false);

                entity.Property(e => e.LvePointsDiscussed).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LeadVisitEntryProspect>(entity =>
            {
                entity.HasIndex(e => e.LveLeadDtlGuid)
                    .HasName("IX1_lead_visit_entry_prospect");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LveActivitiesReq).IsUnicode(false);

                entity.Property(e => e.LveActivityOthers).IsUnicode(false);

                entity.Property(e => e.LveCheckInAccuracy).IsUnicode(false);

                entity.Property(e => e.LveCheckInLat).IsUnicode(false);

                entity.Property(e => e.LveCheckInLong).IsUnicode(false);

                entity.Property(e => e.LveCheckOutAccuracy).IsUnicode(false);

                entity.Property(e => e.LveCheckOutLat).IsUnicode(false);

                entity.Property(e => e.LveCheckOutLong).IsUnicode(false);

                entity.Property(e => e.LveCompActivity).IsUnicode(false);

                entity.Property(e => e.LveLeadDtlGuid).IsUnicode(false);

                entity.Property(e => e.LvePointsDiscussed).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LmsDivision>(entity =>
            {
                entity.HasIndex(e => e.LmsDivisionCode)
                    .HasName("IX1_lms_division");

                entity.Property(e => e.LmsDivisionCode).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LmsDivisionId).ValueGeneratedOnAdd();

                entity.Property(e => e.LmsDivisionName).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LovDetails>(entity =>
            {
                entity.HasKey(e => new { e.LovType, e.LovCode })
                    .HasName("PK_lov_details_1");

                entity.HasIndex(e => e.LovCode)
                    .HasName("IX2_lov_details");

                entity.HasIndex(e => e.LovType)
                    .HasName("IX1_lov_details");

                entity.HasIndex(e => new { e.LovCode, e.Active })
                    .HasName("IX4_lov_details");

                entity.HasIndex(e => new { e.LovType, e.Active })
                    .HasName("IX3_lov_details");

                entity.Property(e => e.LovType).IsUnicode(false);

                entity.Property(e => e.LovCode).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LovField1Value).IsUnicode(false);

                entity.Property(e => e.LovField2Value).IsUnicode(false);

                entity.Property(e => e.LovShrtDesc).IsUnicode(false);

                entity.Property(e => e.LovValue).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<LovMstr>(entity =>
            {
                entity.Property(e => e.LovType).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.LovDesc).IsUnicode(false);

                entity.Property(e => e.LovField1Type)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LovField2Type)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LovField3Type)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LovValue)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<MobileApplicableMenu>(entity =>
            {
                entity.HasKey(e => new { e.MamUserGroupId, e.MamUserId, e.MamMenuId })
                    .HasName("PK_mobile_applicable_menu_1");

                entity.Property(e => e.MamUserId).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.MamDivision).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<MobileMenuMstr>(entity =>
            {
                entity.Property(e => e.MmMenuCode).ValueGeneratedNever();

                entity.Property(e => e.MmMenuName).IsUnicode(false);
            });

            modelBuilder.Entity<NonDecorPriceDtl>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.NdpdBrand).IsUnicode(false);

                entity.Property(e => e.NdpdCategory).IsUnicode(false);

                entity.Property(e => e.NdpdItemId).IsUnicode(false);
            });

            modelBuilder.Entity<NonDecorPriceHdr>(entity =>
            {
                entity.HasKey(e => e.NdphRecipeId)
                    .HasName("PK_non_decor_price");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.NdphLmsDivision).IsUnicode(false);

                entity.Property(e => e.NdphRecipeName).IsUnicode(false);

                entity.Property(e => e.NdphSapDivision).IsUnicode(false);
            });

            modelBuilder.Entity<PartnerAssociates>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.PaDesignation).IsUnicode(false);

                entity.Property(e => e.PaEmail).IsUnicode(false);

                entity.Property(e => e.PaFirstName).IsUnicode(false);

                entity.Property(e => e.PaGender).IsUnicode(false);

                entity.Property(e => e.PaLastName).IsUnicode(false);

                entity.Property(e => e.PaLinkCode).IsUnicode(false);

                entity.Property(e => e.PaLvlCode).IsUnicode(false);

                entity.Property(e => e.PaMiddleName).IsUnicode(false);

                entity.Property(e => e.PaPmId).IsUnicode(false);

                entity.Property(e => e.PaPrimaryContactNo).IsUnicode(false);

                entity.Property(e => e.PaSecondaryContactNo).IsUnicode(false);
            });

            modelBuilder.Entity<PartnerCompanyLinking>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<PartnerDocs>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.PdDocDescription).IsUnicode(false);

                entity.Property(e => e.PdDocExtension).IsUnicode(false);

                entity.Property(e => e.PdDocPath).IsUnicode(false);

                entity.Property(e => e.PdDocType).IsUnicode(false);

                entity.Property(e => e.PdPmCode).IsUnicode(false);
            });

            modelBuilder.Entity<PartnerMstr>(entity =>
            {
                entity.HasKey(e => e.PmId)
                    .HasName("PK_Partner_mstr");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.PmAddressLine1).IsUnicode(false);

                entity.Property(e => e.PmAddressLine2).IsUnicode(false);

                entity.Property(e => e.PmAddressLine3).IsUnicode(false);

                entity.Property(e => e.PmBusinessSubVerticals).IsUnicode(false);

                entity.Property(e => e.PmBusinessVerticals).IsUnicode(false);

                entity.Property(e => e.PmCity).IsUnicode(false);

                entity.Property(e => e.PmDescription).IsUnicode(false);

                entity.Property(e => e.PmFax).IsUnicode(false);

                entity.Property(e => e.PmFirmName).IsUnicode(false);

                entity.Property(e => e.PmLegalEntiry).IsUnicode(false);

                entity.Property(e => e.PmMultiLocation).IsUnicode(false);

                entity.Property(e => e.PmName).IsUnicode(false);

                entity.Property(e => e.PmPin).IsUnicode(false);

                entity.Property(e => e.PmPrimaryContactNumber).IsUnicode(false);

                entity.Property(e => e.PmPrimaryEmail).IsUnicode(false);

                entity.Property(e => e.PmRating).IsUnicode(false);

                entity.Property(e => e.PmState).IsUnicode(false);

                entity.Property(e => e.PmType).IsUnicode(false);

                entity.Property(e => e.PmWebsite).IsUnicode(false);
            });

            modelBuilder.Entity<PaymentTermsMaster>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.PtmDivision).IsUnicode(false);

                entity.Property(e => e.PtmTerms).IsUnicode(false);
            });

            modelBuilder.Entity<PositionRegistrationCompany>(entity =>
            {
                entity.HasKey(e => new { e.PrcCustomerType, e.PrcCompanyId });

                entity.Property(e => e.PrcCustomerType).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.PrcAccuracy).IsUnicode(false);

                entity.Property(e => e.PrcLat).IsUnicode(false);

                entity.Property(e => e.PrcLong).IsUnicode(false);
            });

            modelBuilder.Entity<PositionRegistrationCompanyUpdated>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.PrcAccuracy).IsUnicode(false);

                entity.Property(e => e.PrcLat).IsUnicode(false);

                entity.Property(e => e.PrcLong).IsUnicode(false);
            });

            modelBuilder.Entity<ProspectLeadGeoLocationRegistration>(entity =>
            {
                entity.HasKey(e => new { e.GlrLeadDtlGuid, e.GlrTypeId });

                entity.HasIndex(e => e.GlrLeadDtlGuid)
                    .HasName("IX1_prospect_lead_geo_location_registration");

                entity.Property(e => e.GlrLeadDtlGuid).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.GlrAccuracy).IsUnicode(false);

                entity.Property(e => e.GlrLatitude).IsUnicode(false);

                entity.Property(e => e.GlrLongitude).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<RegularShade>(entity =>
            {
                entity.HasKey(e => new { e.RsBrandCode, e.RsShadeCode });

                entity.HasIndex(e => new { e.RsBrandCode, e.RsShadeCode })
                    .HasName("IX2_regular_shade");

                entity.HasIndex(e => new { e.RsBrandCode, e.RsShadeCode, e.RsPrimaryInd })
                    .HasName("IX1_regular_shade");

                entity.Property(e => e.RsBrandCode).IsUnicode(false);

                entity.Property(e => e.RsShadeCode).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.RsPrimaryInd)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<RelationshipCallDtls>(entity =>
            {
                entity.HasKey(e => new { e.RcdId, e.RchHdrId });

                entity.Property(e => e.RcdId).ValueGeneratedOnAdd();

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.RcdAnsEntered).IsUnicode(false);

                entity.Property(e => e.RcdAnsSelected).IsUnicode(false);

                entity.Property(e => e.RcdRemarks).IsUnicode(false);
            });

            modelBuilder.Entity<RelationshipCallHdr>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.RchCategory).IsUnicode(false);

                entity.Property(e => e.RchTypeId).IsUnicode(false);

                entity.Property(e => e.RchUser).IsUnicode(false);
            });

            modelBuilder.Entity<RelationshipCallQuestionAnsMstr>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.RcqamAnsDesc).IsUnicode(false);
            });

            modelBuilder.Entity<RelationshipCallQuestionMstr>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.RcqmDescription).IsUnicode(false);

                entity.Property(e => e.RcqmType).IsUnicode(false);
            });

            modelBuilder.Entity<SrlControlMstr>(entity =>
            {
                entity.HasKey(e => new { e.SrlId, e.SrlCompany, e.SrlFinYear, e.SrlDocType });

                entity.Property(e => e.SrlId).ValueGeneratedOnAdd();

                entity.Property(e => e.SrlCompany).IsUnicode(false);

                entity.Property(e => e.SrlFinYear).IsUnicode(false);

                entity.Property(e => e.SrlDocType).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.SrlBranch).IsUnicode(false);

                entity.Property(e => e.SrlDept).IsUnicode(false);

                entity.Property(e => e.SrlLoc).IsUnicode(false);

                entity.Property(e => e.SrlPrefix).IsUnicode(false);
            });

            modelBuilder.Entity<StandardParameter>(entity =>
            {
                entity.Property(e => e.ParmName).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.ParamAutoLeadMe).IsUnicode(false);

                entity.Property(e => e.ParamCharValue).IsUnicode(false);

                entity.Property(e => e.ParamFalseLeadMe).IsUnicode(false);

                entity.Property(e => e.ParamStatus).IsUnicode(false);

                entity.Property(e => e.ParamTimeValue).IsUnicode(false);
            });

            modelBuilder.Entity<TempOtpLog>(entity =>
            {
                entity.HasKey(e => e.OtpLogId)
                    .HasName("PK__temp_otp__5984EC301CF98E67");

                entity.Property(e => e.UspContactNo).IsUnicode(false);

                entity.Property(e => e.UspUserId).IsUnicode(false);
            });

            modelBuilder.Entity<TermsConditionsMaster>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsFixedLength();

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.TcmDivision).IsUnicode(false);

                entity.Property(e => e.TcmTermsConditions).IsUnicode(false);
            });

            modelBuilder.Entity<ThanaMstr>(entity =>
            {
                entity.HasIndex(e => e.TmId)
                    .HasName("IX2_thana_mstr");

                entity.HasIndex(e => new { e.TmDstId, e.TmDepotCode })
                    .HasName("IX1_thana_mstr");

                entity.Property(e => e.TmName).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.TmDepotCode).IsUnicode(false);

                entity.Property(e => e.TmId).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<UserApplicableAppMstr>(entity =>
            {
                entity.HasKey(e => new { e.UaamAppId, e.UaamUserId });

                entity.Property(e => e.UaamUserId).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<UserApplicableCluster>(entity =>
            {
                entity.HasKey(e => new { e.UacClCode, e.UacApplicableDivisionCode, e.UacApplicableDepotCode, e.UacUserGroupId });

                entity.HasIndex(e => e.UacUserId)
                    .HasName("IX4_user_applicable_cluster");

                entity.HasIndex(e => new { e.UacClCode, e.UacUserId })
                    .HasName("IX5_user_applicable_cluster");

                entity.HasIndex(e => new { e.UacUserGroupId, e.UacApplicableDivisionCode })
                    .HasName("IX1_user_applicable_cluster");

                entity.HasIndex(e => new { e.UacUserId, e.UacApplicableDivisionCode })
                    .HasName("IX3_user_applicable_cluster");

                entity.HasIndex(e => new { e.UacApplicableDivisionCode, e.UacApplicableDepotCode, e.UacUserId })
                    .HasName("IX6_user_applicable_cluster");

                entity.HasIndex(e => new { e.UacApplicableDivisionCode, e.UacUserGroupId, e.UacApplicableDepotCode })
                    .HasName("IX7_user_applicable_cluster");

                entity.HasIndex(e => new { e.UacClCode, e.UacUserGroupId, e.UacApplicableDivisionCode })
                    .HasName("IX2_user_applicable_cluster");

                entity.Property(e => e.UacClCode).IsUnicode(false);

                entity.Property(e => e.UacApplicableDivisionCode).IsUnicode(false);

                entity.Property(e => e.UacApplicableDepotCode).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.UacUserId).IsUnicode(false);
            });

            modelBuilder.Entity<UserApplicableClusterBackup>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.UacApplicableDepotCode).IsUnicode(false);

                entity.Property(e => e.UacApplicableDivisionCode).IsUnicode(false);

                entity.Property(e => e.UacClCode).IsUnicode(false);

                entity.Property(e => e.UacUserId).IsUnicode(false);
            });

            modelBuilder.Entity<UserApplicableDivision>(entity =>
            {
                entity.HasKey(e => new { e.UadUserId, e.UadDivisionId });

                entity.HasIndex(e => e.UadDivisionId)
                    .HasName("IX3_user_applicable_division");

                entity.HasIndex(e => e.UadUserId)
                    .HasName("IX1_user_applicable_division");

                entity.HasIndex(e => new { e.UadDivisionId, e.UadUserGroup })
                    .HasName("IX2_user_applicable_division");

                entity.HasIndex(e => new { e.UadUserGroup, e.UadDivisionId })
                    .HasName("IX4_user_applicable_division");

                entity.Property(e => e.UadUserId).IsUnicode(false);

                entity.Property(e => e.UadDivisionId).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<UserApplicableDivisionBackup>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.UadDivisionId).IsUnicode(false);

                entity.Property(e => e.UadUserId).IsUnicode(false);
            });

            modelBuilder.Entity<UserApplicableLegalEntity>(entity =>
            {
                entity.HasKey(e => new { e.UalUserId, e.UalLegalEntityId });

                entity.Property(e => e.UalUserId).IsUnicode(false);

                entity.Property(e => e.UalLegalEntityId).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<UserApplicableLegalEntityBackup>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.UalLegalEntityId).IsUnicode(false);

                entity.Property(e => e.UalUserId).IsUnicode(false);
            });

            modelBuilder.Entity<UserDeactivationLog>(entity =>
            {
                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.URemarks).IsUnicode(false);

                entity.Property(e => e.UUserId).IsUnicode(false);
            });

            modelBuilder.Entity<UserDeviceRegistration>(entity =>
            {
                entity.HasKey(e => new { e.UdrUserId, e.UdrUuid });

                entity.Property(e => e.UdrUserId).IsUnicode(false);

                entity.Property(e => e.UdrUuid).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.UdrCordova).IsUnicode(false);

                entity.Property(e => e.UdrFcmToken).IsUnicode(false);

                entity.Property(e => e.UdrModel).IsUnicode(false);

                entity.Property(e => e.UdrName).IsUnicode(false);

                entity.Property(e => e.UdrPlatform).IsUnicode(false);

                entity.Property(e => e.UdrRegistrationStatus).IsUnicode(false);

                entity.Property(e => e.UdrVersion).IsUnicode(false);
            });

            modelBuilder.Entity<UserDeviceRegistrationDelete>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.UdrCordova).IsUnicode(false);

                entity.Property(e => e.UdrFcmToken).IsUnicode(false);

                entity.Property(e => e.UdrModel).IsUnicode(false);

                entity.Property(e => e.UdrName).IsUnicode(false);

                entity.Property(e => e.UdrPlatform).IsUnicode(false);

                entity.Property(e => e.UdrRegistrationStatus).IsUnicode(false);

                entity.Property(e => e.UdrUserId).IsUnicode(false);

                entity.Property(e => e.UdrUuid).IsUnicode(false);

                entity.Property(e => e.UdrVersion).IsUnicode(false);
            });

            modelBuilder.Entity<UserFcmToken>(entity =>
            {
                entity.HasKey(e => new { e.TUserId, e.TToken });

                entity.Property(e => e.TUserId).IsUnicode(false);

                entity.Property(e => e.TToken).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<UserFormsAccess>(entity =>
            {
                entity.HasKey(e => new { e.UserGroupCode, e.UserId, e.FormCode });

                entity.Property(e => e.UserId).IsUnicode(false);

                entity.Property(e => e.FormCode).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.FormAccessType).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.QuickLink)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.UserDivisionCode).IsUnicode(false);
            });

            modelBuilder.Entity<UserGroup>(entity =>
            {
                entity.HasKey(e => e.GrpUserGroupId)
                    .HasName("PK_user_group_1");

                entity.HasIndex(e => e.GrpUserDivision)
                    .HasName("IX1_user_group");

                entity.HasIndex(e => e.GrpUserGroupCode)
                    .HasName("IX3_user_group");

                entity.HasIndex(e => e.GrpUserGroupId)
                    .HasName("IX2_user_group");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.GrpBranchFlag)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.GrpCompanyFlag)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.GrpDeptFlag)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.GrpGpsTrackingYn)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.GrpUserDivision).IsUnicode(false);

                entity.Property(e => e.GrpUserGroupCode).IsUnicode(false);

                entity.Property(e => e.GrpUserGroupDesc).IsUnicode(false);

                entity.Property(e => e.GrpUserGroupType)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.GrpUseridFlag)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LeadAssignAccess)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<UserHistory>(entity =>
            {
                entity.Property(e => e.UhLoggedIp).IsUnicode(false);

                entity.Property(e => e.UhUserGroup).IsUnicode(false);

                entity.Property(e => e.UhUserid).IsUnicode(false);
            });

            modelBuilder.Entity<UserOtp>(entity =>
            {
                entity.Property(e => e.ODeviceId).IsUnicode(false);

                entity.Property(e => e.OEmail).IsUnicode(false);

                entity.Property(e => e.OMobile).IsUnicode(false);

                entity.Property(e => e.OOtp).IsUnicode(false);

                entity.Property(e => e.OUserId).IsUnicode(false);
            });

            modelBuilder.Entity<UserProfile>(entity =>
            {
                entity.HasIndex(e => e.UspUserId)
                    .HasName("IX1_user_profile");

                entity.Property(e => e.UspUserId).IsUnicode(false);

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.UspDesig).IsUnicode(false);

                entity.Property(e => e.UspEmployeeId).IsUnicode(false);

                entity.Property(e => e.UspFirstName).IsUnicode(false);

                entity.Property(e => e.UspGroupCode).IsUnicode(false);

                entity.Property(e => e.UspLastName).IsUnicode(false);

                entity.Property(e => e.UspLegalEntityid).IsUnicode(false);

                entity.Property(e => e.UspMailid).IsUnicode(false);

                entity.Property(e => e.UspMobile).IsUnicode(false);

                entity.Property(e => e.UspReportingGroup).IsUnicode(false);

                entity.Property(e => e.UspReportingUser).IsUnicode(false);
            });

            modelBuilder.Entity<UserProfileBackup>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.UspDesig).IsUnicode(false);

                entity.Property(e => e.UspEmployeeId).IsUnicode(false);

                entity.Property(e => e.UspFirstName).IsUnicode(false);

                entity.Property(e => e.UspGroupCode).IsUnicode(false);

                entity.Property(e => e.UspLastName).IsUnicode(false);

                entity.Property(e => e.UspLegalEntityid).IsUnicode(false);

                entity.Property(e => e.UspMailid).IsUnicode(false);

                entity.Property(e => e.UspMobile).IsUnicode(false);

                entity.Property(e => e.UspReportingGroup).IsUnicode(false);

                entity.Property(e => e.UspReportingUser).IsUnicode(false);

                entity.Property(e => e.UspUserId).IsUnicode(false);
            });

            modelBuilder.Entity<UserSessionAll>(entity =>
            {
                entity.Property(e => e.UsDeviceUuid).IsUnicode(false);

                entity.Property(e => e.UsExpireReason).IsUnicode(false);

                entity.Property(e => e.UsIpAddress).IsUnicode(false);

                entity.Property(e => e.UsToken).IsUnicode(false);

                entity.Property(e => e.UsUserId).IsUnicode(false);
            });

            modelBuilder.Entity<UserSessionCurrent>(entity =>
            {
                entity.Property(e => e.UscDeviceUuid).IsUnicode(false);

                entity.Property(e => e.UscExpireReason).IsUnicode(false);

                entity.Property(e => e.UscIpAddress).IsUnicode(false);

                entity.Property(e => e.UscToken).IsUnicode(false);

                entity.Property(e => e.UscUserId).IsUnicode(false);
            });

            modelBuilder.Entity<UserTrackingHistory>(entity =>
            {
                entity.Property(e => e.UthAccuracy).IsUnicode(false);

                entity.Property(e => e.UthActivityConfidence).IsUnicode(false);

                entity.Property(e => e.UthActivityType).IsUnicode(false);

                entity.Property(e => e.UthAltitude).IsUnicode(false);

                entity.Property(e => e.UthBatteryLevel).IsFixedLength();

                entity.Property(e => e.UthBearing).IsUnicode(false);

                entity.Property(e => e.UthIsBattaryCharging).IsFixedLength();

                entity.Property(e => e.UthIsMoving).IsFixedLength();

                entity.Property(e => e.UthLatitude).IsUnicode(false);

                entity.Property(e => e.UthLongitude).IsUnicode(false);

                entity.Property(e => e.UthRecordedAt).IsUnicode(false);

                entity.Property(e => e.UthSpeed).IsUnicode(false);

                entity.Property(e => e.UthUserId).IsUnicode(false);

                entity.Property(e => e.UthUuid).IsUnicode(false);
            });

            modelBuilder.Entity<VisitActivityApplicableGroup>(entity =>
            {
                entity.HasKey(e => new { e.VamId, e.VamUserGroupId });

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.VamUserDivision).IsUnicode(false);
            });

            modelBuilder.Entity<VisitActivityMstr>(entity =>
            {
                entity.HasKey(e => e.VamId)
                    .HasName("PK_lead_visit_activity");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.VamChildForm).IsUnicode(false);

                entity.Property(e => e.VamSrlNo).IsUnicode(false);

                entity.Property(e => e.VamType).IsUnicode(false);

                entity.Property(e => e.VamTypeName).IsUnicode(false);

                entity.Property(e => e.VamTypeStage).IsUnicode(false);
            });

            modelBuilder.Entity<VisitCompanyDtl>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.VcdAttributeChar1).IsUnicode(false);

                entity.Property(e => e.VcdAttributeChar2).IsUnicode(false);

                entity.Property(e => e.VcdImgPath).IsUnicode(false);

                entity.Property(e => e.VcdSelectedYn)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<VisitCompanyHdr>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);

                entity.Property(e => e.VchCheckInAccuracy).IsUnicode(false);

                entity.Property(e => e.VchCheckInLat).IsUnicode(false);

                entity.Property(e => e.VchCheckInLong).IsUnicode(false);

                entity.Property(e => e.VchCheckOutAccuracy).IsUnicode(false);

                entity.Property(e => e.VchCheckOutLat).IsUnicode(false);

                entity.Property(e => e.VchCheckOutLong).IsUnicode(false);

                entity.Property(e => e.VchCustomerType).IsUnicode(false);

                entity.Property(e => e.VchMode).IsUnicode(false);

                entity.Property(e => e.VchUser).IsUnicode(false);
            });

            modelBuilder.Entity<VwCompanyPartnerMstr>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_company_partner_mstr");

                entity.Property(e => e.ActivityCode).IsUnicode(false);

                entity.Property(e => e.Add1).IsUnicode(false);

                entity.Property(e => e.Add2).IsUnicode(false);

                entity.Property(e => e.Add3).IsUnicode(false);

                entity.Property(e => e.City).IsUnicode(false);

                entity.Property(e => e.ContactNo).IsUnicode(false);

                entity.Property(e => e.CustomerType).IsUnicode(false);

                entity.Property(e => e.Desc).IsUnicode(false);

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.SubVerical).IsUnicode(false);

                entity.Property(e => e.Type).IsUnicode(false);

                entity.Property(e => e.Verical).IsUnicode(false);

                entity.Property(e => e.Website).IsUnicode(false);
            });

            modelBuilder.Entity<XxxxBusinessCallQuestionAnsMstr>(entity =>
            {
                entity.HasKey(e => e.BcqamId)
                    .HasName("PK_business_call_question_ans_mstr");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.BcqamAnsDesc).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            modelBuilder.Entity<XxxxBusinessCallQuestionMstr>(entity =>
            {
                entity.HasKey(e => e.BcqmQid)
                    .HasName("PK_business_call_question_mstr");

                entity.Property(e => e.Active)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.BcqmDescription).IsUnicode(false);

                entity.Property(e => e.BcqmType).IsUnicode(false);

                entity.Property(e => e.CreatedUser).IsUnicode(false);

                entity.Property(e => e.DeletedUser).IsUnicode(false);

                entity.Property(e => e.ModifiedUser).IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
