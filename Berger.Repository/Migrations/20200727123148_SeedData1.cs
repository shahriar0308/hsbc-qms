﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class SeedData1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created", "Sequence" },
                values: new object[] { new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(9395), 1 });

            migrationBuilder.UpdateData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created", "Sequence" },
                values: new object[] { new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(9450), 2 });

            migrationBuilder.UpdateData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(5999));

            migrationBuilder.UpdateData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(6029));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 14, DateTimeKind.Local).AddTicks(9204));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 14, DateTimeKind.Local).AddTicks(9280));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 14, DateTimeKind.Local).AddTicks(9285));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 14, DateTimeKind.Local).AddTicks(5888));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 14, DateTimeKind.Local).AddTicks(5940));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 14, DateTimeKind.Local).AddTicks(5943));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 14, DateTimeKind.Local).AddTicks(5944));

            migrationBuilder.UpdateData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 11, DateTimeKind.Local).AddTicks(7190));

            migrationBuilder.UpdateData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 13, DateTimeKind.Local).AddTicks(3639));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(2196));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(2287));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(2290));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(2293));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 5,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(2295));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 6,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(2297));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created", "ImageUrl" },
                values: new object[] { new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(1461), "Resources\\Images\\Banners\\1.jpg" });

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created", "ImageUrl" },
                values: new object[] { new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(1506), "Resources\\Images\\Banners\\2.jpg" });

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(1508));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(2502));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(2528));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(2530));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(2531));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 5,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(2533));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 6,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(2534));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 7,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(2536));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 8,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(2538));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(7347));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(7381));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(7383));

            migrationBuilder.UpdateData(
                table: "SingleValuedEntities",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(4212));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created", "Sequence" },
                values: new object[] { new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(5787), 0 });

            migrationBuilder.UpdateData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created", "Sequence" },
                values: new object[] { new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(5870), 0 });

            migrationBuilder.UpdateData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(3159));

            migrationBuilder.UpdateData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(3208));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 502, DateTimeKind.Local).AddTicks(7946));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 502, DateTimeKind.Local).AddTicks(8140));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 502, DateTimeKind.Local).AddTicks(8148));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 502, DateTimeKind.Local).AddTicks(5151));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 502, DateTimeKind.Local).AddTicks(5245));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 502, DateTimeKind.Local).AddTicks(5249));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 502, DateTimeKind.Local).AddTicks(5251));

            migrationBuilder.UpdateData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 499, DateTimeKind.Local).AddTicks(1349));

            migrationBuilder.UpdateData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 500, DateTimeKind.Local).AddTicks(9003));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(87));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(182));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(207));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(210));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 5,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(212));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 6,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(214));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created", "ImageUrl" },
                values: new object[] { new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(7751), "1.jpg" });

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created", "ImageUrl" },
                values: new object[] { new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(7820), "2.jpg" });

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(7823));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(8723));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(8760));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(8763));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(8765));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 5,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(8767));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 6,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(8769));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 7,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(8772));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 8,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(8774));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(4318));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(4365));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(4368));

            migrationBuilder.UpdateData(
                table: "SingleValuedEntities",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 504, DateTimeKind.Local).AddTicks(325));
        }
    }
}
