﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class SeedData4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "PaintingServiceCosts",
                columns: new[] { "Id", "AboveNine", "Created", "CreatedBy", "GoldRatePerSft", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy", "PlatinumRatePerSft", "ProductId", "SevenToNine", "SilverRatePerSft", "SurfaceCondition", "SurfaceType", "Vat" },
                values: new object[,]
                {
                    { 1, 0m, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 12m, true, false, null, null, 28m, 1, 0m, 2m, 1, 1, 15m },
                    { 2, 0m, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 12m, true, false, null, null, 23m, 2, 0m, 2m, 1, 1, 15m },
                    { 3, 0m, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 12m, true, false, null, null, 21m, 3, 0m, 2m, 1, 1, 15m },
                    { 4, 0m, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 10m, true, false, null, null, 17m, 4, 0m, 2m, 1, 1, 15m },
                    { 5, 0m, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 12m, true, false, null, null, 26m, 1, 0m, 2m, 2, 1, 15m },
                    { 6, 0m, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 12m, true, false, null, null, 22m, 2, 0m, 2m, 2, 1, 15m },
                    { 7, 0m, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 12m, true, false, null, null, 20m, 3, 0m, 2m, 2, 1, 15m },
                    { 8, 0m, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 7m, true, false, null, null, 14m, 4, 0m, 2m, 2, 1, 15m },
                    { 9, 5m, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 9m, true, false, null, null, 38m, 5, 1m, 1m, 1, 2, 15m },
                    { 10, 5m, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 9m, true, false, null, null, 19m, 6, 1m, 1m, 1, 2, 15m },
                    { 11, 5m, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 9m, true, false, null, null, 16m, 7, 1m, 1m, 1, 2, 15m },
                    { 12, 5m, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 9m, true, false, null, null, 38m, 5, 1m, 1m, 2, 2, 15m },
                    { 13, 5m, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 7m, true, false, null, null, 15m, 6, 1m, 1m, 2, 2, 15m },
                    { 14, 5m, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 7m, true, false, null, null, 13m, 7, 1m, 1m, 2, 2, 15m },
                    { 15, 0m, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 12m, true, false, null, null, 25m, 8, 0m, 2m, 0, 3, 15m },
                    { 16, 0m, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 10m, true, false, null, null, 19m, 9, 0m, 2m, 0, 3, 15m }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "PaintingServiceCosts",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PaintingServiceCosts",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PaintingServiceCosts",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PaintingServiceCosts",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PaintingServiceCosts",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PaintingServiceCosts",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PaintingServiceCosts",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PaintingServiceCosts",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PaintingServiceCosts",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PaintingServiceCosts",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PaintingServiceCosts",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PaintingServiceCosts",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PaintingServiceCosts",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PaintingServiceCosts",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PaintingServiceCosts",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PaintingServiceCosts",
                keyColumn: "Id",
                keyValue: 16);
        }
    }
}
