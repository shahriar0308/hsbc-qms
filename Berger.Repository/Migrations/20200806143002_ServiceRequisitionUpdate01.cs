﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class ServiceRequisitionUpdate01 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Active",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "Attribute1",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "Attribute2",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "Condition",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "ContactEmail",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "ContactType",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "DivisionOrg",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "IsSynced",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "LeadId",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "LegalEntityId",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "Location",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "Opportunity",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "Organization",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "Priority",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "Source",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "Srl",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "ServiceRequisitions");

            migrationBuilder.AlterColumn<string>(
                name: "Stage",
                table: "ServiceRequisitions",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(100)");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ServiceRequisitions",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(100)");

            migrationBuilder.AddColumn<string>(
                name: "AddressLine1",
                table: "ServiceRequisitions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AddressLine2",
                table: "ServiceRequisitions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ColorConsultantName",
                table: "ServiceRequisitions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ColorConsultantPhoneNo",
                table: "ServiceRequisitions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FinancialEstimateUrl",
                table: "ServiceRequisitions",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "InterestedToPurchaseProductOnly",
                table: "ServiceRequisitions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "PaymentSlipUrl",
                table: "ServiceRequisitions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ServiceCategoryId",
                table: "ServiceRequisitions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "WorkId",
                table: "ServiceRequisitions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceRequisitions_ServiceCategoryId",
                table: "ServiceRequisitions",
                column: "ServiceCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceRequisitions_ServiceCategories_ServiceCategoryId",
                table: "ServiceRequisitions",
                column: "ServiceCategoryId",
                principalTable: "ServiceCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceRequisitions_ServiceCategories_ServiceCategoryId",
                table: "ServiceRequisitions");

            migrationBuilder.DropIndex(
                name: "IX_ServiceRequisitions_ServiceCategoryId",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "AddressLine1",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "AddressLine2",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "ColorConsultantName",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "ColorConsultantPhoneNo",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "FinancialEstimateUrl",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "InterestedToPurchaseProductOnly",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "PaymentSlipUrl",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "ServiceCategoryId",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "WorkId",
                table: "ServiceRequisitions");

            migrationBuilder.AlterColumn<string>(
                name: "Stage",
                table: "ServiceRequisitions",
                type: "varchar(100)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ServiceRequisitions",
                type: "varchar(100)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Active",
                table: "ServiceRequisitions",
                type: "nvarchar(1)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Attribute1",
                table: "ServiceRequisitions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Attribute2",
                table: "ServiceRequisitions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Condition",
                table: "ServiceRequisitions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactEmail",
                table: "ServiceRequisitions",
                type: "varchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactType",
                table: "ServiceRequisitions",
                type: "varchar(50)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "ServiceRequisitions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DivisionOrg",
                table: "ServiceRequisitions",
                type: "varchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsSynced",
                table: "ServiceRequisitions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "LeadId",
                table: "ServiceRequisitions",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "LegalEntityId",
                table: "ServiceRequisitions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Location",
                table: "ServiceRequisitions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Opportunity",
                table: "ServiceRequisitions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Organization",
                table: "ServiceRequisitions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Priority",
                table: "ServiceRequisitions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Source",
                table: "ServiceRequisitions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Srl",
                table: "ServiceRequisitions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "ServiceRequisitions",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
