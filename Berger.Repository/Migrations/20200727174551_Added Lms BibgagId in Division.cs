﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class AddedLmsBibgagIdinDivision : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LmsDivisionId",
                table: "Divisions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 134, DateTimeKind.Local).AddTicks(2071));

            migrationBuilder.UpdateData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 134, DateTimeKind.Local).AddTicks(2140));

            migrationBuilder.UpdateData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 133, DateTimeKind.Local).AddTicks(8371));

            migrationBuilder.UpdateData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 133, DateTimeKind.Local).AddTicks(8404));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 133, DateTimeKind.Local).AddTicks(2567));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 133, DateTimeKind.Local).AddTicks(2633));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 133, DateTimeKind.Local).AddTicks(2637));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 132, DateTimeKind.Local).AddTicks(9282));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 132, DateTimeKind.Local).AddTicks(9343));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 132, DateTimeKind.Local).AddTicks(9346));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 132, DateTimeKind.Local).AddTicks(9347));

            migrationBuilder.UpdateData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 130, DateTimeKind.Local).AddTicks(7442));

            migrationBuilder.UpdateData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 131, DateTimeKind.Local).AddTicks(6484));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 133, DateTimeKind.Local).AddTicks(4853));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 133, DateTimeKind.Local).AddTicks(4925));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 133, DateTimeKind.Local).AddTicks(4928));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 133, DateTimeKind.Local).AddTicks(4930));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 5,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 133, DateTimeKind.Local).AddTicks(4931));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 6,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 133, DateTimeKind.Local).AddTicks(4933));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 133, DateTimeKind.Local).AddTicks(6690));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 133, DateTimeKind.Local).AddTicks(6794));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 133, DateTimeKind.Local).AddTicks(6796));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 134, DateTimeKind.Local).AddTicks(4780));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 134, DateTimeKind.Local).AddTicks(4854));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 134, DateTimeKind.Local).AddTicks(4857));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 134, DateTimeKind.Local).AddTicks(6371));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 134, DateTimeKind.Local).AddTicks(6408));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 134, DateTimeKind.Local).AddTicks(6411));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 134, DateTimeKind.Local).AddTicks(6413));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 5,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 134, DateTimeKind.Local).AddTicks(6415));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 6,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 134, DateTimeKind.Local).AddTicks(6416));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 7,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 134, DateTimeKind.Local).AddTicks(6418));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 8,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 134, DateTimeKind.Local).AddTicks(6420));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 133, DateTimeKind.Local).AddTicks(9936));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 133, DateTimeKind.Local).AddTicks(9970));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 133, DateTimeKind.Local).AddTicks(9972));

            migrationBuilder.UpdateData(
                table: "SingleValuedEntities",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 23, 45, 51, 134, DateTimeKind.Local).AddTicks(9086));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LmsDivisionId",
                table: "Divisions");

            migrationBuilder.UpdateData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(9395));

            migrationBuilder.UpdateData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(9450));

            migrationBuilder.UpdateData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(5999));

            migrationBuilder.UpdateData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(6029));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 14, DateTimeKind.Local).AddTicks(9204));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 14, DateTimeKind.Local).AddTicks(9280));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 14, DateTimeKind.Local).AddTicks(9285));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 14, DateTimeKind.Local).AddTicks(5888));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 14, DateTimeKind.Local).AddTicks(5940));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 14, DateTimeKind.Local).AddTicks(5943));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 14, DateTimeKind.Local).AddTicks(5944));

            migrationBuilder.UpdateData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 11, DateTimeKind.Local).AddTicks(7190));

            migrationBuilder.UpdateData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 13, DateTimeKind.Local).AddTicks(3639));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(2196));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(2287));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(2290));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(2293));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 5,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(2295));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 6,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(2297));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(4560));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(4597));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(4600));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(1461));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(1506));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(1508));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(2502));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(2528));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(2530));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(2531));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 5,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(2533));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 6,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(2534));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 7,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(2536));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 8,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(2538));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(7347));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(7381));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 15, DateTimeKind.Local).AddTicks(7383));

            migrationBuilder.UpdateData(
                table: "SingleValuedEntities",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 27, 18, 31, 48, 16, DateTimeKind.Local).AddTicks(4212));
        }
    }
}
