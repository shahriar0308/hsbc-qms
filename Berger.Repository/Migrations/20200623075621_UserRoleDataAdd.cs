﻿using Berger.Common.Constants;
using Berger.Common.Enums;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Berger.Repository.Migrations
{
    public partial class UserRoleDataAdd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // Seed Role 
            migrationBuilder.Sql("INSERT INTO AspNetRoles (Id,Name,NormalizedName,Status,IsActive,IsDeleted,Created) VALUES ('" + Guid.NewGuid() + "','" +
                ConstantsValue.UserRoleName.GeneralUser + "','" + ConstantsValue.UserRoleName.GeneralUser.ToUpper() + "','" + (int)EnumApplicationRoleStatus.GeneralUser + "','true','false','" + DateTime.Now + "')");
            migrationBuilder.Sql("INSERT INTO AspNetRoles (Id,Name,NormalizedName,Status,IsActive,IsDeleted,Created) VALUES ('" + Guid.NewGuid() + "','" +
                ConstantsValue.UserRoleName.AppUser + "','" + ConstantsValue.UserRoleName.AppUser.ToUpper() + "','" + (int)EnumApplicationRoleStatus.GeneralUser + "','true','false','" + DateTime.Now + "')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM AspNetRoles WHERE Name IN ('" +
                ConstantsValue.UserRoleName.GeneralUser + "','" +
                ConstantsValue.UserRoleName.AppUser + "')");
        }
    }
}
