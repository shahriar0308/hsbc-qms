﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class ServiceRequisitionUpdate2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ServiceRequisitionId",
                table: "ServiceRequisitionDetails",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_ServiceRequisitionDetails_ServiceRequisitionId",
                table: "ServiceRequisitionDetails",
                column: "ServiceRequisitionId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceRequisitionDetails_ServiceRequisitions_ServiceRequisitionId",
                table: "ServiceRequisitionDetails",
                column: "ServiceRequisitionId",
                principalTable: "ServiceRequisitions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceRequisitionDetails_ServiceRequisitions_ServiceRequisitionId",
                table: "ServiceRequisitionDetails");

            migrationBuilder.DropIndex(
                name: "IX_ServiceRequisitionDetails_ServiceRequisitionId",
                table: "ServiceRequisitionDetails");

            migrationBuilder.DropColumn(
                name: "ServiceRequisitionId",
                table: "ServiceRequisitionDetails");
        }
    }
}
