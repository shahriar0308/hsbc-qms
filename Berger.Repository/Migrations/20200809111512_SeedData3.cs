﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class SeedData3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "ImageUrl", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy", "Name", "SurfaceType" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Breathe Easy Emulsion", "Resources\\Images\\Products\\Breathe Easy Emulsion_09353c77-566d-4c09-89b7-b057a1b9a6bd.jpg", true, false, null, null, "Breathe Easy Emulsion", 0 },
                    { 2, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Luxury Silk Emulsion", "Resources\\Images\\Products\\Luxury Silk Emulsion_633607f3-56d5-43f4-b177-4d71b3dc2a65.jpg", true, false, null, null, "Luxury Silk Emulsion", 0 },
                    { 3, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Easy Clean Emulsion", "Resources\\Images\\Products\\Easy Clean Emulsion_489f0b2a-aabd-4d7b-80f6-a10071dfec83.jpg", true, false, null, null, "Easy Clean Emulsion", 0 },
                    { 4, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Robbialac Acrylic Plastic Emulsion", "Resources\\Images\\Products\\Robbialac Acrylic Plastic Emulsion_b9d9d9d4-18fc-4cac-b2a3-9b5d8571d5b3.jpg", true, false, null, null, "Robbialac Acrylic Plastic Emulsion", 0 },
                    { 5, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "WeatherCoat AntiDirt Supreme", "Resources\\Images\\Products\\WeatherCoat AntiDirt Supreme_d4074485-8916-4aaf-8946-285fcdb7a2ef.jpg", true, false, null, null, "WeatherCoat AntiDirt Supreme", 0 },
                    { 6, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "WeatherCoat AntiDirt LongLife", "Resources\\Images\\Products\\WeatherCoat AntiDirt LongLife_f2b3d3eb-c419-4268-863d-8ace2d8e8c05.png", true, false, null, null, "WeatherCoat AntiDirt LongLife", 0 },
                    { 7, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "WeatherCoat Smooth", "Resources\\Images\\Products\\WeatherCoat Smooth_3ae9b142-d713-4148-aac8-4a1a0fa6b109.jpg", true, false, null, null, "WeatherCoat Smooth", 0 },
                    { 8, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Breathe Easy Emulsion", "Resources\\Images\\Products\\Breathe Easy Enamel_ae90e494-1dbc-4b3d-887c-3e460a2bcbd0.jpg", true, false, null, null, "Breathe Easy Emulsion", 0 },
                    { 9, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Robbialac Synthetic Enamel", null, true, false, null, null, "Robbialac Synthetic Enamel", 0 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1);
            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2);
            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3);
            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 9);
        }
    }
}
