﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class AddSettingIdinBenefit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SettingId",
                table: "Benefits",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Benefits_SettingId",
                table: "Benefits",
                column: "SettingId");

            migrationBuilder.AddForeignKey(
                name: "FK_Benefits_Settings_SettingId",
                table: "Benefits",
                column: "SettingId",
                principalTable: "Settings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Benefits_Settings_SettingId",
                table: "Benefits");

            migrationBuilder.DropIndex(
                name: "IX_Benefits_SettingId",
                table: "Benefits");

            migrationBuilder.DropColumn(
                name: "SettingId",
                table: "Benefits");
        }
    }
}
