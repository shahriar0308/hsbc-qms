﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class RenameServiceCategoryIdtoTypeOfWorkId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceRequisitions_TypeOfRequirements_ServiceCategoryId",
                table: "ServiceRequisitions");

            migrationBuilder.DropIndex(
                name: "IX_ServiceRequisitions_ServiceCategoryId",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "ServiceCategoryId",
                table: "ServiceRequisitions");

            migrationBuilder.AddColumn<int>(
                name: "TypeOfRequirementId",
                table: "ServiceRequisitions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceRequisitions_TypeOfRequirementId",
                table: "ServiceRequisitions",
                column: "TypeOfRequirementId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceRequisitions_TypeOfRequirements_TypeOfRequirementId",
                table: "ServiceRequisitions",
                column: "TypeOfRequirementId",
                principalTable: "TypeOfRequirements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceRequisitions_TypeOfRequirements_TypeOfRequirementId",
                table: "ServiceRequisitions");

            migrationBuilder.DropIndex(
                name: "IX_ServiceRequisitions_TypeOfRequirementId",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "TypeOfRequirementId",
                table: "ServiceRequisitions");

            migrationBuilder.AddColumn<int>(
                name: "ServiceCategoryId",
                table: "ServiceRequisitions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceRequisitions_ServiceCategoryId",
                table: "ServiceRequisitions",
                column: "ServiceCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceRequisitions_TypeOfRequirements_ServiceCategoryId",
                table: "ServiceRequisitions",
                column: "ServiceCategoryId",
                principalTable: "TypeOfRequirements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
