﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class UserLoyaltyPointUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsCompleted",
                table: "UserLoyaltyProgramPoints",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "LdLeadId",
                table: "UserLoyaltyProgramPoints",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsCompleted",
                table: "UserLoyaltyProgramPoints");

            migrationBuilder.DropColumn(
                name: "LdLeadId",
                table: "UserLoyaltyProgramPoints");
        }
    }
}
