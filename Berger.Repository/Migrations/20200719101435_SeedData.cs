﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Complains",
                columns: new[] { "Id", "ComplainDetails", "ComplainType", "Created", "CreatedBy", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy" },
                values: new object[,]
                {
                    { 1, "General Complain", 1, new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(6336), null, true, false, null, null },
                    { 2, "Non General Complain", 2, new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(6408), null, true, false, null, null }
                });

            migrationBuilder.InsertData(
                table: "Divisions",
                columns: new[] { "Id", "Created", "CreatedBy", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 7, 19, 16, 14, 34, 707, DateTimeKind.Local).AddTicks(1410), null, true, false, null, null, "Dhaka" },
                    { 2, new DateTime(2020, 7, 19, 16, 14, 34, 708, DateTimeKind.Local).AddTicks(3647), null, true, false, null, null, "Sylhet" }
                });

            migrationBuilder.InsertData(
                table: "FAQs",
                columns: new[] { "Id", "Created", "CreatedBy", "FAQType", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy", "QuestionAnswer", "QuestionTitle", "Sequence" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(2752), null, 2, true, false, null, null, "Answer A", "Service FAQ A", 1 },
                    { 2, new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(2812), null, 2, true, false, null, null, "Answer B", "Service FAQ B", 2 },
                    { 3, new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(2814), null, 1, true, false, null, null, "Answer A", "Loyalty Program FAQ A", 1 }
                });

            migrationBuilder.InsertData(
                table: "PromotionalBanners",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "ImageUrl", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy", "Name", "Sequence" },
                values: new object[,]
                {
                    { 3, new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(2024), null, "Ad", "3.jpg", true, false, null, null, "Banner 3", 3 },
                    { 2, new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(2021), null, "Ad", "2.jpg", true, false, null, null, "Banner 2", 2 },
                    { 1, new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(1974), null, "Ad", "1.jpg", true, false, null, null, "Banner 1", 1 }
                });

            migrationBuilder.InsertData(
                table: "ServiceCategories",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(3145), null, null, true, false, null, null, "Decorative Painting" },
                    { 2, new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(3170), null, null, true, false, null, null, "Illusions Painting" },
                    { 3, new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(3172), null, null, true, false, null, null, "Problem Solving" },
                    { 4, new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(3173), null, null, true, false, null, null, "Industrial" },
                    { 5, new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(3174), null, null, true, false, null, null, "Marine" },
                    { 6, new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(3176), null, null, true, false, null, null, "Construction Chemicals" },
                    { 7, new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(3177), null, null, true, false, null, null, "Wood Coating" },
                    { 8, new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(3179), null, null, true, false, null, null, "Others" }
                });

            migrationBuilder.InsertData(
                table: "Settings",
                columns: new[] { "Id", "Category", "Created", "CreatedBy", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy", "Points" },
                values: new object[,]
                {
                    { 1, "Silver", new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(7897), null, true, false, null, null, 5000L },
                    { 2, "Gold", new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(7933), null, true, false, null, null, 15000L },
                    { 3, "Platinum", new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(7935), null, true, false, null, null, 25000L }
                });

            migrationBuilder.InsertData(
                table: "Benefits",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy", "Name", "SettingId" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(9782), null, "For Gold Member", true, false, null, null, "Le Meridien", 2 },
                    { 2, new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(9838), null, "For Platinum Member", true, false, null, null, "Westin", 3 }
                });

            migrationBuilder.InsertData(
                table: "Districts",
                columns: new[] { "Id", "Created", "CreatedBy", "DivisionId", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 7, 19, 16, 14, 34, 709, DateTimeKind.Local).AddTicks(7081), null, 1, true, false, null, null, "Dhaka" },
                    { 3, new DateTime(2020, 7, 19, 16, 14, 34, 709, DateTimeKind.Local).AddTicks(7148), null, 1, false, false, null, null, "Narayanganj" },
                    { 4, new DateTime(2020, 7, 19, 16, 14, 34, 709, DateTimeKind.Local).AddTicks(7150), null, 1, true, false, null, null, "Gazipur" },
                    { 2, new DateTime(2020, 7, 19, 16, 14, 34, 709, DateTimeKind.Local).AddTicks(7145), null, 2, true, false, null, null, "Sylhet" }
                });

            migrationBuilder.InsertData(
                table: "Dealers",
                columns: new[] { "Id", "Address", "Created", "CreatedBy", "DistrictId", "DivisionId", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy", "Latitude", "Longitude", "Name" },
                values: new object[] { 1, "292, Hajee Moron Ali Road,", new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(418), null, 1, 1, true, false, null, null, 23.770050m, 90.400238m, "VIP FORUM (MODERN HARDWAR" });

            migrationBuilder.InsertData(
                table: "Dealers",
                columns: new[] { "Id", "Address", "Created", "CreatedBy", "DistrictId", "DivisionId", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy", "Latitude", "Longitude", "Name" },
                values: new object[] { 2, "196, Green Road, Dhanmondi,", new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(485), null, 1, 1, true, false, null, null, 23.742970m, 90.384109m, "UNITED PAINTS AND HARDWAR" });

            migrationBuilder.InsertData(
                table: "Dealers",
                columns: new[] { "Id", "Address", "Created", "CreatedBy", "DistrictId", "DivisionId", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy", "Latitude", "Longitude", "Name" },
                values: new object[] { 3, "118, Sharshah Suri Road", new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(488), null, 1, 1, true, false, null, null, 23.761101m, 90.365097m, "SUMON PAINT & H/W" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
