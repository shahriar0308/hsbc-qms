﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class BenifitTableSeqClmAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Sequence",
                table: "Benefits",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(5787));

            migrationBuilder.UpdateData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(5870));

            migrationBuilder.UpdateData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(3159));

            migrationBuilder.UpdateData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(3208));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 502, DateTimeKind.Local).AddTicks(7946));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 502, DateTimeKind.Local).AddTicks(8140));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 502, DateTimeKind.Local).AddTicks(8148));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 502, DateTimeKind.Local).AddTicks(5151));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 502, DateTimeKind.Local).AddTicks(5245));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 502, DateTimeKind.Local).AddTicks(5249));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 502, DateTimeKind.Local).AddTicks(5251));

            migrationBuilder.UpdateData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 499, DateTimeKind.Local).AddTicks(1349));

            migrationBuilder.UpdateData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 500, DateTimeKind.Local).AddTicks(9003));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(87));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(182));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(207));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(210));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 5,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(212));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 6,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(214));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(1699));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(1756));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(1759));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(7751));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(7820));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(7823));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(8723));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(8760));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(8763));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(8765));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 5,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(8767));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 6,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(8769));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 7,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(8772));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 8,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(8774));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(4318));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(4365));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 503, DateTimeKind.Local).AddTicks(4368));

            migrationBuilder.UpdateData(
                table: "SingleValuedEntities",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 23, 18, 32, 24, 504, DateTimeKind.Local).AddTicks(325));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Sequence",
                table: "Benefits");

            migrationBuilder.UpdateData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(7383));

            migrationBuilder.UpdateData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(7433));

            migrationBuilder.UpdateData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(4277));

            migrationBuilder.UpdateData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(4316));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 656, DateTimeKind.Local).AddTicks(8934));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 656, DateTimeKind.Local).AddTicks(8994));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 656, DateTimeKind.Local).AddTicks(8997));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 656, DateTimeKind.Local).AddTicks(5494));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 656, DateTimeKind.Local).AddTicks(5542));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 656, DateTimeKind.Local).AddTicks(5545));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 656, DateTimeKind.Local).AddTicks(5546));

            migrationBuilder.UpdateData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 654, DateTimeKind.Local).AddTicks(3720));

            migrationBuilder.UpdateData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 655, DateTimeKind.Local).AddTicks(3205));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(1061));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(1119));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(1122));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(1124));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 5,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(1171));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 6,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(1172));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(2844));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(2886));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(2888));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(1747));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(1859));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(1865));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(3650));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(3871));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(3880));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(3881));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 5,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(3883));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 6,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(3885));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 7,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(3887));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 8,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(3889));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(5717));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(5751));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(5753));

            migrationBuilder.UpdateData(
                table: "SingleValuedEntities",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(6664));
        }
    }
}
