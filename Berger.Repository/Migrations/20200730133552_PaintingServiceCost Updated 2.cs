﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class PaintingServiceCostUpdated2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Package",
                table: "PaintingServiceCosts");

            migrationBuilder.RenameColumn(
                name: "RatePerSft",
                table: "PaintingServiceCosts",
                newName: "SilverRatePerSft");

            migrationBuilder.AddColumn<decimal>(
                name: "GoldRatePerSft",
                table: "PaintingServiceCosts",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PlatinumRatePerSft",
                table: "PaintingServiceCosts",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GoldRatePerSft",
                table: "PaintingServiceCosts");

            migrationBuilder.DropColumn(
                name: "PlatinumRatePerSft",
                table: "PaintingServiceCosts");

            migrationBuilder.RenameColumn(
                name: "SilverRatePerSft",
                table: "PaintingServiceCosts",
                newName: "RatePerSft");

            migrationBuilder.AddColumn<int>(
                name: "Package",
                table: "PaintingServiceCosts",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
