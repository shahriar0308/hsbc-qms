﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class OTPEntityUpdated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserOTPs_AspNetUsers_UserId",
                table: "UserOTPs");

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "UserOTPs",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "UserOTPs",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_UserOTPs_AspNetUsers_UserId",
                table: "UserOTPs",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserOTPs_AspNetUsers_UserId",
                table: "UserOTPs");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "UserOTPs");

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "UserOTPs",
                type: "uniqueidentifier",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_UserOTPs_AspNetUsers_UserId",
                table: "UserOTPs",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
