﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class AddedServiceRequisition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ServiceRequisitions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<Guid>(nullable: true),
                    LastModified = table.Column<DateTime>(nullable: true),
                    LegalEntityId = table.Column<int>(nullable: false),
                    LeadId = table.Column<string>(nullable: false),
                    Name = table.Column<string>(type: "varchar(100)", nullable: false),
                    Organization = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Opportunity = table.Column<string>(nullable: true),
                    Priority = table.Column<string>(nullable: true),
                    Condition = table.Column<string>(nullable: true),
                    Stage = table.Column<string>(type: "varchar(100)", nullable: false),
                    Location = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    DivisionId = table.Column<int>(nullable: false),
                    DistrictId = table.Column<int>(nullable: false),
                    Source = table.Column<string>(nullable: true),
                    ContactNo = table.Column<string>(type: "varchar(50)", nullable: false),
                    ContactEmail = table.Column<string>(type: "varchar(50)", nullable: true),
                    ContactType = table.Column<string>(type: "varchar(50)", nullable: false),
                    ContactName = table.Column<string>(type: "varchar(50)", nullable: false),
                    Attribute1 = table.Column<string>(nullable: true),
                    Attribute2 = table.Column<string>(nullable: true),
                    Active = table.Column<string>(nullable: false),
                    Srl = table.Column<int>(nullable: false),
                    DivisionOrg = table.Column<string>(type: "varchar(50)", nullable: true),
                    IsSynced = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceRequisitions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServiceRequisitions_Districts_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "Districts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceRequisitions_Divisions_DivisionId",
                        column: x => x.DivisionId,
                        principalTable: "Divisions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.UpdateData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(7383));

            migrationBuilder.UpdateData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(7433));

            migrationBuilder.UpdateData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(4277));

            migrationBuilder.UpdateData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(4316));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 656, DateTimeKind.Local).AddTicks(8934));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 656, DateTimeKind.Local).AddTicks(8994));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 656, DateTimeKind.Local).AddTicks(8997));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 656, DateTimeKind.Local).AddTicks(5494));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 656, DateTimeKind.Local).AddTicks(5542));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 656, DateTimeKind.Local).AddTicks(5545));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 656, DateTimeKind.Local).AddTicks(5546));

            migrationBuilder.UpdateData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 654, DateTimeKind.Local).AddTicks(3720));

            migrationBuilder.UpdateData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 655, DateTimeKind.Local).AddTicks(3205));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created", "QuestionAnswer", "QuestionTitle" },
                values: new object[] { new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(1061), "Literally it does not mean anything. It is a sequence of words without a sense of Latin derivation that make up a text also known as filler text, fictitious, blind or placeholder", "What is the meaning of Lorem ipsum?" });

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created", "QuestionAnswer", "QuestionTitle" },
                values: new object[] { new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(1119), "The Lorem Ipsum text is used to fill spaces designated to host texts that have not yet been published. They use programmers, graphic designers, typographers to get a real impression of the digital / advertising / editorial product they are working on.", "Why is Lorem Ipsum Dolor used?" });

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Created", "FAQType", "QuestionAnswer", "QuestionTitle", "Sequence" },
                values: new object[] { new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(1122), 2, "Its origins date back to 45 BC. In fact, his words were randomly extracted from the De finibus bonorum et malorum , a classic of Latin literature written by Cicero over 2000 years ago.", "What are the origins of Lorem Ipsum Dolor Sit?", 3 });

            migrationBuilder.InsertData(
                table: "FAQs",
                columns: new[] { "Id", "Created", "CreatedBy", "FAQType", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy", "QuestionAnswer", "QuestionTitle", "Sequence" },
                values: new object[,]
                {
                    { 4, new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(1124), null, 1, true, false, null, null, "Literally it does not mean anything. It is a sequence of words without a sense of Latin derivation that make up a text also known as filler text, fictitious, blind or placeholder", "What is the meaning of Lorem ipsum?", 1 },
                    { 5, new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(1171), null, 1, true, false, null, null, "The Lorem Ipsum text is used to fill spaces designated to host texts that have not yet been published. They use programmers, graphic designers, typographers to get a real impression of the digital / advertising / editorial product they are working on.", "Why is Lorem Ipsum Dolor used?", 2 },
                    { 6, new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(1172), null, 1, true, false, null, null, "Its origins date back to 45 BC. In fact, his words were randomly extracted from the De finibus bonorum et malorum , a classic of Latin literature written by Cicero over 2000 years ago.", "What are the origins of Lorem Ipsum Dolor Sit?", 3 }
                });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(2844));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(2886));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(2888));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(1747));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(1859));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(1865));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(3650));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(3871));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(3880));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(3881));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 5,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(3883));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 6,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(3885));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 7,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(3887));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 8,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(3889));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(5717));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(5751));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 21, 15, 19, 9, 657, DateTimeKind.Local).AddTicks(5753));

            migrationBuilder.InsertData(
                table: "SingleValuedEntities",
                columns: new[] { "Id", "Created", "CreatedBy", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy", "Name", "Value" },
                values: new object[] { 1, new DateTime(2020, 7, 21, 15, 19, 9, 658, DateTimeKind.Local).AddTicks(6664), null, true, false, null, null, "TNC", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." });

            migrationBuilder.CreateIndex(
                name: "IX_ServiceRequisitions_DistrictId",
                table: "ServiceRequisitions",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceRequisitions_DivisionId",
                table: "ServiceRequisitions",
                column: "DivisionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ServiceRequisitions");

            migrationBuilder.DeleteData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "SingleValuedEntities",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.UpdateData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(9782));

            migrationBuilder.UpdateData(
                table: "Benefits",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(9838));

            migrationBuilder.UpdateData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(6336));

            migrationBuilder.UpdateData(
                table: "Complains",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(6408));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(418));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(485));

            migrationBuilder.UpdateData(
                table: "Dealers",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(488));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 709, DateTimeKind.Local).AddTicks(7081));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 709, DateTimeKind.Local).AddTicks(7145));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 709, DateTimeKind.Local).AddTicks(7148));

            migrationBuilder.UpdateData(
                table: "Districts",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 709, DateTimeKind.Local).AddTicks(7150));

            migrationBuilder.UpdateData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 707, DateTimeKind.Local).AddTicks(1410));

            migrationBuilder.UpdateData(
                table: "Divisions",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 708, DateTimeKind.Local).AddTicks(3647));

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created", "QuestionAnswer", "QuestionTitle" },
                values: new object[] { new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(2752), "Answer A", "Service FAQ A" });

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created", "QuestionAnswer", "QuestionTitle" },
                values: new object[] { new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(2812), "Answer B", "Service FAQ B" });

            migrationBuilder.UpdateData(
                table: "FAQs",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Created", "FAQType", "QuestionAnswer", "QuestionTitle", "Sequence" },
                values: new object[] { new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(2814), 1, "Answer A", "Loyalty Program FAQ A", 1 });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(4704));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(4742));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(4745));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(1974));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(2021));

            migrationBuilder.UpdateData(
                table: "PromotionalBanners",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(2024));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(3145));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(3170));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(3172));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(3173));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 5,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(3174));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 6,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(3176));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 7,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(3177));

            migrationBuilder.UpdateData(
                table: "ServiceCategories",
                keyColumn: "Id",
                keyValue: 8,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 711, DateTimeKind.Local).AddTicks(3179));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(7897));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(7933));

            migrationBuilder.UpdateData(
                table: "Settings",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2020, 7, 19, 16, 14, 34, 710, DateTimeKind.Local).AddTicks(7935));
        }
    }
}
