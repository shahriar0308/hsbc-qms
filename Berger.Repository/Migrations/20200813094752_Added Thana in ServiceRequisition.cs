﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class AddedThanainServiceRequisition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceRequisitions_Divisions_DivisionId",
                table: "ServiceRequisitions");

            migrationBuilder.DropIndex(
                name: "IX_ServiceRequisitions_DivisionId",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "DivisionId",
                table: "ServiceRequisitions");

            migrationBuilder.AddColumn<int>(
                name: "ThanaId",
                table: "ServiceRequisitions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceRequisitions_ThanaId",
                table: "ServiceRequisitions",
                column: "ThanaId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceRequisitions_Thanas_ThanaId",
                table: "ServiceRequisitions",
                column: "ThanaId",
                principalTable: "Thanas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceRequisitions_Thanas_ThanaId",
                table: "ServiceRequisitions");

            migrationBuilder.DropIndex(
                name: "IX_ServiceRequisitions_ThanaId",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "ThanaId",
                table: "ServiceRequisitions");

            migrationBuilder.AddColumn<int>(
                name: "DivisionId",
                table: "ServiceRequisitions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceRequisitions_DivisionId",
                table: "ServiceRequisitions",
                column: "DivisionId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceRequisitions_Divisions_DivisionId",
                table: "ServiceRequisitions",
                column: "DivisionId",
                principalTable: "Divisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
