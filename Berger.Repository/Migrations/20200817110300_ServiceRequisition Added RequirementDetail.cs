﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class ServiceRequisitionAddedRequirementDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddressLine2",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "InterestedToPurchaseProductOnly",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "ServiceRequisitions");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "ServiceRequisitionDetails");

            migrationBuilder.RenameColumn(
                name: "ContactNo",
                table: "ServiceRequisitions",
                newName: "CustomerPhoneNo");

            migrationBuilder.RenameColumn(
                name: "ContactName",
                table: "ServiceRequisitions",
                newName: "CustomerName");

            migrationBuilder.AddColumn<string>(
                name: "RequirementDetail",
                table: "ServiceRequisitions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RequirementDetail",
                table: "ServiceRequisitions");

            migrationBuilder.RenameColumn(
                name: "CustomerPhoneNo",
                table: "ServiceRequisitions",
                newName: "ContactNo");

            migrationBuilder.RenameColumn(
                name: "CustomerName",
                table: "ServiceRequisitions",
                newName: "ContactName");

            migrationBuilder.AddColumn<string>(
                name: "AddressLine2",
                table: "ServiceRequisitions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "InterestedToPurchaseProductOnly",
                table: "ServiceRequisitions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "ServiceRequisitions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "ServiceRequisitionDetails",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
