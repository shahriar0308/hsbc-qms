﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class UserColorCodeTypeOfReqAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceRequisitions_ServiceCategories_ServiceCategoryId",
                table: "ServiceRequisitions");

            migrationBuilder.DropTable(
                name: "ServiceCategories");

            migrationBuilder.CreateTable(
                name: "TypeOfRequirements",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<Guid>(nullable: true),
                    LastModified = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypeOfRequirements", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserColorCodes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    ColorCodeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserColorCodes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserColorCodes_ColorCodes_ColorCodeId",
                        column: x => x.ColorCodeId,
                        principalTable: "ColorCodes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserColorCodes_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "TypeOfRequirements",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Decorative Painting" },
                    { 2, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Illusions Painting" },
                    { 3, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Problem Solving" },
                    { 4, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Industrial" },
                    { 5, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Marine" },
                    { 6, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Construction Chemicals" },
                    { 7, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Wood Coating" },
                    { 8, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Others" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserColorCodes_ColorCodeId",
                table: "UserColorCodes",
                column: "ColorCodeId");

            migrationBuilder.CreateIndex(
                name: "IX_UserColorCodes_UserId",
                table: "UserColorCodes",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceRequisitions_TypeOfRequirements_ServiceCategoryId",
                table: "ServiceRequisitions",
                column: "ServiceCategoryId",
                principalTable: "TypeOfRequirements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceRequisitions_TypeOfRequirements_ServiceCategoryId",
                table: "ServiceRequisitions");

            migrationBuilder.DropTable(
                name: "TypeOfRequirements");

            migrationBuilder.DropTable(
                name: "UserColorCodes");

            migrationBuilder.CreateTable(
                name: "ServiceCategories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModified = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceCategories", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "ServiceCategories",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Decorative Painting" },
                    { 2, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Illusions Painting" },
                    { 3, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Problem Solving" },
                    { 4, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Industrial" },
                    { 5, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Marine" },
                    { 6, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Construction Chemicals" },
                    { 7, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Wood Coating" },
                    { 8, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Others" }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceRequisitions_ServiceCategories_ServiceCategoryId",
                table: "ServiceRequisitions",
                column: "ServiceCategoryId",
                principalTable: "ServiceCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
