﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class AddedCostsInProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "BaseCoatCost",
                table: "Products",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PaintCost",
                table: "Products",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PrimerCost",
                table: "Products",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PuttyCost",
                table: "Products",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SealerCost",
                table: "Products",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TopCoatCost",
                table: "Products",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "PaintCost", "PuttyCost", "SealerCost" },
                values: new object[] { 2730m, 645m, 890m });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "PaintCost", "PuttyCost", "SealerCost" },
                values: new object[] { 1890m, 425m, 755m });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "PaintCost", "PuttyCost", "SealerCost" },
                values: new object[] { 1300m, 425m, 755m });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "PaintCost", "PuttyCost", "SealerCost" },
                values: new object[] { 1082m, 425m, 755m });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BaseCoatCost", "PuttyCost", "TopCoatCost" },
                values: new object[] { 6500m, 590m, 14000m });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "PuttyCost", "SealerCost", "TopCoatCost" },
                values: new object[] { 1510m, 4090m, 8750m });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "PuttyCost", "TopCoatCost" },
                values: new object[] { 1510m, 6650m });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BaseCoatCost",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "PaintCost",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "PrimerCost",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "PuttyCost",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "SealerCost",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "TopCoatCost",
                table: "Products");
        }
    }
}
