﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class ServiceRequisitionUpdate1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropColumn(
            //    name: "ColorConsultantName",
            //    table: "ServiceRequisitions");

            //migrationBuilder.DropColumn(
            //    name: "ColorConsultantPhoneNo",
            //    table: "ServiceRequisitions");

            //migrationBuilder.DropColumn(
            //    name: "FinancialEstimateUrl",
            //    table: "ServiceRequisitions");

            //migrationBuilder.DropColumn(
            //    name: "PaymentSlipUrl",
            //    table: "ServiceRequisitions");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ColorConsultantName",
                table: "ServiceRequisitions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ColorConsultantPhoneNo",
                table: "ServiceRequisitions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FinancialEstimateUrl",
                table: "ServiceRequisitions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PaymentSlipUrl",
                table: "ServiceRequisitions",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
