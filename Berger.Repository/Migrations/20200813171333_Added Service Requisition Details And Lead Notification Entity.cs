﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class AddedServiceRequisitionDetailsAndLeadNotificationEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LeadNotifications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<Guid>(nullable: true),
                    LastModified = table.Column<DateTime>(nullable: true),
                    LeadId = table.Column<string>(nullable: true),
                    LeadDetailId = table.Column<string>(nullable: true),
                    EnumNotificationType = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Body = table.Column<string>(nullable: true),
                    IsSent = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeadNotifications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ServiceRequisitionDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<Guid>(nullable: true),
                    LastModified = table.Column<DateTime>(nullable: true),
                    WorkDetailsId = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Stage = table.Column<string>(nullable: true),
                    FinancialEstimateUrl = table.Column<string>(nullable: true),
                    PaymentSlipUrl = table.Column<string>(nullable: true),
                    ColorConsultantName = table.Column<string>(nullable: true),
                    ColorConsultantPhoneNo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceRequisitionDetails", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceRequisitions_TypeOfRequirements_ServiceCategoryId",
                table: "ServiceRequisitions");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceRequisitions_Thanas_ThanaId",
                table: "ServiceRequisitions");

            migrationBuilder.DropForeignKey(
                name: "FK_UserColorCodes_ColorCodes_ColorCodeId",
                table: "UserColorCodes");

            migrationBuilder.DropForeignKey(
                name: "FK_UserColorCodes_AspNetUsers_UserId",
                table: "UserColorCodes");

            migrationBuilder.DropTable(
                name: "LeadNotifications");

            migrationBuilder.DropTable(
                name: "ServiceRequisitionDetails");

            migrationBuilder.DropIndex(
                name: "IX_ServiceRequisitions_ThanaId",
                table: "ServiceRequisitions");

            migrationBuilder.DeleteData(
                table: "Thanas",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Thanas",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Thanas",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Thanas",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Thanas",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Thanas",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DropColumn(
                name: "ThanaId",
                table: "ServiceRequisitions");

            migrationBuilder.AddColumn<int>(
                name: "DivisionId",
                table: "ServiceRequisitions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ServiceCategories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModified = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceCategories", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "ServiceCategories",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "IsActive", "IsDeleted", "LastModified", "LastModifiedBy", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Decorative Painting" },
                    { 2, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Illusions Painting" },
                    { 3, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Problem Solving" },
                    { 4, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Industrial" },
                    { 5, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Marine" },
                    { 6, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Construction Chemicals" },
                    { 7, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Wood Coating" },
                    { 8, new DateTime(2020, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, true, false, null, null, "Others" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ServiceRequisitions_DivisionId",
                table: "ServiceRequisitions",
                column: "DivisionId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceRequisitions_Divisions_DivisionId",
                table: "ServiceRequisitions",
                column: "DivisionId",
                principalTable: "Divisions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceRequisitions_ServiceCategories_ServiceCategoryId",
                table: "ServiceRequisitions",
                column: "ServiceCategoryId",
                principalTable: "ServiceCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
