﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Berger.Repository.Migrations
{
    public partial class AddedSequenceInFAQ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Sequence",
                table: "FAQs",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Sequence",
                table: "FAQs");
        }
    }
}
