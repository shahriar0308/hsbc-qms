﻿using QMS.Entities;
using QMS.Repository.Context;
using QMS.Repository.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Repository.Interfaces
{
    public interface ICustomerTypeRepository : IRepository<CustomerType, int, ApplicationDbContext>
    {
        
    }
}
