﻿using Berger.Repository.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Interfaces
{
    public interface IDivisionUnitOfWork : IUnitOfWork
    {
        public IDivisionRepository DivisionRepository { get; set; }
    }
}
