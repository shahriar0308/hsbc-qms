﻿using Berger.Entities;
using Berger.Repository.Context;
using Berger.Repository.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Interfaces
{
    public interface IPromotionalBannerRepository : IRepository<PromotionalBanner, int, ApplicationDbContext>
    {
        
    }
}
