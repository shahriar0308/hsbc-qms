﻿using Berger.Repository.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Interfaces
{
    public interface IPromotionalBannerUnitOfWork : IUnitOfWork
    {
        public IPromotionalBannerRepository PromotionalBannerRepository { get; set; }
    }
}
