using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Berger.Entities;
using Berger.Repository.Context;
using Berger.Repository.Core;

namespace Berger.Repository.Interfaces
{
    public interface IComplainRepository : IRepository<Complain, int, ApplicationDbContext>
    {
        
    }
}
