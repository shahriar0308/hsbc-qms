using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Berger.Repository.Core;

namespace Berger.Repository.Interfaces
{
    public interface IComplainUnitOfWork : IUnitOfWork
    {
        public IComplainRepository ComplainRepository { get; set; }
    }
}
