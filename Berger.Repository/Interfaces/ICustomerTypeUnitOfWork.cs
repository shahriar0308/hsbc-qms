﻿
using QMS.Repository.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Repository.Interfaces
{
    public interface ICustomerTypeUnitOfWork :IUnitOfWork
    {
        public ICustomerTypeRepository CustomerTypeRepository { get; set; }
    }
}
