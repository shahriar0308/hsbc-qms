﻿using Berger.Entities;
using QMS.Repository.Context;
using QMS.Repository.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Interfaces
{
    public interface IUserOTPRepository : IRepository<UserOTP, int, ApplicationDbContext>
    {
        
    }
}
