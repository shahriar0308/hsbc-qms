﻿using Berger.Repository.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Interfaces
{
    public interface IPaintUnitOfWork : IUnitOfWork
    {
        public IPaintRepository PaintRepository { get; set; }
    }
}
