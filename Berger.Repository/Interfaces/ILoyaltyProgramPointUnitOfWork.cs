﻿using Berger.Repository.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Repository.Interfaces
{
    public interface ILoyaltyProgramPointUnitOfWork : IUnitOfWork
    {
        public ILoyaltyProgramPointRepository LoyaltyProgramPointRepository { get; set; }

    }
}
