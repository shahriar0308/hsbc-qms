﻿
using QMS.Repository.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Interfaces
{
    public interface IUserOTPUnitOfWork : IUnitOfWork
    {
        public IUserOTPRepository UserOTPRepository { get; set; }
    }
}
