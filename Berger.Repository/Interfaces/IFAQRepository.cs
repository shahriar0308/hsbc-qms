﻿using Berger.Entities;

using Berger.Repository.Core;
using QMS.Repository.Context;
using QMS.Repository.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Interfaces
{
    public interface IFAQRepository : IRepository<FAQ, int, ApplicationDbContext>
    {
        
    }
}
