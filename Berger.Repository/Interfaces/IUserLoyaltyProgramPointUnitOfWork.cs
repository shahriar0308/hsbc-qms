﻿using Berger.Repository.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Repository.Interfaces
{
    public interface IUserLoyaltyProgramPointUnitOfWork : IUnitOfWork
    {
        public IUserLoyaltyProgramPointRepository UserLoyaltyProgramPointRepository { get; set; }

    }
}
