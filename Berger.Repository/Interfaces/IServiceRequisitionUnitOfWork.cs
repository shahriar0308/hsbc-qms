﻿using Berger.Repository.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Interfaces
{
    public interface IServiceRequisitionUnitOfWork : IUnitOfWork
    {
        public IServiceRequisitionRepository ServiceRequisitionRepository { get; set; }
        public IServiceRequisitionDetailRepository ServiceRequisitionDetailRepository { get; set; }
        public ILeadNotificationRepository LeadNotificationRepository { get; set; }
    }
}
