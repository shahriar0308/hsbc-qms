﻿using Berger.Entities;
using Berger.Repository.Context;
using Berger.Repository.Core;

namespace Berger.Repository.Interfaces
{
    public interface IThanaRepository : IRepository<Thana, int, ApplicationDbContext>
    {
    }
}
