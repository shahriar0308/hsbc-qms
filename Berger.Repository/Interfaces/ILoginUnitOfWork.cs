﻿using Berger.Repository.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Interfaces
{
    public interface ILoginUnitOfWork : IUnitOfWork
    {
        public ILoginHistoryRepository LoginHistoryRepository { get; set; }
    }
}
