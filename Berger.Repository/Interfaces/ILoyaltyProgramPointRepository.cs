﻿using Berger.Entities;
using Berger.Repository.Context;
using Berger.Repository.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Repository.Interfaces
{
    public interface ILoyaltyProgramPointRepository : IRepository<LoyaltyProgramPoint, int, ApplicationDbContext>
    {
    }
}
