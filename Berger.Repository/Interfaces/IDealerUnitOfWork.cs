﻿using Berger.Repository.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Repository.Interfaces
{
    public interface IDealerUnitOfWork : IUnitOfWork
    {
        public IDealerRepository DealerRepository { get; set; }

    }
}
