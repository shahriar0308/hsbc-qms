﻿using Berger.Repository.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Repository.Interfaces
{
    public interface ISettingUnitOfWork : IUnitOfWork
    {
        public ISettingRepository SettingRepository { get; set; }

    }
}
