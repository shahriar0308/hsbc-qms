﻿using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Repository.Core
{
    public interface IUnitOfWork : IDisposable
    {
        bool SaveChanges();
        void Rollback();
        Task<bool> SaveChangesAsync();
        public Task<IDbContextTransaction> BeginTransaction();
    }
}
