﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Berger.Repository.LMS.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.LMS.Implements
{
    public class LeadMstrUnitOfWork : UnitOfWork, ILeadMstrUnitOfWork
    {
        public ILeadMstrHdrRepository LeadMstrHdrRepository { get; set; }
        public ILeadMstrDtlRepository LeadMstrDtlRepository { get; set; }

        public LeadMstrUnitOfWork(LMSDbContext dbContext, 
            ILeadMstrHdrRepository leadMstrHdrRepository, 
            ILeadMstrDtlRepository leadMstrDtlRepository) : base(dbContext)
        {
            this.LeadMstrHdrRepository = leadMstrHdrRepository;
            this.LeadMstrDtlRepository = leadMstrDtlRepository;
        }
    }
}
