﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Berger.Repository.LMS.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.LMS.Implements
{
    public class LeadQuotationUnitOfWork : UnitOfWork, ILeadQuotationUnitOfWork
    {
        public ILeadQuotationHdrRepository LeadQuotationHdrRepository { get; set; }
        public ILeadQuotationDtlRepository LeadQuotationDtlRepository { get; set; }
        public ILeadQuotationSilverDtlRepository LeadQuotationSilverDtlRepository { get; set; }

        public LeadQuotationUnitOfWork(LMSDbContext dbContext,
            ILeadQuotationHdrRepository leadQuotationHdrRepository,
            ILeadQuotationDtlRepository leadQuotationDtlRepository,
            ILeadQuotationSilverDtlRepository leadQuotationSilverDtlRepository) : base(dbContext)
        {
            this.LeadQuotationHdrRepository = leadQuotationHdrRepository;
            this.LeadQuotationDtlRepository = leadQuotationDtlRepository;
            this.LeadQuotationSilverDtlRepository = leadQuotationSilverDtlRepository;
        }
    }
}
