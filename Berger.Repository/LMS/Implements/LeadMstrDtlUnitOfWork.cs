﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Berger.Repository.LMS.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Repository.LMS.Implements
{
    public class LeadMstrDtlUnitOfWork : UnitOfWork, ILeadMstrDtlUnitOfWork
    {
        public ILeadMstrDtlRepository LeadMstrDtlRepository { get; set; }

        public LeadMstrDtlUnitOfWork(LMSDbContext dbContext, ILeadMstrDtlRepository leadMstrDtlRepository) : base(dbContext)
        {
            LeadMstrDtlRepository = leadMstrDtlRepository;
        }
    }
}
