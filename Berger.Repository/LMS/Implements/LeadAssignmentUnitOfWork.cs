﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Berger.Repository.LMS.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Repository.LMS.Implements
{
    public class LeadAssignmentUnitOfWork : UnitOfWork, ILeadAssignmentUnitOfWork
    {
        public IUserProfileRepository UserProfileRepository { get; set; }
        public ILeadMstrDtlRepository LeadMstrDtlRepository { get; set; }

        public LeadAssignmentUnitOfWork(LMSDbContext dbContext, IUserProfileRepository userProfileRepository
            , ILeadMstrDtlRepository leadMstrDtlRepository) : base(dbContext)
        {
            UserProfileRepository = userProfileRepository;
            LeadMstrDtlRepository = leadMstrDtlRepository;
        }
    }
}
