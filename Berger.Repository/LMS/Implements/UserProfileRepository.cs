﻿using Berger.Entities;
using Berger.Entities.LMS;
using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Berger.Repository.LMS.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.LMS.Implements
{
    public class UserProfileRepository : Repository<UserProfile, int, LMSDbContext>, IUserProfileRepository
    {
        public UserProfileRepository(LMSDbContext dbContext) : base(dbContext)
        {

        }
    }
}
