﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Berger.Repository.LMS.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Repository.LMS.Implements
{
    public class UserProfileUnitOfWork : UnitOfWork, IUserProfileUnitOfWork
    {
        public IUserProfileRepository UserProfileRepository { get; set; }

        public UserProfileUnitOfWork(LMSDbContext dbContext, IUserProfileRepository userProfileRepository) : base(dbContext)
        {
            UserProfileRepository = userProfileRepository;
        }
    }
}
