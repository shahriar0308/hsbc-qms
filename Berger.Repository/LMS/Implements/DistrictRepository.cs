﻿using Berger.Entities;
using Berger.Entities.LMS;
using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.LMS.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.LMS.Implements
{
    public class DistrictMstrRepository : Repository<DistrictMstr, int, LMSDbContext>, IDistrictMstrRepository
    {
        public DistrictMstrRepository(LMSDbContext dbContext) : base(dbContext)
        {

        }
    }
}
