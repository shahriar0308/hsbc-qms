﻿using Berger.Entities.LMS;
using Berger.Repository.Context;
using Berger.Repository.Core;
using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.LMS.Interfaces
{
    public class LeadQuotationSilverDtlRepository : Repository<LeadQuotationSilverDtl, long, LMSDbContext>, ILeadQuotationSilverDtlRepository
    {
        public LeadQuotationSilverDtlRepository(LMSDbContext dbContext) : base(dbContext)
        {

        }
    }
}
