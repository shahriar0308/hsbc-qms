﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Berger.Repository.LMS.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Repository.LMS.Implements
{
    public class BibhagUnitOfWork : UnitOfWork, IBibhagUnitOfWork
    {
        public IBibhagRepository BibhagRepository { get; set; }

        public BibhagUnitOfWork(LMSDbContext dbContext, IBibhagRepository bibhagRepository) : base(dbContext)
        {
            this.BibhagRepository = bibhagRepository;
        }
    }
}
