﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.LMS.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Repository.LMS.Implements
{
    public class DistrictMstrUnitOfWork : UnitOfWork, IDistrictMstrUnitOfWork
    {
        public IDistrictMstrRepository DistrictMstrRepository { get; set; }

        public DistrictMstrUnitOfWork(LMSDbContext dbContext, IDistrictMstrRepository DistrictRepository) : base(dbContext)
        {
            this.DistrictMstrRepository = DistrictRepository;
        }
    }
}
