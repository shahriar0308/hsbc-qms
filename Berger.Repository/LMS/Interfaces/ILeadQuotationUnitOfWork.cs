﻿using Berger.Repository.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.LMS.Interfaces
{
    public interface ILeadQuotationUnitOfWork : IUnitOfWork
    {
        public ILeadQuotationHdrRepository LeadQuotationHdrRepository { get; set; }
        public ILeadQuotationDtlRepository LeadQuotationDtlRepository { get; set; }
        public ILeadQuotationSilverDtlRepository LeadQuotationSilverDtlRepository { get; set; }
    }
}
