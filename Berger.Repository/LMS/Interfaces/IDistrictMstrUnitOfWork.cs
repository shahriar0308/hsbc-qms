﻿using Berger.Repository.Core;
using Berger.Repository.LMS.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.LMS.Interfaces
{
    public interface IDistrictMstrUnitOfWork : IUnitOfWork
    {
        public IDistrictMstrRepository DistrictMstrRepository { get; set; }
    }
}
