﻿using Berger.Repository.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.LMS.Interfaces
{
    public interface ILeadMstrUnitOfWork : IUnitOfWork
    {
        public ILeadMstrHdrRepository LeadMstrHdrRepository { get; set; }
        public ILeadMstrDtlRepository LeadMstrDtlRepository { get; set; }
    }
}
