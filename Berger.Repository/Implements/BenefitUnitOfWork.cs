﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class BenefitUnitOfWork : UnitOfWork, IBenefitUnitOfWork
    {
        public IBenefitRepository BenefitRepository { get; set; }

        public BenefitUnitOfWork(ApplicationDbContext dbContext, IBenefitRepository benefitRepository) : base(dbContext)
        {
            this.BenefitRepository = benefitRepository;
        }
    }
}
