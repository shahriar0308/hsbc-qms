﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class LoyaltyProgramPointUnitOfWork : UnitOfWork, ILoyaltyProgramPointUnitOfWork
    {
        public ILoyaltyProgramPointRepository LoyaltyProgramPointRepository { get; set; }

        public LoyaltyProgramPointUnitOfWork(ApplicationDbContext dbContext, ILoyaltyProgramPointRepository loyaltyProgramPointRepository) : base(dbContext)
        {
            LoyaltyProgramPointRepository = loyaltyProgramPointRepository;
        }
    }
}
