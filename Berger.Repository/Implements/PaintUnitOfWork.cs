﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;

namespace Berger.Repository.Implements
{
    public class PaintUnitOfWork : UnitOfWork, IPaintUnitOfWork
    {
        public IPaintRepository PaintRepository { get; set; }

        public PaintUnitOfWork(ApplicationDbContext dbContext, IPaintRepository paintRepository) : base(dbContext)
        {
            PaintRepository = paintRepository;
        }
    }
}
