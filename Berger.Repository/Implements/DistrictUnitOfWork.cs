﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class DistrictUnitOfWork : UnitOfWork, IDistrictUnitOfWork
    {
        public IDistrictRepository DistrictRepository { get; set; }

        public DistrictUnitOfWork(ApplicationDbContext dbContext, IDistrictRepository districtRepository) : base(dbContext)
        {
            this.DistrictRepository = districtRepository;
        }
    }
}
