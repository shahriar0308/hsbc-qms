﻿using Berger.Entities;
using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Repository.Implements
{
    public class ThanaRepository : Repository<Thana, int, ApplicationDbContext>, IThanaRepository
    {
        public ThanaRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}
