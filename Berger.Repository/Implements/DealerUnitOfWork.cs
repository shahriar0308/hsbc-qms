﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Repository.Implements
{
    public class DealerUnitOfWork : UnitOfWork, IDealerUnitOfWork
    {
        public IDealerRepository DealerRepository { get; set; }

        public DealerUnitOfWork(ApplicationDbContext dbContext, IDealerRepository dealerRepository) : base(dbContext)
        {
            this.DealerRepository = dealerRepository;
        }
    }
}
