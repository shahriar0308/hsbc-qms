﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class UserLoyaltyProgramPointUnitOfWork : UnitOfWork, IUserLoyaltyProgramPointUnitOfWork
    {
        public IUserLoyaltyProgramPointRepository UserLoyaltyProgramPointRepository { get; set; }

        public UserLoyaltyProgramPointUnitOfWork(ApplicationDbContext dbContext, IUserLoyaltyProgramPointRepository userLoyaltyProgramPointRepository) : base(dbContext)
        {
            UserLoyaltyProgramPointRepository = userLoyaltyProgramPointRepository;
        }
    }
}
