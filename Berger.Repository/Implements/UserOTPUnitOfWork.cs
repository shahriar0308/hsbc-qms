﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class UserOTPUnitOfWork : UnitOfWork, IUserOTPUnitOfWork
    {
        public IUserOTPRepository UserOTPRepository { get; set; }

        public UserOTPUnitOfWork(ApplicationDbContext dbContext, IUserOTPRepository userOTPRepository) : base(dbContext)
        {
            this.UserOTPRepository = userOTPRepository;
        }
    }
}
