﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class FAQUnitOfWork : UnitOfWork, IFAQUnitOfWork
    {
        public IFAQRepository FAQRepository { get; set; }

        public FAQUnitOfWork(ApplicationDbContext dbContext, IFAQRepository FAQRepository) : base(dbContext)
        {
            this.FAQRepository = FAQRepository;
        }
    }
}
