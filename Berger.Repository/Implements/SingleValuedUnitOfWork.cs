﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class SingleValuedUnitOfWork : UnitOfWork, ISingleValuedUnitOfWork
    {
        public ISingleValuedRepository SingleValuedRepository { get; set; }

        public SingleValuedUnitOfWork(ApplicationDbContext dbContext
            , ISingleValuedRepository singleValuedRepository) : base(dbContext)
        {
            this.SingleValuedRepository = singleValuedRepository;
        }
    }
}
