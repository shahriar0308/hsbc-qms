﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class SyncUnitOfWork : UnitOfWork, ISyncUnitOfWork
    {
        public IDistrictRepository DistrictRepository { get; set; }
        public IDivisionRepository DivisionRepository { get; set; }

        public SyncUnitOfWork(ApplicationDbContext dbContext, IDistrictRepository districtRepository, IDivisionRepository divisionRepository) : base(dbContext)
        {
            this.DistrictRepository = districtRepository;
            this.DivisionRepository = divisionRepository;
        }
    }
}
