﻿using QMS.Repository.Context;
using QMS.Repository.Core;
using QMS.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Repository.Implements
{
    public class CustomerTypeUnitOfWork : UnitOfWork, ICustomerTypeUnitOfWork
    {
        public ICustomerTypeRepository CustomerTypeRepository { get; set; }
        //public ICustomerTypeRepository CustomerTypeRepository { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public CustomerTypeUnitOfWork(ApplicationDbContext dbContext, ICustomerTypeRepository customerTypeRepository) : base(dbContext)
        {
            CustomerTypeRepository = customerTypeRepository;
        }
    }
    
}
