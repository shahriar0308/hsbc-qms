﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Repository.Implements
{
    public class SettingUnitOfWork : UnitOfWork, ISettingUnitOfWork
    {
        public ISettingRepository SettingRepository { get; set; }

        public SettingUnitOfWork(ApplicationDbContext dbContext, ISettingRepository settingRepository) : base(dbContext)
        {
            SettingRepository = settingRepository;
        }
    }
}
