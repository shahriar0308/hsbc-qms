﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class DivisionUnitOfWork : UnitOfWork, IDivisionUnitOfWork
    {
        public IDivisionRepository DivisionRepository { get; set; }

        public DivisionUnitOfWork(ApplicationDbContext dbContext, IDivisionRepository divisionRepository) : base(dbContext)
        {
            this.DivisionRepository = divisionRepository;
        }
    }
}
