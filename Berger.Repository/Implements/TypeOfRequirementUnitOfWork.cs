﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class TypeOfRequirementUnitOfWork : UnitOfWork, ITypeOfRequirementUnitOfWork
    {
        public ITypeOfRequirementRepository TypeOfRequirementRepository { get; set; }

        public TypeOfRequirementUnitOfWork(ApplicationDbContext dbContext, ITypeOfRequirementRepository ServiceCategoryRepository) : base(dbContext)
        {
            this.TypeOfRequirementRepository = ServiceCategoryRepository;
        }
    }
}
