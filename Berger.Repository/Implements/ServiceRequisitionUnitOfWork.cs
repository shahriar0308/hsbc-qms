﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class ServiceRequisitionUnitOfWork : UnitOfWork, IServiceRequisitionUnitOfWork
    {
        public IServiceRequisitionRepository ServiceRequisitionRepository { get; set; }
        public IServiceRequisitionDetailRepository ServiceRequisitionDetailRepository { get; set; }
        public ILeadNotificationRepository LeadNotificationRepository { get; set; }

        public ServiceRequisitionUnitOfWork(ApplicationDbContext dbContext, IServiceRequisitionRepository requisitionRepository,
            IServiceRequisitionDetailRepository serviceRequisitionDetailRepository,
            ILeadNotificationRepository leadNotificationRepository) : base(dbContext)
        {
            this.ServiceRequisitionRepository = requisitionRepository;
            ServiceRequisitionDetailRepository = serviceRequisitionDetailRepository;
            LeadNotificationRepository = leadNotificationRepository;
        }
    }
}
