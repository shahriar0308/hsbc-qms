
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Berger.Entities;
using Berger.Repository.Interfaces;

namespace Berger.Repository.Implements
{
    public class ComplainRepository : Repository<Complain, int, ApplicationDbContext>, IComplainRepository
    {
        public ComplainRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}
