﻿using QMS.Entities;
using QMS.Repository.Context;
using QMS.Repository.Core;
using QMS.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QMS.Repository.Implements
{
    public class CustomerTypeRepository: Repository<CustomerType, int, ApplicationDbContext>, ICustomerTypeRepository
    {
        public CustomerTypeRepository(ApplicationDbContext dbContext):base(dbContext)
        {

        }
        
    }
}
