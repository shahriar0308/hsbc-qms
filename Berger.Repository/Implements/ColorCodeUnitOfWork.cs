﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class ColorCodeUnitOfWork : UnitOfWork, IColorCodeUnitOfWork
    {
        public IColorCodeRepository ColorCodeRepository { get; set; }

        public ColorCodeUnitOfWork(ApplicationDbContext dbContext, IColorCodeRepository ColorCodeRepository) : base(dbContext)
        {
            this.ColorCodeRepository = ColorCodeRepository;
        }
    }
}
