﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class LoginUnitOfWork : UnitOfWork, ILoginUnitOfWork
    {
        public ILoginHistoryRepository LoginHistoryRepository { get; set; }

        public LoginUnitOfWork(ApplicationDbContext dbContext, ILoginHistoryRepository loginHistoryRepository) : base(dbContext)
        {
            this.LoginHistoryRepository = loginHistoryRepository;
        }
    }
}
