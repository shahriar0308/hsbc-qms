﻿using Berger.Entities;
using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class DivisionRepository : Repository<Division, int, ApplicationDbContext>, IDivisionRepository
    {
        public DivisionRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}
