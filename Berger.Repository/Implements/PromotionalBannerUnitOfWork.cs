﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class PromotionalBannerUnitOfWork : UnitOfWork, IPromotionalBannerUnitOfWork
    {
        public IPromotionalBannerRepository PromotionalBannerRepository { get; set; }

        public PromotionalBannerUnitOfWork(ApplicationDbContext dbContext, IPromotionalBannerRepository promotionalBannerRepository) : base(dbContext)
        {
            PromotionalBannerRepository = promotionalBannerRepository;
        }
    }
}
