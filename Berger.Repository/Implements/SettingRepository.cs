﻿using Berger.Entities;
using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Repository.Implements
{
    public class SettingRepository : Repository<Setting, int, ApplicationDbContext>, ISettingRepository
    {
        public SettingRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}
