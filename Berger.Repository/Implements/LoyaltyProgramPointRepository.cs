﻿using Berger.Entities;
using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class LoyaltyProgramPointRepository : Repository<LoyaltyProgramPoint, int, ApplicationDbContext>, ILoyaltyProgramPointRepository
    {
        public LoyaltyProgramPointRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}
