﻿using Berger.Entities;
using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Repository.Implements
{
    public class DealerRepository : Repository<Dealer, int, ApplicationDbContext>, IDealerRepository
    {
        public DealerRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}
