
using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class ComplainUnitOfWork : UnitOfWork, IComplainUnitOfWork
    {
        public IComplainRepository ComplainRepository { get; set; }

        public ComplainUnitOfWork(ApplicationDbContext dbContext, IComplainRepository complainRepository) : base(dbContext)
        {
            this.ComplainRepository = complainRepository;
        }  
    }
}
