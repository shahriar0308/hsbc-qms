﻿using Berger.Entities;
using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class UserColorCodeRepository : Repository<UserColorCode, int, ApplicationDbContext>, IUserColorCodeRepository
    {
        public UserColorCodeRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}
