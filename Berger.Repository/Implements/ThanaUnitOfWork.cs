﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Berger.Repository.Implements
{
    public class ThanaUnitOfWork : UnitOfWork, IThanaUnitOfWork
    {
        public IThanaRepository ThanaRepository { get; set; }

        public ThanaUnitOfWork(ApplicationDbContext dbContext, IThanaRepository thanaRepository) : base(dbContext)
        {
            this.ThanaRepository = thanaRepository;
        }
    }
}
