﻿using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class UserColorCodeUnitOfWork : UnitOfWork, IUserColorCodeUnitOfWork
    {
        public IUserColorCodeRepository UserColorCodeRepository { get; set; }

        public UserColorCodeUnitOfWork(ApplicationDbContext dbContext, IUserColorCodeRepository UserColorCodeRepository) : base(dbContext)
        {
            this.UserColorCodeRepository = UserColorCodeRepository;
        }
    }
}
