﻿using Berger.Entities;
using Berger.Repository.Context;
using Berger.Repository.Core;
using Berger.Repository.Interfaces;
using QMS.Repository.Context;
using QMS.Repository.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Berger.Repository.Implements
{
    public class FAQRepository : Repository<FAQ, int, ApplicationDbContext>, IFAQRepository
    {
        public FAQRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}
